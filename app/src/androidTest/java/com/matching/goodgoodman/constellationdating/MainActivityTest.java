package com.matching.goodgoodman.constellationdating;

import android.support.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertNotNull;

/**
 * Created by dorol on 2017-09-15.
 */
public class MainActivityTest {

    MainActivity mainActivity;

    @Rule
    public ActivityTestRule<MainActivity> testRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void setUp() throws Exception {
        mainActivity = testRule.getActivity();
    }

    @Test
    public void onCreate() throws Exception {



        //onView((ImageView) mainActivity.findViewById(R.id.backGround)).check(matches(isDisplayed()));
        onView(withId(R.id.backGround)).check(matches(isDisplayed()));
        onView(withId(R.id.topLogo)).check(matches(isDisplayed()));

        assertNotNull(mainActivity.findViewById(R.id.backGround));
        assertNotNull(mainActivity.findViewById(R.id.topLogo));

        assertNotNull(mainActivity.findViewById(R.id.tab));
        assertNotNull(mainActivity.findViewById(R.id.viewPager));
    }

}