package com.matching.goodgoodman.constellationdating.Suite;

import android.support.test.runner.AndroidJUnit4;

import com.matching.goodgoodman.constellationdating.firebase.ChatRoom;
import com.matching.goodgoodman.constellationdating.firebase.FirebaseChatUtil;
import com.matching.goodgoodman.constellationdating.firebase.model.Chat;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by GoodGoodMan on 2017. 10. 2..
 */

@RunWith(AndroidJUnit4.class)

public class FirebaseInstrumentedTest {

    @Test
    public void firebase_init() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference rootReference = database.getReference();
        assertNotNull(database);
        assertNotNull(rootReference);
    }

    @Test
    public void firebase_create_chat() {
        final String CHAT_ROOM_NODE_NAME = "chatroom";
        final String CHAT_ROOM_HASH = "-KvGioF9THrhgqMgj8dp";
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference rootReference = database.getReference(CHAT_ROOM_NODE_NAME);
        assertNotNull(database);
        assertNotNull(rootReference);

        DatabaseReference roomReference = rootReference.child(CHAT_ROOM_HASH);
        roomReference.push().setValue(new Chat("idid", "NOOOO!"));
        roomReference.push().setValue(new Chat("idid", "NOO3OO!"));
        roomReference.push().setValue(new Chat("idid", "NOO2OO!"));
        Task<Void> setTask = roomReference.push().setValue(new Chat("idid", "N3OOOO!"));

        while (!setTask.isComplete()) { }
        assertTrue(setTask.isSuccessful());
    }

    @Test
    public void firebaseService_create_chat() {
        final String CHAT_ROOM_HASH = "-KvGioF9THrhgqMgj8dp";
        Task<Void> setTask = FirebaseChatUtil.sendChat(CHAT_ROOM_HASH, new Chat("asdas", "zczxcasvava"));
        while (!setTask.isComplete()) { }
        assertTrue(setTask.isSuccessful());
    }

    @Test
    public void chatRoom_create_chat() {
        final String CHAT_ROOM_HASH = "-KvGioF9THrhgqMgj8dp";
        ChatRoom chatRoom = new ChatRoom(CHAT_ROOM_HASH, null, null);
        Task<Void> sendTask = chatRoom.sendChat(new Chat("ididasdasd", "NOO2OO!asdas"));
        while (!sendTask.isComplete()) { }
        assertTrue(sendTask.isSuccessful());
    }
}
