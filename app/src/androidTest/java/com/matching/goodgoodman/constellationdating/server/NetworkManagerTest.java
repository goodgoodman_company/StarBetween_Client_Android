package com.matching.goodgoodman.constellationdating.server;

import android.content.Context;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.server.model.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.fail;

/**
 * Created by dorol on 2017-09-12.
 */

@RunWith(AndroidJUnit4.class)
public class NetworkManagerTest {

    private Context instrumentationCtx;
    private NetworkManager mNetworkManager;

    private UserModel mUserModel;

    @Before
    public void setUp() {
        instrumentationCtx = InstrumentationRegistry.getContext();
        mUserModel = new UserModel();
        mNetworkManager = NetworkManager.getInstance(instrumentationCtx);
    }

    @Test
    public void getUser() {
        final User[] user = {null};
        mNetworkManager.getUser(100000002, result -> user[0] = result);

        SystemClock.sleep(10000);
        if(user[0] == null)
            fail("user is null");

        String id = user[0].getId();
        String expectId = "Ay1GpKjfgUGOuqf8";

        if(!id.equals(expectId))
            fail("id is empty");
    }

    @Test
    public void getSignUp() throws Exception {
    }

    @Test
    public void getLogin() throws Exception {
    }


}
