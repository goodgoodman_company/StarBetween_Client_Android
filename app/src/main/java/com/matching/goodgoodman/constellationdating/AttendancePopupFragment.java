package com.matching.goodgoodman.constellationdating;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttendancePopupFragment extends DialogFragment implements AttendancePopupView {
    private static final String STAR_COUNT_STR = "x%d";

    @BindView(R.id.titleImage)
    ImageView mTitleImage;
    @BindView(R.id.star_count)
    TextView mCountText;
    @BindView(R.id.OK_button)
    View mOkButton;
    @BindView(R.id.star_shop_button)
    View mStarShopButton;

    private AttendancePopupPresenter mPresenter;

    public AttendancePopupFragment() {
        mPresenter = new AttendancePopupPresenter();
        mPresenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_attendance_popup, container, false);
        ButterKnife.bind(this, rootView);
        initialListeners();

        mPresenter.checkPremium();
        return rootView;
    }
    private void initialListeners() {
        mOkButton.setOnClickListener(v -> getDialog().dismiss());
        mStarShopButton.setOnClickListener(v -> new StarShopDialogFragment().show(getFragmentManager(), null));
    }

    @Override
    public void showPremium() {
        mTitleImage.setImageDrawable(getResources().getDrawable(R.drawable.attendance_premium_title, null));
    }

    @Override
    public void showStarCount(int count) {
        mCountText.setText(String.format(STAR_COUNT_STR, count));
    }
}
