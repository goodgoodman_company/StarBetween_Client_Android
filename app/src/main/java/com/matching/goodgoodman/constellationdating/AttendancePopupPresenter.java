package com.matching.goodgoodman.constellationdating;

import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;

/**
 * Created by Hyeong_Wook on 2017-11-05.
 */

public class AttendancePopupPresenter implements Presenter<AttendancePopupView> {
    private static final int PREMIUM_STAR_COUNT = 200;
    private static final int NORMAL_STAR_COUNT = 100;

    private AttendancePopupView mView;
    private UserModel mUserModel;

    public AttendancePopupPresenter() {
        mUserModel = UserPreference.getInstance().getUserModel();
    }

    public void checkPremium() {
        if(mUserModel.getPremiumYn().equals(UserModel.YES)) {
            mView.showPremium();
            mView.showStarCount(PREMIUM_STAR_COUNT);
        } else {
            mView.showStarCount(NORMAL_STAR_COUNT);
        }
    }

    @Override
    public void attachView(AttendancePopupView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
