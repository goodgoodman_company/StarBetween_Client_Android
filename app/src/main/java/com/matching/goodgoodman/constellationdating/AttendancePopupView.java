package com.matching.goodgoodman.constellationdating;

/**
 * Created by Hyeong_Wook on 2017-11-05.
 */

public interface AttendancePopupView {
    void showPremium();
    void showStarCount(int count);
}
