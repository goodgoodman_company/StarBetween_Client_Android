/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.matching.goodgoodman.constellationdating;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardFragment extends Fragment {

	private static final String ARG_POSITION = "position";
	private static final String ARG_BODY = "body";
	private static final String ARG_IMAGE = "image";
	private static final String ARG_TITLE = "title";
	//@BindView(R.id.textView) TextView mTextView;
	@BindView(R.id.imageView) ImageView mTestImage;
	@BindView(R.id.tv_body) TextView mBodyTextView;
	@BindView(R.id.cardfragment_title1) TextView cardfragment_title1;
	@BindView(R.id.cardfragment_title2) TextView cardfragment_title2;

	private View mRootView;
	private int position;
	private String mBody;
	private int mImages;
	private String mTitle;

	public static CardFragment newInstance(int position) {
		CardFragment f = new CardFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(ARG_POSITION, position);
		f.setArguments(bundle);
		return f;
	}

	public static CardFragment newInstance(int position, String body) {
		CardFragment f = new CardFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(ARG_POSITION, position);
		bundle.putString(ARG_BODY, body);
		f.setArguments(bundle);
		return f;
	}

	public static CardFragment newInstance(int position, String body, int imageId, String title) {
		CardFragment f = new CardFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(ARG_POSITION, position);
		bundle.putString(ARG_BODY, body);
		bundle.putInt(ARG_IMAGE,imageId);
		bundle.putString(ARG_TITLE, title);
		f.setArguments(bundle);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		position = getArguments().getInt(ARG_POSITION);
		mBody= getArguments().getString(ARG_BODY);
		mImages = getArguments().getInt(ARG_IMAGE);
		mTitle = getArguments().getString(ARG_TITLE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_card,container,false);
		ButterKnife.bind(this, mRootView);
		//ViewCompat.setElevation(rootView, 50);
		Glide.with(this).load(mImages).into(mTestImage);
		//mTextView.setText("CARD " + position);
		mBodyTextView.setText(mBody);
		String[] tempTitle = mTitle.split("\\\\n\\\\n");
		cardfragment_title1.setText("The Sun of " + tempTitle[0]);
		cardfragment_title2.setText(tempTitle[1]);
		return mRootView;
	}
}