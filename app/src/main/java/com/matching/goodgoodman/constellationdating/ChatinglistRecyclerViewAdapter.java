package com.matching.goodgoodman.constellationdating;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.matching.goodgoodman.constellationdating.model.ChatRoomModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatinglistRecyclerViewAdapter extends RecyclerView.Adapter<ChatinglistRecyclerViewAdapter.ViewHolder> {
    private List<ChatRoomModel> mChatRoomModelList;
    private Fragment mFragment;

    public ChatinglistRecyclerViewAdapter(Fragment fragment) {
        mFragment = fragment;
        mChatRoomModelList = new ArrayList<>();
    }

    public ChatinglistRecyclerViewAdapter(Fragment fragment, List<ChatRoomModel> chatRoomModelList) {
        mFragment = fragment;
        mChatRoomModelList = chatRoomModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_chating_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ChatRoomModel chatRoomModel = mChatRoomModelList.get(position);

        if(chatRoomModel.getProfile() != null)
            holder.mCardImage.setImageBitmap(chatRoomModel.getProfile());

        holder.mCounterName.setText(chatRoomModel.getCounterName());
        holder.mLastChat.setText(chatRoomModel.getLastChat());
        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mFragment.getContext(), ChattingRoomActivity.class);
                intent.putExtra(ChattingRoomActivity.STATE, chatRoomModel.getState());
                intent.putExtra(ChattingRoomActivity.END_DATE, chatRoomModel.getEndDate().getTime());
                intent.putExtra(ChattingRoomActivity.COUNTER_NO, chatRoomModel.getCounterUserNo());

                mFragment.startActivityForResult(intent, 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mChatRoomModelList.size();
    }

    public void setData(List<ChatRoomModel> chatRoomModelList) {
        mChatRoomModelList = chatRoomModelList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.fragment_chating_list_cardView) CardView mCardView;
        @BindView(R.id.fragment_chating_list_cardImage) ImageView mCardImage;
        @BindView(R.id.fragment_chating_list_partnerName) TextView mCounterName;
        @BindView(R.id.fragment_chating_list_lastChat) TextView mLastChat;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}