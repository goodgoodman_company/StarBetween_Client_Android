package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.matching.goodgoodman.constellationdating.model.ChatRoomModel;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ChattingListFragment extends Fragment implements ChattingListView {
    public static final int CHAT_LIST_NEED_REFRESH = 300;
    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;
    @BindView(R.id.progress_wheel) ProgressWheel mProgressWheel;
    @BindView(R.id.noChatNotice) View mNoChatNoticeView;

    private ChatinglistRecyclerViewAdapter mChatinglistRecyclerViewAdapter;
    private ChattingListFragmentPresenter mPresenter;

    private static Context mContext;

    public ChattingListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chating_list, container, false);
        ButterKnife.bind(this, rootView);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));
        mChatinglistRecyclerViewAdapter = new ChatinglistRecyclerViewAdapter(this);
        mRecyclerView.setAdapter(mChatinglistRecyclerViewAdapter);

        mPresenter.loadChattingList(getResources().getString(R.string.facebook_app_id));

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.removeChatRoomEventListeners();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mPresenter = new ChattingListFragmentPresenter(context, this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * chatListVIew를 띄워준다.
     * @param chatRoomModelList chatroom을 그려주기 위한 dataModel
     */
    @Override
    public void showChattingList(List<ChatRoomModel> chatRoomModelList) {
        mChatinglistRecyclerViewAdapter.setData(chatRoomModelList);
        mChatinglistRecyclerViewAdapter.notifyDataSetChanged();

        mNoChatNoticeView.setVisibility(chatRoomModelList.size() <= 0? View.VISIBLE : View.GONE);
        mProgressWheel.setVisibility(View.GONE);
        mPresenter.addChatRoomEventListeners();
    }

    /**
     * lastChat이 바뀔 때 보여준다.
     * @param position lastChat이 바뀌는 chatRoom
     * @param chatRoomModel 바뀐 정보
     */
    @Override
    public void showChangeLastChat(int position, ChatRoomModel chatRoomModel) {
        mChatinglistRecyclerViewAdapter.notifyItemChanged(position, chatRoomModel);
    }

    public void refresh() {
        mProgressWheel.setVisibility(View.VISIBLE);
        mPresenter.removeChatRoomEventListeners();
        mPresenter.loadChattingList(getResources().getString(R.string.facebook_app_id));
    }
}