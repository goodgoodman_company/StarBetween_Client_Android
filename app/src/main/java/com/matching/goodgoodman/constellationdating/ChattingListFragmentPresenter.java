package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.graphics.Bitmap;

import com.matching.goodgoodman.constellationdating.firebase.model.Chat;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.model.ChatRoomModel;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.NetworkManager;
import com.matching.goodgoodman.constellationdating.server.SuccessCallBack;
import com.matching.goodgoodman.constellationdating.server.model.ChatRoom;
import com.matching.goodgoodman.constellationdating.server.model.ChatRoomListModel;
import com.matching.goodgoodman.constellationdating.server.model.User;
import com.matching.goodgoodman.constellationdating.utils.FacebookModule;
import com.facebook.AccessToken;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * ChatingListView와 상호작용하여 비즈니스 로직을 처리하는 클래스.
 * @author Hyeong_Wook
 */

public class ChattingListFragmentPresenter implements Presenter<ChattingListView> {
    private static final int MATCHING_CHAT = 338;
    private static final int FROM_CHAT = 337;
    private static final int TO_CHAT = 336;

    private ChattingListView mView;
    private UserModel mUserModel;

    private NetworkManager mNetworkManager;
    private FacebookModule mFacebookModule;

    private List<ChatRoomModel> mChatRoomModelList;
    private List<ValueEventListener> mValueEventListenerList;

    private DescriptionDBHelper mDescriptionDBHelper;
    private String[] mStateLastChats;

    public ChattingListFragmentPresenter(Context context, ChattingListView view) {
        mUserModel = UserPreference.getInstance().getUserModel();
        mNetworkManager = NetworkManager.getInstance(context);
        mFacebookModule = FacebookModule.getInstance();
        mChatRoomModelList = new ArrayList<>();
        mValueEventListenerList = new ArrayList<>();
        mDescriptionDBHelper = new DescriptionDBHelper(context, DescriptionDBHelper.LANGUAGE_ENGLISH);
        mStateLastChats = new String[] {mDescriptionDBHelper.getDescription(MATCHING_CHAT).getComment(), mDescriptionDBHelper.getDescription(FROM_CHAT).getComment(), mDescriptionDBHelper.getDescription(TO_CHAT).getComment()};
        attachView(view);
    }

    /**
     * chatList의 정보를 설정한다.
     */
    public void loadChattingList(String appId) {
        mChatRoomModelList = new ArrayList<>();
        mNetworkManager.getUserChats(mUserModel.getNo(), new SuccessCallBack<ChatRoomListModel>() {
            @Override
            public void onCallBack(ChatRoomListModel result) {
                List<ChatRoom> chatRoomList = result.getChatRoomList();
                for(ChatRoom chatRoom : chatRoomList) {
                    int state = getChatRoomState(chatRoom);
                    int counterNo = chatRoom.getUserFromNo();
                    if(counterNo == mUserModel.getNo())
                        counterNo = chatRoom.getUserToNo();

                    getChatRoomModelWithFullData(new ChatRoomModel(null, "", mStateLastChats[state], counterNo, chatRoom.getRoomKey(), chatRoom.getEndDate(), state), appId, chatRoomModel -> {

                        mChatRoomModelList.add(chatRoomModel);
                        chatRoomList.remove(chatRoom);

                        if(chatRoomList.size() == 0) {
                            UserPreference.getInstance().setChatRoomModelList(mChatRoomModelList);
                            mView.showChattingList(mChatRoomModelList);
                        }
                    });
                }
                if(chatRoomList.size() == 0)
                    mView.showChattingList(mChatRoomModelList);
            }
        });
    }

    private int getChatRoomState(ChatRoom chatRoom) {
        if(chatRoom.isMatched())
            return ChatRoomModel.STATE_MATCHING;
        else {
            if(chatRoom.getUserFromNo() == mUserModel.getNo())
                return ChatRoomModel.STATE_FROM;
            else
                return ChatRoomModel.STATE_TO;
        }
    }

    /**
     * 채팅방의 마지막 채팅이 바뀌는것을 감지하는 이벤트 리스너들을 추가한다.
     */
    public void addChatRoomEventListeners() {
        for(int i = 0; i < mChatRoomModelList.size(); i++) {
            final int position = i;
            final ChatRoomModel chatRoomModel = mChatRoomModelList.get(i);
            ValueEventListener chatRoomEventListener = getChatEventListener(chatRoomModel, new SuccessCallBack<ChatRoomModel>() {
                @Override
                public void onCallBack(ChatRoomModel result) {
                    mView.showChangeLastChat(position, chatRoomModel);
                }
            });

            com.matching.goodgoodman.constellationdating.firebase.ChatRoom chatRoom = new com.matching.goodgoodman.constellationdating.firebase.ChatRoom(chatRoomModel.getRoomKey(), null, null);
            chatRoom.getChatRoomReference().addValueEventListener(chatRoomEventListener);
            mValueEventListenerList.add(chatRoomEventListener);
        }
    }

    public void removeChatRoomEventListeners() {
        for(int i = 0; i < mChatRoomModelList.size(); i++) {
            if(i >= mValueEventListenerList.size())
                break;

            final ChatRoomModel chatRoomModel = mChatRoomModelList.get(i);

            com.matching.goodgoodman.constellationdating.firebase.ChatRoom chatRoom = new com.matching.goodgoodman.constellationdating.firebase.ChatRoom(chatRoomModel.getRoomKey(), null, null);
            chatRoom.getChatRoomReference().removeEventListener(mValueEventListenerList.get(i));
        }
        mValueEventListenerList.clear();
    }

    private void getChatRoomModelWithFullData(ChatRoomModel chatRoomModel, String appId, SuccessCallBack<ChatRoomModel> successCallBack) {
        mNetworkManager.getUser(chatRoomModel.getCounterUserNo(), new SuccessCallBack<User>() {
            @Override
            public void onCallBack(User result) {
                if(result.getName() != null)
                    chatRoomModel.setCounterName(result.getName());

                AccessToken accessToken = new AccessToken(result.getToken(), appId, result.getId(), null, null, null, null, null);
                //TODO::임시값으로, 서버가 더미값에서 벗어나면 삭제
                //AccessToken accessToken = new AccessToken("EAABtx6fZAa0EBAD13ULk8RioVig0nKZBkLdwXwyLV8jZAUaWDx06IdxDrGSQMyF9eoIj98diMbZCOGPBvwsrZCd9EqpxDMIC1IPLJJnCF9oACP9NYUj2ax94z3hzZAgPf1Ls3ZCeVeRx7JdGFsEEcsDAOS5HeHwfZAiw1e1ZBbAEZBzs6n1SjAIfxZBqHPFa0ZAvc77YH5pyDu2V3KfABWf2xcjMksGfdg2nboQZD",
                  //      appId, "1486769358082067", null, null, null, null, null);
                getChatRoomModelWithProfile(chatRoomModel, accessToken, successCallBack);
            }
        });
    }

    private void getChatRoomModelWithProfile(ChatRoomModel chatRoomModel, AccessToken accessToken, SuccessCallBack<ChatRoomModel> successCallBack) {
        mFacebookModule.getProfilePicture(accessToken, true, new SuccessCallBack<Bitmap>() {
            @Override
            public void onCallBack(Bitmap result) {
                chatRoomModel.setProfile(result);
                getChatRoomModelWithLastChat(chatRoomModel, successCallBack);
            }
        });
    }

    private void getChatRoomModelWithLastChat(ChatRoomModel chatRoomModel, SuccessCallBack<ChatRoomModel> successCallBack) {
        com.matching.goodgoodman.constellationdating.firebase.ChatRoom chatRoom = new com.matching.goodgoodman.constellationdating.firebase.ChatRoom(chatRoomModel.getRoomKey(), null, null);
        chatRoom.getChatRoomReference().limitToLast(1).addListenerForSingleValueEvent(getChatEventListener(chatRoomModel, successCallBack));
    }

    private ValueEventListener getChatEventListener(final ChatRoomModel chatRoomModel, SuccessCallBack<ChatRoomModel> successCallBack) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Chat chat = null;
                for (DataSnapshot childData : dataSnapshot.getChildren()) {
                    try {
                        chat = childData.getValue(Chat.class);
                    } catch (DatabaseException e) {
                        e.printStackTrace();
                    }
                }

                if (chat != null)
                    chatRoomModel.setLastChat(chat.getMessage());

                successCallBack.onCallBack(chatRoomModel);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
            }
        };
    }

    @Override
    public void attachView(ChattingListView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
