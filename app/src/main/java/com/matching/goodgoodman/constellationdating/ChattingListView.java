package com.matching.goodgoodman.constellationdating;

import com.matching.goodgoodman.constellationdating.model.ChatRoomModel;

import java.util.List;

/**
 * Created by Hyeong_Wook on 2017-08-16.
 */

public interface ChattingListView {
    void showChattingList(List<ChatRoomModel> chatRoomModelList);
    void showChangeLastChat(int position, ChatRoomModel chatRoomModel);
}
