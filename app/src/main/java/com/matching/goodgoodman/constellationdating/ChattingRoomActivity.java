package com.matching.goodgoodman.constellationdating;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.matching.goodgoodman.constellationdating.firebase.model.Chat;
import com.matching.goodgoodman.constellationdating.model.ChatRoomModel;
import com.sign.chatmodule.ChatActivity;
import com.sign.chatmodule.model.ChatMessage;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChattingRoomActivity extends ChatActivity implements ChattingRoomView {
    public static final String STATE = "state";
    public static final String END_DATE = "endDate";
    public static final String COUNTER_NO = "counterNo";

    @BindView(R.id.chatList) View mChatListView;
    @BindView(R.id.wait_matching) View mWaitMatchingView;
    @BindView(R.id.wait_response) TextView mWaitResponseText;
    @BindView(R.id.profile_image) ImageView mProfileImage;
    @BindView(R.id.timer) TextView mTimerView;
    @BindView(R.id.notice_response) TextView mNoticeResponseText;
    @BindView(R.id.button_layout) View mButtonsView;
    @BindView(R.id.response_button) Button mResponseButton;
    @BindView(R.id.ignore_button) Button mIgnoreButton;

    ChattingRoomPresenter mPresenter;
    String mTempMessage;
    private int mCounterNo = -1;
    private int mState = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.initialize(R.layout.activity_chatingroom);
        ButterKnife.bind(this);
        initialUI();
        initialEventListeners();

        mPresenter = new ChattingRoomPresenter(getApplicationContext());
        mPresenter.attachView(this);

        mCounterNo = getIntent().getIntExtra(COUNTER_NO, -1);
        show(getIntent().getIntExtra(STATE, -1));
    }

    private void initialUI() {
        setTitleImage(getResources().getDrawable(R.drawable.toplogo, null));
    }

    private void initialEventListeners() {
        mResponseButton.setOnClickListener((v) -> mPresenter.acceptChatRoom(mCounterNo));
        mIgnoreButton.setOnClickListener((v) -> mPresenter.ignoreChatRoom(mCounterNo));
        mProfileImage.setOnClickListener((v) -> onOpponentThumbnailClicked());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mState == ChatRoomModel.STATE_MATCHING)
            mPresenter.removeChatEventListener();

        mPresenter.detachView();
        mPresenter = null;
    }

    @Override
    public void show(int state) {
        mState = state;
        mChatListView.setVisibility(View.GONE);
        mWaitMatchingView.setVisibility(View.GONE);
        mButtonsView.setVisibility(View.GONE);
        mNoticeResponseText.setVisibility(View.GONE);

        if(state == ChatRoomModel.STATE_MATCHING) {
            mChatListView.setVisibility(View.VISIBLE);
            mPresenter.loadExistMessageList(mCounterNo);
            mPresenter.addChatEventListener();

        } else {
            mWaitMatchingView.setVisibility(View.VISIBLE);

            if(state == ChatRoomModel.STATE_FROM)
                mNoticeResponseText.setVisibility(View.VISIBLE);
            else if (state == ChatRoomModel.STATE_TO)
                mButtonsView.setVisibility(View.VISIBLE);

            mPresenter.loadWaitingChatRoom(mCounterNo);
        }
    }

    @Override
    public void onSentMessage(final String id, String body, long date) {
    }


    //Todo 뒤로가기 버튼 클릭 되었을 때 발생
    @Override
    public void onBacked() {
        finish();
    }

    //Todo 채팅방 나가기 이벤트 발생했을 때(완전 나가기) 발생
    @Override
    public void onLeaved() {
        mPresenter.ignoreChatRoom(mCounterNo);
    }

    //Todo 상대 썸네일 클릭 되었을 때 발생
    @Override
    public void onOpponentThumbnailClicked() {
        if(mCounterNo == -1)
            return;

        Intent intent = new Intent(getApplicationContext(), CounterProfileActivity.class);
        intent.putExtra(CounterProfileActivity.EXTRA_COUNTER_USER_NO, mCounterNo);
        intent.putExtra(CounterProfileActivity.EXTRA_LIKE_VISIBLE, false);
        startActivity(intent);
    }

    @Override
    protected void onSystemTextClicked() {
        onOpponentThumbnailClicked();
    }

    @Override
    public void showExistMessages(ArrayList<ChatMessage> chatMessageList, String userName, String counterName, Bitmap userProfile, Bitmap counterProfile) {
        initRecyclerAdapter(chatMessageList, userName, counterName, userProfile, counterProfile);
    }

    @Override
    public void showReceiveMessage(Chat chat) {
        receiveOpponentMessage(chat.getMessage(), chat.getTime());
    }

    @Override
    public void showWaitTime(String leftTimeStr) {
        mTimerView.setText(leftTimeStr);
    }

    @Override
    protected void sendMessage() {
        mTempMessage = getEditTextValue();
        super.sendMessage();
        mPresenter.sendMessage(mTempMessage);
    }

    @Override
    public void showSendErrorToast() {
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.chat_send_error_message), Toast.LENGTH_LONG).show();
        setEditTextMessage(mTempMessage);
    }

    @Override
    public void showButtonTexts(String responseText, String ignoreText) {
        mResponseButton.setText(responseText);
        mIgnoreButton.setText(ignoreText);
    }

    @Override
    public void showWaitMessages(String title, Bitmap profile, String notify) {
        mWaitResponseText.setText(title);
        mProfileImage.setImageBitmap(profile);
        mNoticeResponseText.setText(notify);
    }

    @Override
    public void destroyChatRoom() {
        setResult(ChattingListFragment.CHAT_LIST_NEED_REFRESH);
        finish();
    }

    @Override
    public void setChattingListRefresh() {
        setResult(ChattingListFragment.CHAT_LIST_NEED_REFRESH);
    }
}
