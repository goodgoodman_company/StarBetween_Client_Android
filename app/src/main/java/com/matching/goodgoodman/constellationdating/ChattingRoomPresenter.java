package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.ValueEventListener;
import com.matching.goodgoodman.constellationdating.firebase.ChatRoom;
import com.matching.goodgoodman.constellationdating.firebase.model.Chat;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.model.ChatRoomModel;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.NetworkManager;
import com.matching.goodgoodman.constellationdating.server.model.ResultStatus;
import com.sign.chatmodule.model.ChatMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hyeong_Wook on 2017-10-07.
 */

public class ChattingRoomPresenter implements Presenter<ChattingRoomView> {
    private static final int SECONDS_IN_HOUR = 60 * 60;
    private static final String LEFT_TIME_STR = "%02d:%02d:%02d";

    private static final int FROM_WAIT_DESCRIPTION = 344;
    private static final int NOTIFY_RESPONSE_DESCRIPTION = 345;
    private static final int TO_WAIT_DESCRIPTION = 346;
    private static final int RESPONSE_DESCRIPTION = 347;
    private static final int IGNORE_DESCRIPTION = 348;

    private Context mContext;
    private ChattingRoomView mView;
    private ChatRoom mChatRoom;
    private UserModel mUserModel;
    private ChildEventListener mChatEventListener;
    private DescriptionDBHelper mDescriptionDBHelper;
    private NetworkManager mNetworkManager;

    private Handler mTimerHandler = null;
    private boolean mTimerIsRunning = false;

    private int mExistChatCount = 0;
    private int mNewChatCount = 0;

    public ChattingRoomPresenter(Context context) {
        mContext = context;
        mUserModel = UserPreference.getInstance().getUserModel();
        mChatEventListener = getChatEventListener();
        mNetworkManager = NetworkManager.getInstance(context);

        //TODO::sharedPreference 정립되면 언어 바꾸고 수정
        SharedPreferences sharedPreferences = context.getSharedPreferences("pref", Context.MODE_PRIVATE);
        mDescriptionDBHelper = new DescriptionDBHelper(context, DescriptionDBHelper.LANGUAGE_ENGLISH);
    }

    public void loadExistMessageList(int counterNo) {
        ChatRoomModel chatRoomModel = UserPreference.getInstance().getChatRoomModel(counterNo);
        if(chatRoomModel == null)
            return;

        mChatRoom = new ChatRoom(chatRoomModel.getRoomKey(), null, null);

        List<Chat> chatList = new ArrayList<>();
        ArrayList<ChatMessage> chatMessageList = new ArrayList<>();

        mChatRoom.getChatRoomReference().limitToLast(ChatRoom.MAX_NUMBER_OF_CHILDREN).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot childData : dataSnapshot.getChildren()) {
                    try {
                        chatList.add(childData.getValue(Chat.class));
                    } catch (DatabaseException e) {
                        e.printStackTrace();
                    }
                }

                mExistChatCount = chatList.size();
                for(Chat chat : chatList) {
                    int type = ChatMessage.MSG_TYPE_YOUR_MESSAGE;
                    if(mUserModel.getId().equals(chat.getUserID()))
                        type = ChatMessage.MSG_TYPE_MY_MESSAGE;

                    chatMessageList.add(new ChatMessage(chat.getUserID(), type, chat.getMessage(), chat.getTime()));
                }

                mView.showExistMessages(chatMessageList, mUserModel.getName(), chatRoomModel.getCounterName(), mUserModel.getProfileImage(), chatRoomModel.getProfile());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
            }
        });
    }

    public void loadWaitingChatRoom(int counterNo) {
        ChatRoomModel chatRoomModel = UserPreference.getInstance().getChatRoomModel(counterNo);
        if(chatRoomModel == null)
            return;

        String title;
        if(chatRoomModel.getState() == ChatRoomModel.STATE_FROM)
            title = String.format(mDescriptionDBHelper.getDescription(FROM_WAIT_DESCRIPTION).getComment(), chatRoomModel.getCounterName());
        else
            title = String.format(mDescriptionDBHelper.getDescription(TO_WAIT_DESCRIPTION).getComment(), chatRoomModel.getCounterName());

        String responseText = mDescriptionDBHelper.getDescription(RESPONSE_DESCRIPTION).getComment();
        String ignoreText = mDescriptionDBHelper.getDescription(IGNORE_DESCRIPTION).getComment();
        Bitmap profile = chatRoomModel.getProfile();
        String notifyComment = mContext.getResources().getString(R.string.wait_chatroom);

        mView.showButtonTexts(responseText, ignoreText);
        mView.showWaitMessages(title, profile, notifyComment);
        startWaitTimer(chatRoomModel.getEndDate().getTime());
    }

    public void acceptChatRoom(int counterNo) {
        mNetworkManager.putLike(mUserModel.getNo(), counterNo, result -> {
            if(result.getResult().equals(ResultStatus.SUCCESS)) {
                endWaitTimer();
                mView.setChattingListRefresh();
                mView.show(ChatRoomModel.STATE_MATCHING);
            }
        });
    }

    public void ignoreChatRoom(int counterNo) {
        ChatRoomModel chatRoomModel = UserPreference.getInstance().getChatRoomModel(counterNo);
        if(chatRoomModel == null)
            return;

        mNetworkManager.putDestroyChatRoom(chatRoomModel.getRoomKey(), (resultStatus) -> {
            if(resultStatus.getResult().equals(ResultStatus.SUCCESS))
                mView.destroyChatRoom();
        });
    }

    private void startWaitTimer(final long endTime) {
        mTimerHandler = new Handler() {
            @Override
            public synchronized void handleMessage(Message msg) {
                if(!mTimerIsRunning)
                    return;

                long leftTimeSeconds = (endTime - System.currentTimeMillis()) / 1000;
                long hour = leftTimeSeconds / SECONDS_IN_HOUR;
                long secondsHour = leftTimeSeconds % SECONDS_IN_HOUR;
                long minutes = secondsHour / 60;
                long seconds = secondsHour % 60;

                if(hour <= 0 && minutes <= 0 && seconds <= 0)
                    mView.destroyChatRoom();
                else
                    mView.showWaitTime(String.format(LEFT_TIME_STR, hour, minutes, seconds));

                this.sendEmptyMessageDelayed(0, 1000);
            }
        };
        mTimerIsRunning = true;
        mTimerHandler.sendEmptyMessage(0);
    }

    private synchronized void endWaitTimer() {
        mTimerIsRunning = false;
        mTimerHandler.removeCallbacksAndMessages(null);
    }

    public void addChatEventListener() {
        mChatRoom.getChatRoomReference().limitToLast(ChatRoom.MAX_NUMBER_OF_CHILDREN).addChildEventListener(mChatEventListener);
    }

    public void removeChatEventListener() {
        mChatRoom.getChatRoomReference().removeEventListener(mChatEventListener);
    }

    public void sendMessage(String body) {
        mChatRoom.sendChat(new Chat(mUserModel.getId(), body)).addOnFailureListener(e -> mView.showSendErrorToast());
    }

    private ChildEventListener getChatEventListener() {
        return new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                    Chat chat = dataSnapshot.getValue(Chat.class);

                    mNewChatCount++;
                    if(!chat.getUserID().equals(mUserModel.getId()) && mNewChatCount > mExistChatCount)
                        mView.showReceiveMessage(chat);

                } catch (DatabaseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
            }
        };
    }

    @Override
    public void attachView(ChattingRoomView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        if(mTimerHandler != null)
            endWaitTimer();

        mView = null;
    }
}
