package com.matching.goodgoodman.constellationdating;

import android.graphics.Bitmap;

import com.matching.goodgoodman.constellationdating.firebase.model.Chat;
import com.sign.chatmodule.model.ChatMessage;

import java.util.ArrayList;

/**
 * Created by Hyeong_Wook on 2017-10-07.
 */

public interface ChattingRoomView {
    void show(int state);
    void showButtonTexts(String responseText, String ignoreText);
    void showExistMessages(ArrayList<ChatMessage> chatMessageList, String userName, String counterName, Bitmap userProfile, Bitmap counterProfile);
    void showReceiveMessage(Chat chat);
    void showWaitTime(String leftTimeStr);
    void showSendErrorToast();
    void showWaitMessages(String title, Bitmap profile, String notify);
    void destroyChatRoom();
    void setChattingListRefresh();
}
