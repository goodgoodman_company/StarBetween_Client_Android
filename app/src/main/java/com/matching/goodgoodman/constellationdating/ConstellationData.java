package com.matching.goodgoodman.constellationdating;

/**
 * 별자리 관련 정보를 담고 있는 data 클래스
 * @author Hyeong_Wook
 */

public abstract class ConstellationData {
    public static final String ARIES = "AR";
    public static final String TAURUS = "TA";
    public static final String GEMINI = "GE";
    public static final String CANCER = "CN";
    public static final String LEO = "LE";
    public static final String VIRGO = "VI";
    public static final String LIBRA = "LI";
    public static final String SCORPIO = "SC";
    public static final String SAGITTARUS = "SG";
    public static final String CAPRICORN = "CP";
    public static final String AQUARIUS = "AQ";
    public static final String PISEES = "PI";

    public static final String[] CONSTELLATIONS = {ARIES, TAURUS, GEMINI, CANCER, LEO, VIRGO, LIBRA, SCORPIO, SAGITTARUS, CAPRICORN, AQUARIUS, PISEES};
    public static final int[] DEGREES = {60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 0, 30};
    public static final int[] CONSTELLATION_DRAWABLE_IDS = {
            R.drawable.aries,
            R.drawable.taurus,
            R.drawable.gemini,
            R.drawable.cancer,
            R.drawable.leo,
            R.drawable.virgo,
            R.drawable.libra,
            R.drawable.scorpio,
            R.drawable.sagittarus,
            R.drawable.capricorn,
            R.drawable.aquarius,
            R.drawable.pisces,
    };

    public static int getIndex(String constellation) {
        for (int i = 0; i < CONSTELLATIONS.length; i++) {
            if (CONSTELLATIONS[i].equals(constellation))
                return i;
        }
        return -1;
    }
}
