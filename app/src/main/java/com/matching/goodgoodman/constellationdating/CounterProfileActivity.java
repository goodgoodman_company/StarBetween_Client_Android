package com.matching.goodgoodman.constellationdating;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.model.ChatRoomModel;
import com.matching.goodgoodman.constellationdating.model.ColorData;
import com.matching.goodgoodman.constellationdating.model.DrawableData;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.popup.SlidePopUpDialog;
import com.matching.goodgoodman.constellationdating.utils.ColorIndexHelper;
import com.matching.goodgoodman.constellationdating.utils.StarPointChangeHandler;
import com.matching.goodgoodman.constellationdating.utils.ViewMover;
import com.pnikosis.materialishprogress.ProgressWheel;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dao.Zodiac;

public class CounterProfileActivity extends AppCompatActivity implements CounterProfileView {
    public static final String EXTRA_COUNTER_USER_NO = "counterUserNo";
    public static final String EXTRA_SCORE = "score";
    public static final String EXTRA_LIKE_VISIBLE = "likeVisible";

    private static final String PERCENTAGE_STR = "%d%%";

    @BindView(R.id.activity_counterprofile_backGround) ImageView mBackgroud;
    @BindView(R.id.activity_counterprofile_backButton) ImageView mBackButton;
    @BindView(R.id.activity_counterprofile_maintopLogo) ImageView mTopLogo;

    @BindView(R.id.activity_counterprofile_constellation) ImageView mConstellationImage;
    @BindView(R.id.activity_counterprofile_Name) TextView mName;
    @BindView(R.id.activity_counterprofile_Age) TextView mAge;
    @BindView(R.id.activity_counterprofile_location_image) ImageView mLocationImage;
    @BindView(R.id.activity_counterprofile_location_text) TextView mLocationTextView;
    @BindView(R.id.activity_counterprofile_accessStateImage)ImageView mAccessImage;
    @BindView(R.id.activity_counterprofile_accessStateLabel) TextView mAccessStateTextView;

    @BindView(R.id.synastry_non_percentage) ImageView mSynastryNonPercentage;
    @BindView(R.id.synastry_percentage) ImageView mSynastryPercentage;
    @BindView(R.id.synastry_text) TextView mSynastryText;

    @BindView(R.id.activity_counterprofile_content) TextView mContent;

    @BindView(R.id.activity_counterprofile_photo_frame) FrameLayout mPhotoFrame;
    @BindView(R.id.photo_viewpager) ViewPager mViewPager;
    @BindView(R.id.indicator) MagicIndicator mIndicator;
    @BindView(R.id.activity_counterprofile_floatingLayout) LinearLayout mFloatingLayout;
    @BindView(R.id.progress_wheel) ProgressWheel mProgressWheel;

    @BindView(R.id.activity_counterprofile_floatingActionButton) FloatingActionButton mFloatingActionButton;
    @BindView(R.id.activity_counterprofile_likeButton) FloatingActionButton mLikeButton;

    @BindView(R.id.activity_counterprofile_starPointTextView) TextView starPointText;
    @BindView(R.id.activity_counterprofile_starPointImage) ImageView starPointImage;

    @BindView(R.id.starSignSymbol) NatalSymbol mStarSignSymbol;
    @BindView(R.id.planetSymbol) NatalSymbol mPlanetSymbol;
    @BindView(R.id.elementalSymbol) NatalSymbol mElementalSymbol;
    @BindView(R.id.colorSymbol) NatalSymbol mColorSymbol;

    private CounterProfilePresenter mPresenter;
    private CounterProfileToast toast;
    private DescriptionDBHelper mDescriptionDBHelper;

    private float mFloatingLayoutMinY = -1.0f;
    private float mFloatingLayoutMaxY = -1.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter_profile);
        ButterKnife.bind(this);

        mPresenter = new CounterProfilePresenter(getApplicationContext(), this);
        mDescriptionDBHelper = new DescriptionDBHelper(getApplicationContext(), DescriptionDBHelper.LANGUAGE_ENGLISH);

        StarPointChangeHandler.getInstance().addChangeListener(this::showStarPoint);

        TouchEventLinearLayout linearLayout = (TouchEventLinearLayout) findViewById(R.id.activity_counterprofile_linearLayout2);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (mFloatingLayoutMinY == -1.0f) {
                    mFloatingLayoutMinY = mFloatingLayout.getY() - mFloatingLayout.getHeight();
                    mFloatingLayoutMaxY = mFloatingLayout.getY();
                }
                ViewMover.moveVerticalWithScrollMotion(mFloatingLayout, motionEvent, mFloatingLayoutMinY, mFloatingLayoutMaxY, true);
                return true;
            }
        });

        initView();
        mPresenter.loadStarPoint();

        Intent Intent = getIntent();
        /*2개를 받는데, 위에는 No, 밑에는 리스트의 posotion
         * position은 서버쪽 matchingList에 "Y"표시를 해야해서 뺄 수 없음
         * -> 좋아요 누르는 리스트쪽에서만 position을 쓰게 해둠 + -1일땐, 그냥 좋아요가 눌린 상태로 표시하게 해뒀음
         * -> -1일때 넘어가는 부분의 체크가 필요
        */
        int counterUserNo = Intent.getIntExtra(EXTRA_COUNTER_USER_NO, -1);
        mPresenter.setScore(Intent.getIntExtra(EXTRA_SCORE, -1));
        mPresenter.loadCounterUserProfile(counterUserNo, getResources().getString(R.string.facebook_app_id));
        //showLikeButton(counterUserPosition);
    }

    @Override
    public void showStarPoint(int starPoint) {
        starPointText.setText(String.valueOf(starPoint));
    }

    private void initView() {
        Glide.with(this).load(R.drawable.background_blue).into(mBackgroud);
        Glide.with(this).load(R.drawable.toplogo).into(mTopLogo);
        Glide.with(this).load(R.drawable.counteruser_location).into(mLocationImage);
        Glide.with(this).load(R.drawable.back).into(mBackButton);
        Glide.with(this).load(R.drawable.stars).into(starPointImage);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Glide.with(this).load(R.mipmap.more).into(mFloatingActionButton);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RotateAnimation rotateDialogButton = new RotateAnimation(0, 45, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotateDialogButton.setDuration(300);
                rotateDialogButton.setRepeatCount(Animation.ABSOLUTE);
                rotateDialogButton.setFillAfter(true);
                mFloatingActionButton.startAnimation(rotateDialogButton);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        showInfoDialog();
                    }
                }, 300);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mFloatingActionButton.clearAnimation();
                    }
                }, 600);
            }
        });

        mLikeButton.setVisibility(getIntent().getBooleanExtra(EXTRA_LIKE_VISIBLE, true) ? View.VISIBLE : View.GONE);
    }

    public void showInfoDialog() {

        //Todo 1. 객체 생성
        SlidePopUpDialog slidePopUpDialog = new SlidePopUpDialog();
        slidePopUpDialog.setPopupType(SlidePopUpDialog.COUNTERUSER_POPUP);
        slidePopUpDialog.setCounterUserSynastryAspects(mPresenter.getSynastryAspects());
        slidePopUpDialog.setCounterUserNo(getIntent().getIntExtra(EXTRA_COUNTER_USER_NO, -1));

        slidePopUpDialog.setTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ViewMover.moveVerticalWithScrollMotion(slidePopUpDialog.getFloatingLayout(), event,
                        v.getHeight() - getResources().getDimensionPixelSize(R.dimen.activity_main_tabLayout_height), v.getHeight(), true);
                return true;
            }
        });

        slidePopUpDialog.show(getSupportFragmentManager(), "tag");
    }

    private void showLikeCheckDialog() {
        int score = getIntent().getIntExtra(EXTRA_SCORE, -1);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        if (score >= 90)
            alertDialogBuilder.setMessage(getResources().getString(R.string.perfect_like));
        else
            alertDialogBuilder.setMessage(getResources().getString(R.string.normal_like));


        alertDialogBuilder.setTitle(getResources().getString(R.string.send_like));
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (mPresenter.getScore() < 90)
                    mPresenter.loadClickLikeButton();
                else
                    mPresenter.putLike(0);
            }
        });

        alertDialogBuilder.setCancelable(false).setNegativeButton("Nope",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void showPhotos(List<Bitmap> photoList, boolean isOpen) {
        if(!isOpen) {
            mPhotoFrame.setVisibility(View.GONE);
            return;
        }

        LayoutInflater layoutInflater = getLayoutInflater();

        mViewPager.setAdapter(new PhotoPagerAdapter(photoList, layoutInflater));

        ScaleCircleNavigator scaleCircleNavigator = new ScaleCircleNavigator(getApplicationContext());
        scaleCircleNavigator.setCircleCount(mViewPager.getAdapter().getCount());

        scaleCircleNavigator.setNormalCircleColor(ContextCompat.getColor(getApplicationContext(), R.color.deep_gray));
        scaleCircleNavigator.setSelectedCircleColor(ContextCompat.getColor(getApplicationContext(), R.color.sign_purple));

        mIndicator.setNavigator(scaleCircleNavigator);
        ViewPagerHelper.bind(mIndicator, mViewPager);
    }

    @Override
    public void showProfile(UserModel counterUserModel) {
        mConstellationImage.setImageBitmap(counterUserModel.getProfileImage());
        mName.setText(counterUserModel.getName());
        mContent.setText(counterUserModel.getContent());
    }

    @Override
    public void showLocationText(String distanceString) {
        mLocationTextView.setText(distanceString);
    }

    @Override
    public void showAge(String age) {
        mAge.setText(age);
    }

    @Override
    public void showSynastryScore(int score) {
        LinearLayout.LayoutParams scoreParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, score);
        LinearLayout.LayoutParams nonScoreParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 100 - score);

        int colorId = ColorData.ID_LOVES[ColorIndexHelper.getLoveIndex(score)];
        int color = ContextCompat.getColor(getApplicationContext(), colorId);

        mSynastryPercentage.setColorFilter(color);
        mSynastryPercentage.setLayoutParams(scoreParam);
        mSynastryNonPercentage.setLayoutParams(nonScoreParam);
        mSynastryText.setText(String.format(PERCENTAGE_STR, score));
    }

    //TODO : chating으로 이동 필요
    @Override
    public void showLikeSuccess() {
        mLikeButton.setImageResource(R.drawable.send_like_check);
        mLikeButton.setOnClickListener(v -> Toast.makeText(getApplicationContext(), getResources().getString(R.string.alreadyLike), Toast.LENGTH_SHORT).show());
    }

    @Override
    public void openChatRoom(int counterNo) {
        Intent intent = new Intent(this, ChattingRoomActivity.class);
        intent.putExtra(ChattingRoomActivity.COUNTER_NO, counterNo);
        ChatRoomModel chatRoomModel = UserPreference.getInstance().getChatRoomModel(counterNo);

        if (chatRoomModel != null)
            startChatRoom(intent, chatRoomModel.getEndDate().getTime(), chatRoomModel.getState());
        else
            mPresenter.loadChatRoomModel(counterNo, result -> startChatRoom(intent, result.getEndDate().getTime(), result.getState()));

        setResult(ChattingListFragment.CHAT_LIST_NEED_REFRESH);
    }

    private void startChatRoom(Intent intent, long endTime, int state) {
        intent.putExtra(ChattingRoomActivity.END_DATE, endTime);
        intent.putExtra(ChattingRoomActivity.STATE, state);
        startActivity(intent);
    }

    @Override
    public void showLikeButton() {
        if (mPresenter.checkLike()) {
            showLikeSuccess();
        } else {
            mLikeButton.setImageResource(R.mipmap.like);
            mLikeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showLikeCheckDialog();
                }
            });
        }
    }

    //TODO 나중에 toast 수정 필요
    @Override
    public void showNoStars() {
        Log.v("showNoStars", "showNoStars");
        Toast.makeText(getApplicationContext(), "별이 부족합니다", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressWheel() {
        mProgressWheel.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressWheel() {
        mProgressWheel.setVisibility(View.GONE);
    }

    @Override
    public void showAccessState(String accessState, int onlineImageNum) {

        int[] onlineImage = {
                R.drawable.counteruser_access_online_1,
                R.drawable.counteruser_access_online_2,
                R.drawable.counteruser_access_online_3
        };

        Glide.with(this).load(onlineImage[onlineImageNum]).into(mAccessImage);
        mAccessStateTextView.setText(accessState);
    }

    @Override
    public void showNatalSymbolList(Zodiac zodiac) {
        int index = zodiac.getMonth() - 1;

        mStarSignSymbol.setSymbol(DrawableData.ID_ZODIACS[index]);
        mPlanetSymbol.setSymbol(DrawableData.ID_PLANETS[index]);
        mElementalSymbol.setSymbol(DrawableData.ID_ELEMENTS[index]);
        mColorSymbol.setSymbol(DrawableData.ID_COLORS[index]);

        mStarSignSymbol.setSymbolText(zodiac.getConstellationName().replace("\\n", "\n"));
        mPlanetSymbol.setSymbolText(zodiac.getPlanetName().replace("\\n", "\n"));
        mElementalSymbol.setSymbolText(zodiac.getElement().replace("\\n", "\n"));
        mColorSymbol.setSymbolText(zodiac.getColor().replace("\\n", "\n"));
    }

    public class PhotoPagerAdapter extends PagerAdapter {
        public static final int NUMBER_OF_PHOTOS_ON_PAGE = 6;
        public static final int MAX_PAGES = 5;

        private List<Bitmap> mPhotoList = null;
        private LayoutInflater mLayoutInflater;

        public PhotoPagerAdapter(List<Bitmap> photoList, LayoutInflater inflater) {
            mPhotoList = photoList;
            mLayoutInflater = inflater;
        }

        @Override
        public int getCount() {
            if (mPhotoList.size() > NUMBER_OF_PHOTOS_ON_PAGE * MAX_PAGES)
                return MAX_PAGES;
            int count = mPhotoList.size() / NUMBER_OF_PHOTOS_ON_PAGE;
            boolean isExistLastPage = (mPhotoList.size() % NUMBER_OF_PHOTOS_ON_PAGE) > 0;
            if (isExistLastPage)
                count++;

            return count;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            int start = position * NUMBER_OF_PHOTOS_ON_PAGE;
            int end = start + NUMBER_OF_PHOTOS_ON_PAGE;

            View pageView = mLayoutInflater.inflate(R.layout.photo_viewpager_child, null);
            GridLayout gridLayout = (GridLayout) pageView.findViewById(R.id.photo_grid);
            gridLayout.setColumnCount(3);
            gridLayout.setRowCount(2);

            for (int i = start; i < end; i++) {
                if (i >= mPhotoList.size())
                    break;

                View itemView = mLayoutInflater.inflate(R.layout.photo_item, null);
                ImageView photoImage = (ImageView) itemView.findViewById(R.id.photo);
                photoImage.setImageBitmap(mPhotoList.get(i));

                /*
                if(i == MAX_PAGES * NUMBER_OF_PHOTOS_ON_PAGE - 1)
                    setOnClickEventWithFacebookConnection(photoImage);*/

                gridLayout.addView(itemView);
            }

            container.addView(pageView);
            return pageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }

}

