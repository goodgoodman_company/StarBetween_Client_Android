package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.facebook.AccessToken;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.ZodiacDBHelper;
import com.matching.goodgoodman.constellationdating.model.ChatRoomModel;
import com.matching.goodgoodman.constellationdating.model.HoroscopeModel;
import com.matching.goodgoodman.constellationdating.model.SynastryAspect;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.FailureCallBack;
import com.matching.goodgoodman.constellationdating.server.NetworkManager;
import com.matching.goodgoodman.constellationdating.server.SuccessCallBack;
import com.matching.goodgoodman.constellationdating.server.model.ChatRoom;
import com.matching.goodgoodman.constellationdating.server.model.ResultStatus;
import com.matching.goodgoodman.constellationdating.server.model.User;
import com.matching.goodgoodman.constellationdating.utils.ConstellationCalculator;
import com.matching.goodgoodman.constellationdating.utils.DateConverter;
import com.matching.goodgoodman.constellationdating.utils.FacebookModule;
import com.matching.goodgoodman.constellationdating.utils.StarPointChangeHandler;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;

/**
 * Created by dorol on 2017-09-30.
 */

public class CounterProfilePresenter implements Presenter<CounterProfileView> {
    private static final int LIKE_POINT = 214;

    private CounterProfileView mView;
    private NetworkManager mNetworkManager;
    private List<UserModel> mCounterUserModelList;
    private UserModel mUserModel;

    private UserModel mCounterUser;
    private Context mContext;

    private List<SynastryAspect> mSynastryAspects;
    private int mScore = -1;

    private DescriptionDBHelper mDescriptionDBHelper;

    public CounterProfilePresenter(Context context, CounterProfileView view) {
        mContext = context;
        mNetworkManager = NetworkManager.getInstance(context);
        mUserModel = UserPreference.getInstance().getUserModel();
        mCounterUserModelList = UserPreference.getInstance().getCounterUserModelList();
        mCounterUser = new UserModel();
        mDescriptionDBHelper = new DescriptionDBHelper(mContext,DescriptionDBHelper.LANGUAGE_ENGLISH);
        attachView(view);
    }

    public void loadCounterUserProfile(int counterUserNo, String appId) {

        mNetworkManager.getUser(counterUserNo, mUserModel.getNo(), new SuccessCallBack<User>() {
            @Override
            public void onCallBack(User result) {
                mCounterUser.setNo(counterUserNo);
                mCounterUser.setId(result.getId());
                mCounterUser.setFaceBookToken(result.getToken());
                mCounterUser.setName(result.getName());
                mCounterUser.setEmail(result.getEmail());
                mCounterUser.setGender(result.getSex());
                mCounterUser.setContent(result.getContent());
                mCounterUser.setLatitude(result.getLatitude());
                mCounterUser.setLongitude(result.getLongitude());
                mCounterUser.setBirthday(result.getBirthday());
                mCounterUser.setLastConTime(result.getLastConTime());
                mCounterUser.setUseLikeYn(result.getUseLikeYn());
                mCounterUser.setMatchingNo(result.getMatchingNo());
                mCounterUser.setCurrentLongitude(result.getLastLongitude());
                mCounterUser.setCurrentLatitude(result.getLastLatitude());
                mCounterUser.setImageOpenYn(result.getImageOpenYn());

                getHoroscopeModel((horoscopeModel) -> {
                    mSynastryAspects = getSynastryAspects();
                    loadSynastryScore();
                    loadNatalSymbolList(mContext, counterUserNo);
                });
                loadCounterUserPicture(appId);
                calcLocation();
                calcAccessTime();
                calcAge();
                mView.showLikeButton();
            }
        }, new FailureCallBack<User>() {
            @Override
            public void onCallBack(Call<User> call, Throwable t) {
                Log.v("loadCounterUserPicture", "FailureCallBack");
            }
        });
    }

    public void setScore(int score) {
        mScore = score;
    }

    public int getScore() {
        if(mScore == -1)
            mScore = ConstellationCalculator.getSynastryScore(mSynastryAspects);

        return mScore;
    }

    private void loadSynastryScore() {
        if(mScore == -1)
            mScore = ConstellationCalculator.getSynastryScore(mSynastryAspects);

        mView.showSynastryScore(mScore);
    }

    private void getHoroscopeModel(SuccessCallBack<HoroscopeModel> successCallBack) {
        HoroscopeModel horoscopeModel = UserPreference.getInstance().getCounterHoroscopeModel(mCounterUser.getNo());
        if (horoscopeModel != null) {
            successCallBack.onCallBack(horoscopeModel);
            return;
        }

        int[] birthday = DateConverter.dateToDividedTime(mCounterUser.getBirthday());
        ConstellationCalculator.getHoroscopeModel(mContext, birthday[0], birthday[1], birthday[2], birthday[3], birthday[4], mCounterUser.getLatitude(), mCounterUser.getLongitude(),
                result -> {
                    result.setCounterUserNo(mCounterUser.getNo());
                    UserPreference.getInstance().addCounterHoroscopeModel(result);
                    successCallBack.onCallBack(result);
                });
    }

    public List<SynastryAspect> getSynastryAspects() {
        if(mSynastryAspects != null)
            return mSynastryAspects;

        HoroscopeModel horoscopeModel = UserPreference.getInstance().getCounterHoroscopeModel(mCounterUser.getNo());
        if(horoscopeModel == null)
            return null;

        return ConstellationCalculator.getSynastryAspectList(UserPreference.getInstance().getHoroscopeModel(), horoscopeModel);
    }

    public void loadNatalSymbolList(Context context, int counterNo) {
        ZodiacDBHelper zodiacDBHelper = new ZodiacDBHelper(context, ZodiacDBHelper.LANGUAGE_ENGLISH);
        HoroscopeModel horoscopeModel = UserPreference.getInstance().getCounterHoroscopeModel(counterNo);
        String constellation = horoscopeModel.getPlanets()[0].getConstellation();

        mView.showNatalSymbolList(zodiacDBHelper.getZodiac(constellation));
    }

    //TODO :: accessToken에 더미들어있음
    public void loadCounterUserPicture(String appId) {
        FacebookModule facebookModule = FacebookModule.getInstance();

        AccessToken accessToken = new AccessToken(mCounterUser.getFaceBookToken(), appId, mCounterUser.getId(),
             Arrays.asList(FacebookModule.PERMISSIONS), null, null, null, null);
        //Log.v("loadCounterUserPicture", appId);
        //AccessToken accessToken = new AccessToken("EAABtx6fZAa0EBAD13ULk8RioVig0nKZBkLdwXwyLV8jZAUaWDx06IdxDrGSQMyF9eoIj98diMbZCOGPBvwsrZCd9EqpxDMIC1IPLJJnCF9oACP9NYUj2ax94z3hzZAgPf1Ls3ZCeVeRx7JdGFsEEcsDAOS5HeHwfZAiw1e1ZBbAEZBzs6n1SjAIfxZBqHPFa0ZAvc77YH5pyDu2V3KfABWf2xcjMksGfdg2nboQZD",
          //      appId, "1486769358082067",
            //    Arrays.asList(FacebookModule.PERMISSIONS), null, null, null, null);

        facebookModule.getProfilePicture(accessToken, true, new SuccessCallBack<Bitmap>() {
            @Override
            public void onCallBack(Bitmap result) {
                Log.v(getClass().getSimpleName(), result.toString());
                mCounterUser.setProfileImage(result);

                mView.showProfile(mCounterUser);
            }
        });

        facebookModule.getPhotos(accessToken, new SuccessCallBack<List<Bitmap>>() {
            @Override
            public void onCallBack(List<Bitmap> result) {
                boolean isOpen = mCounterUser.getImageOpenYn() != null && mCounterUser.getImageOpenYn().equals(UserModel.YES);
                mView.showPhotos(result, isOpen);
            }
        });
    }

    //TODO : 천궁도가 되면 counterUserGrade 넣어줘야함 ==  더미 빼야함
    public void loadClickLikeButton() {
        mNetworkManager.putStarPoint(mUserModel.getNo(), -LIKE_POINT, resultStatus -> {
            if (resultStatus.getResult().equals(ResultStatus.SUCCESS))
                putLike(LIKE_POINT);
            else
                mView.showNoStars();
        });
    }

    public void putLike(int point) {
        mNetworkManager.putLike(mUserModel.getNo(), mCounterUser.getNo(), result -> {
            if (result.getResult().equals(ResultStatus.SUCCESS)) {
                mUserModel.setStarPoint(mUserModel.getStarPoint() - point);
                StarPointChangeHandler.getInstance().notifyChangeData(mUserModel.getStarPoint());
                mCounterUser.setUseLikeYn("Y");
                mView.showLikeSuccess();
                mView.openChatRoom(mCounterUser.getNo());
            }
        });
    }

    public void loadChatRoomModel(int counterNo, SuccessCallBack<ChatRoomModel> successCallBack) {
        mView.showProgressWheel();

        int userNo = UserPreference.getInstance().getUserModel().getNo();
        ChatRoom ourChatRoom = new ChatRoom(userNo, counterNo, null);

        mNetworkManager.getUserChats(userNo, result -> {
            ChatRoomModel chatRoomModel = new ChatRoomModel(mCounterUser.getProfileImage(), mCounterUser.getName(), null, counterNo, null, null, ChatRoomModel.STATE_FROM);
            for (ChatRoom chatRoom : result.getChatRoomList()) {
                if (chatRoom.equals(ourChatRoom)) {
                    chatRoomModel.setRoomKey(chatRoom.getRoomKey());
                    chatRoomModel.setEndDate(chatRoom.getEndDate());
                    UserPreference.getInstance().addChatRoomModel(chatRoomModel);
                    mView.hideProgressWheel();
                    successCallBack.onCallBack(chatRoomModel);
                }
            }
        });
    }

    public boolean checkLike() {
        if (mCounterUser.getUseLikeYn().equals("Y"))
            return true;
        else
            return false;
    }

    public void loadStarPoint() {
        mView.showStarPoint(mUserModel.getStarPoint());
    }

    //TODO :: 상대방의 위치를 지금 서버에서 못받아오고 있음 수정필요
    public void calcLocation() {
        double distance = distance(mCounterUser.getCurrentLatitude(), mCounterUser.getCurrentLongitude(), mUserModel.getCurrentLatitude(), mUserModel.getCurrentLongitude());

        String distanceString = String.format(mDescriptionDBHelper.getDescription(321).getComment(), String.valueOf(Math.round(distance * 10) / 10.0));

        mView.showLocationText(distanceString);
    }

    private static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515 * 1.609344;

        return (dist);
    }
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public void calcAccessTime() {

        long different = System.currentTimeMillis() - mCounterUser.getLastConTime().getTime();

        long secondsInMilli = 1000;//밀리로 봤을 때 1초
        long minutesInMilli = secondsInMilli * 60;//밀리로 봤을 때 1분
        long hoursInMilli = minutesInMilli * 60;//1시간
        long daysInMilli = hoursInMilli * 24;//하루

        long totalMinutes = different / minutesInMilli;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        int descriptionId = 322;
        int index = 0;

        long[] SHOW_TIMES = {0, 0, elapsedMinutes, elapsedHours, elapsedDays, 0};
        long[] cutTimesByMin = {5, 16, 60, 24 * 60, 24 * 60 * 7};
        for(long min : cutTimesByMin) {
            if (totalMinutes < min) {
                break;
            }

            index++;
            descriptionId++;
        }

        String accessState = "";
        int imageNum = 2;
        if(descriptionId == 322) {
            accessState = "Online";
            imageNum = 0;
        }
        else if (descriptionId == 323 || descriptionId == 324){
            accessState = String.format(mDescriptionDBHelper.getDescription(descriptionId).getComment(), String.valueOf(SHOW_TIMES[index]));
            imageNum = 1;
        }
        else {
            accessState = String.format(mDescriptionDBHelper.getDescription(descriptionId).getComment(), String.valueOf(SHOW_TIMES[index]));
            imageNum = 2;
        }

        mView.showAccessState(accessState, imageNum);
    }

    private void calcAge() {
        long age = 0;
        long birthday = mCounterUser.getBirthday().getTime();
        long nowDate = System.currentTimeMillis();

        if(birthday < 0)
             age = Math.abs(birthday) + nowDate;
        else
            age = nowDate - birthday;

        long secondsInMilli = 1000;//밀리로 봤을 때 1초
        long minutesInMilli = secondsInMilli * 60;//밀리로 봤을 때 1분
        long hoursInMilli = minutesInMilli * 60;//1시간
        long daysInMilli = hoursInMilli * 24;//하루
        long yearsInMilli = daysInMilli * 365;

        long elapsedYears = age / yearsInMilli;

        mView.showAge(String.valueOf(elapsedYears));
    }

    @Override
    public void attachView(CounterProfileView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
