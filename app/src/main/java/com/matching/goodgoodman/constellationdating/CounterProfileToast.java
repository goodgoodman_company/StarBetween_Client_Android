package com.matching.goodgoodman.constellationdating;


import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;


public class CounterProfileToast extends Toast {

    Context mContext;
    private ImageView mimageView;

    public CounterProfileToast(Context context) {
        super(context);
        mContext = context;
    }

    //@SuppressLint()
    public void showToast(int duration) {
        LayoutInflater inflater;
        View v;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.toast_layout_counterprofile, null);

        ImageView mimageView = (ImageView) v.findViewById(R.id.counter_profile_toast_image);
        Glide.with(mContext).load(R.drawable.send_like).into(mimageView);

        show(this, v, duration);
    }

    private void show(Toast toast, View v, int duration) {
        toast.setGravity(Gravity.CENTER_VERTICAL,0,0);
        toast.setDuration(duration);
        toast.setView(v);
        toast.show();
    }
}
