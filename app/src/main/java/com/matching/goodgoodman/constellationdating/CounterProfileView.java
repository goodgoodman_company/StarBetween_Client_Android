package com.matching.goodgoodman.constellationdating;

import android.graphics.Bitmap;

import com.matching.goodgoodman.constellationdating.model.UserModel;

import java.util.List;

import dao.Zodiac;

/**
 * Created by dorol on 2017-09-30.
 */

public interface CounterProfileView {
    void showPhotos(List<Bitmap> photoList, boolean isOpen);
    void showProfile(UserModel counterUserModel);
    void showLikeSuccess();
    void showLikeButton();
    void openChatRoom(int counterNo);
    void showNoStars();
    void showStarPoint(int starPoints);
    void showProgressWheel();
    void hideProgressWheel();
    void showLocationText(String distanceString);
    void showAge(String age);
    void showAccessState(String accessState, int onlineImageNum);
    void showSynastryScore(int score);
    void showNatalSymbolList(Zodiac zodiac);
}
