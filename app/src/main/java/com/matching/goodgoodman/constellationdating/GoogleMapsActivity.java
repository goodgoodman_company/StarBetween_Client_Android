package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 구글 맵을 통해 사용자의 출생지를 받아오는 클래스.
 * @author Hyeong_Wook
 */
public class GoogleMapsActivity extends FragmentActivity {

    private LatLng mLatLng;
    private static String MARKER_TITLE = "";

    public static boolean sIsUpdatedLocation = false;
    private GoogleMap mMap;

    private SharedPreferences sharedpreferences;
    private DescriptionDBHelper mDescriptionDBHelper;

    @BindView(R.id.activity_google_maps_text) TextView mTextView;
    @BindView(R.id.login_dialog_back_button) ImageView mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        ButterKnife.bind(this);

        sharedpreferences = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
        mDescriptionDBHelper = new DescriptionDBHelper(getApplicationContext(),sharedpreferences.getString("DESCRIPTION_LANGUAGE", DescriptionDBHelper.LANGUAGE_ENGLISH));

        //mTextView.setText(mDescriptionDBHelper.getDescription(369).getComment());

        initView();

        initialResources();
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                initialListeners(googleMap);
            }
        });
    }

    private void initView() {
        //Glide.with(this).load(R.drawable.back).into(mBackButton);
        mBackButton.setOnClickListener(v -> onBackPressed());
    }

    private void initialResources() {
        MARKER_TITLE = getResources().getString(R.string.regist_coordinate);
    }

    private void initialListeners(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                addMarker(latLng);
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                setCoordinate(mLatLng);
                back();
            }
        });
    }

    /**
     * 구글맵에 마커를 더한다.
     * @param latLng 더해질 마커의 좌표
     */
    public void addMarker(LatLng latLng) {
        mMap.clear();
        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(MARKER_TITLE));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        marker.showInfoWindow();

        mLatLng = latLng;
    }

    /**
     * 모델에 좌표를 저장한다.
     * @param latLng 저장될 좌표
     */
    public void setCoordinate(LatLng latLng) {
        if(latLng != null) {
            UserModel userModel = UserPreference.getInstance().getUserModel();
            userModel.setLatitude((float) latLng.latitude);
            userModel.setLongitude((float) latLng.longitude);
        }
    }

    /**
     * 정해진 activity로 돌아간다.
     * WARNING::메서드의 인자값 및 내부 구현이 달라질 수 있는 메서드입니다.
     */
    private void back() {
        sIsUpdatedLocation = true;
        finish();
        //Intent intent  = new Intent(this, MainActivity.class);
        //startActivity(intent);
    }

}
