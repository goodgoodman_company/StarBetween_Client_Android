package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.matching.goodgoodman.constellationdating.model.Aspect;
import com.matching.goodgoodman.constellationdating.model.HoroscopeModel;
import com.matching.goodgoodman.constellationdating.utils.ConstellationCalculator;

import java.util.ArrayList;
import java.util.List;

import dao.Ascendent;
import dao.MediumCoeli;
import dao.Planet;

/**
 * 천궁도를 그리는 custom view입니다.
 *
 * @author Hyeong_Wook
 */
public class Horoscope extends LinearLayout {
    private static final int[] COLORS_ASPECT = {
            R.color.lightGreen,
            R.color.blue,
            R.color.green,
            R.color.red,
            R.color.yellow
    };

    private static final int LOW_ALPHA = 24;
    private static final int HIGH_ALPHA = 255;

    private Ascendent mAscendent;
    private MediumCoeli mMediumCoeli;
    private Planet[] mPlanets;
    private List<Aspect> mAspectList;

    private String mHighlightPlanetName = null;
    private String[] mSynastryHighlightPlanetNames = null;
    private int mAngle = 0;

    private static List<Drawable> sConstellationDrawableList = new ArrayList<>();
    private static List<Drawable> sPlanetDrawableList = new ArrayList<>();
    private static List<Integer> sAspectColorList = new ArrayList<>();
    private static Drawable sAscendentDrawable;
    private static Drawable sMediumCoeliDrawable;

    private Paint mPaint;
    private Canvas mCanvas = null;

    public Horoscope(Context context) {
        super(context);
        initialize();
    }

    public Horoscope(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize();
        getAttrs(attrs);
    }

    public Horoscope(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
        getAttrs(attrs, defStyleAttr);
    }

    private void initialize() {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        setWillNotDraw(false);
        setDrawingCacheEnabled(true);

        mAscendent = new Ascendent(null, null, null, null, null, 0.0, ConstellationData.ARIES);
        mMediumCoeli = new MediumCoeli(null, null, null, null, ConstellationData.CANCER, 0);
        mPlanets = new Planet[]{};
        mAspectList = new ArrayList<>();
        mPaint = initialPaint();
        mCanvas = new Canvas();
    }

    private Paint initialPaint() {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(5.0f);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);

        return paint;
    }

    public static void initialData(Context context) {
        for (int id : Planet.IDS)
            sPlanetDrawableList.add(context.getResources().getDrawable(id, null));

        for (int id : ConstellationData.CONSTELLATION_DRAWABLE_IDS)
            sConstellationDrawableList.add(context.getResources().getDrawable(id, null));

        for (int id : COLORS_ASPECT)
            sAspectColorList.add(ContextCompat.getColor(context, id));

        sAscendentDrawable = context.getResources().getDrawable(R.drawable.asc, null);
        sMediumCoeliDrawable = context.getResources().getDrawable(R.drawable.mc, null);
    }

    public void setHoroscopeModel(HoroscopeModel horoscopeModel) {
        setAscendent(horoscopeModel.getAscendent());
        setMediumCoeli(horoscopeModel.getMediumCoeli());
        setPlanets(horoscopeModel.getPlanets());
        setAspects(horoscopeModel.getAspectList());
    }

    public void setAscendent(Ascendent ascendent) {
        mAscendent = ascendent;
    }

    public void setMediumCoeli(MediumCoeli mediumCoeli) {
        mMediumCoeli = mediumCoeli;
    }

    public void setPlanets(Planet[] planets) {
        mPlanets = planets;
    }

    public void setHighlightPlanet(String highlightPlanetName) {
        mHighlightPlanetName = highlightPlanetName;
    }

    public void setSynastryHighlightPlanetNames(String[] synastryHighlightPlanetNames) {
        mSynastryHighlightPlanetNames = synastryHighlightPlanetNames;
    }

    public void setAspects(List<Aspect> aspects) {
        mAspectList = aspects;
    }

    public void setAngle(int angle) {
        mAngle = angle;
    }

    public void setStrokeColor(int color) {
        mPaint.setColor(color);
    }

    public void setStrokeWidth(float width) {
        mPaint.setStrokeWidth(width);
    }

    public void setPaintAntiAlias(boolean isAntiAlias) {
        mPaint.setAntiAlias(isAntiAlias);
    }

    public void setCanvas(Canvas canvas) {
        mCanvas = canvas;
    }

    public void refresh() {
        mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        invalidate();
    }

    /**
     * 천궁도를 그린다.
     */
    public void draw(int width, int height, int x, int y) {
        if(sConstellationDrawableList.size() == 0)
            return;

        int outerRadius = height * 7 / 10;
        int radius = height * 3 / 5;
        int smallRadius = (int)(radius * 3.5 / 5);
        int innerRadius = (int)(radius * 3 / 5);

        drawBenchmarkCircle(x, y, radius);
        drawConstellations(x, y, radius, smallRadius);
        drawBenchmark(x, y, outerRadius, radius);
        drawPlanets(x, y, outerRadius, radius);
        drawGraduations(x, y, smallRadius, innerRadius);
        drawAspects(x, y, innerRadius);
    }

    public void drawBenchmarkCircle(int width, int height, int radius) {
        mCanvas.drawCircle(width, height, radius, mPaint);
    }

    public void drawConstellations(int width, int height, int outerRadius, int innerRadius) {
        int imageHeight = outerRadius / 7;
        mCanvas.drawCircle(width, height, innerRadius, mPaint);

        int constellationIndex = ConstellationData.getIndex(mAscendent.getConstellation());
        for (int i = 180; i < 540; i += 30) {
            double angle = i + mAscendent.getLocation() + mAngle;
            double constellationX = getXOnCircle(angle - 15, (outerRadius + innerRadius) / 2, width);
            double constellationY = getYOnCircle(angle - 15, (outerRadius + innerRadius) / 2, height);

            drawLineOuterToInnerCircle(width, height, angle, outerRadius, innerRadius, true);

            Drawable drawable = sConstellationDrawableList.get(constellationIndex);
            drawDrawable(drawable, (int) constellationX, (int) constellationY, imageHeight);

            constellationIndex--;
            if (constellationIndex < 0)
                constellationIndex = ConstellationData.CONSTELLATIONS.length - 1;
        }
    }

    public void drawBenchmark(int width, int height, int outerRadius, int innerRadius) {
        Planet ascPlanet = new Planet(null, null, null, null, mAscendent.getLocation(), mAscendent.getConstellation(), Planet.ASC);
        Planet mcPlanet = new Planet(null, null, null, null, (double) mMediumCoeli.getLocation(), mMediumCoeli.getConstellation(), Planet.MC);

        drawPlanet(ascPlanet, width, height, outerRadius, innerRadius);
        drawPlanet(mcPlanet, width, height, outerRadius, innerRadius);
    }

    public void drawPlanets(int width, int height, int outerRadius, int innerRadius) {
        for (Planet planet : mPlanets) {
            if(Planet.getIndex(planet.getName()) < Planet.IDS.length)
                drawPlanet(planet, width, height, outerRadius, innerRadius);
        }
    }

    public void drawAspects(int width, int height, int radius) {
        for (Aspect aspect : mAspectList) {
            if(isDrawableAspect(aspect))
                drawAspect(aspect, width, height, radius, mSynastryHighlightPlanetNames != null);
        }
    }

    private void drawPlanet(Planet planet, int width, int height, int outerRadius, int innerRadius) {
        double ascAngle = 180 + mAngle;
        Planet ascPlanet = new Planet(null, null, null, null, mAscendent.getLocation(), mAscendent.getConstellation(), Planet.ASC);
        int angleDifference = ConstellationCalculator.getAngleDifference(new Planet[]{ascPlanet, planet});
        int imageHeight = outerRadius / 4;
        double planetX = getXOnCircle(ascAngle + angleDifference, outerRadius + imageHeight / 2, width);
        double planetY = getYOnCircle(ascAngle + angleDifference, outerRadius + imageHeight / 2, height);
        boolean isHighlight = mHighlightPlanetName == null || planet.getName().equals(mHighlightPlanetName);

        drawLineOuterToInnerCircle(width, height, ascAngle + angleDifference, outerRadius, innerRadius, isHighlight);

        Drawable drawable = null;
        int planetIndex = Planet.getIndex(planet.getName());
        if (planetIndex < Planet.IDS.length)
            drawable = sPlanetDrawableList.get(planetIndex);

        else if (planet.getName().equals(Planet.ASC))
            drawable = sAscendentDrawable;

        else if (planet.getName().equals(Planet.MC))
            drawable = sMediumCoeliDrawable;

        drawDrawable(drawable, (int) planetX, (int) planetY, imageHeight, isHighlight);
    }

    private boolean isDrawableAspect(Aspect aspect) {
        if(aspect.getName().equals(Aspect.SEMI_SQUARE))
            return false;

        for(Planet planet : aspect.getPlanets()) {
            if(Planet.getIndex(planet.getName()) >= Planet.IDS.length)
                return false;
        }

        return true;
    }

    public void drawGraduations(int width, int height, int outerRadius, int innerRadius) {
        int ascAngle = 180 + mAngle;

        int middleRadius = (outerRadius - innerRadius) * 2 / 5  + innerRadius;
        int smallRadius = (outerRadius - innerRadius) * 4 / 5 + innerRadius;

        for(int i = ascAngle; i < ascAngle + 360; i += 5) {
            if((i - mAngle) % 90 == 0)
                drawFillArc(width, height, outerRadius, middleRadius, i - 2.5, i + 2.5);

            if((i - mAngle) % 30 == 0)
                drawLineOuterToInnerCircle(width, height, i, outerRadius, innerRadius, true);
            else if ((i - mAngle) % 10 == 0)
                drawLineOuterToInnerCircle(width, height, i, outerRadius, middleRadius, true);
            else
                drawLineOuterToInnerCircle(width, height, i, outerRadius, smallRadius, true);
        }

        mCanvas.drawCircle(width, height, outerRadius, mPaint);
        mCanvas.drawCircle(width, height, innerRadius, mPaint);
    }

    public void drawAspect(Aspect aspect, int width, int height, int radius, boolean isSynastryAspect) {
        Planet[] planets = aspect.getPlanets();
        Planet ascPlanet = new Planet(null, null, null, null, mAscendent.getLocation(), mAscendent.getConstellation(), Planet.ASC);
        double ascAngle = 180 + mAngle;
        int startPlanetLocation = (int) (ConstellationCalculator.getAngleDifference(new Planet[]{ascPlanet, planets[0]}) + ascAngle);
        int endPlanetLocation = (int) (ConstellationCalculator.getAngleDifference(new Planet[]{ascPlanet, planets[1]}) + ascAngle);
        boolean isHighlight = mHighlightPlanetName == null || planets[0].getName().equals(mHighlightPlanetName) || planets[1].getName().equals(mHighlightPlanetName);
        if(isSynastryAspect)
            isHighlight = planets[0].getName().equals(mSynastryHighlightPlanetNames[0]) && planets[1].getName().equals(mSynastryHighlightPlanetNames[1]);

        drawLineOnCircle(width, height, radius, startPlanetLocation, endPlanetLocation, COLORS_ASPECT[Aspect.getIndex(aspect.getName())], isHighlight);
    }

    private void drawDrawable(Drawable drawable, int x, int y, int height, boolean isHighlight) {
        int width = height * drawable.getIntrinsicWidth() / drawable.getIntrinsicHeight();
        drawable.setBounds(x - width / 2, y - height / 2, x + width / 2, y + height / 2);
        drawable.setAlpha(isHighlight ? HIGH_ALPHA : LOW_ALPHA);
        drawable.draw(mCanvas);
    }

    private void drawDrawable(Drawable drawable, int x, int y, int height) {
        drawDrawable(drawable, x, y, height, true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mCanvas = canvas;
        draw(getWidth() / 2, getHeight() / 2, getWidth() / 2, getHeight() / 2);
    }

    private void drawFillArc(double centerX, double centerY, double radius, double innerRadius, double startAngle, double endAngle) {
        mPaint.setStyle(Paint.Style.FILL);

        double centerAngle = (startAngle + endAngle) / 2;
        RectF oval = new RectF((float)(centerX - radius), (float)(centerY - radius), (float)(centerX + radius), (float)(centerY + radius));

        Path path = new Path();
        path.moveTo((float)getXOnCircle(centerAngle, innerRadius, centerX), (float)getYOnCircle(centerAngle, innerRadius, centerY));
        path.lineTo((float)getXOnCircle(startAngle, radius, centerX), (float)getYOnCircle(startAngle, radius, centerY));
        path.lineTo((float)getXOnCircle(endAngle, radius, centerX), (float)getYOnCircle(endAngle, radius, centerY));
        path.lineTo((float)getXOnCircle(centerAngle, innerRadius, centerX), (float)getYOnCircle(centerAngle, innerRadius, centerY));
        path.arcTo(oval, (float)startAngle, (float)(endAngle - startAngle));

        mCanvas.drawPath(path, mPaint);
        mPaint.setStyle(Paint.Style.STROKE);
    }

    private void drawLineOnCircle(double centerX, double centerY, double radius, double startAngle, double endAngle) {
        mCanvas.drawLine((float) getXOnCircle(startAngle, radius, centerX)
                , (float) getYOnCircle(startAngle, radius, centerY)
                , (float) getXOnCircle(endAngle, radius, centerX)
                , (float) getYOnCircle(endAngle, radius, centerY)
                , mPaint);
    }

    private void drawLineOnCircle(double centerX, double centerY, double radius, double startAngle, double endAngle, int color, boolean isHighlight) {
        int originColor = mPaint.getColor();
        mPaint.setColor(ContextCompat.getColor(getContext(), color));
        mPaint.setAlpha(isHighlight ? HIGH_ALPHA : LOW_ALPHA);

        drawLineOnCircle(centerX, centerY, radius, startAngle, endAngle);

        mPaint.setColor(originColor);
        mPaint.setAlpha(HIGH_ALPHA);
    }

    private void drawLineOuterToInnerCircle(double centerX, double centerY, double angle, double outerRadius, double innerRadius, boolean isHighlight) {
        mPaint.setAlpha(isHighlight ? HIGH_ALPHA : LOW_ALPHA);
        mCanvas.drawLine((float) getXOnCircle(angle, outerRadius, centerX)
                , (float) getYOnCircle(angle, outerRadius, centerY)
                , (float) getXOnCircle(angle, innerRadius, centerX)
                , (float) getYOnCircle(angle, innerRadius, centerY)
                , mPaint);
        mPaint.setAlpha(HIGH_ALPHA);
    }

    /**
     * 원 위의 한 점의 X값을 얻는다.
     *
     * @param angle   원 위의 원하는 각도(0~360)
     * @param radius  원의 반지름
     * @param centerX 원의 중앙 X값
     * @return 원 위의 한 점 X값
     */
    private double getXOnCircle(double angle, double radius, double centerX) {
        return Math.cos(Math.toRadians(angle)) * radius + centerX;
    }

    /**
     * 원 위의 한 점의 Y값을 얻는다.
     *
     * @param angle   원 위의 원하는 각도(0~360)
     * @param radius  원의 반지름
     * @param centerY 원의 중앙 Y값
     * @return 원 위의 한 점 Y값
     */
    private double getYOnCircle(double angle, double radius, double centerY) {
        return Math.sin(Math.toRadians(angle)) * radius + centerY;
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Horoscope);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Horoscope, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        int strokeColor = typedArray.getColor(R.styleable.Horoscope_stroke_color, Color.WHITE);
        setStrokeColor(strokeColor);

        float strokeWidth = typedArray.getDimension(R.styleable.Horoscope_stroke_width, 1.0f);
        setStrokeWidth(strokeWidth);

        boolean isAntiAlias = typedArray.getBoolean(R.styleable.Horoscope_isAntiAlias, true);
        setPaintAntiAlias(isAntiAlias);

        int angle = typedArray.getInteger(R.styleable.Horoscope_angle, 0);
        setAngle(angle);

        typedArray.recycle();
    }
}
