package com.matching.goodgoodman.constellationdating;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.popup.adapter.ScaleCircleNavigator;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LandingPageActivity extends AppCompatActivity {

    @BindView(R.id.landingpage_page_view_pager) ViewPager mViewPager;
    @BindView(R.id.landingpage_page_button) ImageView mImageButton;
    @BindView(R.id.landingpage_page_button_layout) FrameLayout mFrameLayout;

    int[] mResources = {
            R.drawable.landingpage01,
            R.drawable.landingpage02,
            R.drawable.landingpage03,
            R.drawable.landingpage04,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = this.findViewById(android.R.id.content);
        setContentView(R.layout.activity_landing_page);
        ButterKnife.bind(this);

        initView();
        initMagicIndicator(view);
    }

    private void initView() {

        mViewPager.setAdapter(new pagerAdapter(this));
        mViewPager.setCurrentItem(0);

        Glide.with(this).load(R.drawable.landingpage_start).into(mImageButton);
        mFrameLayout.setVisibility(View.GONE);
        Intent intent = new Intent(this, LoginActivity.class);
        mFrameLayout.setOnClickListener(v -> {
            finish();
            startActivity(intent);
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                Log.v("instantiateItem", position + "");
                if(position == mResources.length -1 )
                    mFrameLayout.setVisibility(View.VISIBLE);
                else
                    mFrameLayout.setVisibility(View.GONE);
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    ScaleCircleNavigator.OnCircleClickListener ccl = new ScaleCircleNavigator.OnCircleClickListener() {
        @Override
        public void onClick(int index) {
            mViewPager.setCurrentItem(index);
        }
    };

    private void initMagicIndicator(View view) {
        MagicIndicator magicIndicator = (MagicIndicator) view.findViewById(R.id.landing_page_magic_indicator);

        ScaleCircleNavigator scaleCircleNavigator = new ScaleCircleNavigator(this);
        scaleCircleNavigator.setCircleCount(mResources.length);//탭의 수대로가 아닌, 페이지 수 대로 나오도록

        scaleCircleNavigator.setNormalCircleColor(ContextCompat.getColor(this, R.color.deep_gray));
        scaleCircleNavigator.setSelectedCircleColor(ContextCompat.getColor(this, R.color.sign_purple));
        scaleCircleNavigator.setCircleClickListener(ccl);

        magicIndicator.setNavigator(scaleCircleNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

    private class pagerAdapter extends PagerAdapter
    {
        Context mContext;
        LayoutInflater mLayoutInflater;

        public pagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return  view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.landing_page_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.landing_page_item_image);
            Glide.with(mContext).load(mResources[position]).into(imageView);

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }


    }
}
