package com.matching.goodgoodman.constellationdating;

/**
 * Created by Hyeong_Wook on 2017-09-14.
 */

public class Location {
    int mDegree;
    int mMinute;

    public Location(double location) {
        setDegree((int)location);
        setMinute((int)(Math.round((location - getDegree()) * 100)));
    }

    public int getDegree() {
        return mDegree;
    }

    public void setDegree(int degree) {
        mDegree = degree;
    }

    public int getMinute() {
        return mMinute;
    }

    public void setMinute(int minute) {
        mMinute = minute;

        if(minute >= 60 || minute < 0) {
            setDegree(getDegree() + minute / 60);
            mMinute %= 60;

            if(minute < 0) {
                setDegree(getDegree() - 1);
                mMinute = 60 + mMinute;
            }
        }
    }

    public double getDouble() {
        return getDegree() + getMinute() / 100.0;
    }

    public int getMinutesAll() {
        return getDegree() * 60 + getMinute();
    }

    /**
     * location 객체 2개를 더한다.
     * @param location1
     * @param location2
     * @return 더해진 값
     */
    public static Location sum(Location location1, Location location2) {
        Location result = new Location(0.0);
        result.setDegree(location1.getDegree() + location2.getDegree());
        result.setMinute(location1.getMinute() + location2.getMinute());
        return result;
    }

    /**
     * location 객체 2개를 뺸다.
     * 둘 중 아무것이나 큰 것에서 작은 것을 뺀다.
     * @param location1
     * @param location2
     * @return 뺀 값
     */
    public static Location subtraction(Location location1, Location location2) {
        Location highLocation = location1;
        Location lowLocation = location2;
        Location result = new Location(0.0);

        if(location1.getMinutesAll() < location2.getMinutesAll()) {
            highLocation = location2;
            lowLocation = location1;
        }

        result.setMinute(highLocation.getMinutesAll() - lowLocation.getMinutesAll());

        return result;
    }

    /**
     * location 객체 2개를 뺸다.
     * @param location1 기본 값
     * @param location2 빼질 값
     * @return 뺀 값
     */
    public static Location sub(Location location1, Location location2) {
        Location result = new Location(0.0);
        result.setMinute(location1.getMinutesAll() - location2.getMinutesAll());
        return result;
    }
}
