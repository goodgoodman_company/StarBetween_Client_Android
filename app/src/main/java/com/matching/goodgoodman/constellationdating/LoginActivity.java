package com.matching.goodgoodman.constellationdating;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.firebase.ChatAlarmService;
import com.matching.goodgoodman.constellationdating.utils.FacebookModule;
import com.matching.goodgoodman.constellationdating.utils.FileTransformHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements LoginView, DialogInterface.OnDismissListener {
    public static final int PERMISSION_ACCESS_FINE_LOCATION = 1;
    public static final int SETTING_LOCATION = 3;

    @BindView(R.id.activity_login_logo)
    ImageView mLoginLogo;
    @BindView(R.id.activity_login_button)
    View mLoginButton;
    @BindView(R.id.login_background)
    ImageView mBackground;
    @BindView(R.id.login_logo)
    FrameLayout mFrameLayout;
    @BindView(R.id.topLogo)
    ImageView mTopLogo;

    private LoginDialog loginDialog = null;
    private LoginPresenter mPresenter;
    private Intent mIntent;

    private boolean showDialogOnce = false;

    private SharedPreferences sharedpreferences;
    private Boolean startLandingPage;
    private static String SEE_LANDING_PAGE = "landingPage";
    private boolean mIsStateAlreadySaved;
    private boolean mPendingShowDialog;

    public LoginActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        sharedpreferences = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
        startLandingPage = sharedpreferences.getBoolean(SEE_LANDING_PAGE, false);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        if (!startLandingPage) {
            editor.putBoolean(SEE_LANDING_PAGE, true);
            editor.apply();
            finish();
            Intent intent = new Intent(this, LandingPageActivity.class);
            startActivity(intent);
        }


        mPresenter = new LoginPresenter(getApplicationContext(), this, getSupportFragmentManager());
        mPresenter.attachView(this);

        mIntent = getIntent();

        FileTransformHelper.copyDatabases(getApplicationContext());

        initView();
        setLoginButtonAction();
        mPresenter.loadInfo();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        mIntent = intent;
    }

    private void initView() {
        Glide.with(this).load(R.drawable.splashpage_logo).into(mLoginLogo);
        Glide.with(this).load(R.drawable.splashpage_background).into(mBackground);
        Glide.with(this).load(R.drawable.toplogo).into(mTopLogo);
    }

    @Override
    public void permissionCheck() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
                showPermissionSetting();
            else
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FacebookModule.getInstance().onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SETTING_LOCATION:
                mPresenter.login();
                return;
        }
    }

    private void setLoginButtonAction() {
        mLoginButton.setOnClickListener(v -> {
            mPresenter.startLogin();
            setLoginButtonVisibility(View.GONE);
        });

    }

    @Override
    public void setLoginButtonVisibility(int visibility) {
        mLoginButton.setVisibility(visibility);
    }

    @Override
    public void onResumeFragments(){
        super.onResumeFragments();
        mIsStateAlreadySaved = false;
        if(mPendingShowDialog){
            mPendingShowDialog = false;
            showDialog();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mIsStateAlreadySaved = true;
    }

    @Override
    public void showDialog() {
        if(mIsStateAlreadySaved){
            mPendingShowDialog = true;
        }else{
            if (!showDialogOnce) {
                showDialogOnce = true;
                mFrameLayout.setVisibility(View.VISIBLE);
                mLoginLogo.setVisibility(View.GONE);

                //Intent intent = new Intent(this, Register.class);
                //startActivity(intent);
                loginDialog = new LoginDialog();
                loginDialog.show(getSupportFragmentManager(), "show");
            }
        }
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        if (mPresenter != null)
            mPresenter.signUp();
    }

    @Override
    public void goMainApp() {
        finish();
        Intent intent = new Intent(this, MainActivity.class);

        if (mIntent != null) {
            boolean mustGoToChatList = mIntent.getBooleanExtra(ChatAlarmService.CHAT_NOTIFICATION, false);
            intent.putExtra(ChatAlarmService.CHAT_NOTIFICATION, mustGoToChatList);
        }

        startActivity(intent);
    }

    @Override
    public void showLocationSetting() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, SETTING_LOCATION);
    }

    private void showPermissionSetting() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, SETTING_LOCATION);

        Toast.makeText(getApplicationContext(), getResources().getString(R.string.checkLocation), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    mPresenter.login();
                else
                    permissionCheck();

                return;
        }
    }
}
