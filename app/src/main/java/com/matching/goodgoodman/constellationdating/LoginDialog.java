package com.matching.goodgoodman.constellationdating;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.utils.DateConverter;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dorol on 2017-09-19.
 */

public class LoginDialog extends DialogFragment {

    private int PAGESIZE = 6;
    private int NOWPAGE = 0;

    public static final String FORMAT_DATE = "yyyy-MM-dd HH:mm";
    public static final String FORMAT_DATE_STR = "%d-%d-%d %d:%d";

    @BindView(R.id.splashpage_view_pager) ViewPager mLoginDialogViewPager;
    @BindView(R.id.login_dialog_birth_back_button) ImageView mBackButton;

    private LoginDialogFragmentIntro mFragmentIntro;
    private LoginDialogFragmentBirthDay mFragmentBirthDay;
    private LoginDialogFragmentBirthTime mFragmentBirthTime;
    private LoginDialogFragmentSelfIntroduction mFragmentSelfIntroduction;
    private LoginDialogFragmentMap mFragmentMap;
    private LoginDialogFragmentThanks mFragmentThanks;

    private UserModel mUserModel;

    public LoginDialog() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.LoginDialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login_dialog, container, false);
        ButterKnife.bind(this, view);
        mUserModel = UserPreference.getInstance().getUserModel();
        setCancelable(false);

        initView(view);
        setViewListener(view);
        //initMagicIndicator(view);

        return view;
    }

    private void initView(final View view) {

        mFragmentIntro = new LoginDialogFragmentIntro();
        mFragmentBirthDay = new LoginDialogFragmentBirthDay();
        mFragmentBirthTime = new LoginDialogFragmentBirthTime();
        mFragmentSelfIntroduction = new LoginDialogFragmentSelfIntroduction();
        mFragmentMap = new LoginDialogFragmentMap();
        mFragmentThanks = new LoginDialogFragmentThanks();

        SplashLoginPagerAdapter pagerAdapter = new SplashLoginPagerAdapter(getChildFragmentManager());
        mLoginDialogViewPager.setAdapter(pagerAdapter);
        mLoginDialogViewPager.setCurrentItem(0);
        mLoginDialogViewPager.setOffscreenPageLimit(6);

        Glide.with(this).load(R.drawable.back).into(mBackButton);
        mBackButton.setVisibility(View.GONE);
    }

    private void setViewListener(final View view) {

        mBackButton.setOnClickListener(v -> {

            if(mLoginDialogViewPager.getCurrentItem() == 2)
                mBackButton.setVisibility(View.GONE);
            else
                mBackButton.setVisibility(View.VISIBLE);

            mFragmentSelfIntroduction.hideKeyBoard();
            mLoginDialogViewPager.setCurrentItem(NOWPAGE-1);
        });

        //summary 등의 바에서 해당 페이지가 됬을때
        mLoginDialogViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                NOWPAGE = position;
                Log.v(getClass().getSimpleName(), NOWPAGE + "");
                mLoginDialogViewPager.setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        mFragmentIntro.setLoginIntroButtonClick(new LoginDialogFragmentIntro.LoginIntroButtonClick() {
            @Override
            public void onLoginIntroButtonClick() {
                mLoginDialogViewPager.setCurrentItem(1);
                //Toast.makeText(getContext(), "0", Toast.LENGTH_SHORT).show();
            }
        });

        int[] birthday = new int[3];
        mFragmentBirthDay.setLoginBirthButtonClick(new LoginDialogFragmentBirthDay.LoginBirthButtonClick() {
            @Override
            public void onLoginBirthButtonClick() {
                mBackButton.setVisibility(View.VISIBLE);
                birthday[0] = mFragmentBirthDay.getBirthDay()[0];
                birthday[1] = mFragmentBirthDay.getBirthDay()[1];
                birthday[2] = mFragmentBirthDay.getBirthDay()[2];
                mLoginDialogViewPager.setCurrentItem(2);
            }
        });


        mFragmentBirthTime.setLoginBirthButtonClick(new LoginDialogFragmentBirthTime.LoginBirthButtonClick() {
            @Override
            public void onLoginBirthButtonClick() {

                String dateStr = String.format(FORMAT_DATE_STR,
                        birthday[0],
                        birthday[1],
                        birthday[2],
                        mFragmentBirthTime.getBirthTime()[0],
                        mFragmentBirthTime.getBirthTime()[1]
                );

                Date tempDate = DateConverter.strToDate(FORMAT_DATE, dateStr);
                Log.v("onLoginBirthButtonClick",tempDate.toString());
                mUserModel.setBirthday(tempDate);

                mLoginDialogViewPager.setCurrentItem(3);
            }
        });

        mFragmentMap.setLoginDialogMapButtonClick(new LoginDialogFragmentMap.LoginDialogMapButtonClick() {
            @Override
            public void onLoginDialogMapButtonClick() {
                mLoginDialogViewPager.setCurrentItem(4);
            }
        });

        mFragmentSelfIntroduction.setLoginGenderButtonClick(new LoginDialogFragmentSelfIntroduction.LoginGenderButtonClick() {
            @Override
            public void onLoginGenderButtonClick() {
                mUserModel.setContent(mFragmentSelfIntroduction.getContent());
                mLoginDialogViewPager.setCurrentItem(5);
            }
        });

        mFragmentThanks.setLoginThankButtonClick(new LoginDialogFragmentThanks.LoginThankButtonClick() {
            @Override
            public void onLoginThankButtonClick() {
                dismiss();
            }
        });

    }



    class SplashLoginPagerAdapter extends FragmentStatePagerAdapter {

        public SplashLoginPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    Log.v("tlqkf","tlqkf");
                    fragment = mFragmentIntro;
                    break;
                case 1:
                    fragment = mFragmentBirthDay;
                    break;
                case 2:
                    fragment = mFragmentBirthTime;
                    break;
                case 3:
                    fragment = mFragmentMap;
                    break;
                case 4:
                    fragment = mFragmentSelfIntroduction;
                    break;
                case 5:
                    fragment = mFragmentThanks;
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return PAGESIZE;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            if (position >= getCount()) {
                FragmentManager manager = ((Fragment) object).getFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove((Fragment) object);
                trans.commit();
            }
        }
    }


    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }

}
