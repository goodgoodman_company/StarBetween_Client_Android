package com.matching.goodgoodman.constellationdating;


import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.utils.DateConverter;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginDialogFragmentBirthDay extends Fragment implements DatePickerDialog.OnDateSetListener{

    private static final String FORMAT_DATE = "yyyy-MM-dd HH:mm";
    private static final String FORMAT_DATE_STR = "%d-%d-%d %d:%d";

    @BindView(R.id.fragment_login_dialog_birth_text) TextView mLoginDialogBirthText;
    @BindView(R.id.fragment_login_dialog_birth_button_timePicker) ImageView mLoginDialogBirthButtonDatePicker;
    @BindView(R.id.fragment_login_dialog_birth_text_timePicker) TextView mLoginDialogBirthTextDatePicker;
    @BindView(R.id.fragment_login_dialog_birth_button) ImageView mLoginDialogBirthButton;
    @BindView(R.id.fragment_login_dialog_birth_text_alert) TextView mAlertTextView;

    private int[] mUserBirthDay = new int[3];

    private SharedPreferences sharedpreferences;
    private DescriptionDBHelper mDescriptionDBHelper;

    public LoginDialogFragmentBirthDay() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedpreferences = getContext().getSharedPreferences("pref", getContext().MODE_PRIVATE);
        mDescriptionDBHelper = new DescriptionDBHelper(getContext(),sharedpreferences.getString("DESCRIPTION_LANGUAGE", DescriptionDBHelper.LANGUAGE_ENGLISH));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_dialog_birth, container, false);
        ButterKnife.bind(this, view);

        initView(view);
        initListener(view);

        mLoginDialogBirthButtonDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        LoginDialogFragmentBirthDay.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );

                dpd.setVersion(DatePickerDialog.Version.VERSION_1);

                dpd.setAccentColor(ContextCompat.getColor(getContext(), R.color.loginDialog_clock));

                dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Log.d("DatePicker", "DatePicker was cancelled");
                    }
                });

                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

            }
        });

        return view;
    }
    //TODO ::  숫자 바꿔야함.
    private void initView(View view) {
        mLoginDialogBirthText.setText(mDescriptionDBHelper.getDescription(301).getComment());
        mLoginDialogBirthTextDatePicker.setText(mDescriptionDBHelper.getDescription(304).getComment());
        mAlertTextView.setText(mDescriptionDBHelper.getDescription(302).getComment());

        Glide.with(this).load(R.drawable.login_dialog_enter).into(mLoginDialogBirthButtonDatePicker);
        Glide.with(this).load(R.drawable.login_dialog_confirm).into(mLoginDialogBirthButton);
        mLoginDialogBirthButton.setColorFilter(Color.GRAY, PorterDuff.Mode.LIGHTEN);
    }

    private void initListener(View view) {

        mLoginDialogBirthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginBirthButtonClick.onLoginBirthButtonClick();
            }
        });

        mLoginDialogBirthButton.setClickable(false);
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        mUserBirthDay[0] = year;
        mUserBirthDay[1] = monthOfYear + 1;
        mUserBirthDay[2] = dayOfMonth;

        mLoginDialogBirthTextDatePicker.setText(stringMonth(mUserBirthDay[1]) + " / " + mUserBirthDay[2] + " / " + mUserBirthDay[0]);

        int ageType = ageCheck(year, monthOfYear, dayOfMonth);
        if(ageType < 0) {
            mLoginDialogBirthButton.clearColorFilter();
            mLoginDialogBirthButton.setClickable(true);
        }
        else {
            dontAgeDialog(ageType);
        }
    }

    private int ageCheck(int year, int monthOfYear, int dayOfMonth) {
        int check = -1;

        long nowDateLong = System.currentTimeMillis();
        Date nowDate = new Date();
        nowDate.setTime(nowDateLong);

        String inputDateStr = String.format(FORMAT_DATE_STR,
                year,
                monthOfYear,
                dayOfMonth,
                0,
                0
        );
        Date inputDate = DateConverter.strToDate(FORMAT_DATE, inputDateStr);
        long inputDateLong = inputDate.getTime();

        long sec = 1000;
        long min = sec * 60;
        long hour = min * 60;
        long day = hour * 24;
        long limitDateLong = day * 365 * 18;


        Log.v("ageCheck", String.valueOf(nowDateLong - inputDateLong) + " / " + limitDateLong);
        if(year < 1960) {
            check = 0;
        }
        else if((nowDateLong - inputDateLong) < limitDateLong) {
            check = 1;
        }

        return check;
    }

    public LoginBirthButtonClick mLoginBirthButtonClick;
    public interface  LoginBirthButtonClick {
        void onLoginBirthButtonClick();
    }
    public void setLoginBirthButtonClick(LoginBirthButtonClick loginBirthButtonClick) {
        this.mLoginBirthButtonClick = loginBirthButtonClick;
    }

    public int[] getBirthDay() {
        return mUserBirthDay;
    }

    private String stringMonth(int month) {
        String stringMonth = "";
        switch(month) {
            case 1:
                stringMonth = "Jan";
                break;
            case 2:
                stringMonth = "Feb";
                break;
            case 3:
                stringMonth = "Mar";
                break;
            case 4:
                stringMonth = "Apr";
                break;
            case 5:
                stringMonth = "May";
                break;
            case 6:
                stringMonth = "Jun";
                break;
            case 7:
                stringMonth = "Jul";
                break;
            case 8:
                stringMonth = "Aug";
                break;
            case 9:
                stringMonth = "Sep";
                break;
            case 10:
                stringMonth = "Oct";
                break;
            case 11:
                stringMonth = "Nov";
                break;
            case 12:
                stringMonth = "Dec";
                break;
        }

        return stringMonth;
    }

    private void dontAgeDialog(int ageType) {
        int[] ageDialogMessage = {
                R.string.overAge,
                R.string.underAge
        };

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

        alertDialogBuilder.setMessage(ageDialogMessage[ageType]);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
