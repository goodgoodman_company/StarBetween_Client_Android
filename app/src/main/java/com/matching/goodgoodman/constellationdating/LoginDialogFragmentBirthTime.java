package com.matching.goodgoodman.constellationdating;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginDialogFragmentBirthTime extends Fragment implements TimePickerDialog.OnTimeSetListener {

    @BindView(R.id.fragment_login_dialog_birth_text) TextView mLoginDialogBirthText;
    @BindView(R.id.fragment_login_dialog_birth_button_timePicker) ImageView mLoginDialogBirthButtonTimePicker;
    @BindView(R.id.fragment_login_dialog_birth_text_timePicker) TextView mLoginDialogBirthTextTimePicker;
    @BindView(R.id.fragment_login_dialog_birth_button) ImageView mLoginDialogBirthButton;
    @BindView(R.id.fragment_login_dialog_birth_dontknowtime) TextView mDontKnowTimeTextView;
    @BindView(R.id.fragment_login_dialog_birth_text_alert) TextView mAlertTextView;

    private int[] mUserBirthTime = new int[2];
    private int[] mUserBirthDay = new int[3];

    private SharedPreferences sharedpreferences;
    private DescriptionDBHelper mDescriptionDBHelper;

    public LoginDialogFragmentBirthTime() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserBirthTime[0] = 0;
        mUserBirthTime[1] = 0;

        sharedpreferences = getContext().getSharedPreferences("pref", getContext().MODE_PRIVATE);
        mDescriptionDBHelper = new DescriptionDBHelper(getContext(),sharedpreferences.getString("DESCRIPTION_LANGUAGE", DescriptionDBHelper.LANGUAGE_ENGLISH));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_dialog_birth_time, container, false);
        ButterKnife.bind(this, view);

        initView(view);
        initListener(view);

        mLoginDialogBirthButtonTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now2 = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        LoginDialogFragmentBirthTime.this,
                        now2.get(Calendar.HOUR_OF_DAY),
                        now2.get(Calendar.MINUTE),
                        false
                );

                tpd.setVersion(TimePickerDialog.Version.VERSION_2);

                tpd.setAccentColor(ContextCompat.getColor(getContext(), R.color.loginDialog_clock));

                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Log.d("TimePicker", "TimePicker was cancelled");
                    }
                });

                tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
            }
        });

        return view;
    }


    private void initView(View view) {
        mLoginDialogBirthText.setText(mDescriptionDBHelper.getDescription(301).getComment());
        mLoginDialogBirthTextTimePicker.setText(mDescriptionDBHelper.getDescription(304).getComment());
        mAlertTextView.setText(mDescriptionDBHelper.getDescription(302).getComment());

        Glide.with(this).load(R.drawable.login_dialog_enter).into(mLoginDialogBirthButtonTimePicker);
        Glide.with(this).load(R.drawable.login_dialog_confirm).into(mLoginDialogBirthButton);
        mLoginDialogBirthButton.setColorFilter(Color.GRAY, PorterDuff.Mode.LIGHTEN);

        String dontKonwString = mDescriptionDBHelper.getDescription(303).getComment();
        SpannableString content = new SpannableString(dontKonwString);
        content.setSpan(new UnderlineSpan(), 0, dontKonwString.length(), 0);
        mDontKnowTimeTextView.setText(content);
    }

    private void initListener(View view) {

        mLoginDialogBirthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginBirthButtonClick.onLoginBirthButtonClick();
            }
        });
        mLoginDialogBirthButton.setClickable(false);

        mDontKnowTimeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDialog();
            }
        });
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        mUserBirthTime[0] = hourOfDay;
        mUserBirthTime[1] = minute;
        boolean isPM = hourOfDay >= 12;

        mLoginDialogBirthTextTimePicker.setText(String.format("%02d : %02d  %s", hourOfDay, minute, isPM? "PM" : "AM"));

        mLoginDialogBirthButton.clearColorFilter();
        mLoginDialogBirthButton.setClickable(true);
    }



    public LoginBirthButtonClick mLoginBirthButtonClick;
    public interface  LoginBirthButtonClick {
        void onLoginBirthButtonClick();
    }
    public void setLoginBirthButtonClick(LoginBirthButtonClick loginBirthButtonClick) {
        this.mLoginBirthButtonClick = loginBirthButtonClick;
    }

    public int[] getBirthTime() {
        return mUserBirthTime;
    }

    private void checkDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),R.style.AlertDialogTheme);

        TextView title = new TextView(getContext());
        title.setText(mDescriptionDBHelper.getDescription(306).getComment());
        title.setPadding(10, 30, 10, 20);
        title.setGravity(Gravity.CENTER);
        title.setTextSize(20);

        builder.setCustomTitle(title)
                .setMessage(mDescriptionDBHelper.getDescription(307).getComment())// 메세지 설정
                .setCancelable(false);        // 뒤로 버튼 클릭시 취소 가능 설정

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
            // 확인 버튼 클릭시 설정
            public void onClick(DialogInterface dialog, int whichButton){
                mUserBirthTime[0] = 0;
                mUserBirthTime[1] = 0;
                mLoginBirthButtonClick.onLoginBirthButtonClick();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener(){
            // 취소 버튼 클릭시 설정
            public void onClick(DialogInterface dialog, int whichButton){
                dialog.cancel();
            }
        });


        final AlertDialog alertDialog = builder.create();    // 알림창 객체 생성

        alertDialog.show();    // 알림창 띄우기

        final Button btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        final Button btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) btnPositive.getLayoutParams();
        LinearLayout.LayoutParams layoutParamsNegative = (LinearLayout.LayoutParams) btnNegative.getLayoutParams();
        layoutParams.weight = 10;
        layoutParamsNegative.weight = 10;
        btnPositive.setLayoutParams(layoutParams);
        btnPositive.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimaryDark));
        btnNegative.setLayoutParams(layoutParamsNegative);
        btnNegative.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimaryDark));

        TextView messageText = (TextView) alertDialog.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);

    }

}
