package com.matching.goodgoodman.constellationdating;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dorol on 2017-09-19.
 */

public class LoginDialogFragmentIntro extends Fragment {

    @BindView(R.id.fragment_login_dialog_intro_button) ImageView mLoginDialogIntroButton;
    @BindView(R.id.fragment_login_dialog_intro_frame) FrameLayout mLoginDialogIntroButtonLayout;
    @BindView(R.id.fragment_login_dialog_intro_text) TextView introTextView;
    @BindView(R.id.rotoRl) ImageView test;

    private SharedPreferences sharedpreferences;
    private DescriptionDBHelper mDescriptionDBHelper;

    public LoginIntroButtonClick mLoginIntroButtonClick;
    public interface  LoginIntroButtonClick {
        void onLoginIntroButtonClick();
    }
    public void setLoginIntroButtonClick(LoginIntroButtonClick loginIntroButtonClick) {
        this.mLoginIntroButtonClick = loginIntroButtonClick;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_dialog_intro, container, false);
        ButterKnife.bind(this, view);

        sharedpreferences = getContext().getSharedPreferences("pref", getContext().MODE_PRIVATE);
        mDescriptionDBHelper = new DescriptionDBHelper(getContext(),sharedpreferences.getString("DESCRIPTION_LANGUAGE", DescriptionDBHelper.LANGUAGE_ENGLISH));

        introTextView.setText(mDescriptionDBHelper.getDescription(300).getComment());

        Glide.with(this).load(R.drawable.login_dialog_confirm).into(mLoginDialogIntroButton);
        mLoginDialogIntroButton.setOnClickListener(v -> {
            introTextView.setVisibility(View.GONE);
            mLoginDialogIntroButtonLayout.setVisibility(View.GONE);
            mLoginDialogIntroButton.setVisibility(View.GONE);
            mLoginIntroButtonClick.onLoginIntroButtonClick();
            test.setVisibility(View.VISIBLE);
        });

        return view;
    }

}

