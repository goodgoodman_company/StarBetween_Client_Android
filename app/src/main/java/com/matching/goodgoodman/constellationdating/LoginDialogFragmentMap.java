package com.matching.goodgoodman.constellationdating;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginDialogFragmentMap extends Fragment {

    @BindView(R.id.fragment_login_dialog_map_textView) TextView mMapText;
    @BindView(R.id.fragment_login_dialog_map_button) ImageView mMapButton;
    @BindView(R.id.fragment_login_dialog_map_text) TextView mMapButtonText;
    @BindView(R.id.fragment_login_dialog_map_next_botton) ImageView mMapNextButton;
    @BindView(R.id.fragment_login_dialog_map_LinearLayout) LinearLayout mLayout;
    @BindView(R.id.fragment_login_dialog_map_text_alert) TextView mAlertTextView;

    private SharedPreferences sharedpreferences;
    private DescriptionDBHelper mDescriptionDBHelper;

    private UserModel mUserModel;

    private LoginDialogMapButtonClick mLoginDialogMapButtonClick;

    public interface LoginDialogMapButtonClick {
        void onLoginDialogMapButtonClick();
    }

    public void setLoginDialogMapButtonClick(LoginDialogMapButtonClick loginDialogMapButtonClick) {
        this.mLoginDialogMapButtonClick = loginDialogMapButtonClick;
    }


    public LoginDialogFragmentMap() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_dialog_fragment_map, container, false);
        ButterKnife.bind(this, view);

        sharedpreferences = getContext().getSharedPreferences("pref", getContext().MODE_PRIVATE);
        mDescriptionDBHelper = new DescriptionDBHelper(getContext(),sharedpreferences.getString("DESCRIPTION_LANGUAGE", DescriptionDBHelper.LANGUAGE_ENGLISH));
        mUserModel = UserPreference.getInstance().getUserModel();

        initView(view);
        initListener(view);

        return view;
    }

    private void initView(View view) {

        mMapText.setText(mDescriptionDBHelper.getDescription(308).getComment());
        mMapButtonText.setText(mDescriptionDBHelper.getDescription(309).getComment());
        mAlertTextView.setText(mDescriptionDBHelper.getDescription(310).getComment());

        Glide.with(this).load(R.drawable.login_dialog_enter).into(mMapButton);
        Glide.with(this).load(R.drawable.login_dialog_confirm).into(mMapNextButton);
        mMapNextButton.setColorFilter(Color.GRAY, PorterDuff.Mode.LIGHTEN);

    }

    @Override
    public void onResume() {
        super.onResume();

        if(GoogleMapsActivity.sIsUpdatedLocation) {
            mMapButtonText.setText("Latitude : " + String.valueOf(mUserModel.getLatitude())
                    + ", Longitude : " + String.valueOf(mUserModel.getLongitude()));
            mMapButtonText.setTextSize(15);

            mMapNextButton.clearColorFilter();
            mMapNextButton.setClickable(true);
        }
    }


    private void initListener(View view) {

        mMapButton.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), GoogleMapsActivity.class);
            startActivity(intent);
        });

        mMapNextButton.setOnClickListener(v -> mLoginDialogMapButtonClick.onLoginDialogMapButtonClick());
        mMapNextButton.setClickable(false);

    }

}
