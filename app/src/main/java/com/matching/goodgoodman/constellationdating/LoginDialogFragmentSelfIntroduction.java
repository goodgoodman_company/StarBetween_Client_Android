package com.matching.goodgoodman.constellationdating;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.utils.KeyboardController;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LoginDialogFragmentSelfIntroduction extends Fragment {

    private static final int LIMIT_CONTENT_TEXT = 200;

    @BindView(R.id.fragment_login_dialog_selfIntroduction_text) TextView mTextView;
    @BindView(R.id.fragment_login_dialog_selfIntroduction_content) ImageView mContentImage;
    @BindView(R.id.fragment_login_dialog_selfIntroduction_content_edit) EditText mContentEditText;
    @BindView(R.id.fragment_login_dialog_selfIntroduction_button) ImageView mButton;
    @BindView(R.id.fragment_login_dialog_selfIntroduction_screen) LinearLayout mScreenLayout;

    private String mContent = "";

    private SharedPreferences sharedpreferences;
    private DescriptionDBHelper mDescriptionDBHelper;

    public LoginDialogFragmentSelfIntroduction() {
    }

    public LoginGenderButtonClick mLoginGenderButtonClick;
    public interface  LoginGenderButtonClick {
        void onLoginGenderButtonClick();
    }
    public void setLoginGenderButtonClick(LoginGenderButtonClick loginGenderButtonClick) {
        this.mLoginGenderButtonClick = loginGenderButtonClick;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_dialog_selfintroduction, container, false);
        ButterKnife.bind(this, view);

        sharedpreferences = getContext().getSharedPreferences("pref", getContext().MODE_PRIVATE);
        mDescriptionDBHelper = new DescriptionDBHelper(getContext(),sharedpreferences.getString("DESCRIPTION_LANGUAGE", DescriptionDBHelper.LANGUAGE_ENGLISH));

        initView(view);
        initListener(view);

        return view;
    }

    private void initView(View view) {
        mTextView.setText(mDescriptionDBHelper.getDescription(311).getComment());

        Glide.with(this).load(R.drawable.login_dialog_enter_2).into(mContentImage);
        Glide.with(this).load(R.drawable.login_dialog_confirm).into(mButton);

        ViewTreeObserver vto = mContentImage.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                mContentImage.getViewTreeObserver().removeOnPreDrawListener(this);
                mContentEditText.setHeight(mContentImage.getMeasuredHeight());
                mContentEditText.setWidth(mContentImage.getMeasuredWidth());
                return true;
            }
        });


        mContentEditText.requestFocus();

        mContentEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(LIMIT_CONTENT_TEXT), new SpecialCharBlockFilter()});
    }

    private void initListener(View view) {
        mButton.setOnClickListener(v -> {
            mContent = mContentEditText.getText().toString();
            hideKeyBoard();
            mLoginGenderButtonClick.onLoginGenderButtonClick();
        });

        mScreenLayout.setOnClickListener(v -> hideKeyBoard());
    }

    public void hideKeyBoard() {
        KeyboardController.hideKeyboard(getActivity().getApplicationContext(), mContentEditText);
    }


    public String getContent() {
        return mContent;
    }

}
