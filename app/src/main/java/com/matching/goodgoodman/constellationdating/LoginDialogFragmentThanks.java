package com.matching.goodgoodman.constellationdating;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginDialogFragmentThanks extends Fragment {

    @BindView(R.id.fragment_login_dialog_thanks_button) ImageView mLoginDialogThanksButton;
    @BindView(R.id.fragment_login_dialog_thanks_text) TextView mLoginDialogThanksTextView;
    @BindView(R.id.login_last_image) ImageView mImageView;
    @BindView(R.id.login_last_button_layout) FrameLayout mButtonLayout;
    @BindView(R.id.login_last_page_button) ImageView mPageButton;

    private SharedPreferences sharedpreferences;
    private DescriptionDBHelper mDescriptionDBHelper;

    public LoginThankButtonClick mLoginThankButtonClick;
    public interface  LoginThankButtonClick {
        void onLoginThankButtonClick();
    }
    public void setLoginThankButtonClick(LoginThankButtonClick loginThanksButtonClick) {
        this.mLoginThankButtonClick = loginThanksButtonClick;
    }

    public LoginDialogFragmentThanks() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_dialog_fragment_thanks, container, false);
        ButterKnife.bind(this, view);

        sharedpreferences = getContext().getSharedPreferences("pref", getContext().MODE_PRIVATE);
        mDescriptionDBHelper = new DescriptionDBHelper(getContext(),sharedpreferences.getString("DESCRIPTION_LANGUAGE", DescriptionDBHelper.LANGUAGE_ENGLISH));

        //mLoginDialogThanksTextView.setText();
        Glide.with(this).load(R.drawable.login_dialog_confirm).into(mLoginDialogThanksButton);
        Glide.with(this).load(R.drawable.login_last_image).into(mImageView);
        Glide.with(this).load(R.drawable.landingpage_start).into(mPageButton);
        mButtonLayout.setVisibility(View.GONE);
        mImageView.setVisibility(View.GONE);


        initListener(view);

        return view;
    }

    private void initListener(View view) {

        mLoginDialogThanksButton.setOnClickListener(v -> {
            mImageView.setVisibility(View.VISIBLE);
            mButtonLayout.setClickable(true);
            mButtonLayout.setVisibility(View.VISIBLE);
        });

        mButtonLayout.setOnClickListener(v -> mLoginThankButtonClick.onLoginThankButtonClick());
        mButtonLayout.setClickable(false);
    }

}
