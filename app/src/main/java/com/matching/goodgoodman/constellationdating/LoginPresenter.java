package com.matching.goodgoodman.constellationdating;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.NetworkManager;
import com.matching.goodgoodman.constellationdating.server.SuccessCallBack;
import com.matching.goodgoodman.constellationdating.server.model.ResultStatus;
import com.matching.goodgoodman.constellationdating.utils.ConstellationCalculator;
import com.matching.goodgoodman.constellationdating.utils.DateConverter;
import com.matching.goodgoodman.constellationdating.utils.DeviceStatusHelper;
import com.matching.goodgoodman.constellationdating.utils.FacebookModule;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dorol on 2017-09-17.
 */

public class LoginPresenter implements Presenter<LoginView> {
    private static final int MIN_TIMES_UPDATES = 1000 * 60 * 1;
    private static final int MIN_DISTANCE_UPDATES = 10;

    private static final String NONE_PROVIDER = "NONE";
    private static final int NONE_LATITUDE = 900;
    private static final int NONE_LONGITUDE = 900;

    private Context mContext;
    private LoginView mView;
    private UserModel mUserModel;
    private NetworkManager mNetrowkManager;

    public LoginPresenter(Context context, LoginView view, FragmentManager fragmentManager) {
        mContext = context;
        mUserModel = UserPreference.getInstance().getUserModel();
        mNetrowkManager = NetworkManager.getInstance(context);
        attachView(view);
    }

    /**
     * 페이스북 관련 정보를 받아온다.
     */
    public void startLogin() {
        loginFacebook(result -> login());
    }

    /**
     * 페이스북 관련 정보를 받아온다.
     */
    public void loadInfo() {
        if (FacebookModule.getInstance().isLogin()) {
            startLogin();
            mView.setLoginButtonVisibility(View.GONE);
        }
    }

    /**
     * 페이스북에 로그인을 한다.
     */
    public void loginFacebook(SuccessCallBack<UserModel> successCallBack) {
        Activity activity = null;
        if (mView instanceof Activity)
            activity = (Activity) mView;
        else
            return;

        final FacebookModule facebookModule = FacebookModule.getInstance();
        facebookModule.setLoginArea(activity);
        facebookModule.login(new FacebookCallback<AccessToken>() {
            @Override
            public void onSuccess(AccessToken accessToken) {
                facebookModule.getUserInfo(accessToken, result -> {
                    Log.d("facebook userId", accessToken.getUserId());
                    mUserModel.setId(accessToken.getUserId());
                    mUserModel.setFaceBookToken(accessToken.getToken());
                    mUserModel.setName(result.getName());
                    mUserModel.setGender(result.getGender());
                    mUserModel.setEmail(result.getEmail());
                    successCallBack.onCallBack(mUserModel);
                });
            }

            @Override
            public void onCancel() {
                Log.d("facebook login", "cancel");
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
            }
        });


    }

    /**
     * 어플에 로그인 한다.
     * 회원가입이 되어있으면 메인 어플로 이동하고,
     * 안되어있으면 페이스북 로그인하고 다이얼 로그로 이동해서 장소 찍음 -> 그 후 로그인을 한다.
     */
    //TODO : setUseFreePointYn, setUseStarPointYn, setGrade  설정해 줘야함 -> null 들어있어서 바꿈
    //TODO : gps에 더미 들어가있음 (매칭이 안되서), 회원가입후 어플이동이 안되서 더미 있음
    public void login() {
        setLocation(getLocationResult -> {
            mUserModel.setCurrentLatitude((float) getLocationResult.getLatitude());
            mUserModel.setCurrentLongitude((float) getLocationResult.getLongitude());

            //mUserModel.setCurrentLatitude((float) 48.9512);
            //mUserModel.setCurrentLongitude((float) 2.4141);

            //userDataTestDummy();

            mNetrowkManager.getLogin(mUserModel, loginResult -> {
                if (loginResult.getResult() != null && loginResult.getResult().equals("fail")) {
                    mView.showDialog();
                } else {
                    mUserModel.setNo(loginResult.getUser().getNo());
                    mUserModel.setLatitude(loginResult.getUser().getLatitude());
                    mUserModel.setLongitude(loginResult.getUser().getLongitude());
                    mUserModel.setContent(loginResult.getUser().getContent());
                    mUserModel.setBirthday(loginResult.getUser().getBirthday());
                    mUserModel.setFreePoint(loginResult.getPoint().getFreePoint());
                    mUserModel.setStarPoint(loginResult.getPoint().getStarPoint());
                    mUserModel.setAttendancePoint(loginResult.getPoint().getAttendancePoint());
                    mUserModel.setPremiumYn(loginResult.getUser().getPremiumYn());
                    mUserModel.setMatchingUpdateYn(loginResult.getUser().getMatchingUpdateYn());
                    mUserModel.setTodayFirst(loginResult.getUser().getTodayFirst());
                    mUserModel.setLastConTime(loginResult.getUser().getLastConTime());
                    mUserModel.setImageOpenYn(loginResult.getUser().getImageOpenYn());

                    if (mUserModel.getCurrentLatitude() == NONE_LATITUDE) {
                        mUserModel.setCurrentLatitude(mUserModel.getLatitude());
                        mUserModel.setCurrentLongitude(mUserModel.getLongitude());
                    }
                    Log.d("mUserModel.getId", mUserModel.getId());
                    UserPreference.getInstance().getCounterUserModelList().clear();

                    for (int i = 0; i < loginResult.getMatchingList().size(); ++i) {
                        UserModel counterUserModel = new UserModel();
                        counterUserModel.setNo(loginResult.getMatchingList().get(i).getNo());
                        counterUserModel.setId(loginResult.getMatchingList().get(i).getId());
                        counterUserModel.setBirthday(loginResult.getMatchingList().get(i).getBirthday());
                        counterUserModel.setMatchingNo(loginResult.getMatchingList().get(i).getMatchingNo());
                        counterUserModel.setUseBlurYn(loginResult.getMatchingList().get(i).getUseBlurYn());
                        counterUserModel.setUseLikeYn(loginResult.getMatchingList().get(i).getUseLikeYn());
                        counterUserModel.setFaceBookToken(loginResult.getMatchingList().get(i).getToken());

                        UserPreference.getInstance().addCounterUserModel(counterUserModel);
                    }
                    loadConstellationCalculateResult(mUserModel);
                }
            }, (call, t) -> mView.showDialog());
        });
    }

    //TODO : setUseFreePointYn, setUseStarPointYn, setGrade  설정해 줘야함 -> null 들어있어서 바꿈
    public void signUp() {
        //userDataTestDummy();
        mNetrowkManager.getSignUp(mUserModel, result -> {
            if (result.getResult().equals(ResultStatus.SUCCESS))
                login();
        });
    }

    private void setLocation(SuccessCallBack<Location> successCallBack) {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mView.permissionCheck();
            return;
        } else if (!DeviceStatusHelper.isEnabledGPS(mContext)) {
            mView.showLocationSetting();
            return;
        }

        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        Location resultLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (resultLocation == null) {
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (isNetworkEnabled) {
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                locationManager.requestSingleUpdate(criteria, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        Log.i("location", location.getLatitude() + " " + location.getLongitude());
                        successCallBack.onCallBack(location);
                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {
                    }

                    @Override
                    public void onProviderEnabled(String s) {
                    }

                    @Override
                    public void onProviderDisabled(String s) {
                    }
                }, null);
            } else {
                resultLocation = new Location(NONE_PROVIDER);
                resultLocation.setLatitude(NONE_LATITUDE);
                resultLocation.setLongitude(NONE_LONGITUDE);
                successCallBack.onCallBack(resultLocation);
                return;
            }
        } else {
            Log.i("location", resultLocation.getLatitude() + " " + resultLocation.getLongitude());
            successCallBack.onCallBack(resultLocation);
        }
    }

    public void loadConstellationCalculateResult(UserModel userModel) {
        int[] birthday = DateConverter.dateToDividedTime(userModel.getBirthday());
        ConstellationCalculator.getHoroscopeModel(mContext
                , birthday[0], birthday[1], birthday[2], birthday[3], birthday[4]
                , userModel.getLatitude(), userModel.getLongitude()
                , result -> {
                    UserPreference.getInstance().setHoroscopeModel(result);
                    loadHoroscopeDatas();
                });
    }

    public void loadHoroscopeDatas() {
        new AsyncTask<Object, Object, Object>() {
            @Override
            protected Object doInBackground(Object... objects) {
                Horoscope.initialData(mContext);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                mView.goMainApp();
            }
        }.execute();
    }

    //TODO : 테스트를 위한 일시적 더미값 지정 나중에 지울것(userId, userToken, Email, Name, Gender, BirthDay)
    //TODO : latitude와 longitude는 dialog에서 지정해 주지만, matching을 위해 지정값을 넘김
    public void userDataTestDummy() {
        mUserModel.setNo(0);
        mUserModel.setId("qwertest54");
        mUserModel.setFaceBookToken("test");
        mUserModel.setEmail("cucumori219@naver.com");
        mUserModel.setName("Hufstory");
        mUserModel.setGender("F");

        mUserModel.setLatitude((float) 48.9512);
        mUserModel.setLongitude((float) 2.4141);

        DateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date tempDate = null;
        try {
            tempDate = sdFormat.parse("1990-10-15 09:27:22.376");
            Log.v("userDataTestDummy", tempDate + "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mUserModel.setBirthday(tempDate);
    }

    @Override
    public void attachView(LoginView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }

}
