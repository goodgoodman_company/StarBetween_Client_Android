package com.matching.goodgoodman.constellationdating;

/**
 * Created by dorol on 2017-09-17.
 */

public interface LoginView{
    void setLoginButtonVisibility(int visibility);
    void showDialog();
    void goMainApp();
    void showLocationSetting();
    void permissionCheck();
}
