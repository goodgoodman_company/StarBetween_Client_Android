package com.matching.goodgoodman.constellationdating;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.firebase.ChatAlarmService;
import com.matching.goodgoodman.constellationdating.firebase.RestartService;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.NetworkManager;
import com.matching.goodgoodman.constellationdating.server.model.ResultStatus;
import com.matching.goodgoodman.constellationdating.utils.ChattingListRefreshHandler;
import com.matching.goodgoodman.constellationdating.utils.StarPointChangeHandler;
import com.matching.goodgoodman.constellationdating.utils.ViewMover;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    // 탭 및 페이저 속성 정의
    final int TAB_COUNT = 4;
    // 현재 페이지
    private int page_position = 0;
    // 페이지 이동경로를 저장하는 stack 변수
    //Stack<Integer> pageStack = new Stack<>();
    //boolean backPress = false;
    private boolean doubleBackToExitPressedOnce = false;

    private float mTabLayoutMinY = -1.0f;
    private float mTabLayoutMaxY = -1.0f;

    @BindView(R.id.backGround) ImageView backGround;
    @BindView(R.id.tab) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;
    MainFragment main;
    MatchingFragment mMatchingFragment;
    ChattingListFragment mChattingListFragment;
    SettingFragment mSettingFragment;
    @BindView(R.id.topLogo) ImageView mainActivityLogo;
    @BindView(R.id.starPointTextView) TextView mStarPointTextView;
    @BindView(R.id.starPointImage) ImageView mStarPointImage;

    UserModel userModel;

    private RestartService mRestartService;

    //탭 레이아웃 이미지 추가
    private int[] tabIcons = {
            R.mipmap.main,
            R.mipmap.list,
            R.mipmap.talk,
            R.mipmap.option
    };

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //pageStack.push(page_position);
        ButterKnife.bind(this);
        userModel = UserPreference.getInstance().getUserModel();
        StarPointChangeHandler.getInstance().addChangeListener(this::showStarPoint);
        ChattingListRefreshHandler.getInstance().addChangeListener(this::refreshChattingList);

        main = new MainFragment();
        mMatchingFragment = new MatchingFragment();
        mChattingListFragment = new ChattingListFragment();
        mSettingFragment = new SettingFragment();

        initialService();
        initView();

        // - 탭 Layout 정의
        //final TabLayout tabLayout = (TabLayout) findViewById(R.id.tab);
        // 탭 생성 및 타이틀 입력
        tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.main));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.list));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.talk));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.mipmap.option));
        tabLayout.bringToFront();

        ImageView imageView0 = (ImageView) findViewById(R.id.tabLayout_icon_main);
        imageView0.setImageResource(tabIcons[0]);
        tabLayout.getTabAt(0).setCustomView(imageView0);
        //tabLayout.addTab( tabLayout.newTab().setCustomView(imageView0));

        ImageView imageView1 = (ImageView) findViewById(R.id.tabLayout_icon_list);
        imageView1.setImageResource(tabIcons[1]);
        tabLayout.getTabAt(1).setCustomView(imageView1);
        //tabLayout.addTab( tabLayout.newTab().setCustomView(imageView1));
        tabLayout.getTabAt(1).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);

        ImageView imageView2 = (ImageView) findViewById(R.id.tabLayout_icon_talk);
        imageView2.setImageResource(tabIcons[2]);
        tabLayout.getTabAt(2).setCustomView(imageView2);
        //tabLayout.addTab( tabLayout.newTab().setCustomView(imageView2));
        tabLayout.getTabAt(2).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);

        ImageView imageView3 = (ImageView) findViewById(R.id.tabLayout_icon_option);
        imageView3.setImageResource(tabIcons[3]);
        tabLayout.getTabAt(3).setCustomView(imageView3);
        //tabLayout.addTab( tabLayout.newTab().setCustomView(imageView3));
        tabLayout.getTabAt(3).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);

        // 아답터 생성
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);

        // 1. 페이저 리스너 : 페이저가 변경되었을때 탭을 바꿔주는 리스너
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        // 2. 페이지의 변경사항을 체크한다.
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                tabLayout.getTabAt(page_position).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                page_position = position;
                Log.v("page position : ", "" + page_position);
                tabLayout.getTabAt(page_position).getIcon().clearColorFilter();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //Log.v(this.getClass().getSimpleName(), "onPageScrollStateChanged()");
            }
        });

        // 3. 탭 리스너 : 탭이 변경되었을 때 페이지를 바꿔저는 리스너
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {

                                               @Override
                                               public void onTabSelected(TabLayout.Tab tab) {
                                                   super.onTabSelected(tab);
                                                   tab.getIcon().clearColorFilter();
                                               }

                                               @Override
                                               public void onTabUnselected(TabLayout.Tab tab) {
                                                   super.onTabUnselected(tab);
                                                   //int tabIconColor = ContextCompat.getColor(context, R.color.tabUnselectedIconColor);
                                                   tab.getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                                               }

                                               @Override
                                               public void onTabReselected(TabLayout.Tab tab) {
                                                   super.onTabReselected(tab);
                                                   //Log.v(this.getClass().getSimpleName(), "onTabReselected()");
                                               }
                                           }
        );

        mStarPointTextView.setOnClickListener(view -> new StarShopDialogFragment().show(getSupportFragmentManager(), null));

        checkChatNotification(getIntent());
        checkDailyBonus();
    }

    private void initView() {
        //글라이드를 이용한 배경
        //backGround = (ImageView)findViewById(R.id.backGround);
        Glide.with(this).load(R.drawable.background).into(backGround);
        Glide.with(this).load(R.drawable.toplogo).into(mainActivityLogo);

        Glide.with(this).load(R.drawable.stars).into(mStarPointImage);

        showStarPoint(userModel.getStarPoint());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        checkChatNotification(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mRestartService);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case ChattingListFragment.CHAT_LIST_NEED_REFRESH:
                mChattingListFragment.refresh();
                break;
        }
    }

    private void initialService() {
        mRestartService = new RestartService();
        IntentFilter intentFilter = new IntentFilter(".firebase.ChatAlarmService");
        registerReceiver(mRestartService, intentFilter);

        Intent intent = new Intent(this, ChatAlarmService.class);
        startService(intent);
    }

    private void checkChatNotification(Intent intent) {
        boolean mustGoToChatList = intent.getBooleanExtra(ChatAlarmService.CHAT_NOTIFICATION, false);
        if(mustGoToChatList)
            viewPager.setCurrentItem(2);
    }

    private void checkDailyBonus() {
        UserModel userModel = UserPreference.getInstance().getUserModel();
        if(userModel.getTodayFirst() != null && userModel.getTodayFirst().equals(UserModel.YES)) {
            NetworkManager.getInstance(getApplicationContext()).putTodayFirst(userModel.getNo(), result -> {
                if(result.getResult().equals(ResultStatus.SUCCESS))
                    new AttendancePopupFragment().show(getSupportFragmentManager(), null);
            });
        }
    }

    //TODO::starPoint를 view에 표시해주는 부분입니다. starPointUI가 진행되면 Toast를 삭제하고 진행한다.
    private void showStarPoint(int starPoint) {
        mStarPointTextView.setText(String.valueOf(starPoint));
        //Toast.makeText(getApplicationContext(), starPoint + "개", Toast.LENGTH_LONG).show();
    }

    private void refreshChattingList(boolean needRefresh) {
        if(needRefresh)
            mChattingListFragment.refresh();
    }

    class PagerAdapter extends FragmentStatePagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = main;
                    break;
                case 1:
                    fragment = mMatchingFragment;
                    break;
                case 2:
                    fragment = mChattingListFragment;
                    break;
                case 3:
                    fragment = mSettingFragment;
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (mTabLayoutMinY == -1.0f) {
            mTabLayoutMinY = tabLayout.getY();
            mTabLayoutMaxY = tabLayout.getY() + tabLayout.getHeight();
        }
        ViewMover.moveVerticalWithScrollMotion(tabLayout, motionEvent, mTabLayoutMinY, mTabLayoutMaxY);

        return super.dispatchTouchEvent(motionEvent);
    }

    @Override
    public void onBackPressed() {
        goBackStack();
    }

    // stack 뒤로가기
    private void goBackStack() {
        if (page_position == 0) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getResources().getText(R.string.exit_the_app), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            viewPager.setCurrentItem(0);
        }
    }
}
