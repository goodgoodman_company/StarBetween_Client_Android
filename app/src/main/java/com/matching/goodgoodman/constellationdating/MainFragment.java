package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewSwitcher;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.model.DrawableData;
import com.matching.goodgoodman.constellationdating.popup.SlidePopUpDialog;
import com.matching.goodgoodman.constellationdating.utils.FacebookModule;
import com.matching.goodgoodman.constellationdating.utils.KeyboardController;
import com.matching.goodgoodman.constellationdating.utils.ViewMover;
import com.pnikosis.materialishprogress.ProgressWheel;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dao.Zodiac;


/**
 * 메인 페이지의 UI를 나타내는 클래스
 * MainView 인터페이스를 상속받아 구현한다.
 * @author Hyeong_Wook
 */
public class MainFragment extends Fragment implements MainView {
    private static final int LIMIT_CONTENT_TEXT = 200;
    //private OnFragmentInteractionListener mListener;
    //LayoutInflater mInflater;

    //UserModel mUserModel;
    MainFragmentPresenter mPresenter;
    Context mContext;
    boolean mContentEditMode = false;

    @BindView(R.id.fragment_main_userName) TextView mUserNameText;
    @BindView(R.id.fragment_main_age) TextView mUserAgeText;
    @BindView(R.id.fragment_main_content) TextView mContentText;
    @BindView(R.id.fragment_main_content_edit) EditText mContentEditText;
    @BindView(R.id.fragment_main_constellation) ImageView mConstellationImage;
    @BindView(R.id.photo_frame) FrameLayout mPhotoFrame;
    @BindView(R.id.fragment_main_floatingActionButton) FloatingActionButton mShowDialogButton;
    @BindView(R.id.textEditButton) CheckBox mContentEditButton;
    @BindView(R.id.floatingLayout) LinearLayout mFloatingLayout;
    @BindView(R.id.contentSwitcher) ViewSwitcher mViewSwitcher;
    @BindView(R.id.linearLayout) LinearLayout mScreenLayout;
    @BindView(R.id.photo_viewpager) ViewPager mViewPager;
    @BindView(R.id.pictureOpen) ToggleButton mPictureToggleButton;
    @BindView(R.id.shareText) TextView mPictureToggleText;
    @BindView(R.id.indicator) MagicIndicator mIndicator;
    @BindView(R.id.starSignSymbol) NatalSymbol mStarSignSymbol;
    @BindView(R.id.planetSymbol) NatalSymbol mPlanetSymbol;
    @BindView(R.id.elementalSymbol) NatalSymbol mElementalSymbol;
    @BindView(R.id.colorSymbol) NatalSymbol mColorSymbol;
    @BindView(R.id.progress_wheel)
    ProgressWheel mProgressWheel;

    private float mFloatingLayoutMinY = -1.0f;
    private float mFloatingLayoutMaxY = -1.0f;

    private boolean dialogAnimProcess = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPresenter = new MainFragmentPresenter(context, this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPresenter.detachView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //mInflater = inflater;

        View v = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, v);
        mContext = getContext();
        initialView();
        initialListeners(v);

        mPresenter.loadPictures();
        mPresenter.loadUserInfo();
        mPresenter.loadNatalSymbolList(getContext().getApplicationContext());

        return v;
    }

    private void initialView() {
        mPictureToggleButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.facebook_toggle, null));
        mPictureToggleButton.setTextOff("");
        mPictureToggleButton.setTextOn("");
    }

    private void initialListeners(View v) {
        TouchEventLinearLayout linearLayout = (TouchEventLinearLayout) v.findViewById(R.id.touchEventLinearLayout);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(mFloatingLayoutMinY == -1.0f) {
                    mFloatingLayoutMinY = mFloatingLayout.getY() - mFloatingLayout.getHeight();
                    mFloatingLayoutMaxY = mFloatingLayout.getY();
                }
                ViewMover.moveVerticalWithScrollMotion(mFloatingLayout, motionEvent, mFloatingLayoutMinY, mFloatingLayoutMaxY, true);
                return true;
            }
        });

        mScreenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContentEditButton.setChecked(false);
            }
        });

        Glide.with(this).load(R.mipmap.more).into(mShowDialogButton);
        //mShowDialogButton.setImageResource(R.mipmap.more);
        mShowDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RotateAnimation rotateDialogButton = new RotateAnimation(0, 45, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotateDialogButton.setDuration(300);
                rotateDialogButton.setRepeatCount(Animation.ABSOLUTE);
                rotateDialogButton.setFillAfter(true);
                mShowDialogButton.startAnimation(rotateDialogButton);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        showInfoDialog();
                    }
                }, 300);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mShowDialogButton.clearAnimation();
                    }
                }, 600);
            }
        });
        mContentEditButton.setButtonDrawable(getResources().getDrawable(R.drawable.content_edit_button, null));
        mContentEditButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                showContent(isChecked);
            }
        });

        mContentEditText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(LIMIT_CONTENT_TEXT), new SpecialCharBlockFilter()});

        mPictureToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProgressWheel.setVisibility(View.VISIBLE);
                mPresenter.setPhotosOpen(mPictureToggleButton.isChecked());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mContentEditButton.setChecked(false);
    }

    /**
     * user의 기본정보를 UI에 표시한다.
     * @param userName user의 이름
     * @param userContent user의 content
     */
    @Override
    public void showBasicInformation(String userName, String userContent, String age) {

        //Log.v("showBasicInformation", userName + " / " + userContent);
        mUserNameText.setText(userName);
        mContentText.setText(userContent);
        mUserAgeText.setText(age);
    }

    @Override
    public void showProfile(Bitmap photo) {
        mConstellationImage.setImageBitmap(photo);
    }

    @Override
    public void showPhotos(List<Bitmap> photoList, boolean isOpen) {
        showPictureToggleText(isOpen);
        mPictureToggleButton.setChecked(isOpen);
        mProgressWheel.setVisibility(View.GONE);
        if(!isOpen) {
            mPhotoFrame.setVisibility(View.GONE);
            return;
        }

        mPhotoFrame.setVisibility(View.VISIBLE);
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        //View photosLayout = layoutInflater.inflate(R.layout.photo_viewpager, null);

        //ViewPager viewPager = (ViewPager) photosLayout.findViewById(R.id.photo_viewpager);
        mViewPager.setAdapter(new PhotoPagerAdapter(photoList, layoutInflater));

        //MagicIndicator indicator = (MagicIndicator) photosLayout.findViewById(R.id.indicator);

        ScaleCircleNavigator scaleCircleNavigator = new ScaleCircleNavigator(getContext());
        scaleCircleNavigator.setCircleCount(mViewPager.getAdapter().getCount());

        scaleCircleNavigator.setNormalCircleColor(ContextCompat.getColor(getContext(), R.color.deep_gray));
        scaleCircleNavigator.setSelectedCircleColor(ContextCompat.getColor(getContext(), R.color.sign_purple));

        mIndicator.setNavigator(scaleCircleNavigator);
        ViewPagerHelper.bind(mIndicator, mViewPager);

        //mPhotoFrame.removeAllViewsInLayout();
        //mPhotoFrame.addView(photosLayout);
    }

    private void showPictureToggleText(boolean isOpen) {
        if(isOpen)
            mPictureToggleText.setText(getResources().getString(R.string.facebook_dont_share));
        else
            mPictureToggleText.setText(getResources().getString(R.string.facebook_share));
    }

    @Override
    public void showContentText(String content) {
        mContentText.setText(mContentEditText.getText());
    }

    @Override
    public void showNatalSymbolList(Zodiac zodiac) {
        int index = zodiac.getMonth() - 1;

        mStarSignSymbol.setSymbol(DrawableData.ID_ZODIACS[index]);
        mPlanetSymbol.setSymbol(DrawableData.ID_PLANETS[index]);
        mElementalSymbol.setSymbol(DrawableData.ID_ELEMENTS[index]);
        mColorSymbol.setSymbol(DrawableData.ID_COLORS[index]);

        mStarSignSymbol.setSymbolText(zodiac.getConstellationName().replace("\\n", "\n"));
        mPlanetSymbol.setSymbolText(zodiac.getPlanetName().replace("\\n", "\n"));
        mElementalSymbol.setSymbolText(zodiac.getElement().replace("\\n", "\n"));
        mColorSymbol.setSymbolText(zodiac.getColor().replace("\\n", "\n"));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FacebookModule.getInstance().onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 에디트모드인지, 아닌지에 따라 콘텐츠를 보여준다.
     * @param isEditMode
     */
    public void showContent(boolean isEditMode) {
        if(isEditMode) {
            mViewSwitcher.showNext();
            mContentEditText.setText(mContentText.getText());//원래 있던 글을 담음
            KeyboardController.showKeyboard(getActivity().getApplicationContext(), mContentEditText);
        } else {
            mViewSwitcher.showPrevious();
            mPresenter.modifyContent(mContentEditText.getText().toString());
            KeyboardController.hideKeyboard(getActivity().getApplicationContext(), mContentEditText);
        }
    }



    /**
     * Dialog를 보여준다.
     */
    public void showInfoDialog() {

        //Todo 1. 객체 생성
        SlidePopUpDialog slidePopUpDialog = new SlidePopUpDialog();
        slidePopUpDialog.setPopupType(SlidePopUpDialog.USER_POPUP);

        slidePopUpDialog.setTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ViewMover.moveVerticalWithScrollMotion(slidePopUpDialog.getFloatingLayout(), event,
                        v.getHeight() - getResources().getDimensionPixelSize(R.dimen.activity_main_tabLayout_height), v.getHeight(), true);
                return true;
            }
        });

        slidePopUpDialog.show(getFragmentManager(), "tag");
    }

    public class PhotoPagerAdapter extends PagerAdapter {
        public static final int NUMBER_OF_PHOTOS_ON_PAGE = 6;
        public static final int MAX_PAGES = 5;

        private List<Bitmap> mPhotoList = null;
        private LayoutInflater mLayoutInflater;

        public PhotoPagerAdapter(List<Bitmap> photoList, LayoutInflater inflater) {
            mPhotoList = photoList;
            mLayoutInflater = inflater;
        }

        @Override
        public int getCount() {
            if(mPhotoList.size() > NUMBER_OF_PHOTOS_ON_PAGE * MAX_PAGES)
                return MAX_PAGES;
            int count = mPhotoList.size() / NUMBER_OF_PHOTOS_ON_PAGE;
            boolean isExistLastPage = (mPhotoList.size() % NUMBER_OF_PHOTOS_ON_PAGE) > 0;
            if(isExistLastPage)
                count++;

            return count;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            int start = position * NUMBER_OF_PHOTOS_ON_PAGE;
            int end = start + NUMBER_OF_PHOTOS_ON_PAGE;

            View pageView = mLayoutInflater.inflate(R.layout.photo_viewpager_child, null);
            GridLayout gridLayout = (GridLayout) pageView.findViewById(R.id.photo_grid);
            gridLayout.setColumnCount(3);
            gridLayout.setRowCount(2);

            for(int i = start; i < end; i++) {
                if(i >= mPhotoList.size())
                    break;

                View itemView = mLayoutInflater.inflate(R.layout.photo_item, null);
                ImageView photoImage = (ImageView) itemView.findViewById(R.id.photo);
                photoImage.setImageBitmap(mPhotoList.get(i));

                /*
                if(i == MAX_PAGES * NUMBER_OF_PHOTOS_ON_PAGE - 1)
                    setOnClickEventWithFacebookConnection(photoImage);*/

                gridLayout.addView(itemView);
            }

            container.addView(pageView);
            return pageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View)object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        private void setOnClickEventWithFacebookConnection(View view) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPresenter.connectFacebook(getActivity().getApplicationContext());
                }
            });
        }
    }
}
