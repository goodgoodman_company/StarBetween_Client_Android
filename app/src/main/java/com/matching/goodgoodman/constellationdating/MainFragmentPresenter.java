package com.matching.goodgoodman.constellationdating;

import android.content.Context;

import com.facebook.AccessToken;
import com.matching.goodgoodman.constellationdating.localDB.ZodiacDBHelper;
import com.matching.goodgoodman.constellationdating.model.FacebookModel;
import com.matching.goodgoodman.constellationdating.model.HoroscopeModel;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.NetworkManager;
import com.matching.goodgoodman.constellationdating.server.model.ResultStatus;
import com.matching.goodgoodman.constellationdating.utils.FacebookModule;


/**
 * MainView와 상호작용하여 비즈니스 로직을 처리하는 클래스.
 * @author Hyeong_Wook
 */

public class MainFragmentPresenter implements Presenter<MainView> {
    private UserModel mUserModel;
    private MainView mView;

    private NetworkManager mNetrowkManager;

    public MainFragmentPresenter(Context context, MainView view) {
        mNetrowkManager = NetworkManager.getInstance(context);
        mUserModel = UserPreference.getInstance().getUserModel();
        attachView(view);
    }

    /**
     * 유저의 기본 정보를 설정한다.
     */
    public void loadUserInfo() {

        String age = calcAge();
        mView.showBasicInformation(mUserModel.getName(), mUserModel.getContent(), age);
    }

    private String calcAge() {
        long age = 0;
        long birthday = mUserModel.getBirthday().getTime();
        long nowDate = System.currentTimeMillis();

        if(birthday < 0)
            age = Math.abs(birthday) + nowDate;
        else
            age = nowDate - birthday;

        long secondsInMilli = 1000;//밀리로 봤을 때 1초
        long minutesInMilli = secondsInMilli * 60;//밀리로 봤을 때 1분
        long hoursInMilli = minutesInMilli * 60;//1시간
        long daysInMilli = hoursInMilli * 24;//하루
        long yearsInMilli = daysInMilli * 365;

        long elapsedYears = age / yearsInMilli;

        return String.valueOf(elapsedYears);
    }

    /**
     * 프로필을 포함한 페이스북 사진들을 가져온다.
     */
    public void loadPictures() {
        FacebookModule facebookModule = FacebookModule.getInstance();
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        facebookModule.getProfilePicture(accessToken, true, result -> {
            mView.showProfile(result);
            UserPreference.getInstance().getUserModel().setProfileImage(result);
            //UserPreference.getInstance().getFacebookModel(UserPreference.getInstance().getFacebookModelList().size()-1).setProfile(result);
        });

        loadPhotos(facebookModule, accessToken);
    }

    private void loadPhotos(FacebookModule facebookModule, AccessToken accessToken) {
        facebookModule.getPhotos(accessToken, result -> {
            if(mUserModel.getImageOpenYn() != null && mUserModel.getImageOpenYn().equals("Y"))
                mView.showPhotos(result, true);
            else
                mView.showPhotos(result, false);
            UserPreference.getInstance().getFacebookModel(UserPreference.getInstance().getFacebookModelList().size()-1).setPhotoList(result);
        });
    }

    public void setPhotosOpen(boolean isOpen) {
        String value = isOpen ? UserModel.YES : UserModel.NO;
        mNetrowkManager.setFacebookOpen(mUserModel.getNo(), value, result -> {
            if(result.getResult().equals(ResultStatus.SUCCESS)) {
                mUserModel.setImageOpenYn(value);
                loadPhotos(FacebookModule.getInstance(), AccessToken.getCurrentAccessToken());
            }
        });
    }

    /**
     * 특정 user의 페이스북에 연결한다.
     * @param context 연결을 요청한 context
     */
    public void connectFacebook(Context context) {
        FacebookModel facebookModel = UserPreference.getInstance().getFacebookModel(UserPreference.getInstance().getFacebookModelList().size()-1);
        FacebookModule.getInstance().connectFacebook(context, facebookModel.getUserId());
    }

    public void modifyContent(String content) {
        mNetrowkManager.putUser(mUserModel.getNo(), content, result -> {
            if (result.getResult().equals(ResultStatus.SUCCESS)) {
                mUserModel.setContent(content);
                mView.showContentText(content);
            }
        });
    }

    public void loadNatalSymbolList(Context context) {
        ZodiacDBHelper zodiacDBHelper = new ZodiacDBHelper(context, ZodiacDBHelper.LANGUAGE_ENGLISH);
        HoroscopeModel horoscopeModel = UserPreference.getInstance().getHoroscopeModel();
        String constellation = horoscopeModel.getPlanets()[0].getConstellation();

        mView.showNatalSymbolList(zodiacDBHelper.getZodiac(constellation));
    }

    @Override
    public void attachView(MainView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
