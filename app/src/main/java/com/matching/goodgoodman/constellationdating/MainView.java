package com.matching.goodgoodman.constellationdating;

import android.graphics.Bitmap;

import java.util.List;

import dao.Zodiac;

/**
 * Created by Hyeong_Wook on 2017-08-11.
 */

public interface MainView {
    void showBasicInformation(String userName, String userContent, String age);
    void showProfile(Bitmap photo);
    void showPhotos(List<Bitmap> photoList, boolean isOpen);
    void showContentText(String content);
    void showNatalSymbolList(Zodiac zodiac);
}
