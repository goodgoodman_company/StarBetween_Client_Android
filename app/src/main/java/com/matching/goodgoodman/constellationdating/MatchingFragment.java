package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.utils.FreePointChangeHandler;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MatchingFragment extends Fragment implements MatchingView {

    // TODO: Customize parameters
    private int mColumnCount = 3;
    private int mSpaceInPixels = 0;
    private final int MAX_STAR = 3;

    private ImageView[] mStarImage;

    private MatchingFragmentPresenter mPresenter;

    View view;
    @BindView(R.id.matchinglist_clockImage) ImageView matchingListClockImage;
    @BindView(R.id.matchinglist_clockTime) TextView matchingListClockTime;

    @BindView(R.id.matchinglist_starLinearLayout) LinearLayout matchingListStarLayout;

    @BindView(R.id.matchinglist_freePoint) TextView matchingListFreePoint;
    @BindView(R.id.progress_wheel) ProgressWheel mProgressWheel;
    @BindView(R.id.no_matching_notice) View mNoMatchingNoticeView;

    private RecyclerView mRecyclerView;
    private MatchingRecyclerViewAdapter mMatchingRecyclerViewAdapter;

    public MatchingFragment() {
        //Log.v(this.getClass().getSimpleName(), "MatchingFragment()");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_matching_list, container, false);
        ButterKnife.bind(this, view);
        FreePointChangeHandler.getInstance().addChangeListener(this::showFreeStars);

        mPresenter = new MatchingFragmentPresenter(getActivity(), this);
        mPresenter.loadCounterUsersData(getResources().getString(R.string.facebook_app_id));

        //Log.v(this.getClass().getSimpleName(), "onCreateView");
        initview();
        mPresenter.startChangeTimer();
        mPresenter.loadFreeStars();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //mPresenter = new MatchingFragmentPresenter(context, this);
        //mPresenter.loadCounterUsersData(getResources().getString(R.string.facebook_app_id));
        //testSet();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mPresenter.detachView();
        //Log.v(this.getClass().getSimpleName(),"onDetach()");
    }

    /**
     * 매칭된 상대방에 대한 정보를 보여준다.
     *
     * @param userModel        나의 정보
     * @param counterUserModel 상대방의 정보
     */
    @Override
    public void showCounterUsers(UserModel userModel, List<UserModel> counterUserModel) {
        //UserPreference.getInstance().getMaintainedUserModel();
        Log.v(this.getClass().getSimpleName(), "showCounterUsers");

        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_matchinglist_recyclerView);

        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), mColumnCount));

        mMatchingRecyclerViewAdapter = new MatchingRecyclerViewAdapter(getActivity(), userModel, counterUserModel);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mMatchingRecyclerViewAdapter);

        initListener();
        mNoMatchingNoticeView.setVisibility(counterUserModel.size() <= 0? View.VISIBLE : View.GONE);
        mProgressWheel.setVisibility(View.GONE);
    }
    /**
     * 가지고 있는 무료 별의 갯수를 보여준다
     *
     * @param starCount        나의 정보
     */
    //TODO : 무료 별의 최대 갯수가 3개 고정이 아닌경우 수정필요
    @Override
    public void showFreeStars(int starCount) {
        Log.v("showFreeStars", "setText" + starCount);
        matchingListFreePoint.setText(String.valueOf(starCount));
    }
    /**
     * CounterUser를 눌렀을 때
     *
     * @param freeStarCount 나의 무료 별 갯수
     * @param starCount     나의 유료 별 갯수
     */
    @Override
    public void showClickCounterUser(int freeStarCount, int starCount, int position, MatchingRecyclerViewAdapter.ViewHolder viewHolder) {
            Log.v("showClickCounterUser", position + " / " + freeStarCount);
            showFreeStars(freeStarCount);
            mMatchingRecyclerViewAdapter.removeBlur(position, viewHolder);
    }

    @Override
    public void showSelectCounterUser(int position) {

    }

    @Override
    public void showNoStars() {
        Toast.makeText(getContext(), "No Extra Stars.", Toast.LENGTH_LONG).show();
    }


    private void initview() {
        Glide.with(getContext()).load(R.drawable.clock).into(matchingListClockImage);
    }

    //TODO : 만약 유료포인트와 무료포인트를 썼을 때의 구분이 필요하다면 useStarPoint 쓰면 됨
    private void initListener() {

        mMatchingRecyclerViewAdapter.setMatchingImageClick(new MatchingRecyclerViewAdapter.MatchingImageClick() {
            @Override
            public void onMatchingImageClick(final int position, MatchingRecyclerViewAdapter.ViewHolder viewHolder) {
                mPresenter.loadCheckAreadyClicked(position, viewHolder);

                //mPresenter.loadClickCounterUser(position, useStarPoint);
            }
        });
    }

    @Override
    public void showMatchingTime(String time) {
        matchingListClockTime.setText(time);
    }


    public class MatchingListToast extends Toast {

        Context mContext;
        private ImageView mimageView;

        public MatchingListToast(Context context) {
            super(context);
            mContext = context;
        }

        //@SuppressLint()
        public void showToast(int duration) {
            LayoutInflater inflater;
            View v;

            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.toast_layout_counterprofile, null);

            ImageView mimageView = (ImageView) v.findViewById(R.id.counter_profile_toast_image);
            Glide.with(mContext).load(R.drawable.send_like).into(mimageView);

            show(this, v, duration);
        }

        private void show(Toast toast, View v, int duration) {
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(duration);
            toast.setView(v);
            toast.show();
        }
    }

}
