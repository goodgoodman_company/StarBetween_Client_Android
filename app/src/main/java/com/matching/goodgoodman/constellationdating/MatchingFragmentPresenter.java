package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.facebook.AccessToken;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.NetworkManager;
import com.matching.goodgoodman.constellationdating.server.SuccessCallBack;
import com.matching.goodgoodman.constellationdating.server.model.ResultStatus;
import com.matching.goodgoodman.constellationdating.utils.DateConverter;
import com.matching.goodgoodman.constellationdating.utils.FacebookModule;
import com.matching.goodgoodman.constellationdating.utils.StarPointChangeHandler;

import java.util.Date;
import java.util.List;

/**
 * MatchingView와 상호작용하여 비즈니스 로직을 처리하는 클래스.
 *
 * @author Hyeong_Wook
 */

public class MatchingFragmentPresenter implements Presenter<MatchingView> {

    private static final int SECONDS_IN_HOUR = 60 * 60;
    public static final String FORMAT_DATE_STR_SEC = "%d-%d-%d %d:%d:%d";
    public static final String FORMAT_DATE_SEC = "yyyy-MM-dd HH:mm:ss";
    private static final String LEFT_TIME_STR = "%02d:%02d:%02d";

    private MatchingView mView;
    private UserModel mUserModel;
    private List<UserModel> mCounterUserModelList;
    private Context mContext;

    private int mMatchingListNumber = 0;

    private Handler mTimerHandler = null;
    private boolean mTimerIsRunning = false;


    private NetworkManager mNetrowkManager;

    public MatchingFragmentPresenter(Context context, MatchingView view) {
        mNetrowkManager = NetworkManager.getInstance(context);
        mUserModel = UserPreference.getInstance().getUserModel();
        mCounterUserModelList = UserPreference.getInstance().getCounterUserModelList();
        mContext = context;
        attachView(view);
    }

    /**
     * 매칭된 상대의 no를 가져와서 (userModel의 List<UserModel> mCounterUserModelList에 들어있다)
     * 상대의
     */
    public void loadCounterUsersData(String appId) {
        mMatchingListNumber = mCounterUserModelList.size();
        if(mMatchingListNumber == 0)
            mView.showCounterUsers(mUserModel, mCounterUserModelList);

        SuccessCallBack<List<UserModel>> successCallBack = new SuccessCallBack<List<UserModel>>() {
            @Override
            public void onCallBack(List<UserModel> result) {
                mView.showCounterUsers(mUserModel, mCounterUserModelList);
            }
        };

        for (int i = 0; i < mCounterUserModelList.size(); ++i) {
            loadCounterUserPicture(i, appId, successCallBack);
        }
    }


    /**
     * 매칭된 상대의 no를 가져와서 (userModel의 List<UserModel> mCounterUserModelList에 들어있다)
     * 상대의 이미지를 가져온다.
     * TODO : AccessToken에 더미값으로 넣어둠 나중에 바꿔야 함.
     */

    private void loadCounterUserPicture(int position, String appId, SuccessCallBack<List<UserModel>> successCallBack) {
        FacebookModule facebookModule = FacebookModule.getInstance();
        AccessToken accessToken = new AccessToken(mCounterUserModelList.get(position).getFaceBookToken(), appId, mCounterUserModelList.get(position).getId(), null, null, null, null, null);

        //AccessToken accessToken = new AccessToken("EAABtx6fZAa0EBAD13ULk8RioVig0nKZBkLdwXwyLV8jZAUaWDx06IdxDrGSQMyF9eoIj98diMbZCOGPBvwsrZCd9EqpxDMIC1IPLJJnCF9oACP9NYUj2ax94z3hzZAgPf1Ls3ZCeVeRx7JdGFsEEcsDAOS5HeHwfZAiw1e1ZBbAEZBzs6n1SjAIfxZBqHPFa0ZAvc77YH5pyDu2V3KfABWf2xcjMksGfdg2nboQZD",
          //      appId, "1486769358082067",
            //    Arrays.asList(FacebookModule.PERMISSIONS), null, null, null, null);

        facebookModule.getProfilePicture(accessToken, true, new SuccessCallBack<Bitmap>() {
            @Override
            public void onCallBack(Bitmap result) {
                mCounterUserModelList.get(position).setProfileImage(result);
                mMatchingListNumber--;
                if (mMatchingListNumber <= 0)
                    successCallBack.onCallBack(mCounterUserModelList);
            }
        });
    }

    public void loadFreeStars() {
        mView.showFreeStars(mUserModel.getFreePoint());
    }

    public void loadCheckAreadyClicked(int position, MatchingRecyclerViewAdapter.ViewHolder viewHolder) {
        if (mCounterUserModelList.get(position).getUseBlurYn().equals(UserModel.NO))
            loadClickSuccess(position, viewHolder);
        else {
            mView.showSelectCounterUser(position);
        }
    }

    public void loadClickSuccess(int position, MatchingRecyclerViewAdapter.ViewHolder viewHolder) {

        if (mUserModel.getFreePoint() > 0) {
            mNetrowkManager.putUseFreePoint(mCounterUserModelList.get(position), new SuccessCallBack<ResultStatus>() {
                @Override
                public void onCallBack(ResultStatus result) {
                    if (result.getResult().equals("success")) {
                        Log.v("loadClickSuccess", "success");
                        mCounterUserModelList.get(position).setUseBlurYn(UserModel.YES);
                        int temp = mUserModel.getFreePoint() - 1;
                        mUserModel.setFreePoint(temp);//별갯수 감소
                        mView.showClickCounterUser(mUserModel.getFreePoint(), mUserModel.getStarPoint(), position, viewHolder);
                    }
                }
            });
        } else if (mUserModel.getStarPoint() > 0) {
            useStarPointDialog(position, viewHolder);
        } else {
            mView.showNoStars();
        }
    }

    public void useStarPointDialog(int position, MatchingRecyclerViewAdapter.ViewHolder viewHolder) {
        UserModel counterUser = mCounterUserModelList.get(position);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);

        alertDialogBuilder.setTitle("Use Star Point");

        alertDialogBuilder.setMessage("Use 10 Star Point?");
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mNetrowkManager.putUseStarPointforBlur(counterUser, result -> {
                    if (result.getResult().equals("success")) {
                        counterUser.setUseBlurYn(UserModel.YES);
                        mView.showClickCounterUser(mUserModel.getFreePoint(), mUserModel.getStarPoint(), position, viewHolder);
                        if (counterUser.getGrade().equals("S")) {

                        } else {
                            int temp = mUserModel.getStarPoint() - UserModel.INT_POINT10;
                            mUserModel.setStarPoint(temp);
                            StarPointChangeHandler.getInstance().notifyChangeData(mUserModel.getStarPoint());
                        }
                    }
                    else {
                        if(mView instanceof Fragment)
                            new StarShopDialogFragment().show(((Fragment)mView).getFragmentManager(), null);
                    }
                });
            }
        });

        alertDialogBuilder.setCancelable(false).setNegativeButton("Nope",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void startChangeTimer() {

        int[] userConTimes
                = DateConverter.dateToDividedTime(mUserModel.getLastConTime());

        String dateStr = String.format(FORMAT_DATE_STR_SEC,
                userConTimes[0],
                userConTimes[1],
                userConTimes[2] + 1,
                0,
                0,
                0
        );


        Date endTime = DateConverter.strToDate(FORMAT_DATE_SEC, dateStr);

        mTimerHandler = new Handler() {
            private long userLastConTime = mUserModel.getLastConTime().getTime();

            @Override
            public synchronized void handleMessage(Message msg) {
                if(!mTimerIsRunning)
                    return;



                long leftTimeSeconds = (endTime.getTime() - userLastConTime) / 1000;
                long hour = leftTimeSeconds / SECONDS_IN_HOUR;
                long secondsHour = leftTimeSeconds % SECONDS_IN_HOUR;
                long minutes = secondsHour / 60;
                long seconds = secondsHour % 60;

                userLastConTime += 1000;

                if(leftTimeSeconds <= 0) {
                    mView.showMatchingTime(String.format(LEFT_TIME_STR, 00, 00, 00));
                }
                else {
                    mView.showMatchingTime(String.format(LEFT_TIME_STR, hour, minutes, seconds));
                }
                this.sendEmptyMessageDelayed(0, 1000);
            }
        };

        mTimerIsRunning = true;
        mTimerHandler.sendEmptyMessage(0);
    }

    private synchronized void endWaitTimer() {
        mTimerIsRunning = false;
        mTimerHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void attachView(MatchingView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        if(mTimerHandler != null)
            endWaitTimer();
        mView = null;
    }
}
