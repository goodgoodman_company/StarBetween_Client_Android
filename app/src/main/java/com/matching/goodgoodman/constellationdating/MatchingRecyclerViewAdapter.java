package com.matching.goodgoodman.constellationdating;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.matching.goodgoodman.constellationdating.localDB.MatchingScoreDBHelper;
import com.matching.goodgoodman.constellationdating.model.ColorData;
import com.matching.goodgoodman.constellationdating.model.SynastryAspect;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.SuccessCallBack;
import com.matching.goodgoodman.constellationdating.utils.ColorIndexHelper;
import com.matching.goodgoodman.constellationdating.utils.ConstellationCalculator;
import com.matching.goodgoodman.constellationdating.utils.DateConverter;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dao.MatchingScore;

public class MatchingRecyclerViewAdapter extends RecyclerView.Adapter<MatchingRecyclerViewAdapter.ViewHolder> {
    private static final String SCORE_STR = "%d%%";
    private static int ORIGINAL_MATCHILST_SIZE = 0;

    private Context mContext;
    private MatchingRecyclerViewAdapter.ViewHolder mViewHolder;

    private UserModel mUserModel = new UserModel();
    private final List<UserModel> mCounterUserModels;
    private MatchingScoreDBHelper mMatchingScoreDBHelper;

    private MatchingImageClick mMatchingImageClick;

    public interface MatchingImageClick {
        void onMatchingImageClick(final int position, ViewHolder viewHolder);
    }

    public MatchingRecyclerViewAdapter(Context context, UserModel userModel, List<UserModel> counterUserModels) {
        this.mContext = context;
        this.mUserModel = userModel;
        this.mCounterUserModels = new ArrayList<UserModel>();
        mCounterUserModels.clear();
        this.mCounterUserModels.addAll(counterUserModels);
        ORIGINAL_MATCHILST_SIZE = mCounterUserModels.size();
        mMatchingScoreDBHelper = new MatchingScoreDBHelper(context);

        UserModel dummyUserModel = new UserModel();
        Bitmap dummyBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.matching_list_nomatch);
        dummyBitmap = Bitmap.createScaledBitmap(dummyBitmap, 64, 64, false);
        dummyUserModel.setProfileImage(dummyBitmap);
    }

    public void setMatchingImageClick(MatchingImageClick matchingImageClick) {
        this.mMatchingImageClick = matchingImageClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.fragment_matching_list_item, parent, false);

        mViewHolder = new ViewHolder(view);
        return mViewHolder;
    }

    public void showItem(final ViewHolder holder, final int position, boolean isMatched) {
        UserModel counterUserModel = mCounterUserModels.get(position);
        Bitmap bitmap = Bitmap.createScaledBitmap(counterUserModel.getProfileImage(), 128, 128, false);
        BitmapAvatarDrawable bitmapAvatarDrawable = getBitmapAvartarDrawable(bitmap, !isMatched);//육각형
        holder.fragment_matching_list_matchingImage.setImageDrawable(bitmapAvatarDrawable);

        if(isMatched)
            showScoreProfile(holder, counterUserModel);

        holder.edgeImage.setVisibility(isMatched ? View.VISIBLE : View.GONE);

        holder.mBlurAlreadyRemoved = isMatched;
    }

    private void showScoreProfile(ViewHolder viewHolder, UserModel counterUser) {
        MatchingScore matchingScore;
        viewHolder.mProgressWheel.setVisibility(View.VISIBLE);
        if(counterUser.getScore() != -1) {
            showScoreText(viewHolder.edgeImage, counterUser.getScore());
            viewHolder.mProgressWheel.setVisibility(View.GONE);
            return;
        } else if ((matchingScore = mMatchingScoreDBHelper.getMatchingScore(mUserModel.getNo(), counterUser.getNo())) != null) {
            counterUser.setScore(matchingScore.getScore());
            showScoreText(viewHolder.edgeImage, counterUser.getScore());
            viewHolder.mProgressWheel.setVisibility(View.GONE);
            return;
        }

        getScore(counterUser, result -> {
            showScoreText(viewHolder.edgeImage, result);
            viewHolder.mProgressWheel.setVisibility(View.GONE);
        });
    }

    private void showScoreText(ImageView view, int score) {

        int colorDrawable = ColorData.ID_LOVES_DRAWAVLE[ColorIndexHelper.getLoveIndex(score)];
        BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(colorDrawable, null);
        Bitmap textbitmap2 = drawable.getBitmap();
        view.setImageDrawable(getBitmapAvartarDrawable(textbitmap2, true));
        view.setVisibility(View.VISIBLE);
    }

    private void getScore(UserModel counterUser, SuccessCallBack<Integer> successCallBack) {
        int[] counterBirthday = DateConverter.dateToDividedTime(counterUser.getBirthday());
        ConstellationCalculator.getHoroscopeModel(mContext, counterBirthday[0], counterBirthday[1], counterBirthday[2], counterBirthday[3], counterBirthday[4],
                counterUser.getLatitude(), counterUser.getLongitude(), result -> {
                    List<SynastryAspect> synastryAspects = ConstellationCalculator.getSynastryAspectList(UserPreference.getInstance().getHoroscopeModel(), result);
                    counterUser.setScore(ConstellationCalculator.getSynastryScore(synastryAspects));
                    result.setCounterUserNo(counterUser.getNo());
                    UserPreference.getInstance().addCounterHoroscopeModel(result);
                    mMatchingScoreDBHelper.insert(mUserModel.getNo(), counterUser.getNo(), counterUser.getScore());

                    successCallBack.onCallBack(counterUser.getScore());
                });
    }

    private BitmapAvatarDrawable getBitmapAvartarDrawable(Bitmap bitmap, boolean isBlur) {
        if (isBlur)
            bitmap = BlurBuilder.blur(mContext, bitmap);

        return new BitmapAvatarDrawable(bitmap);
    }

    //TODO : 매칭등급은 현재 계산이 없어서 더미로 들어가있음
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        UserModel counterUserModel = mCounterUserModels.get(position);

        if (position < mCounterUserModels.size()) {
            boolean isMatch = counterUserModel.getUseBlurYn().equals(UserModel.YES);
            showItem(holder, position, isMatch);

        } else if (position > ORIGINAL_MATCHILST_SIZE - 1) {

            Bitmap bitmap = Bitmap.createScaledBitmap(mCounterUserModels.get(position).getProfileImage(), 128, 128, false);
            BitmapAvatarDrawable dummyBitmapAvatarDrawable = new BitmapAvatarDrawable(bitmap);
            holder.fragment_matching_list_matchingImage.setImageDrawable(dummyBitmapAvatarDrawable);

            holder.edgeImage.setVisibility(View.GONE);

            holder.mBlurAlreadyRemoved = true;
        }

        holder.itemView.setOnClickListener(v -> {
            if (!holder.mBlurAlreadyRemoved) {
                if(counterUserModel.getGrade() != null) {
                    mMatchingImageClick.onMatchingImageClick(holder.getAdapterPosition(), holder);
                } else {
                    getScore(counterUserModel, result -> {
                        counterUserModel.setGrade(result >= 90 ? "S" : "A");
                        mMatchingImageClick.onMatchingImageClick(holder.getAdapterPosition(), holder);
                    });
                }

                //notifyItemChanged(holder.getAdapterPosition());
            } else if (holder.mBlurAlreadyRemoved && position < ORIGINAL_MATCHILST_SIZE) {
                UserModel counterUser = mCounterUserModels.get((holder.getAdapterPosition()));

                Intent intent = new Intent(mContext, CounterProfileActivity.class);
                intent.putExtra(CounterProfileActivity.EXTRA_COUNTER_USER_NO, counterUser.getNo());
                intent.putExtra(CounterProfileActivity.EXTRA_SCORE, counterUser.getScore());
                if(mContext instanceof Activity)
                    ((Activity)mContext).startActivityForResult(intent, 0);
                else
                    mContext.startActivity(intent);
            }
        });

    }

    //TODO : 유료포인트의 쓰임이 바뀌면 바뀔 수 있음
    public void removeBlur(final int position, ViewHolder viewHolder) {
        mCounterUserModels.get(position).setUseBlurYn(UserModel.YES);
        viewHolder.mBlurAlreadyRemoved = true;
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return mCounterUserModels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.fragment_matchinglist_Image)
        ImageView fragment_matching_list_matchingImage;
        @BindView(R.id.fragment_matchinglist_layout)
        FrameLayout fragment_matching_list_Layout;
        @BindView(R.id.progress_wheel)
        ProgressWheel mProgressWheel;

        @BindView(R.id.bounder) ImageView edgeImage;

        boolean mBlurAlreadyRemoved = false;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            //Log.v(this.getClass().getSimpleName() , "ViewHolder()");
        }
    }

    /**
     *
     */
    public static class BlurBuilder {
        private static final float BITMAP_SCALE = 1.0f;
        private static final float BLUR_RADIUS = 7.5f;

        public static Bitmap blur(Context context, Bitmap image) {
            int width = Math.round(image.getWidth() * BITMAP_SCALE);
            int height = Math.round(image.getHeight() * BITMAP_SCALE);

            Bitmap inputBitmap = Bitmap.createScaledBitmap(image.copy(Bitmap.Config.ARGB_8888, true), width, height, false);
            Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

            RenderScript rs = RenderScript.create(context);
            ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
            Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
            theIntrinsic.setRadius(BLUR_RADIUS);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
            tmpOut.copyTo(outputBitmap);

            return outputBitmap;
        }
    }

}
