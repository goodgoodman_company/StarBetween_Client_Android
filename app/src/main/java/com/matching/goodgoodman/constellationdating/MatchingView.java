package com.matching.goodgoodman.constellationdating;

import com.matching.goodgoodman.constellationdating.model.UserModel;

import java.util.List;

/**
 * Created by Hyeong_Wook on 2017-08-15.
 */

public interface MatchingView {
    void showCounterUsers(UserModel userModel, List<UserModel> counterUserModel);
    void showFreeStars(int starCount);
    void showClickCounterUser(int freeStarCount, int starCount, int position, MatchingRecyclerViewAdapter.ViewHolder viewHolder);
    void showSelectCounterUser(int position);
    void showNoStars();
    void showMatchingTime(String time);
}
