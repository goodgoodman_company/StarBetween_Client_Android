package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * ImageView 1개와 textView 2개로 이루어진 custom view 클래스입니다.
 * @author Hyeong_Wook
 */

public class NatalSymbol extends LinearLayout {
    private ImageView mSymbol;
    private TextView mTitle;
    private TextView mSymbolText;

    public NatalSymbol(Context context) {
        super(context);
        initView();
    }

    public NatalSymbol(Context context, AttributeSet attrs) {
        super(context, attrs);

        initView();
        getAttrs(attrs);
    }

    public NatalSymbol(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);

        initView();
        getAttrs(attrs, defStyle);
    }

    private void initView() {
        String inflaterService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(inflaterService);
        View view = layoutInflater.inflate(R.layout.natal_symbol, null);
        addView(view);

        mSymbol = (ImageView) findViewById(R.id.symbol);
        mTitle = (TextView) findViewById(R.id.titleText);
        mSymbolText = (TextView) findViewById(R.id.symbolText);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.NatalSymbol);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.NatalSymbol, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        int symbolImageId = typedArray.getResourceId(R.styleable.NatalSymbol_symbol, R.drawable.com_facebook_button_send_icon_white);
        mSymbol.setBackgroundResource(symbolImageId);

        String titleText = typedArray.getString(R.styleable.NatalSymbol_titleText);
        mTitle.setText(titleText);

        String symbolText = typedArray.getString(R.styleable.NatalSymbol_symbolText);
        mSymbolText.setText(symbolText);

        typedArray.recycle();
    }

    public void setSymbol(int imageId) {
        mSymbol.setBackgroundResource(imageId);
    }

    public void setTitleText(String text) {
        mTitle.setText(text);
    }

    public void setSymbolText(String text) {
        mSymbolText.setText(text);
    }
}
