package com.matching.goodgoodman.constellationdating;

/**
 * Created by Hyeong_Wook on 2017-08-11.
 */

public interface Presenter<V> {
    void attachView(V view);
    void detachView();
}
