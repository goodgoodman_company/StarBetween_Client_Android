package com.matching.goodgoodman.constellationdating;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.popup_text.QnAPopup;
import com.matching.goodgoodman.constellationdating.popup_text.TextPopup;
import com.matching.goodgoodman.constellationdating.utils.ViewMover;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment implements SettingView {

    private SettingPresenter mPresenter;

    Context mContext;

    @BindView(R.id.buyStar) GridLayout mBuyStar;
    @BindView(R.id.buyStar_textView) TextView mBuyStarTextView;
    @BindView(R.id.select_alarm_textView) TextView mSelectAlarmTextView;
    @BindView(R.id.chattingNotification) Switch mChattingNotificationSwitch;
    @BindView(R.id.message_textView) TextView mMessageTextView;
    @BindView(R.id.getLikeNotification) Switch mGetLikeNotificationSwitch;
    @BindView(R.id.wink_notification_textView) TextView mWinkNotification;

    @BindView(R.id.privacy) TextView mPrivacyTextView;
    @BindView(R.id.help) TextView mHelpTextView;
    @BindView(R.id.problem) TextView mProblemTextView;

    @BindView(R.id.logout) GridLayout mLogout;

    private DescriptionDBHelper mDescriptionDBHelper;

    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        ButterKnife.bind(this, view);
        mPresenter = new SettingPresenter(getContext(), this);

        mDescriptionDBHelper = new DescriptionDBHelper(getContext(),DescriptionDBHelper.LANGUAGE_ENGLISH);

        initView();
        initStarListener();
        initSwitchListener();

        initEtcSection();

        return view;
    }

    private void initView() {

        mBuyStarTextView.setText(mDescriptionDBHelper.getDescription(350).getComment());
        mSelectAlarmTextView.setText(mDescriptionDBHelper.getDescription(352).getComment());
        mMessageTextView.setText(mDescriptionDBHelper.getDescription(356).getComment());
        mWinkNotification.setText(mDescriptionDBHelper.getDescription(354).getComment());

        mPrivacyTextView.setText(mDescriptionDBHelper.getDescription(363).getComment());
        mHelpTextView.setText(mDescriptionDBHelper.getDescription(364).getComment());
        mProblemTextView.setText(mDescriptionDBHelper.getDescription(365).getComment());

        mPresenter.loadSwitch();
    }

    private void initStarListener() {

        mBuyStar.setOnClickListener(v -> {
            new StarShopDialogFragment().show(getFragmentManager(), null);
        });

        mLogout.setOnClickListener(v -> {
            mPresenter.logout();
        });

    }

    @Override
    public void setSwitch(boolean newCounterUser, boolean chating, boolean matching, boolean getLike, boolean distance) {

        mChattingNotificationSwitch.setChecked(chating);
        mGetLikeNotificationSwitch.setChecked(getLike);

    }

    @Override
    public void restartApplication() {
        getActivity().finish();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
    }

    private void initSwitchListener() {

        mChattingNotificationSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.changeState(2, isChecked));

        mGetLikeNotificationSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> mPresenter.changeState(4, isChecked));

    }

    private void initEtcSection() {
        mPrivacyTextView.setOnClickListener(v -> {

            TextPopup textPopup = new TextPopup();

            textPopup.setTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    ViewMover.moveVerticalWithScrollMotion(textPopup.getFloatingLayout(), event,
                            v.getHeight() - getResources().getDimensionPixelSize(R.dimen.activity_main_tabLayout_height), v.getHeight(), true);
                    return true;
                }
            });

            textPopup.show(getFragmentManager(), "textPopup");
        });

        mHelpTextView.setOnClickListener(v -> {
            QnAPopup qnaPopup = new QnAPopup();

            qnaPopup.setTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    ViewMover.moveVerticalWithScrollMotion(qnaPopup.getFloatingLayout(), event,
                            v.getHeight() - getResources().getDimensionPixelSize(R.dimen.activity_main_tabLayout_height), v.getHeight(), true);
                    return true;
                }
            });

            qnaPopup.show(getFragmentManager(), "qnaPopup");
        });

        mProblemTextView.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getContext().getResources().getString(R.string.facebook_url)));
            startActivity(browserIntent);
        });
    }



}
