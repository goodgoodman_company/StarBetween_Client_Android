package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.content.SharedPreferences;

import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.NetworkManager;
import com.matching.goodgoodman.constellationdating.utils.FacebookModule;

/**
 * Created by dorol on 2017-10-26.
 */

public class SettingPresenter implements Presenter<SettingView>{

    private Context mContext;
    private UserModel mUserModel;
    private NetworkManager mNetrowkManager;
    private SettingView mView;

    private boolean mNewCounterUserNotice = true;
    private boolean mChattingNotice = true;
    private boolean mMatchingNotice = true;
    private boolean mLikeNotice = true;
    private boolean mMeter = true;
    private String mLanguage = DescriptionDBHelper.LANGUAGE_ENGLISH;

    private SharedPreferences sharedpreferences;

    public SettingPresenter(Context context, SettingView view) {
        mContext = context;
        mNetrowkManager = NetworkManager.getInstance(context);
        mUserModel = UserPreference.getInstance().getUserModel();
        mContext = context;
        sharedpreferences = mContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
        attachView(view);
    }

    public static final String NEW_COUNTER_NOTICE_KEY = "newCounterUserNotice";//없음
    public static final String CHATTING_NOTICE_KEY = "chattingNotice";
    public static final String MATCHING_NOTICE_KEY = "matchingNotice";//없음
    public static final String LIKE_NOTICE_KEY = "likeNotice";
    public static final String DISTANCE_METER_NOTICE_KEY = "distanceNotice";
    public static final String DESCRIPTION_LANGUAGE = "descriptionLanguage";


    public void loadSwitch() {

        boolean newCounterUser = sharedpreferences.getBoolean(NEW_COUNTER_NOTICE_KEY, mNewCounterUserNotice);
        boolean chating = sharedpreferences.getBoolean(CHATTING_NOTICE_KEY, mChattingNotice);
        boolean matching = sharedpreferences.getBoolean(MATCHING_NOTICE_KEY, mMatchingNotice);
        boolean getLike = sharedpreferences.getBoolean(LIKE_NOTICE_KEY, mLikeNotice);

        boolean distance = sharedpreferences.getBoolean(DISTANCE_METER_NOTICE_KEY, mMeter);

        String descriptionLanguage = sharedpreferences.getString(DESCRIPTION_LANGUAGE, mLanguage);

        mView.setSwitch(newCounterUser, chating, matching, getLike, distance);
    }

    public void changeState(int stateNum, boolean isChecked) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences("pref",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        switch (stateNum) {
            case 1:
                editor.putBoolean(NEW_COUNTER_NOTICE_KEY, isChecked);
                editor.apply();
                break;
            case 2:
                editor.putBoolean(CHATTING_NOTICE_KEY, isChecked);
                editor.apply();
                break;
            case 3:
                editor.putBoolean(MATCHING_NOTICE_KEY, isChecked);
                editor.apply();
                break;
            case 4:
                editor.putBoolean(LIKE_NOTICE_KEY, isChecked);
                editor.apply();
                break;
            case 5:
                editor.putBoolean(DISTANCE_METER_NOTICE_KEY, isChecked);
                editor.apply();
                break;
        }
    }

    public void logout() {
        FacebookModule.getInstance().logOut();
        mView.restartApplication();
    }

    @Override
    public void attachView(SettingView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
