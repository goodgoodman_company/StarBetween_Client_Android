package com.matching.goodgoodman.constellationdating;

/**
 * Created by dorol on 2017-10-26.
 */

public interface SettingView {
    void setSwitch(boolean newCounterUser, boolean chating, boolean matching, boolean getLike, boolean distance);
    void restartApplication();

}
