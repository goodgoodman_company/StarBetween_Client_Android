package com.matching.goodgoodman.constellationdating;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Created by Hyeong_Wook on 2017-11-08.
 */

public class SpecialCharBlockFilter implements InputFilter {
    private static final String BLOCK_CHARACTER_SET = "~#^|$%&*!";

    @Override
    public CharSequence filter(CharSequence charSequence, int i, int i1, Spanned spanned, int i2, int i3) {
        if (charSequence != null && BLOCK_CHARACTER_SET.contains(("" + charSequence))) {
            return "";
        }
        return null;
    }
}
