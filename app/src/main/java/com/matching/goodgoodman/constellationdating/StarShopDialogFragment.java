package com.matching.goodgoodman.constellationdating;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.matching.goodgoodman.constellationdating.model.PaymentItem;
import com.matching.goodgoodman.constellationdating.model.UserModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class StarShopDialogFragment extends DialogFragment implements StarShopView {
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.premium)
    ImageView mPremiumButton;
    @BindView(R.id.premium_layout)
    LinearLayout mPremiumLayout;
    @BindView(R.id.premium_price)
    TextView mPremiumPrice;
    @BindView(R.id.premium_previous_price)
    View mPremiumPreviousPrice;

    private View mRootView;
    private PaymentRecyclerViewAdapter mPaymentRecyclerViewAdapter;
    private StarShopPresenter mPresenter;

    public StarShopDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_star_shop_dialog, container, false);
        ButterKnife.bind(this, mRootView);
        setCancelable(true);
        getDialog().setCanceledOnTouchOutside(true);
        initialView();
        initialListeners();

        mPaymentRecyclerViewAdapter = new PaymentRecyclerViewAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));
        mRecyclerView.setAdapter(mPaymentRecyclerViewAdapter);

        mPresenter.loadItems();

        return mRootView;
    }

    private void initialView() {
        mPremiumLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.deep_gray));
    }

    private void initialListeners() {
        mPremiumButton.setOnClickListener(v -> mPresenter.consumeItem(StarShopPresenter.SKU_PREMIUM, getActivity()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPresenter = new StarShopPresenter(context);
        mPresenter.attachView(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getDialog() == null)
            return;

        int width = getResources().getDimensionPixelSize(R.dimen.star_shop_width);
        int height = getResources().getDimensionPixelSize(R.dimen.star_shop_height);

        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPresenter.detachView();
        mPresenter = null;
    }

    @Override
    public void showItems(List<PaymentItem> paymentItems) {
        mPaymentRecyclerViewAdapter.setData(paymentItems);
        mPaymentRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void showPremium(PaymentItem paymentItem, String premiumYn) {
        if(premiumYn.equals(UserModel.YES)) {
            mPremiumPrice.setVisibility(View.GONE);
            mPremiumPreviousPrice.setVisibility(View.GONE);
            mPremiumButton.setImageDrawable(getResources().getDrawable(R.drawable.starsshop_premium_used, null));
            mPremiumButton.setOnClickListener(null);
        } else {
            mPremiumPrice.setVisibility(View.VISIBLE);
            mPremiumPreviousPrice.setVisibility(View.VISIBLE);
            mPremiumButton.setImageDrawable(getResources().getDrawable(R.drawable.starsshop_premium, null));
            mPremiumPrice.setText(paymentItem.getPrice());
        }
    }

    public class PaymentRecyclerViewAdapter extends RecyclerView.Adapter<PaymentRecyclerViewAdapter.ViewHolder> {
        private List<PaymentItem> mPaymentItemList;

        public PaymentRecyclerViewAdapter() {
            mPaymentItemList = new ArrayList<>();
        }

        public void setData(List<PaymentItem> paymentItemList) {
            mPaymentItemList = paymentItemList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_star_shop_dialog_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            PaymentItem paymentItem = mPaymentItemList.get(position);

            holder.mStarNumberText.setText(String.valueOf(paymentItem.getCount()));
            holder.mPriceText.setText(paymentItem.getPrice());
            holder.mSpecialDescription.setVisibility(View.VISIBLE);

            if(paymentItem.getSpecialDescription() == null)
                holder.mSpecialDescription.setVisibility(View.GONE);
            else
                holder.mSpecialText.setText(paymentItem.getSpecialDescription());

            holder.mPriceButton.setOnClickListener(view -> mPresenter.consumeItem(paymentItem.getSkuId(), getActivity()));
        }

        @Override
        public int getItemCount() {
            return mPaymentItemList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.starNumber) TextView mStarNumberText;
            @BindView(R.id.specialDescription) View mSpecialDescription;
            @BindView(R.id.specialText) TextView mSpecialText;
            @BindView(R.id.priceButton) ImageView mPriceButton;
            @BindView(R.id.priceText) TextView mPriceText;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
