package com.matching.goodgoodman.constellationdating;

import android.app.Activity;
import android.content.Context;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;
import com.matching.goodgoodman.constellationdating.inAppBilling.InAppBillingHelper;
import com.matching.goodgoodman.constellationdating.model.PaymentItem;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.NetworkManager;
import com.matching.goodgoodman.constellationdating.server.model.ResultStatus;
import com.matching.goodgoodman.constellationdating.utils.FreePointChangeHandler;
import com.matching.goodgoodman.constellationdating.utils.StarPointChangeHandler;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Hyeong_Wook on 2017-10-09.
 */

public class StarShopPresenter implements Presenter<StarShopView> {
    private static final String[] SPECIAL_DESCRIPTIONS = {null, "49%\nOFF", "59%\nOFF"};
    private static final String SKU_ID_STR = "star_";
    public static final String SKU_PREMIUM = "premium";
    private static final int PREMIUM_COUNT = 1290;
    private static final int FREE_COUNT = 3;

    private StarShopView mView;
    private InAppBillingHelper mInAppBillingHelper;
    private NetworkManager mNetworkManager;
    private UserModel mUserModel;

    public StarShopPresenter(Context context) {
        mInAppBillingHelper = new InAppBillingHelper(context, this::onConsumeSuccess);
        mNetworkManager = NetworkManager.getInstance(context);
        mUserModel = UserPreference.getInstance().getUserModel();
    }

    public void loadItems() {
        ArrayList<PaymentItem> paymentItems = new ArrayList<>();
        mInAppBillingHelper.connect(result -> {
            if(result == BillingClient.BillingResponse.OK)
                mInAppBillingHelper.getBillingItemList(skuDetailsList -> {
                    for(SkuDetails skuDetails : skuDetailsList) {
                        String skuId = skuDetails.getSku();
                        if(skuId.equals(SKU_PREMIUM)) {
                            mView.showPremium(new PaymentItem(skuId, PREMIUM_COUNT, skuDetails.getPrice(), null), mUserModel.getPremiumYn());
                            continue;
                        }
                        int count = Integer.parseInt(skuId.replace(SKU_ID_STR, ""));
                        int descriptionIndex = InAppBillingHelper.getIndexSkuId(skuId);

                        paymentItems.add(new PaymentItem(skuId, count, skuDetails.getPrice(), SPECIAL_DESCRIPTIONS[descriptionIndex]));
                    }

                    Collections.sort(paymentItems, (paymentItem, t1) -> new Integer(paymentItem.getCount()).compareTo(new Integer(t1.getCount())));
                    mView.showItems(paymentItems);
                });
        });
    }

    public void consumeItem(String skuId, Activity activity) {
        if(skuId.equals(SKU_PREMIUM) && mUserModel.getPremiumYn().equals(UserModel.YES))
            return;

        mInAppBillingHelper.consumeItem(skuId, activity);
    }

    private void onConsumeSuccess(String consumeItemId) {
        if(consumeItemId.equals(SKU_PREMIUM)) {
            putPremium();
            return;
        }

        int starCount = Integer.parseInt(consumeItemId.replace(SKU_ID_STR, ""));
        mNetworkManager.putStarPoint(mUserModel.getNo(), starCount, result -> {
            if(result.getResult().equals(ResultStatus.SUCCESS)) {
                mUserModel.setStarPoint(mUserModel.getStarPoint() + starCount);
                StarPointChangeHandler.getInstance().notifyChangeData(mUserModel.getStarPoint());
            }
        });
    }

    private void putPremium() {
        mNetworkManager.putPremium(mUserModel.getNo(), result -> {
            if(result.getResult().equals(ResultStatus.SUCCESS)) {

                mNetworkManager.putStarPoint(mUserModel.getNo(), PREMIUM_COUNT, putResult -> {
                    if(putResult.getResult().equals(ResultStatus.SUCCESS)) {
                        mUserModel.setPremiumYn(UserModel.YES);
                        mUserModel.setStarPoint(mUserModel.getStarPoint() + PREMIUM_COUNT);
                        StarPointChangeHandler.getInstance().notifyChangeData(mUserModel.getStarPoint());
                    }
                });

                mNetworkManager.putFreePoint(mUserModel.getNo(), FREE_COUNT, putResult -> {
                    if(putResult.getResult().equals(ResultStatus.SUCCESS)) {
                        mUserModel.setFreePoint(mUserModel.getFreePoint() + FREE_COUNT);
                        FreePointChangeHandler.getInstance().notifyChangeData(mUserModel.getFreePoint());
                    }
                });

                mView.showPremium(new PaymentItem("", PREMIUM_COUNT, "", ""), UserModel.YES);
            }
        });
    }

    @Override
    public void attachView(StarShopView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
