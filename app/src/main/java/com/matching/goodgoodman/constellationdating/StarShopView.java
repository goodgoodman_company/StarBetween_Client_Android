package com.matching.goodgoodman.constellationdating;

import com.matching.goodgoodman.constellationdating.model.PaymentItem;

import java.util.List;

/**
 * Created by Hyeong_Wook on 2017-10-09.
 */

public interface StarShopView {
    void showItems(List<PaymentItem> paymentItems);
    void showPremium(PaymentItem paymentItem, String premiumYn);
}
