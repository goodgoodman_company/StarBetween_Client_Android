package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.matching.goodgoodman.constellationdating.model.Aspect;
import com.matching.goodgoodman.constellationdating.model.HoroscopeModel;
import com.matching.goodgoodman.constellationdating.model.SynastryAspect;
import com.matching.goodgoodman.constellationdating.model.SynastryHoroscopeModel;
import com.matching.goodgoodman.constellationdating.utils.ConstellationCalculator;

import java.util.ArrayList;
import java.util.List;

import dao.Ascendent;
import dao.Planet;

/**
 * Created by Hyeong_Wook on 2017-10-11.
 */

public class SynastryChart extends LinearLayout {
    private Horoscope mUserHoroscope;
    private Horoscope mCounterHoroscope;

    private HoroscopeModel mUserHoroscopeModel;
    private HoroscopeModel mCounterHoroscopeModel;

    public SynastryChart(Context context) {
        super(context);
        initialize(context);
    }

    public SynastryChart(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
        getAttrs(attrs);
    }

    public SynastryChart(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
        getAttrs(attrs);
    }

    private void initialize(Context context) {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        setWillNotDraw(false);
        setDrawingCacheEnabled(true);

        mUserHoroscope = new Horoscope(context);
        mCounterHoroscope = new Horoscope(context);
    }

    public void setHighlightPlanets(String[] planetNames) {
        mUserHoroscope.setSynastryHighlightPlanetNames(planetNames);
        mUserHoroscope.setHighlightPlanet(planetNames[0]);
        mCounterHoroscope.setHighlightPlanet(planetNames[1]);
    }

    public void setStrokeColor(int color) {
        mUserHoroscope.setStrokeColor(color);
        mCounterHoroscope.setStrokeColor(color);
    }

    public void setStrokeWidth(float width) {
        mUserHoroscope.setStrokeWidth(width);
        mCounterHoroscope.setStrokeWidth(width);
    }

    public void setPaintAntiAlias(boolean isAntiAlias) {
        mUserHoroscope.setPaintAntiAlias(isAntiAlias);
        mCounterHoroscope.setPaintAntiAlias(isAntiAlias);
    }

    public void setSynastryHoroscopeModel(SynastryHoroscopeModel synastryHoroscopeModel) {
        setUserHoroscopeModel(synastryHoroscopeModel.getUserHoroscopeModel());
        setCounterHoroscopeModel(synastryHoroscopeModel.getCounterHoroscopeModel());
        setSynastryAspects(synastryHoroscopeModel.getSynastryAspectList());
    }

    public void setUserHoroscopeModel(HoroscopeModel horoscopeModel) {
        mUserHoroscope.setHoroscopeModel(horoscopeModel);
        mUserHoroscopeModel = horoscopeModel;
    }

    public void setCounterHoroscopeModel(HoroscopeModel horoscopeModel) {
        mCounterHoroscope.setHoroscopeModel(horoscopeModel);
        mCounterHoroscopeModel = horoscopeModel;
    }

    public void setSynastryAspects(List<SynastryAspect> synastryAspects) {
        List<Aspect> aspectList = new ArrayList<>();
        for (SynastryAspect synastryAspect : synastryAspects)
            aspectList.add(synastryAspect.getAspect());

        mUserHoroscope.setAspects(aspectList);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mUserHoroscope.setCanvas(canvas);
        mCounterHoroscope.setCanvas(canvas);
        draw();
    }

    private void draw() {
        if(mUserHoroscopeModel == null || mCounterHoroscopeModel == null)
            return;

        int width = getWidth() / 2;
        int height = getHeight() / 2;
        int outerRadius = height * 7 / 10;
        int middleRadius = (int)(height * 6.5 / 10);
        int radius = height * 3 / 5;

        mUserHoroscope.draw(radius, radius, getWidth() / 2, getHeight() / 2);

        mCounterHoroscope.setAngle(getAngle());
        mCounterHoroscope.drawBenchmarkCircle(width, height, radius);
        mCounterHoroscope.drawBenchmark(width, height, outerRadius, radius);
        mCounterHoroscope.drawGraduations(width, height, middleRadius, radius);
        mCounterHoroscope.drawPlanets(width, height, outerRadius, middleRadius);
    }

    private int getAngle() {
        Ascendent userAscendent = mUserHoroscopeModel.getAscendent();
        Ascendent counterAscendent = mCounterHoroscopeModel.getAscendent();
        Planet userASC = new Planet(null, null, null, null, userAscendent.getLocation(), userAscendent.getConstellation(), Planet.ASC);
        Planet counterASC = new Planet(null, null, null, null, counterAscendent.getLocation(), counterAscendent.getConstellation(), Planet.ASC);

        return ConstellationCalculator.getAngleDifference(new Planet[]{userASC, counterASC});
    }

    public void refresh() {
        mUserHoroscope.refresh();
        mCounterHoroscope.refresh();
        invalidate();
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Horoscope);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Horoscope, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        int strokeColor = typedArray.getColor(R.styleable.Horoscope_stroke_color, Color.WHITE);
        setStrokeColor(strokeColor);

        float strokeWidth = typedArray.getDimension(R.styleable.Horoscope_stroke_width, 1.0f);
        setStrokeWidth(strokeWidth);

        boolean isAntiAlias = typedArray.getBoolean(R.styleable.Horoscope_isAntiAlias, true);
        setPaintAntiAlias(isAntiAlias);

        typedArray.recycle();
    }
}
