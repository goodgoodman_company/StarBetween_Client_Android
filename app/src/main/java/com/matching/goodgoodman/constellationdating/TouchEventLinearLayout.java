package com.matching.goodgoodman.constellationdating;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * focus를 잃어도 touchEvent를 받을 수 있는 LinearLayout입니다.
 * @author Hyeong_Wook
 */

public class TouchEventLinearLayout extends LinearLayout {
    private OnTouchListener mOnTouchListener;

    public TouchEventLinearLayout(Context context) {
        super(context);
    }

    public TouchEventLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TouchEventLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        mOnTouchListener.onTouch(this, motionEvent);
        return super.dispatchTouchEvent(motionEvent);
    }

    @Override
    public void setOnTouchListener(OnTouchListener onTouchListener) {
        mOnTouchListener = onTouchListener;
    }
}
