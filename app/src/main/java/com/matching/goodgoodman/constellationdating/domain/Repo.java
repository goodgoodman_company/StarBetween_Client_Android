package com.matching.goodgoodman.constellationdating.domain;

/**
 * Created by Songmoo on 2017-04-21.
 */

public class Repo {
    int id;
    String name;
    String full_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
}
