package com.matching.goodgoodman.constellationdating.firebase;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.matching.goodgoodman.constellationdating.LoginActivity;
import com.matching.goodgoodman.constellationdating.R;
import com.matching.goodgoodman.constellationdating.SettingPresenter;
import com.matching.goodgoodman.constellationdating.firebase.model.Chat;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.NetworkManager;
import com.matching.goodgoodman.constellationdating.utils.DeviceStatusHelper;

public class ChatAlarmService extends Service {
    public static final String CHAT_NOTIFICATION_CHANNEL = "com.matching.goodgoodman.constellationdating.channel";
    public static final String CHAT_NOTIFICATION = "chat";

    private NetworkManager mNetworkManager;
    private UserModel mUserModel;
    private SharedPreferences mSharedPreferences;

    private boolean mIsFirstAlarm = true;

    public ChatAlarmService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        mIsFirstAlarm = true;
        return null;
    }

    @Override
    public void onCreate() {
        unregisterRestartAlarm();
        super.onCreate();

        initialData();
        initialChatAlarms();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mIsFirstAlarm = true;
        startForeground(0, new Notification());

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new NotificationCompat.Builder(this, CHAT_NOTIFICATION_CHANNEL)
                .setContentTitle("")
                .setContentText("")
                .setSmallIcon(R.drawable.app_icon_512)
                .build();

        notificationManager.notify(0, notification);
        notificationManager.cancel(0);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        registerRestartAlarm();
    }

    private void initialData() {
        mNetworkManager = NetworkManager.getInstance(getApplicationContext());
        mUserModel = UserPreference.getInstance().getUserModel();
    }

    private void initialChatAlarms() {
        //TODO::서버로부터 정상값이 올 경우 주석을 지우고 사용
        mNetworkManager.getUserChats(mUserModel.getNo(), result -> {
            for(com.matching.goodgoodman.constellationdating.server.model.ChatRoom chatRoom : result.getChatRoomList())
                setChatAlarms(chatRoom.getRoomKey());
        });

        /*//TODO::서버로부터 더미값이 아닌 제대로 된 값이 올 경우 삭제
        final String CHAT_ROOM_HASH = "-KvGioF9THrhgqMgj8dp";
        setChatAlarms(CHAT_ROOM_HASH);*/
    }

    public void setChatAlarms(String chatRoomKey) {
        ChatRoom chatRoom = new ChatRoom(chatRoomKey, null, null);

        chatRoom.getChatRoomReference().limitToLast(1).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(mIsFirstAlarm){
                    mIsFirstAlarm = false;
                    return;
                }

                try {
                    Chat chat = dataSnapshot.getValue(Chat.class);
                    showChatNotification(chat.getMessage());
                } catch (DatabaseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        //TODO::서버로부터 더미값이 아닌 제대로 된 값이 올 경우 삭제
        /*new CountDownTimer(1000*1000, 5000) {
            public void onTick(long millisUntilFinished) {
                chatRoom.sendChat(new Chat("12312312", "안녕하십니까 친구친구"));
            }
            public void onFinish() {
            }
        }.start();*/
    }

    private boolean onAlarm() {
        if(mSharedPreferences == null)
            mSharedPreferences = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
        return mSharedPreferences.getBoolean(SettingPresenter.CHATTING_NOTICE_KEY, true);
    }

    private void showChatNotification(String body) {
        if(DeviceStatusHelper.isAppRunningInForeground(getApplicationContext()) || !onAlarm())
            return;

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(CHAT_NOTIFICATION, true);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHAT_NOTIFICATION_CHANNEL)
                .setContentTitle("City of star")
                .setContentText(body)
                .setSmallIcon(R.drawable.app_icon_512)
                .setContentIntent(pendingIntent)
                .setVibrate(new long[]{0, 1000})
                .setAutoCancel(true)
                .build();

        notificationManager.notify(1, notification);
    }

    private void registerRestartAlarm() {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, getRestartIntent(), 0);
        long firstTime = SystemClock.elapsedRealtime() + 1 * 1000;

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime, 1 * 1000, pendingIntent);
    }

    private void unregisterRestartAlarm() {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, getRestartIntent(), 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        alarmManager.cancel(pendingIntent);
    }

    private Intent getRestartIntent() {
        Intent intent = new Intent(this, RestartService.class);
        intent.setAction("ACTION.RESTART.ChatAlarmService");

        return intent;
    }
}
