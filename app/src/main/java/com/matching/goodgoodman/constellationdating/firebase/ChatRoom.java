package com.matching.goodgoodman.constellationdating.firebase;

import com.matching.goodgoodman.constellationdating.firebase.model.Chat;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by GoodGoodMan on 2017. 10. 2..
 */

public class ChatRoom {
    public static final int MAX_NUMBER_OF_CHILDREN = 100;
    public ChatRoom(String keyHash, UserModel userModel, UserModel opponentUserModel) {
        mKeyHash = keyHash;
        mChatRoomReference = FirebaseChatUtil.getChatRoomReference(mKeyHash);
        mUserModel = userModel;
        mOpponentUserModel = opponentUserModel;
    }

    private final String mKeyHash;
    private final DatabaseReference mChatRoomReference;
    private UserModel mUserModel;
    private UserModel mOpponentUserModel;


    public String getKeyHash() {
        return mKeyHash;
    }

    public DatabaseReference getChatRoomReference() {
        return mChatRoomReference;
    }

    public UserModel getUserModel() {
        return mUserModel;
    }

    public UserModel getOpponentUserModel() {
        return mOpponentUserModel;
    }

    public Task<Void> sendChat(Chat chat) {
        return mChatRoomReference.push().setValue(chat);
    }
}
