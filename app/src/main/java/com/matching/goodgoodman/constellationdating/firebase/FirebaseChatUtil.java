package com.matching.goodgoodman.constellationdating.firebase;

import com.matching.goodgoodman.constellationdating.firebase.model.Chat;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by GoodGoodMan on 2017. 10. 2..
 */

public class FirebaseChatUtil {
    private static final String CHAT_ROOM_NODE_NAME = "chatroom";

    private static final FirebaseDatabase sDatabase = FirebaseDatabase.getInstance();
    private static final DatabaseReference sRootReference = sDatabase.getReference();
    private static final DatabaseReference sChatRoomNodeReferece = sRootReference.child(CHAT_ROOM_NODE_NAME);

    public static DatabaseReference getChatRoomReference(String roomKey) {
        return sChatRoomNodeReferece.child(roomKey);
    }


    public static Task<Void> sendChat(String roomKey, Chat chat) {
        return sChatRoomNodeReferece.child(roomKey).push().setValue(chat);
    }
}
