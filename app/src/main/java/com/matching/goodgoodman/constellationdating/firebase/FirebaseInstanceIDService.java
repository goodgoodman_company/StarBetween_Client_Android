package com.matching.goodgoodman.constellationdating.firebase;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshedToken);
    }


    /**
     * 토큰이 리프래쉬 될 때, 무언가 작업이 필요하다면 해당 메소드를 수정합니다.
     * @param token
     */
    private void sendRegistrationToServer(String token) {

    }
}