package com.matching.goodgoodman.constellationdating.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.matching.goodgoodman.constellationdating.LoginActivity;
import com.matching.goodgoodman.constellationdating.R;
import com.matching.goodgoodman.constellationdating.SettingPresenter;
import com.matching.goodgoodman.constellationdating.utils.ChattingListRefreshHandler;
import com.matching.goodgoodman.constellationdating.utils.DeviceStatusHelper;

public class FirebaseMessageService extends FirebaseMessagingService {
    private SharedPreferences mSharedPreferences;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        if (remoteMessage.getData().size() > 0) {
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage.getNotification().getBody());
        }

    }

    private boolean onAlarm() {
        if(mSharedPreferences == null)
            mSharedPreferences = getApplicationContext().getSharedPreferences("pref", Context.MODE_PRIVATE);
        return mSharedPreferences.getBoolean(SettingPresenter.LIKE_NOTICE_KEY, true);
    }

    /**
     * 푸시 메세지가 왔을때 처리해 주는 메소드입니다.
     * @param messageBody FCM으로 타고오는 메세지의 내용.
     */
    private void sendNotification(String messageBody) {
        if(DeviceStatusHelper.isAppRunningInForeground(getApplicationContext()) || !onAlarm()) {
            ChattingListRefreshHandler.getInstance().notifyChangeData(true);
            return;
        }
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(ChatAlarmService.CHAT_NOTIFICATION, true);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, ChatAlarmService.CHAT_NOTIFICATION_CHANNEL)
                        .setContentTitle("City of stars")
                        .setContentText(messageBody)
                        .setSmallIcon(R.drawable.app_icon_512)
                        .setVibrate(new long[]{0, 1000})
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, notificationBuilder.build());
    }
}
