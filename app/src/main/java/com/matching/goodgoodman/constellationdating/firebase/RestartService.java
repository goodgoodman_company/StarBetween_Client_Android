package com.matching.goodgoodman.constellationdating.firebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class RestartService extends BroadcastReceiver {
    public RestartService() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("ACTION.RESTART.ChatAlarmService")) {
            Intent restartIntent = new Intent(context, ChatAlarmService.class);
            context.startService(restartIntent);
        }

        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Intent restartIntent = new Intent(context, ChatAlarmService.class);
            context.startService(restartIntent);
        }
    }
}
