package com.matching.goodgoodman.constellationdating.firebase.model;

import com.google.firebase.database.PropertyName;
import com.sign.chatmodule.helper.DateHelper;

/**
 * Created by GoodGoodMan on 2017. 10. 2..
 */

public class Chat {
    public Chat() { }

    public Chat(String userID, String message) {
        mUserID = userID;
        mMessage = message;
        mTime = DateHelper.getCurrentTime();
    }

    @PropertyName("user_id")
    private String mUserID;
    @PropertyName("message")
    private String mMessage;
    @PropertyName("time")
    private long mTime;

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String userID) {
        mUserID = userID;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }
}
