package com.matching.goodgoodman.constellationdating.inAppBilling;

import android.app.Activity;
import android.content.Context;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.matching.goodgoodman.constellationdating.server.SuccessCallBack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Hyeong_Wook on 2017-10-08.
 */

public class InAppBillingHelper {
    public static final String[] SKU_ARRAY = {"star_280", "star_2750", "star_5640", "premium"};

    private BillingClient mBillingClient;
    private SuccessCallBack<String> mConsumeSuccessCallBack;

    public InAppBillingHelper(Context context) {
        this(context, null);
    }

    public InAppBillingHelper(Context context, SuccessCallBack<String> consumeSuccessCallBack) {
        mBillingClient = BillingClient.newBuilder(context).setListener(this::onPurchasesUpdated).build();
        mConsumeSuccessCallBack = consumeSuccessCallBack;
    }

    public void connect(SuccessCallBack<Integer> successCallBack) {
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(int responseCode) {
                successCallBack.onCallBack(responseCode);
            }

            @Override
            public void onBillingServiceDisconnected() {
                onEndConnection();
            }
        });
    }

    public void getBillingItemList(SuccessCallBack<List<SkuDetails>> successCallBack) {
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(new ArrayList<>(Arrays.asList(SKU_ARRAY))).setType(BillingClient.SkuType.INAPP);
        mBillingClient.querySkuDetailsAsync(params.build(), (responseCode, skuDetailsList) -> {
            if (responseCode == BillingClient.BillingResponse.OK)
                successCallBack.onCallBack(skuDetailsList);
        });
    }

    public void consumeItem(String skuId, Activity activity) {
        BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                .setSku(skuId)
                .setType(BillingClient.SkuType.INAPP)
                .build();
        mBillingClient.launchBillingFlow(activity, flowParams);
    }

    private void onPurchasesUpdated(int responseCode, List<Purchase> purchases) {
        if (responseCode != BillingClient.BillingResponse.OK)
            return;

        for (final Purchase purchase : purchases) {
            mBillingClient.consumeAsync(purchase.getPurchaseToken(), (consumeResponseCode, purchaseToken) -> {
                if(consumeResponseCode == BillingClient.BillingResponse.OK && mConsumeSuccessCallBack != null)
                    mConsumeSuccessCallBack.onCallBack(purchase.getSku());
            });
        }
    }

    public void disconnect() {
        mBillingClient.endConnection();
    }

    private void onEndConnection() {

    }

    public static int getIndexSkuId(String skuId) {
        for(int i = 0; i < SKU_ARRAY.length; i++) {
            if(skuId.equals(SKU_ARRAY[i]))
                return i;
        }
        return -1;
    }
}
