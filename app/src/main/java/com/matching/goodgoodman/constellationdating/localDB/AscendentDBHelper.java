package com.matching.goodgoodman.constellationdating.localDB;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import dao.Ascendent;
import dao.AscendentDao;
import dao.DaoMaster;
import dao.DaoSession;
import dao.SiderealTime;

/**
 * Created by Hyeong_Wook on 2017-09-13.
 */

public class AscendentDBHelper {
    public static final String DATABASE_NAME = "ascendent.db";

    private Context mContext;
    private AscendentDao mAscendentDao = null;

    /**
     * 기본적인 생성자.
     *
     * @param context 어플리케이션의 context, getApplicationContext()로 사용
     */
    public AscendentDBHelper(Context context) {
        mContext = context;

        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, DATABASE_NAME, null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        mAscendentDao = daoSession.getAscendentDao();
    }


    /**
     * 데이터를 DB에 삽입한다.
     *
     * @param hour          siderealTime의 시
     * @param minute        siderealTime의 분
     * @param second        siderealTime의 초
     * @param latitude      user의 위도
     * @param constellation 별자리
     * @param location      별자리 위치
     * @return
     */
    public boolean insert(int hour, int minute, int second, double latitude, double location, String constellation) {
        boolean isNotExist = (getAscendent(hour, minute, second, latitude) == null);
        if (isNotExist)
            mAscendentDao.insertInTx(new Ascendent(null, hour, minute, second, latitude, location, constellation));

        return isNotExist;
    }

    /**
     * 테이블에 데이터리스트를 삽입한다.
     *
     * @param mediumCoeliList 삽입할 데이터리스트
     */
    public void insert(List<Ascendent> mediumCoeliList) {
        mAscendentDao.insertInTx(mediumCoeliList);
    }

    /**
     * 테이블에 있는 모든 데이터를 삭제한다.
     */
    public void deleteAll() {
        mAscendentDao.deleteAll();
    }

    /**
     * siderealTime과 위도에 맞는 ASC를 얻는다.
     * 만약 이에 맞는것이 없을 경우 null값을 반환한다.
     *
     * @param hour     siderealTime의 시
     * @param minute   siderealTime의 분
     * @param second   siderealTime의 초
     * @param latitude 위도
     * @return 원하는 객체
     */
    public Ascendent getAscendent(int hour, int minute, int second, double latitude) {
        QueryBuilder<Ascendent> queryBuilder = mAscendentDao.queryBuilder();
        return queryBuilder
                .where(AscendentDao.Properties.Hour.eq(hour))
                .where(AscendentDao.Properties.Minute.eq(minute))
                .where(AscendentDao.Properties.Second.eq(second))
                .where(AscendentDao.Properties.Latitude.eq(latitude))
                .unique();
    }

    /**
     * 원하는 값의 근사값 4가지를 얻는다.
     * 0, 2번째 index의 latitude < 1, 3번째 index latitude
     * 0, 1번째 index의 siderealTime < 2, 3번째 index의 siderealTime
     * 일치하는 값이 있을 경우 그 양쪽 값을 얻는다.
     * @param hour     siderealTime의 시
     * @param minute   siderealTime의 분
     * @param second   siderealTime의 초
     * @param latitude 위도
     * @return 원하는 근사값을 담은 array
     */
    public Ascendent[] getApproximateAscendent(int hour, int minute, int second, double latitude) {
        int siderealTimeSeconds = hour * 3600 + minute * 60 + second;
        SiderealTime[] siderealTimes = new SiderealTime[]{null, null};
        double[] latitudes = new double[]{-1, -1};
        Ascendent[] result = new Ascendent[4];

        List<Ascendent> ascendentList = getAscendentAll();
        for (int i = 0; i < ascendentList.size(); i++) {
            Ascendent ascendent = ascendentList.get(i);
            int ascendentSeconds = ascendent.getHour() * 3600 + ascendent.getMinute() * 60 + ascendent.getSecond();
            int siderealTimeDifference = ascendentSeconds - siderealTimeSeconds;
            double latitudeDifference = ascendent.getLatitude() - latitude;
            int lowIndex = i > 0? i - 1 : i;

            if(latitudes[0] == -1 && latitudeDifference > 0) {
                Ascendent lowAscendent = ascendentList.get(lowIndex);
                latitudes[0] = lowAscendent.getLatitude();
                latitudes[1] = ascendent.getLatitude();
            }

            if (siderealTimes[0] == null && siderealTimeDifference > 0) {
                Ascendent lowAscendent = ascendentList.get(i - 1);
                siderealTimes[0] = new SiderealTime(null, null, null, lowAscendent.getHour(), lowAscendent.getMinute(), lowAscendent.getSecond());
                siderealTimes[1] = new SiderealTime(null, null, null, ascendent.getHour(), ascendent.getMinute(), ascendent.getSecond());
                break;
            }
        }
        result[0] = getAscendent(siderealTimes[0].getHour(), siderealTimes[0].getMinute(), siderealTimes[0].getSecond(), latitudes[0]);
        result[1] = getAscendent(siderealTimes[0].getHour(), siderealTimes[0].getMinute(), siderealTimes[0].getSecond(), latitudes[1]);
        result[2] = getAscendent(siderealTimes[1].getHour(), siderealTimes[1].getMinute(), siderealTimes[1].getSecond(), latitudes[0]);
        result[3] = getAscendent(siderealTimes[1].getHour(), siderealTimes[1].getMinute(), siderealTimes[1].getSecond(), latitudes[1]);

        return result;
    }

    /**
     * 위도에 맞는 데이터리스트를 얻는다.
     * @param latitude 위도
     * @return ascendentList
     */
    public List<Ascendent> getAscendentList(double latitude) {
        QueryBuilder<Ascendent> queryBuilder = mAscendentDao.queryBuilder();
        return queryBuilder
                .where(AscendentDao.Properties.Latitude.eq(latitude))
                .list();
    }

    /**
     * siderealTime에 맞는 데이터리스트를 얻는다.
     * @param hour siderealTime의 시
     * @param minute siderealTime의 분
     * @param second siderealTime의 초
     * @return ascendentList
     */
    public List<Ascendent> getAscendentList(int hour, int minute, int second) {
        QueryBuilder<Ascendent> queryBuilder = mAscendentDao.queryBuilder();
        return queryBuilder
                .where(AscendentDao.Properties.Hour.eq(hour))
                .where(AscendentDao.Properties.Minute.eq(minute))
                .where(AscendentDao.Properties.Second.eq(second))
                .list();
    }

    /**
     * 테이블에 있는 모든 데이터객체들을 얻는다.
     * @return 데이터 리스트
     */
    public List<Ascendent> getAscendentAll() {
        QueryBuilder<Ascendent> queryBuilder = mAscendentDao.queryBuilder();
        return queryBuilder.list();
    }
}
