package com.matching.goodgoodman.constellationdating.localDB;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import dao.DaoMaster;
import dao.DaoSession;
import dao.Description;
import dao.DescriptionDao;

/**
 * description localDB와의 접근을 쉽게 하기 위한 greenDAO 래핑 클래스
 * insert, delete, get 등의 단순 메소드 호출로 DB의 데이터를 가져온다.
 * @author Hyeong_Wook
 */

public class DescriptionDBHelper {
    public static final String LANGUAGE_KOREAN = "string_table_korean.db";
    public static final String LANGUAGE_ITALY = "string_table_italy.db";
    public static final String LANGUAGE_FRANCE = "string_table_france.db";
    public static final String LANGUAGE_ENGLISH = "string_table_english.db";

    public static final String[] LANGUAGES = {LANGUAGE_ENGLISH, LANGUAGE_KOREAN};

    private Context mContext;
    private DescriptionDao mDescriptionDao = null;

    /**
     * 기본적인 생성자.
     * @param context 어플리케이션의 context, getApplicationContext()로 사용
     * @param language 사용할 언어, DescriptionDBHelper.LANGUAGE_KOREAN과 같은 방식으로 사용
     */
    public DescriptionDBHelper(Context context, String language) {
        mContext = context;
        setLanguage(language);
    }

    public void setLanguage(String language) {
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, language, null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        mDescriptionDao = daoSession.getDescriptionDao();
    }

    /**
     * 테이블에 데이터를 삽입한다.
     * @param id 데이터의 id, id가 중복일 경우 삽입되지 않는다.
     * @param comment 실제 데이터
     * @return 데이터 id의 중복여부.
     */
    public boolean insert(int id, String comment) {
        boolean isNotExist = (getDescription(id) == null);
        if(isNotExist)
            mDescriptionDao.insert(new Description(null, id, comment));

        return isNotExist;
    }

    /**
     * 테이블에 데이터리스트를 삽입한다.
     * @param descriptionList 삽입할 데이터리스트
     */
    public void insert(List<Description> descriptionList) {
        mDescriptionDao.insertInTx(descriptionList);
    }

    /**
     * 테이블에 있는 데이터를 삭제한다.
     * @param id 삭제할 데이터의 id
     */
    public void delete(int id) {
        Description deleteEntity = getDescription(id);
        if(deleteEntity != null)
            mDescriptionDao.delete(getDescription(id));
    }

    /**
     * 테이블에 있는 모든 데이터를 삭제한다.
     */
    public void deleteAll() {
        mDescriptionDao.deleteAll();
    }

    /**
     * 데이터의 id를 통해 테이블에 있는 데이터 객체를 얻는다.
     * @param id 원하는 데이터의 id
     * @return 데이터
     */
    public Description getDescription(int id) {
        QueryBuilder<Description> queryBuilder = mDescriptionDao.queryBuilder();
        return queryBuilder.where(DescriptionDao.Properties.IntId.eq(id)).unique();
    }

    /**
     * 테이블에 있는 모든 데이터객체들을 얻는다.
     * @return 데이터 리스트
     */
    public List<Description> getDescriptionAll() {
        QueryBuilder<Description> queryBuilder = mDescriptionDao.queryBuilder();
        return queryBuilder.list();
    }
}
