package com.matching.goodgoodman.constellationdating.localDB;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import dao.DaoMaster;
import dao.DaoSession;
import dao.LongitudeAdjustment;
import dao.LongitudeAdjustmentDao;

/**
 * longitudeAdjustment localDB와의 접근을 쉽게 하기 위한 greenDAO 래핑 클래스
 * insert, delete, get 등의 단순 메소드 호출로 DB의 데이터를 가져온다.
 * @author Hyeong_Wook
 */

public class LongitudeAdjustmentDBHelper {
    public static final String DATABASE_NAME = "longitudeAdjustment.db";

    private Context mContext;
    private LongitudeAdjustmentDao mLongitudeAdjustmentDao = null;

    /**
     * 기본적인 생성자.
     * @param context 어플리케이션의 context, getApplicationContext()로 사용
     */
    public LongitudeAdjustmentDBHelper(Context context) {
        mContext = context;

        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, DATABASE_NAME, null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        mLongitudeAdjustmentDao = daoSession.getLongitudeAdjustmentDao();
    }

    /**
     * 데이터를 DB에 삽입한다.
     * 이미 longitude가 있는 데이터를 삽입시 삽입이 되지 않는다.
     * @param longitude 경도
     * @param hour 시
     * @param minute 분
     * @return 삽입 성공여부
     */
    public boolean insert(int longitude, int hour, int minute) {
        boolean isNotExist = (getLongitudeAdjustment(longitude) == null);
        if(isNotExist)
            mLongitudeAdjustmentDao.insertInTx(new LongitudeAdjustment(null, longitude, hour, minute));

        return isNotExist;
    }

    /**
     * 테이블에 데이터리스트를 삽입한다.
     * @param longitudeAdjustmentList 삽입할 데이터리스트
     */
    public void insert(List<LongitudeAdjustment> longitudeAdjustmentList) {
        mLongitudeAdjustmentDao.insertInTx(longitudeAdjustmentList);
    }

    /**
     * 테이블에 있는 모든 데이터를 삭제한다.
     */
    public void deleteAll() {
        mLongitudeAdjustmentDao.deleteAll();
    }

    /**
     * 경도에 맞는 longitudeAdjustment객체를 얻는다.
     * @param longitude 경도
     * @return 원하는 객체
     */
    public LongitudeAdjustment getLongitudeAdjustment(int longitude) {
        QueryBuilder<LongitudeAdjustment> queryBuilder = mLongitudeAdjustmentDao.queryBuilder();
        return queryBuilder.where(LongitudeAdjustmentDao.Properties.Longitude.eq(longitude)).unique();
    }

    /**
     * 테이블에 있는 모든 데이터객체들을 얻는다.
     * @return 데이터 리스트
     */
    public List<LongitudeAdjustment> getLongitudeAdjustmentAll() {
        QueryBuilder<LongitudeAdjustment> queryBuilder = mLongitudeAdjustmentDao.queryBuilder();
        return queryBuilder.list();
    }
}
