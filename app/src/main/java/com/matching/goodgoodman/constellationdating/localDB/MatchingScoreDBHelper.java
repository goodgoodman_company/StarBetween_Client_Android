package com.matching.goodgoodman.constellationdating.localDB;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import dao.DaoMaster;
import dao.DaoSession;
import dao.MatchingScore;
import dao.MatchingScoreDao;

/**
 * Created by Hyeong_Wook on 2017-11-04.
 */

public class MatchingScoreDBHelper {
    public static final String DATABASE_NAME = "matchingScore.db";

    private Context mContext;
    private MatchingScoreDao mMatchingScoreDao;

    /**
     * 기본적인 생성자.
     *
     * @param context 어플리케이션의 context, getApplicationContext()로 사용
     */
    public MatchingScoreDBHelper(Context context) {
        mContext = context;
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, DATABASE_NAME, null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        mMatchingScoreDao = daoSession.getMatchingScoreDao();
    }

    /**
     * 데이터를 DB에 삽입한다.
     * 이미 matchingScore가 있는 데이터를 삽입시 삽입이 되지 않는다.
     * @return 삽입 여부
     */
    public boolean insert(int userId, int counterId, int score) {
        boolean isNotExist = (getMatchingScore(userId, counterId) == null);
        if (isNotExist)
            mMatchingScoreDao.insertInTx(new MatchingScore(null, userId, counterId, score));

        return isNotExist;
    }

    /**
     * 테이블에 있는 모든 데이터를 삭제한다.
     */
    public void deleteAll() {
        mMatchingScoreDao.deleteAll();
    }

    /**
     * 조건에 맞는 matchingScore 객체를 얻는다.
     * @param userId userId
     * @param counterId counterId
     * @return planet
     */
    public MatchingScore getMatchingScore(int userId, int counterId) {
        QueryBuilder<MatchingScore> queryBuilder = mMatchingScoreDao.queryBuilder();
        return queryBuilder
                .where(MatchingScoreDao.Properties.UserId.eq(userId))
                .where(MatchingScoreDao.Properties.CounterId.eq(counterId))
                .unique();
    }
}
