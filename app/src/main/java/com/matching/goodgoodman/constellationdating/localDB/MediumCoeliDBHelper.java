package com.matching.goodgoodman.constellationdating.localDB;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import dao.DaoMaster;
import dao.DaoSession;
import dao.MediumCoeli;
import dao.MediumCoeliDao;

/**
 * MediumCoeli localDB와의 접근을 쉽게 하기 위한 greenDAO 래핑 클래스
 * insert, delete, get 등의 단순 메소드 호출로 DB의 데이터를 가져온다.
 * @author Hyeong_Wook
 */

public class MediumCoeliDBHelper {
    public static final String DATABASE_NAME = "mediumCoeli.db";

    private Context mContext;
    private MediumCoeliDao mMediumCoeliDao = null;

    /**
     * 기본적인 생성자.
     * @param context 어플리케이션의 context, getApplicationContext()로 사용
     */
    public MediumCoeliDBHelper(Context context) {
        mContext = context;

        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, DATABASE_NAME, null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        mMediumCoeliDao = daoSession.getMediumCoeliDao();
    }


    /**
     * 데이터를 DB에 삽입한다.
     * @param hour siderealTime의 시
     * @param minute siderealTime의 분
     * @param second siderealTime의 초
     * @param constellation 별자리
     * @param location 별자리 위치
     * @return
     */
    public boolean insert(int hour, int minute, int second, String constellation, int location) {
        boolean isNotExist = (getMediumCoeli(hour, minute, second) == null);
        if(isNotExist)
            mMediumCoeliDao.insertInTx(new MediumCoeli(null, hour, minute, second, constellation, location));

        return isNotExist;
    }

    /**
     * 테이블에 데이터리스트를 삽입한다.
     * @param mediumCoeliList 삽입할 데이터리스트
     */
    public void insert(List<MediumCoeli> mediumCoeliList) {
        mMediumCoeliDao.insertInTx(mediumCoeliList);
    }

    /**
     * 테이블에 있는 모든 데이터를 삭제한다.
     */
    public void deleteAll() {
        mMediumCoeliDao.deleteAll();
    }

    /**
     * siderealTime에 맞는 MediumCoeli를 얻는다.
     * 만약 이에 맞는것이 없을 경우 null값을 반환한다.
     * @param hour siderealTime의 시
     * @param minute siderealTime의 분
     * @param second siderealTime의 초
     * @return 원하는 객체
     */
    public MediumCoeli getMediumCoeli(int hour, int minute, int second) {
        QueryBuilder<MediumCoeli> queryBuilder = mMediumCoeliDao.queryBuilder();
        return queryBuilder
                .where(MediumCoeliDao.Properties.Hour.eq(hour))
                .where(MediumCoeliDao.Properties.Minute.eq(minute))
                .where(MediumCoeliDao.Properties.Second.eq(second))
                .unique();
    }

    /**
     * 원하는 값의 근사값 2가지를 얻는다.
     * 0번째 index의 siderealTime < 1번째 index siderealTime
     * 일치하는 값이 있을 경우 그 양쪽 값을 얻는다.
     * @param hour siderealTime의 시
     * @param minute siderealTime의 분
     * @param second siderealTime의 초
     * @return 원하는 근사값을 담은 array
     */
    public MediumCoeli[] getApproximateMediumCoeli(int hour, int minute, int second) {
        int siderealTimeSeconds = hour * 3600 + minute * 60 + second;
        int approximateValue = -1;
        List<MediumCoeli> mediumCoeliList = getMediumCoeliAll();
        MediumCoeli[] result = new MediumCoeli[] { new MediumCoeli(null), mediumCoeliList.get(0) };
        for(MediumCoeli mediumCoeli : mediumCoeliList) {
            int mediumCoeliSeconds = mediumCoeli.getHour() * 3600 + mediumCoeli.getMinute() * 60 + mediumCoeli.getSecond();
            int difference = mediumCoeliSeconds - siderealTimeSeconds;
            int absDifference = Math.abs(difference);

            if(absDifference < approximateValue || approximateValue == -1) {
                approximateValue = absDifference;
                if(difference < 0)
                    result[0] = mediumCoeli;
            }

            if(difference > 0) {
                result[1] = mediumCoeli;
                return result;
            }
        }

        return result;
    }

    /**
     * 테이블에 있는 모든 데이터객체들을 얻는다.
     * @return 데이터 리스트
     */
    public List<MediumCoeli> getMediumCoeliAll() {
        QueryBuilder<MediumCoeli> queryBuilder = mMediumCoeliDao.queryBuilder();
        return queryBuilder.list();
    }
}
