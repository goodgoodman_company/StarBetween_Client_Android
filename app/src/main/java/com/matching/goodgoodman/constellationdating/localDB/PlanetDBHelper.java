package com.matching.goodgoodman.constellationdating.localDB;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import dao.DaoMaster;
import dao.DaoSession;
import dao.Planet;
import dao.PlanetDao;

/**
 * Created by Hyeong_Wook on 2017-09-22.
 */

public class PlanetDBHelper {
    public static final String DATABASE_NAME = "planet.db";

    private Context mContext;
    private PlanetDao mPlanetDao;

    /**
     * 기본적인 생성자.
     *
     * @param context 어플리케이션의 context, getApplicationContext()로 사용
     */
    public PlanetDBHelper(Context context) {
        mContext = context;

        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, DATABASE_NAME, null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        mPlanetDao = daoSession.getPlanetDao();
    }

    /**
     * 데이터를 DB에 삽입한다.
     * 이미 planet가 있는 데이터를 삽입시 삽입이 되지 않는다.
     * @param year 년
     * @param month 월
     * @param day 일
     * @param name 이름
     * @param constellation 별자리
     * @param location 별자리 위치
     * @return 삽입여부
     */
    public boolean insert(int year, int month, int day, String name, String constellation, double location) {
        boolean isNotExist = (getPlanet(year, month, day, name) == null);
        if (isNotExist)
            mPlanetDao.insertInTx(new Planet(null, year, month, day, location, constellation, name));

        return isNotExist;
    }

    /**
     * 테이블에 데이터리스트를 삽입한다.
     *
     * @param planetList 삽입할 데이터리스트
     */
    public void insert(List<Planet> planetList) {
        mPlanetDao.insertInTx(planetList);
    }

    /**
     * 테이블에 있는 모든 데이터를 삭제한다.
     */
    public void deleteAll() {
        mPlanetDao.deleteAll();
    }

    /**
     * 조건에 맞는 planet 객체를 얻는다.
     * @param year 년
     * @param month 월
     * @param day 일
     * @param name 행성이름
     * @return planet
     */
    public Planet getPlanet(int year, int month, int day, String name) {
        QueryBuilder<Planet> queryBuilder = mPlanetDao.queryBuilder();
        return queryBuilder.where(PlanetDao.Properties.Year.eq(year))
                .where(PlanetDao.Properties.Month.eq(month))
                .where(PlanetDao.Properties.Day.eq(day))
                .where(PlanetDao.Properties.Name.eq(name))
                .unique();
    }

    /**
     * 유저의 날짜에 맞는 행성리스트를 얻는다.
     * @param year 년
     * @param month 월
     * @param day 일
     * @return 행성리스트
     */
    public List<Planet> getPlanetList(int year, int month, int day) {
        QueryBuilder<Planet> queryBuilder = mPlanetDao.queryBuilder();
        return queryBuilder.where(PlanetDao.Properties.Year.eq(year))
                .where(PlanetDao.Properties.Month.eq(month))
                .where(PlanetDao.Properties.Day.eq(day))
                .list();
    }

    /**
     * 테이블에 있는 모든 데이터객체들을 얻는다.
     *
     * @return 데이터 리스트
     */
    public List<Planet> getPlanetAll() {
        QueryBuilder<Planet> queryBuilder = mPlanetDao.queryBuilder();
        return queryBuilder.list();
    }
}
