package com.matching.goodgoodman.constellationdating.localDB;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import dao.DaoMaster;
import dao.DaoSession;
import dao.SiderealTimeAdjustment;
import dao.SiderealTimeAdjustmentDao;

/**
 * siderealTime localDB와의 접근을 쉽게 하기 위한 greenDAO 래핑 클래스
 * insert, delete, get 등의 단순 메소드 호출로 DB의 데이터를 가져온다.
 * @author Hyeong_Wook
 */

public class SiderealTimeAdjustmentDBHelper {
    public static final String DATABASE_NAME = "siderealTimeAdjustment.db";

    private Context mContext;
    private SiderealTimeAdjustmentDao mSiderealTimeAdjustmentDao = null;

    /**
     * 기본적인 생성자.
     * @param context 어플리케이션의 context, getApplicationContext()로 사용
     */
    public SiderealTimeAdjustmentDBHelper(Context context) {
        mContext = context;

        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, DATABASE_NAME, null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        mSiderealTimeAdjustmentDao = daoSession.getSiderealTimeAdjustmentDao();
    }

    /**
     * 데이터를 DB에 삽입한다.
     * 이미 year이 있는 데이터를 삽입했을 경우 삽입이 되지 않는다.
     * @param year 년
     * @param hour 시
     * @param minute 분
     * @param second 초
     * @return 삽입 성공여부
     */
    public boolean insert(int year, int hour, int minute, int second) {
        boolean isNotExist = (getSiderealTimeAdjustment(year) == null);
        if(isNotExist)
            mSiderealTimeAdjustmentDao.insertInTx(new SiderealTimeAdjustment(null, year, hour, minute, second));

        return isNotExist;
    }

    /**
     * 테이블에 데이터리스트를 삽입한다.
     * @param siderealTimeAdjustmentList 삽입할 데이터리스트
     */
    public void insert(List<SiderealTimeAdjustment> siderealTimeAdjustmentList) {
        mSiderealTimeAdjustmentDao.insertInTx(siderealTimeAdjustmentList);
    }

    /**
     * 테이블에 있는 모든 데이터를 삭제한다.
     */
    public void deleteAll() {
        mSiderealTimeAdjustmentDao.deleteAll();
    }

    /**
     * year에 맞는 SiderealTimeAdjustment 객체를 얻는다.
     * @param year 원하는 year
     * @return 원하는 객체
     */
    public SiderealTimeAdjustment getSiderealTimeAdjustment(int year) {
        QueryBuilder<SiderealTimeAdjustment> queryBuilder = mSiderealTimeAdjustmentDao.queryBuilder();
        return queryBuilder.where(SiderealTimeAdjustmentDao.Properties.Year.eq(year)).unique();
    }

    /**
     * 테이블에 있는 모든 데이터객체들을 얻는다.
     * @return 데이터 리스트
     */
    public List<SiderealTimeAdjustment> getSiderealTimeAdjustmentAll() {
        QueryBuilder<SiderealTimeAdjustment> queryBuilder = mSiderealTimeAdjustmentDao.queryBuilder();
        return queryBuilder.list();
    }
}
