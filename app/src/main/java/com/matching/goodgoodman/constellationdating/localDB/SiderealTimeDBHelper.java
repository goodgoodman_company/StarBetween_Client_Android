package com.matching.goodgoodman.constellationdating.localDB;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import dao.DaoMaster;
import dao.DaoSession;
import dao.SiderealTime;
import dao.SiderealTimeDao;

/**
 * siderealTime localDB와의 접근을 쉽게 하기 위한 greenDAO 래핑 클래스
 * insert, delete, get 등의 단순 메소드 호출로 DB의 데이터를 가져온다.
 * @author Hyeong_Wook
 */

public class SiderealTimeDBHelper {
    private static final String TABLE_NAME = ".db";
    public static final String[] TABLE_NAMES = {"0.db", "1.db", "2.db", "3.db"};

    private Context mContext;
    private SiderealTimeDao mSiderealTimeDao = null;

    /**
     * 기본적인 생성자.
     * @param context 어플리케이션의 context, getApplicationContext()로 사용
     * @param tableNumber 원하는 테이블, (user의 year - 1920) % 4 수식을 사용
     */
    public SiderealTimeDBHelper(Context context, int tableNumber) {
        mContext = context;
        setTable(tableNumber);
    }

    /**
     * 원하는 table을 설정한다.
     * @param tableNumber 원하는 테이블 번호, (user의 year - 1920) % 4 수식을 사용한다.
     */
    public void setTable(int tableNumber) {
        String table = String.valueOf(tableNumber) + TABLE_NAME;
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, table, null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        mSiderealTimeDao = daoSession.getSiderealTimeDao();
    }

    /**
     * 데이터를 DB에 삽입한다.
     * 이미 month와 day가 있는 데이터를 삽입했을 경우 삽입이 되지 않는다.
     * @param month 월
     * @param day 일
     * @param hour 시
     * @param minute 분
     * @param second 초
     * @return 삽입 성공여부
     */
    public boolean insert(int month, int day, int hour, int minute, int second) {
        boolean isNotExist = (getSiderealTime(month, day) == null);
        if(isNotExist)
            mSiderealTimeDao.insert(new SiderealTime(null, month, day, hour, minute, second));

        return isNotExist;
    }

    /**
     * 데이터리스트를 DB에 삽입한다.
     * @param siderealTimeList 데이터리스트
     */
    public void insert(List<SiderealTime> siderealTimeList) {
        mSiderealTimeDao.insertInTx(siderealTimeList);
    }

    /**
     * 테이블에 있는 모든 데이터를 삭제한다.
     */
    public void deleteAll() {
        mSiderealTimeDao.deleteAll();
    }

    public SiderealTime getSiderealTime(int month, int day) {
        QueryBuilder<SiderealTime> queryBuilder = mSiderealTimeDao.queryBuilder();
        return queryBuilder
                .where(SiderealTimeDao.Properties.Month.eq(month))
                .where(SiderealTimeDao.Properties.Day.eq(day))
                .unique();
    }

    /**
     * 테이블에 있는 모든 데이터객체들을 얻는다.
     * @return 데이터 리스트
     */
    public List<SiderealTime> getSiderealTimeAll() {
        QueryBuilder<SiderealTime> queryBuilder = mSiderealTimeDao.queryBuilder();
        return queryBuilder.list();
    }
}
