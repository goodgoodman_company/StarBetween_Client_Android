package com.matching.goodgoodman.constellationdating.localDB;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import dao.DaoMaster;
import dao.DaoSession;
import dao.SynastryAspectScore;
import dao.SynastryAspectScoreDao;

/**
 * Created by Hyeong_Wook on 2017-10-06.
 */

public class SynastryAspectScoreDBHelper {
    public static final String DATABASE_NAME = "synastryAspectScore.db";

    private Context mContext;
    private SynastryAspectScoreDao mSynastryAspectScoreDao = null;

    /**
     * 기본적인 생성자.
     * @param context 어플리케이션의 context, getApplicationContext()로 사용
     */
    public SynastryAspectScoreDBHelper(Context context) {
        mContext = context;

        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, DATABASE_NAME, null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        mSynastryAspectScoreDao = daoSession.getSynastryAspectScoreDao();
    }

    /**
     * 테이블에 데이터를 삽입한다.
     * @param id 데이터의 id, id가 중복일 경우 삽입되지 않는다.
     * @param score 데이터
     * @return 데이터 id의 중복여부.
     */
    public boolean insert(int id, int score) {
        boolean isNotExist = (getSynastryAspectScore(id) == null);
        if(isNotExist)
            mSynastryAspectScoreDao.insert(new SynastryAspectScore(null, id, score));

        return isNotExist;
    }

    /**
     * 테이블에 데이터리스트를 삽입한다.
     * @param synastryAspectScoreList 삽입할 데이터리스트
     */
    public void insert(List<SynastryAspectScore> synastryAspectScoreList) {
        mSynastryAspectScoreDao.insertInTx(synastryAspectScoreList);
    }

    /**
     * 테이블에 있는 데이터를 삭제한다.
     * @param id 삭제할 데이터의 id
     */
    public void delete(int id) {
        SynastryAspectScore deleteEntity = getSynastryAspectScore(id);
        if(deleteEntity != null)
            mSynastryAspectScoreDao.delete(getSynastryAspectScore(id));
    }

    /**
     * 테이블에 있는 모든 데이터를 삭제한다.
     */
    public void deleteAll() {
        mSynastryAspectScoreDao.deleteAll();
    }

    /**
     * 데이터의 id를 통해 테이블에 있는 데이터 객체를 얻는다.
     * @param id 원하는 데이터의 id
     * @return 데이터
     */
    public SynastryAspectScore getSynastryAspectScore(int id) {
        QueryBuilder<SynastryAspectScore> queryBuilder = mSynastryAspectScoreDao.queryBuilder();
        return queryBuilder.where(SynastryAspectScoreDao.Properties.IntId.eq(id)).unique();
    }

    /**
     * 테이블에 있는 모든 데이터객체들을 얻는다.
     * @return 데이터 리스트
     */
    public List<SynastryAspectScore> getSynastryAspectScoreAll() {
        QueryBuilder<SynastryAspectScore> queryBuilder = mSynastryAspectScoreDao.queryBuilder();
        return queryBuilder.list();
    }
}
