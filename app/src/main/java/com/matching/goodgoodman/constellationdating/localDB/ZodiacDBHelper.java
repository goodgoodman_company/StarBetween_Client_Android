package com.matching.goodgoodman.constellationdating.localDB;

import android.content.Context;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import dao.DaoMaster;
import dao.DaoSession;
import dao.Zodiac;
import dao.ZodiacDao;

/**
 * Created by Hyeong_Wook on 2017-09-24.
 */

public class ZodiacDBHelper {
    public static final String LANGUAGE_ENGLISH = "zodiac_english.db";
    public static final String[] LANGUAGES = {LANGUAGE_ENGLISH};

    private Context mContext;
    private ZodiacDao mZodiacDao;

    /**
     * 기본적인 생성자.
     *
     * @param context 어플리케이션의 context, getApplicationContext()로 사용
     */
    public ZodiacDBHelper(Context context, String language) {
        mContext = context;
        setLanguage(language);
    }

    public void setLanguage(String language) {
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(mContext, language, null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDb());
        DaoSession daoSession = daoMaster.newSession();
        mZodiacDao = daoSession.getZodiacDao();
    }

    /**
     * 데이터를 DB에 삽입한다.
     * 이미 zodiac가 있는 데이터를 삽입시 삽입이 되지 않는다.
     * @param constellation 별자리, 이는 ConstellationData의 별자리와 일치함
     * @param constellationName 별자리의 실제 이름
     * @param month 별자리가 표시하는 달
     * @param planetName 별자리에 해당하는 행성이름
     * @param element 별자리 원소
     * @param color 별자리 색깔
     * @param degree 별자리 각도
     * @param keyword 별자리 키워드
     * @return 삽입 여부
     */
    public boolean insert(String constellation, String constellationName, int month, String planetName, String element, String color, int degree, String keyword) {
        boolean isNotExist = (getZodiac(constellation) == null);
        if (isNotExist)
            mZodiacDao.insertInTx(new Zodiac(null, constellation, constellationName, month, planetName, element, color, degree, keyword));

        return isNotExist;
    }

    /**
     * 테이블에 데이터리스트를 삽입한다.
     *
     * @param zodiacList 삽입할 데이터리스트
     */
    public void insert(List<Zodiac> zodiacList) {
        mZodiacDao.insertInTx(zodiacList);
    }

    /**
     * 테이블에 있는 모든 데이터를 삭제한다.
     */
    public void deleteAll() {
        mZodiacDao.deleteAll();
    }

    /**
     * 조건에 맞는 zodiac 객체를 얻는다.
     * @param constellation 별자리
     * @return planet
     */
    public Zodiac getZodiac(String constellation) {
        QueryBuilder<Zodiac> queryBuilder = mZodiacDao.queryBuilder();
        return queryBuilder
                .where(ZodiacDao.Properties.Constellation.eq(constellation))
                .unique();
    }

    /**
     * 테이블에 있는 모든 데이터객체들을 얻는다.
     *
     * @return 데이터 리스트
     */
    public List<Zodiac> getZodiacAll() {
        QueryBuilder<Zodiac> queryBuilder = mZodiacDao.queryBuilder();
        return queryBuilder.list();
    }
}
