package com.matching.goodgoodman.constellationdating.model;

import dao.Planet;

/**
 * 행성간의 각도를 위한 Model 클래스
 * @author Hyeong_Wook
 */

public class Aspect {
    public static final String CONJUNCTION = "conjunction";
    public static final String TRINE = "trine";
    public static final String SEXTILE = "sextile";
    public static final String OPPOSITION = "opposition";
    public static final String SQUARE = "square";
    public static final String SEMI_SQUARE = "semiSquare";

    public static final String[] NAMES = {CONJUNCTION, TRINE, SEXTILE, OPPOSITION, SQUARE, SEMI_SQUARE};
    public static final int[] DISTANCES = {0, 120, 60, 180, 90, 45};
    public static final int[] ORBS = {8, 8, 6, 8, 8, 2};
    public static final String[] SUN_MOON_NAMES = {Planet.SUN, Planet.MOON};
    public static final int[] SUN_MOON_EXCEPTION = {10, 8, 6, 10, 8, 2};
    public static final int[] RULING_PLANET_EXCEPTION = {8, 8, 6, 10, 8, 2};

    private Planet[] mPlanets;
    private String mName;

    public Aspect(Planet[] planets, String name) {
        mPlanets = planets;
        mName = name;
    }

    public Planet[] getPlanets() {
        return mPlanets;
    }

    public String getName() {
        return mName;
    }

    public static int getIndex(String name) {
        for(int i = 0; i < NAMES.length; i++) {
            if(name.equals(NAMES[i]))
                return i;
        }
        return -1;
    }
}
