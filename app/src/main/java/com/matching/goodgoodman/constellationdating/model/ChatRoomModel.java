package com.matching.goodgoodman.constellationdating.model;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by Hyeong_Wook on 2017-10-07.
 */

public class ChatRoomModel {
    public static final int STATE_MATCHING = 0;
    public static final int STATE_FROM = 1;
    public static final int STATE_TO = 2;

    private Bitmap mProfile;
    private String mCounterName;
    private String mLastChat;
    private int mCounterUserNo;
    private String mRoomKey;
    private Date mEndDate;
    private int mState;

    public ChatRoomModel(Bitmap profile, String counterName, String lastChat, int counterUserNo, String roomKey, Date endDate, int state) {
        mProfile = profile;
        mCounterName = counterName;
        mLastChat = lastChat;
        mCounterUserNo = counterUserNo;
        mRoomKey = roomKey;
        mEndDate = endDate;
        mState = state;
    }

    public Bitmap getProfile() {
        return mProfile;
    }

    public void setProfile(Bitmap profile) {
        mProfile = profile;
    }

    public String getCounterName() {
        return mCounterName;
    }

    public void setCounterName(String counterName) {
        mCounterName = counterName;
    }

    public String getLastChat() {
        return mLastChat;
    }

    public void setLastChat(String lastChat) {
        mLastChat = lastChat;
    }

    public int getCounterUserNo() {
        return mCounterUserNo;
    }

    public void setCounterUserNo(int counterUserNo) {
        mCounterUserNo = counterUserNo;
    }

    public String getRoomKey() {
        return mRoomKey;
    }

    public void setRoomKey(String roomKey) {
        mRoomKey = roomKey;
    }

    public Date getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Date endDate) {
        mEndDate = endDate;
    }

    public int getState() {
        return mState;
    }

    public void setState(int state) {
        mState = state;
    }
}
