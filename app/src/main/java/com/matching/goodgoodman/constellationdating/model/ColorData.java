package com.matching.goodgoodman.constellationdating.model;

import com.matching.goodgoodman.constellationdating.R;

/**
 * Created by Hyeong_Wook on 2017-11-05.
 */

public abstract class ColorData {
    public static final int[] ID_LOVES = {
            R.color.love_90,
            R.color.love_80,
            R.color.love_70,
            R.color.love_60,
            R.color.love_50,
            R.color.love_40,
            R.color.love_30,
            R.color.love_20,
            R.color.love_10,
            R.color.love_0
    };

    public static final int[] ID_LOVES_DRAWAVLE = {
            R.drawable.match_1,
            R.drawable.match_1,
            R.drawable.match_2,
            R.drawable.match_2,
            R.drawable.match_3,
            R.drawable.match_3,
            R.drawable.match_4,
            R.drawable.match_4,
            R.drawable.match_5,
            R.drawable.match_5
    };
}
