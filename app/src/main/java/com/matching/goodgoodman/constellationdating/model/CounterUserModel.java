package com.matching.goodgoodman.constellationdating.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.matching.goodgoodman.constellationdating.R;

import java.util.Date;

/**
 * Created by dorol on 2017-05-28.
 */

public class CounterUserModel implements Parcelable {

    private int mNo;//no
    private int mUserNo;
    private String mId;
    private Date mBirthday;
    private String mGender;//0 male, 1 female
    private String mName;
    private int mAge;
    private String mArea;
    private String mContent;
    private String mNatalchartImage;
    private String mStarsignImg;
    private String mSynastryChartImage;
    private int mGrade;
    private TextView mGradeLabel;

    public CounterUserModel() {    }

    public CounterUserModel(Parcel in) {
        readFromParcel(in);
    }

    public int getNo() {
        return mNo;
    }

    public void setNo(int number) {
        this.mNo = number;
    }

    public int getUserNo() {
        return mUserNo;
    }

    public void setUserNo(int userId) {
        this.mUserNo = userId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String counterUserId) {
        this.mId = counterUserId;
    }

    public Date getBirthday() {
        return mBirthday;
    }

    public void setBirthday(Date matchingDate) {
        this.mBirthday = matchingDate;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        this.mGender = gender;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getAge() {
        return mAge;
    }

    public void setAge(int age) {
        this.mAge = age;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String area) {
        this.mArea = area;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        this.mContent = content;
    }

    public String getNatalchartImage() {
        return mNatalchartImage;
    }

    public void setNatalchartImage(String natalchartImage) {
        this.mNatalchartImage = "http://13.124.123.255:8000/" + natalchartImage;
    }

    public String getStarsignImg() {
        return mStarsignImg;
    }

    public void setStarsignImg(String mstarsignImg) {
        this.mStarsignImg = "http://13.124.123.255:8000/" + mstarsignImg;
    }

    public String getSynastryChartImage() {
        return mSynastryChartImage;
    }

    public void setSynastryChartImage(String msynastryChartImage) {
        this.mSynastryChartImage = "http://13.124.123.255:8000/" + msynastryChartImage;
    }

    public int getGrade() {
        return mGrade;
    }

    public void setGrade(int mgrade) {
        this.mGrade = mgrade;
    }

    public TextView getcounterUserGradeLabel(Context context, int grade) {
        switch (grade) {
            case 5:
                mGradeLabel.setText("S");
                mGradeLabel.setTextColor(ContextCompat.getColor(context, R.color.matchingGrade_S));
                break;
            case 4:
                mGradeLabel.setText("A");
                mGradeLabel.setTextColor(ContextCompat.getColor(context,R.color.matchingGrade_A));
                break;
            case 3:
                mGradeLabel.setText("B");
                mGradeLabel.setTextColor(ContextCompat.getColor(context,R.color.matchingGrade_B));
                break;
            case 2:
                mGradeLabel.setText("C");
                mGradeLabel.setTextColor(ContextCompat.getColor(context,R.color.matchingGrade_C));
                break;
            case 1:
                mGradeLabel.setText("D");
                mGradeLabel.setTextColor(ContextCompat.getColor(context,R.color.matchingGrade_D));
                break;
            case 0:
                mGradeLabel.setText("F");
                mGradeLabel.setTextColor(ContextCompat.getColor(context,R.color.matchingGrade_F));
                break;
        }
        return mGradeLabel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mGender);
        dest.writeString(mName);
        dest.writeInt(mAge);
        dest.writeString(mArea);
        dest.writeString(mContent);
        dest.writeString(mNatalchartImage);
        dest.writeString(mStarsignImg);
        dest.writeInt(mGrade);
    }

    private void readFromParcel(Parcel in){
        mId = in.readString();
        mGender = in.readString();
        mName = in.readString();
        mAge = in.readInt();
        mArea = in.readString();
        mContent =in.readString();
        mNatalchartImage = in.readString();
        mStarsignImg = in.readString();
        mGrade = in.readInt();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CounterUserModel createFromParcel(Parcel in) {
            return new CounterUserModel(in);
        }

        public CounterUserModel[] newArray(int size) {
            return new CounterUserModel[size];
        }
    };


}
