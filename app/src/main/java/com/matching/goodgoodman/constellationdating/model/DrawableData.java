package com.matching.goodgoodman.constellationdating.model;

import com.matching.goodgoodman.constellationdating.R;

/**
 * Created by Hyeong_Wook on 2017-10-31.
 */

public abstract class DrawableData {
    public static final int[] ID_ELEMENTS = {
            R.drawable.air, R.drawable.water, R.drawable.fire, R.drawable.earth,
            R.drawable.air, R.drawable.water, R.drawable.fire, R.drawable.earth,
            R.drawable.air, R.drawable.water, R.drawable.fire, R.drawable.earth, };

    public static final int[] ID_PLANETS = {
            R.drawable.uranus_i,
            R.drawable.neptune_i,
            R.drawable.mars_i,
            R.drawable.venus_i,
            R.drawable.mercury_i,
            R.drawable.moon_i,
            R.drawable.sun_i,
            R.drawable.mercury_i,
            R.drawable.venus_i,
            R.drawable.pluto_i,
            R.drawable.jupiter_i,
            R.drawable.saturn_i
    };

    public static final int[] ID_ZODIACS = {
            R.drawable.aquarius_i,
            R.drawable.pisces_i,
            R.drawable.aries_i,
            R.drawable.tourus_i,
            R.drawable.gemini_i,
            R.drawable.cancer_i,
            R.drawable.leo_i,
            R.drawable.virgo_i,
            R.drawable.libra_i,
            R.drawable.scorpio_i,
            R.drawable.sagittarius_i,
            R.drawable.capricorn_i
    };

    public static final int[] ID_CONSTELLATIONS = {
            R.drawable.aquarius_d,
            R.drawable.pisces_d,
            R.drawable.aries_d,
            R.drawable.taurus_d,
            R.drawable.gemini_d,
            R.drawable.cancer_d,
            R.drawable.leo_d,
            R.drawable.virgo_d,
            R.drawable.libra_d,
            R.drawable.scorpio_d,
            R.drawable.sagittarius_d,
            R.drawable.capricorn_d
    };

    public static final int[] ID_COLORS = {
            R.drawable.turquoise,
            R.drawable.sea_green,
            R.drawable.red,
            R.drawable.pink,
            R.drawable.yellow,
            R.drawable.silver,
            R.drawable.colors_of_sun,
            R.drawable.navy_blue,
            R.drawable.ultramarine,
            R.drawable.dark_red,
            R.drawable.dark_blue,
            R.drawable.dark_grey
    };

    public static final int[] ID_LOVES = {
            R.drawable.great_love,
            R.drawable.good_love,
            R.drawable.love,
            R.drawable.bad_love,
            R.drawable.fail_love
    };
}
