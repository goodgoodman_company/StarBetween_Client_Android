package com.matching.goodgoodman.constellationdating.model;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created by Hyeong_Wook on 2017-08-29.
 */

public class FacebookModel {
    private String mToken = null;
    private String mUserId = null;
    private String mUserName = null;
    private Bitmap mProfile = null;
    private List<Bitmap> mPhotoList = null;

    public FacebookModel(String token, String userId) {
        mToken = token;
        mUserId = userId;
    }

    public String getToken() {
        return mToken;
    }

    public String getUserId() {
        return mUserId;
    }

    public String getUserName() {
        return mUserName;
    }

    public Bitmap getProfile() {
        return mProfile;
    }

    public List<Bitmap> getPhotoList() {
        return mPhotoList;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public void setProfile(Bitmap profile) {
        mProfile = profile;
    }

    public void setPhotoList(List<Bitmap> photoList) {
        mPhotoList = photoList;
    }

}
