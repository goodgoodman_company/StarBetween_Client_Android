package com.matching.goodgoodman.constellationdating.model;

import java.util.List;

import dao.Ascendent;
import dao.MediumCoeli;
import dao.Planet;

/**
 * Created by Hyeong_Wook on 2017-09-20.
 */

public class HoroscopeModel {
    private MediumCoeli mMediumCoeli;
    private Ascendent mAscendent;
    private Planet[] mPlanets;
    private List<Aspect> mAspectList;
    private List<House> mHouseList;
    private int mCounterUserNo = -1;

    public HoroscopeModel(Ascendent ASC, MediumCoeli MC, Planet[] planets, List<Aspect> aspectList, List<House> houseList) {
        mAscendent = ASC;
        mMediumCoeli = MC;
        mPlanets = planets;
        mAspectList = aspectList;
        mHouseList = houseList;
    }

    public Ascendent getAscendent() {
        return mAscendent;
    }

    public MediumCoeli getMediumCoeli() {
        return mMediumCoeli;
    }

    public Planet[] getPlanets() {
        return mPlanets;
    }

    public List<Aspect> getAspectList() {
        return mAspectList;
    }

    public List<House> getHouseList() {
        return mHouseList;
    }

    public void setAscendent(Ascendent ascendent) {
        mAscendent = ascendent;
    }

    public void setMediumCoeli(MediumCoeli mediumCoeli) {
        mMediumCoeli = mediumCoeli;
    }

    public void setPlanets(Planet[] planets) {
        mPlanets = planets;
    }

    public void setAspectList(List<Aspect> aspectList) {
        mAspectList = aspectList;
    }

    public void setHouseList(List<House> houseList) {
        mHouseList = houseList;
    }

    public int getCounterUserNo() {
        return mCounterUserNo;
    }

    public void setCounterUserNo(int counterUserNo) {
        mCounterUserNo = counterUserNo;
    }
}
