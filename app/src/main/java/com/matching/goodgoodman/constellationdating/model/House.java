package com.matching.goodgoodman.constellationdating.model;

/**
 * Created by Hyeong_Wook on 2017-09-24.
 */

public class House {
    private String mPlanet;
    private double mDistance;
    private int mNumber;

    public House(String planet, double distance, int number) {
        mPlanet = planet;
        setDistance(distance);
        mNumber = number;
    }

    public String getPlanet() {
        return mPlanet;
    }

    public void setPlanet(String planet) {
        mPlanet = planet;
    }

    public double getDistance() {
        return mDistance;
    }

    public void setDistance(double distance) {
        mDistance = distance;
        if(distance >= 360)
            mDistance -= 360;
    }

    public int getNumber() {
        return mNumber;
    }

    public void setNumber(int number) {
        mNumber = number;
    }
}
