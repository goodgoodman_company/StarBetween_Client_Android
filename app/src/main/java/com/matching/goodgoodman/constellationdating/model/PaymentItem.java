package com.matching.goodgoodman.constellationdating.model;

import android.support.annotation.Nullable;

/**
 * Created by Hyeong_Wook on 2017-10-08.
 */

public class PaymentItem {
    private String mSkuId;
    private int mCount;
    private String mPrice;
    private String mSpecialDescription;

    public PaymentItem(String skuId, int count, String price, @Nullable String specialDescription) {
        mSkuId = skuId;
        mCount = count;
        mPrice = price;
        mSpecialDescription = specialDescription;
    }

    public String getSkuId() {
        return mSkuId;
    }

    public void setSkuId(String skuId) {
        mSkuId = skuId;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getSpecialDescription() {
        return mSpecialDescription;
    }

    public void setSpecialDescription(String specialDescription) {
        mSpecialDescription = specialDescription;
    }
}
