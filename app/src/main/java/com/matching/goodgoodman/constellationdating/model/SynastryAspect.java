package com.matching.goodgoodman.constellationdating.model;

/**
 * Created by Hyeong_Wook on 2017-10-05.
 */

public class SynastryAspect {
    private Aspect mAspect;
    private int mAspectNumber;
    private int mScore;

    public SynastryAspect(Aspect aspect, int aspectNumber, int score) {
        mAspect = aspect;
        mAspectNumber = aspectNumber;
        mScore = score;
    }

    public Aspect getAspect() {
        return mAspect;
    }

    public void setAspect(Aspect aspect) {
        mAspect = aspect;
    }

    public int getAspectNumber() {
        return mAspectNumber;
    }

    public void setAspectNumber(int aspectNumber) {
        mAspectNumber = aspectNumber;
    }

    public int getScore() {
        return mScore;
    }

    public void setScore(int score) {
        mScore = score;
    }
}
