package com.matching.goodgoodman.constellationdating.model;

import java.util.List;

/**
 * Created by Hyeong_Wook on 2017-10-05.
 */

public class SynastryHoroscopeModel {
    private HoroscopeModel mUserHoroscopeModel;
    private HoroscopeModel mCounterHoroscopeModel;
    private List<SynastryAspect> mSynastryAspectList;

    public SynastryHoroscopeModel(HoroscopeModel userHoroscopeModel, HoroscopeModel counterHoroscopeModel, List<SynastryAspect> synastryAspectList) {
        mUserHoroscopeModel = userHoroscopeModel;
        mCounterHoroscopeModel = counterHoroscopeModel;
        mSynastryAspectList = synastryAspectList;
    }

    public HoroscopeModel getUserHoroscopeModel() {
        return mUserHoroscopeModel;
    }

    public void setUserHoroscopeModel(HoroscopeModel userHoroscopeModel) {
        mUserHoroscopeModel = userHoroscopeModel;
    }

    public HoroscopeModel getCounterHoroscopeModel() {
        return mCounterHoroscopeModel;
    }

    public void setCounterHoroscopeModel(HoroscopeModel counterHoroscopeModel) {
        mCounterHoroscopeModel = counterHoroscopeModel;
    }

    public List<SynastryAspect> getSynastryAspectList() {
        return mSynastryAspectList;
    }

    public void setSynastryAspectList(List<SynastryAspect> synastryAspectList) {
        mSynastryAspectList = synastryAspectList;
    }
}
