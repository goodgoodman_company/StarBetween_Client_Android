package com.matching.goodgoodman.constellationdating.model;

import android.graphics.Bitmap;
import android.net.ParseException;

import com.matching.goodgoodman.constellationdating.utils.DateConverter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by dorol on 2017-05-28.
 */

public class UserModel {
    public static final String GENDER_MALE = "M";
    public static final String GENDER_FEMALE = "F";

    public static final String YES = "Y";
    public static final String NO = "N";

    public static final String POINT10 = "POINT10";
    public static final String POINT314 = "POINT314";
    public static final int INT_POINT10 = 10;

    private int mNo;
    private String mId = null;
    private String mFaceBookToken;
    private String mEmail = null;
    private String mGender = null;
    private float mLatitude = 0;
    private float mLongitude = 0;
    private Date mBirthday = null;
    private int mScore = -1;
    private Date mRegisteredDate;
    private Date mLastConTime;
    private String testBirthday;

    private String mName;
    private int mAge;
    private String mArea;
    private String mContent;
    private Bitmap mProfileImage;
    private Bitmap mStarsignImg;
    private String mChatRoomId;
    private int mFreePoint;
    private int mStarPoint;
    private String mGrade;
    private String mLocation;

    private int mMatchingNo;
    private String mUseBlurYn;
    private String mUseLikeYn;
    private int mAttendancePoint;
    private String mPremiumYn;
    private String mMatchingUpdateYn;
    private String mTodayFirst;
    private String mImageOpenYn;

    private float mCurrentLatitude = 0;
    private float mCurrentLongitude = 0;

    public String getTestBirthday() {
        return testBirthday;
    }

    public void setTestBirthday(String testBirthday) {
        this.testBirthday = testBirthday;
    }

    //TODO : 임시 :: 1이면 누른거
    private int userCheckLike = 0;

    public UserModel() {    }

    public int getNo() {
        return mNo;
    }

    public void setNo(int userNo) {
        this.mNo = userNo;
    }

    public String getId() {
        return mId;
    }

    public void setId(String userId) {
        this.mId = userId;
    }

    public String getFaceBookToken() {
        return mFaceBookToken;
    }

    public void setFaceBookToken(String userFaceBookToken) {
        this.mFaceBookToken = userFaceBookToken;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String userEmail) {
        this.mEmail = userEmail;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String userGender) {
        this.mGender = userGender;
    }

    public void setLatitude(float latitude) {
        mLatitude = latitude;
    }

    public float getLatitude() {
        return mLatitude;
    }

    public void setLongitude(float longitude) {
        mLongitude = longitude;
    }

    public float getLongitude() {
        return mLongitude;
    }

    public Date getBirthday() {
        return mBirthday;
    }

    public String getBirthdaySimpleDateFormat() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(DateConverter.GMT));
        try {
            String format = simpleDateFormat.format(mBirthday);
            return format;
        } catch (ParseException e) {
            return null;
        }

    }

    public void setBirthday(Date userBirthday) {
        this.mBirthday = userBirthday;
    }

    public int getScore() {
        return mScore;
    }

    public void setScore(int userScore) {
        this.mScore = userScore;
    }

    public Date getRegisteredDate() {
        return mRegisteredDate;
    }

    public void setRegisteredDate(Date userRegisteredDate) {
        this.mRegisteredDate = userRegisteredDate;
    }

    public Date getLastConTime() {
        return mLastConTime;
    }

    public void setLastConTime(Date userLastConDate) {
        this.mLastConTime = userLastConDate;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mUserName) {
        this.mName = mUserName;
    }

    public int getAge() {
        return mAge;
    }

    public void setAge(int mUserAge) {
        this.mAge = mUserAge;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String userArea) {
        this.mArea = userArea;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String userContent) {
        this.mContent = userContent;
    }

    public Bitmap getProfileImage() {
        return mProfileImage;
    }

    public void setProfileImage(Bitmap profileImage) {
        this.mProfileImage = profileImage;
    }

    public Bitmap getStarsignImg() {
        return mStarsignImg;
    }

    public void setStarsignImg(Bitmap mStarsignImg) {
        this.mStarsignImg = mStarsignImg;
    }

    public String getChatRoomId() {
        return mChatRoomId;
    }

    public void setChatRoomId(String userchatId) {
        this.mChatRoomId = userchatId;
    }

    public int getCheckLike() {
        return userCheckLike;
    }

    public void setCheckLike(int userCheckLike) {
        this.userCheckLike = userCheckLike;
    }

    public int getFreePoint() {
        return mFreePoint;
    }

    public void setFreePoint(int freePoint) {
        mFreePoint = freePoint;
    }

    public int getStarPoint() {
        return mStarPoint;
    }

    public void setStarPoint(int starPoint) {
        mStarPoint = starPoint;
    }

    public String getGrade() {
        return mGrade;
    }

    public void setGrade(String grade) {
        mGrade = grade;
    }

    public int getMatchingNo() {
        return mMatchingNo;
    }

    public void setMatchingNo(int matchingNo) {
        mMatchingNo = matchingNo;
    }

    public String getUseBlurYn() {
        return mUseBlurYn;
    }

    public void setUseBlurYn(String useBlurYn) {
        mUseBlurYn = useBlurYn;
    }

    public String getUseLikeYn() {
        return mUseLikeYn;
    }

    public void setUseLikeYn(String useLikeYn) {
        mUseLikeYn = useLikeYn;
    }

    public int getAttendancePoint() {
        return mAttendancePoint;
    }

    public void setAttendancePoint(int attendancePoint) {
        mAttendancePoint = attendancePoint;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public float getCurrentLatitude() {
        return mCurrentLatitude;
    }

    public void setCurrentLatitude(float currentLatitude) {
        mCurrentLatitude = currentLatitude;
    }

    public float getCurrentLongitude() {
        return mCurrentLongitude;
    }

    public void setCurrentLongitude(float currentLongitude) {
        mCurrentLongitude = currentLongitude;
    }

    public String getPremiumYn() {
        return mPremiumYn;
    }

    public void setPremiumYn(String premiumYn) {
        mPremiumYn = premiumYn;
    }

    public String getMatchingUpdateYn() {
        return mMatchingUpdateYn;
    }

    public void setMatchingUpdateYn(String matchingUpdateYn) {
        mMatchingUpdateYn = matchingUpdateYn;
    }

    public String getTodayFirst() {
        return mTodayFirst;
    }

    public void setTodayFirst(String todayFirst) {
        mTodayFirst = todayFirst;
    }

    public String getImageOpenYn() {
        return mImageOpenYn;
    }

    public void setImageOpenYn(String imageOpenYn) {
        mImageOpenYn = imageOpenYn;
    }
}
