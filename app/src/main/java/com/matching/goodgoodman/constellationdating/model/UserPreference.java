package com.matching.goodgoodman.constellationdating.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 유저에 관한 객체를 관리하는 클래스
 * 새로운 객체를 반환하거나, 기존의 객체를 반환하는 서비스를 제공한다.
 * @author Hyeong_Wook
 */

public class UserPreference {
    private UserModel mUserModel;
    private List<UserModel> mCounterUserModelList;
    private List<FacebookModel> mFacebookModelList;
    private HoroscopeModel mHoroscopeModel;
    private List<ChatRoomModel> mChatRoomModelList;
    private List<HoroscopeModel> mCounterHoroscopeModels;

    private static volatile UserPreference mInstance;
    public static UserPreference getInstance() {
        if(mInstance == null) {
            synchronized (UserPreference.class) {
                if(mInstance == null)
                    mInstance = new UserPreference();
            }
        }
        return mInstance;
    }
    private UserPreference() {
        mUserModel = new UserModel();
        mCounterUserModelList = new ArrayList<>();
        mFacebookModelList = new ArrayList<>();
        mFacebookModelList.add(new FacebookModel(null, null));
        mHoroscopeModel = new HoroscopeModel(null, null, null, null, null);
        mChatRoomModelList = new ArrayList<>();
        mCounterHoroscopeModels = new ArrayList<>();
    }

    /**
     * UserModel 객체를 얻습니다.
     * 이 model을 받아 데이터 변경시, 다른 곳에서 이 메서드를 호출할 경우 변경된 데이터를 얻습니다.
     * @return 지속적인 userModel의 객체
     */
    public UserModel getUserModel() {
        return mUserModel;
    }

    /**
     * UserModel의 전체 데이터를 변경합니다.
     */
    public void setUserModel() {
        mUserModel = new UserModel();
    }

    /**
     * CounterUserModel객체 리스트를 얻습니다.
     * @return counterUserModelList
     */
    public List<UserModel> getCounterUserModelList() {
        return mCounterUserModelList;
    }

    /**
     * counterUserModel의 list에서의 위치를 얻습니다.
     * @param userNo 얻고자 하는 counterUserModel의 number
     * @return index
     */
    public int getCounterUserModelIndex(int userNo) {
        for(int i = 0; i < mCounterUserModelList.size(); i++) {
            if(mCounterUserModelList.get(i).getNo() == userNo)
                return i;
        }
        return -1;
    }

    /**
     * userNo에 맞는 counterUserModel을 얻습니다.
     * @param userNo
     * @return
     */
    public UserModel getCounterUser(int userNo) {
        for(UserModel counterUserModel : mCounterUserModelList) {
            if(counterUserModel.getNo() == userNo)
                return counterUserModel;
        }
        return null;
    }

    /**
     * 새로운 CounterUserModelList를 설정합니다.
     * @param counterUserModelList 새로운 counterUserModelList
     */
    public void setCounterUserModelList(List<UserModel> counterUserModelList) {
        mCounterUserModelList = counterUserModelList;
    }

    public void addCounterUserModel(UserModel counterUserModel) {
        mCounterUserModelList.add(counterUserModel);
    }

    public List<FacebookModel> getFacebookModelList() {
        return mFacebookModelList;
    }
    public void setFacebookModelList(List<FacebookModel> facebookModelList) {
        mFacebookModelList = facebookModelList;
    }

    /**
     * FacebookModel 객체를 반환합니다.
     * 이는 유일한 객체입니다.
     * @param position FacebookModelList의 반환하고 싶은 객체의 위치
     * @return facebookModel 객체
     */
    public FacebookModel getFacebookModel(int position) {
        return mFacebookModelList.get(position);
    }

    /**
     * facebookModel 객체를 변경합니다.
     * @param facebookModel 변경되는 facebookModel 객체
     * @param position 변경되는 facebokModel 위치
     */
    public void setFacebookModel(FacebookModel facebookModel, int position) {
        mFacebookModelList.set(position,facebookModel);
    }

    public void addFacebookModel(FacebookModel facebookModel) {
        mFacebookModelList.add(facebookModel);
    }

    public HoroscopeModel getHoroscopeModel() {
        return mHoroscopeModel;
    }

    public void setHoroscopeModel(HoroscopeModel horoscopeModel) {
        mHoroscopeModel = horoscopeModel;
    }

    public List<ChatRoomModel> getChatRoomModelList() {
        return mChatRoomModelList;
    }

    public void setChatRoomModelList(List<ChatRoomModel> chatRoomModelList) {
        mChatRoomModelList = chatRoomModelList;
    }

    public void addChatRoomModel(ChatRoomModel chatRoomModel) {
        mChatRoomModelList.add(chatRoomModel);
    }

    public ChatRoomModel getChatRoomModel(int counterNo) {
        for(ChatRoomModel chatRoomModel : mChatRoomModelList) {
            if(chatRoomModel.getCounterUserNo() == counterNo)
                return chatRoomModel;
        }
        return null;
    }

    public List<HoroscopeModel> getCounterHoroscopeModels() {
        return mCounterHoroscopeModels;
    }

    public void setCounterHoroscopeModels(List<HoroscopeModel> counterHoroscopeModels) {
        mCounterHoroscopeModels = counterHoroscopeModels;
    }

    public void addCounterHoroscopeModel(HoroscopeModel horoscopeModel) {
        if(getCounterHoroscopeModel(horoscopeModel.getCounterUserNo()) != null)
            return;

        mCounterHoroscopeModels.add(horoscopeModel);
    }

    public HoroscopeModel getCounterHoroscopeModel(int counterNo) {
        for(HoroscopeModel horoscopeModel : mCounterHoroscopeModels) {
            if(horoscopeModel.getCounterUserNo() == counterNo)
                return horoscopeModel;
        }
        return null;
    }
}
