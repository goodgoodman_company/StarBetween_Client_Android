package com.matching.goodgoodman.constellationdating.popup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.matching.goodgoodman.constellationdating.R;
import com.matching.goodgoodman.constellationdating.model.ColorData;
import com.matching.goodgoodman.constellationdating.model.DrawableData;
import com.matching.goodgoodman.constellationdating.model.SynastryAspect;
import com.matching.goodgoodman.constellationdating.utils.HoroscopeDescriptionGetter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dorol on 2017-11-05.
 */

public class MatchingPopupFragment extends Fragment implements MatchingPopupView {

    private int mImageType = -1;

    @BindView(R.id.popup_matching_imageview) ImageView mImageView;
    @BindView(R.id.popup_matching_title) TextView mTitle;
    @BindView(R.id.popup_matching_subtitle) TextView mSubTitle;
    @BindView(R.id.popup_matching_body01) TextView mBody1;
    @BindView(R.id.popup_matching_body02) TextView mBody2;
    @BindView(R.id.popup_matching_body03) TextView mBody3;
    @BindView(R.id.popup_matching_body04) TextView mBody4;
    @BindView(R.id.popup_matching_body05) TextView mBody5;

    private MatchingPopupPresenter mMatchingPresenter;
    private View view;

    private List<SynastryAspect> mCounterUserSynastryAspects = null;

    public void setCounterUserSynastryAspects(List<SynastryAspect> counterUserSynastryAspects) {
        this.mCounterUserSynastryAspects = counterUserSynastryAspects;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.popup_fragment_matching,container,false);
        ButterKnife.bind(this, view);
        mMatchingPresenter = new MatchingPopupPresenter(getContext(), this);

        mMatchingPresenter.loadDescription(mCounterUserSynastryAspects);

        return view;
    }

    @Override
    public void showDescription(String[] descriptions) {
        int imageIndex = Integer.parseInt(descriptions[HoroscopeDescriptionGetter.INDEX_IMAGE]);
        int colorIndex = Integer.parseInt(descriptions[HoroscopeDescriptionGetter.INDEX_COLOR]);

        mImageView.setImageResource(DrawableData.ID_LOVES[imageIndex]);
        mTitle.setText(descriptions[HoroscopeDescriptionGetter.INDEX_SUBTITLE]);
        mTitle.setTextColor(ContextCompat.getColor(getContext(), ColorData.ID_LOVES[colorIndex]));
        mSubTitle.setText(descriptions[HoroscopeDescriptionGetter.INDEX_TITLE]);
        mSubTitle.setTextColor(ContextCompat.getColor(getContext(), ColorData.ID_LOVES[colorIndex]));

        String[] body = descriptions[HoroscopeDescriptionGetter.INDEX_COMMENT].split("\n");
        TextView temp = view.findViewById(R.id.tempTextView);
        TextView[] textView = {mBody1,mBody2,mBody3,mBody4,mBody5,temp, temp};
        temp.setVisibility(View.GONE);
        int i = 0;
        for(String b : body) {
            textView[i].setText(b);
            ++i;
        }
    }
}