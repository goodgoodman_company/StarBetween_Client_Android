package com.matching.goodgoodman.constellationdating.popup;

import android.content.Context;

import com.matching.goodgoodman.constellationdating.Presenter;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.model.SynastryAspect;
import com.matching.goodgoodman.constellationdating.utils.HoroscopeDescriptionGetter;

import java.util.List;

/**
 * Created by dorol on 2017-09-28.
 */

public class MatchingPopupPresenter implements Presenter<MatchingPopupView> {

    private MatchingPopupView mView;
    private Context mContext;

    public MatchingPopupPresenter(Context context, MatchingPopupView view) {
        mContext = context;
        attachView(view);
    }

    public void loadDescription(List<SynastryAspect> counterUserSynastryAspects) {
        String[] descriptions
                = HoroscopeDescriptionGetter.getMatchingDescription(mContext.getApplicationContext(), DescriptionDBHelper.LANGUAGE_ENGLISH, counterUserSynastryAspects);

        mView.showDescription(descriptions);
    }

    @Override
    public void attachView(MatchingPopupView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
