package com.matching.goodgoodman.constellationdating.popup;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.matching.goodgoodman.constellationdating.Horoscope;
import com.matching.goodgoodman.constellationdating.R;
import com.matching.goodgoodman.constellationdating.utils.HoroscopeDescriptionGetter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PopUpDescriptionFragment extends Fragment implements PopUpDescriptionView {

    @BindView(R.id.popup_description_horoscope) Horoscope mHoroscope;
    @BindView(R.id.popup_description_title) TextView mTitle;
    @BindView(R.id.popup_description_subtitle) TextView mSubTitle;
    @BindView(R.id.popup_description_body) TextView mBody;

    private int mPageNumber = 1;

    private PopUpDescriptionPresenter mPresenter;

    public PopUpDescriptionFragment() {
        // Required empty public constructor
    }
    public void setSlidePopUpDescriptionFragmentposition(int position) {
        this.mPageNumber = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.popup_fragment_slide_description, container, false);
        ButterKnife.bind(this, view);
        mPresenter = new PopUpDescriptionPresenter(this);
        mHoroscope.setHoroscopeModel(mPresenter.loadHoroscopeModel());

        mPresenter.loadHoroscopeDescriptionGetter(getContext().getApplicationContext(), mPageNumber);

        return view;
    }


    @Override
    public void showHoroscopeDescription(String[] descriptions) {
        String planetName = descriptions[HoroscopeDescriptionGetter.INDEX_PLANET];
        mHoroscope.setHighlightPlanet(planetName);
        mHoroscope.refresh();

        mTitle.setText(descriptions[HoroscopeDescriptionGetter.INDEX_TITLE]);
        mSubTitle.setText(descriptions[HoroscopeDescriptionGetter.INDEX_SUBTITLE]);
        mBody.setText(descriptions[HoroscopeDescriptionGetter.INDEX_COMMENT]);
    }

}
