package com.matching.goodgoodman.constellationdating.popup;

import android.content.Context;

import com.matching.goodgoodman.constellationdating.Presenter;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.model.HoroscopeModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.utils.HoroscopeDescriptionGetter;

/**
 * Created by dorol on 2017-09-28.
 */

public class PopUpDescriptionPresenter implements Presenter<PopUpDescriptionView> {

    private PopUpDescriptionView mView;
    private HoroscopeModel mHoroscopeModel;

    public PopUpDescriptionPresenter(PopUpDescriptionView view) {
        mHoroscopeModel = UserPreference.getInstance().getHoroscopeModel();
        attachView(view);
    }

    public HoroscopeModel loadHoroscopeModel () {
        return mHoroscopeModel;
    }

    public void loadHoroscopeDescriptionGetter(Context context, int position) {
        String[] descriptions = HoroscopeDescriptionGetter.
                getHouseDescription(context.getApplicationContext(), DescriptionDBHelper.LANGUAGE_ENGLISH,
                                    mHoroscopeModel.getHouseList().get(position - 2), position);

        mView.showHoroscopeDescription(descriptions);
    }


    @Override
    public void attachView(PopUpDescriptionView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
