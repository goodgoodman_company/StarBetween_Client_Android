package com.matching.goodgoodman.constellationdating.popup;

/**
 * Created by dorol on 2017-09-28.
 */

public interface PopUpDescriptionView {
    void showHoroscopeDescription(String[] descriptions);
}
