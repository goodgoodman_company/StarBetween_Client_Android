package com.matching.goodgoodman.constellationdating.popup;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.matching.goodgoodman.constellationdating.R;
import com.matching.goodgoodman.constellationdating.SynastryChart;
import com.matching.goodgoodman.constellationdating.model.HoroscopeModel;
import com.matching.goodgoodman.constellationdating.model.SynastryAspect;
import com.matching.goodgoodman.constellationdating.model.SynastryHoroscopeModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.utils.HoroscopeDescriptionGetter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PopUpMatchingDescriptionFragment extends Fragment implements PopUpMatchingDescriptionView {

    @BindView(R.id.popup_matching_description_synastrychart) SynastryChart mSynastryChart;
    @BindView(R.id.popup_matching_description_title) TextView mTitle;
    @BindView(R.id.popup_matching_description_body) TextView mBody;

    private int mPageNumber = 1;

    private PopUpMatchingDescriptionPresenter mPresenter;

    private int mCounterUserNo = -1;
    private List<SynastryAspect> mCounterUserSynastryAspects = null;

    public void setCounterUserSynastryAspects(List<SynastryAspect> counterUserSynastryAspects) {
        this.mCounterUserSynastryAspects = counterUserSynastryAspects;
    }

    public void setCounterUserNo(int counterUserNo) {
        mCounterUserNo = counterUserNo;
    }

    public void setSlidePopUpDescriptionFragmentposition(int position) {
        this.mPageNumber = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.popup_fragment_matching_description, container, false);
        ButterKnife.bind(this, view);
        HoroscopeModel horoscopeModel = UserPreference.getInstance().getHoroscopeModel();
        HoroscopeModel counterHoroscopeModel = UserPreference.getInstance().getCounterHoroscopeModel(mCounterUserNo);
        mPresenter = new PopUpMatchingDescriptionPresenter(this);
        mSynastryChart.setSynastryHoroscopeModel(new SynastryHoroscopeModel(horoscopeModel, counterHoroscopeModel, mCounterUserSynastryAspects));

        mPresenter.loadHoroscopeDescriptionGetter(getContext().getApplicationContext(), mCounterUserSynastryAspects ,mPageNumber);

        return view;
    }


    @Override
    public void showSynastryDescription(String[] descriptions) {
        String planetNames = descriptions[HoroscopeDescriptionGetter.INDEX_PLANET];
        mSynastryChart.setHighlightPlanets(planetNames.split(":"));
        mSynastryChart.refresh();

        if(mPageNumber < 5)
            mTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.good));
        else
            mTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.bad));
        mTitle.setText(descriptions[HoroscopeDescriptionGetter.INDEX_TITLE].replace("\n", ""));
        mBody.setText(descriptions[HoroscopeDescriptionGetter.INDEX_COMMENT]);
    }

}
