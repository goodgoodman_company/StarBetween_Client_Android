package com.matching.goodgoodman.constellationdating.popup;

import android.content.Context;

import com.matching.goodgoodman.constellationdating.Presenter;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.model.SynastryAspect;
import com.matching.goodgoodman.constellationdating.utils.HoroscopeDescriptionGetter;

import java.util.List;

/**
 * Created by dorol on 2017-09-28.
 */

public class PopUpMatchingDescriptionPresenter implements Presenter<PopUpMatchingDescriptionView> {

    private PopUpMatchingDescriptionView mView;

    public PopUpMatchingDescriptionPresenter(PopUpMatchingDescriptionView view) {
        attachView(view);
    }

    public void loadHoroscopeDescriptionGetter(Context context, List<SynastryAspect> counterUserSynastryAspects, int position) {
        String[] descriptions = HoroscopeDescriptionGetter.
                getSynastryAspectDescription(context.getApplicationContext(), DescriptionDBHelper.LANGUAGE_ENGLISH, counterUserSynastryAspects, position);

        if(descriptions != null)
            mView.showSynastryDescription(descriptions);
    }


    @Override
    public void attachView(PopUpMatchingDescriptionView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
