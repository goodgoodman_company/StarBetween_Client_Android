package com.matching.goodgoodman.constellationdating.popup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.matching.goodgoodman.constellationdating.R;
import com.matching.goodgoodman.constellationdating.model.DrawableData;
import com.matching.goodgoodman.constellationdating.utils.HoroscopeDescriptionGetter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by donghwan on 2017-05-17.
 */

public class SlideFragment extends Fragment implements SlideView {

    private int mImageType = -1;

    @BindView(R.id.popup_title) TextView mTitle;
    @BindView(R.id.popup_subtitle) TextView mSubTitle;
    @BindView(R.id.popup_body) TextView mBody;
    @BindView(R.id.popup_imageview) ImageView mImageView;

    private SlidePresenter mSlidePresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.popup_fragment_slide,container,false);
        ButterKnife.bind(this, view);
        mSlidePresenter = new SlidePresenter(getContext(), this);

        mSlidePresenter.loadDiscription();

        return view;
    }

    @Override
    public void showDescription(String[] descriptions) {
        String month = descriptions[HoroscopeDescriptionGetter.INDEX_MONTH];
        int constellationId = Integer.parseInt(month) - 1;

        mImageView.setImageResource(DrawableData.ID_CONSTELLATIONS[constellationId]);
        mTitle.setText(descriptions[HoroscopeDescriptionGetter.INDEX_TITLE]);
        mSubTitle.setText(descriptions[HoroscopeDescriptionGetter.INDEX_SUBTITLE]);
        mBody.setText(descriptions[HoroscopeDescriptionGetter.INDEX_COMMENT]);
    }
}