package com.matching.goodgoodman.constellationdating.popup;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.matching.goodgoodman.constellationdating.R;
import com.matching.goodgoodman.constellationdating.model.SynastryAspect;
import com.matching.goodgoodman.constellationdating.popup.adapter.CounterUserCustomFragmentPagerAdapter;
import com.matching.goodgoodman.constellationdating.popup.adapter.CustomFragmentPagerAdapter;
import com.matching.goodgoodman.constellationdating.popup.adapter.ScaleCircleNavigator;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;

import java.util.List;


/**
 * Created by dorol on 2017-05-17.
 */

public class SlidePopUpDialog extends DialogFragment {

    //TODO :: page갯수 DB들어가면 바꿔줘야함 11로
    public static int USER_PAGENUM = 11;
    public static int COUNTERUSER_PAGENUM = 6;
    public static int USER_POPUP = 0;
    public static int COUNTERUSER_POPUP = 1;

    private ViewPager mViewPager;

    Context mContext;

    private ImageView mImageViewXout;
    private View mContainer;
    private View mFloatingLayout;
    View.OnTouchListener mContainerTouchListener;

    private List<SynastryAspect> mCounterUserSynastryAspects = null;
    private int mCounterUserNo = -1;

    int popupType = 0;

    public SlidePopUpDialog() {}

    public void setPopupType(int popupType) {
        this.popupType = popupType;
    }

    public void setCounterUserSynastryAspects(List<SynastryAspect> counterUserSynastryAspects) {
        this.mCounterUserSynastryAspects = counterUserSynastryAspects;
    }

    public void setCounterUserNo(int counterUserNo) {
        mCounterUserNo = counterUserNo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);

        mContext = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.popup_dialog_slide, container);
        getDialog().setCanceledOnTouchOutside(true);

        //setmTabwidths(view);
        initView(view);
        initMagicIndicator(view);
        setViewListener(view);

        return view;
    }

    public View getFloatingLayout() {
        return mFloatingLayout;
    }

    public void setTouchListener(View.OnTouchListener onTouchListener) {
        mContainerTouchListener = onTouchListener;
    }

    //밑의 동그라미
    private void initMagicIndicator(View view) {
        MagicIndicator magicIndicator = (MagicIndicator) view.findViewById(R.id.popup_magic_indicator);

        ScaleCircleNavigator scaleCircleNavigator = new ScaleCircleNavigator(getContext());
        if(popupType == USER_POPUP) {
            scaleCircleNavigator.setCircleCount(USER_PAGENUM);//탭의 수대로가 아닌, 페이지 수 대로 나오도록
        }
        else {
            scaleCircleNavigator.setCircleCount(COUNTERUSER_PAGENUM);
        }
        scaleCircleNavigator.setNormalCircleColor(ContextCompat.getColor(getContext(), R.color.deep_gray));
        scaleCircleNavigator.setSelectedCircleColor(ContextCompat.getColor(getContext(), R.color.sign_purple));
        scaleCircleNavigator.setCircleClickListener(ccl);

        magicIndicator.setNavigator(scaleCircleNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

    ScaleCircleNavigator.OnCircleClickListener ccl = new ScaleCircleNavigator.OnCircleClickListener() {
        @Override
        public void onClick(int index) {
            mViewPager.setCurrentItem(index);
        }
    };

    private void initView(final View view) {

        mViewPager = (ViewPager) view.findViewById(R.id.popup_view_pager);
        mViewPager.setOffscreenPageLimit(12);

        if(popupType == USER_POPUP) {
            CustomFragmentPagerAdapter mFragmentPagerAdapter;
            mFragmentPagerAdapter = new CustomFragmentPagerAdapter(getChildFragmentManager(), USER_PAGENUM);
            mViewPager.setAdapter(mFragmentPagerAdapter);
        }
        else {
            CounterUserCustomFragmentPagerAdapter mCounterUserFragmentPagerAdapter;
            mCounterUserFragmentPagerAdapter = new CounterUserCustomFragmentPagerAdapter(getChildFragmentManager(), COUNTERUSER_PAGENUM);
            mCounterUserFragmentPagerAdapter.setCounterUserSynastryAspects(mCounterUserSynastryAspects);
            mCounterUserFragmentPagerAdapter.setCounterUserNo(mCounterUserNo);
            mViewPager.setAdapter(mCounterUserFragmentPagerAdapter);
        }

        mContainer = view.findViewById(R.id.popup_container);//팝업의 내용 창부분
        mFloatingLayout = view.findViewById(R.id.popup_floating_layout);
        mFloatingLayout.setBackground(getContext().getResources().getDrawable(R.drawable.popup_bottom, null));

        mImageViewXout = (ImageView) view.findViewById(R.id.popup_floatingActionButton_xout);//x버튼
        mImageViewXout.setImageResource(R.mipmap.more);
        mImageViewXout.setRotation(45);

        mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private void setViewListener(final View view) {



        //summary 등의 바에서 해당 페이지가 됬을때
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setSelectedTab(position);
                //Fragment fragment = mFragmentPagerAdapter.getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        if (mContainerTouchListener != null)
            mContainer.setOnTouchListener(mContainerTouchListener);

        mImageViewXout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RotateAnimation rotateDialogButton = new RotateAnimation(0, -45, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotateDialogButton.setDuration(300);
                rotateDialogButton.setRepeatCount(Animation.ABSOLUTE);
                rotateDialogButton.setFillAfter(true);
                mImageViewXout.startAnimation(rotateDialogButton);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismiss();
                    }
                }, 300);
            }
        });

    }

    //탭의 position으로 page가 이동하도록 하는것
    private void setSelectedTab(final int num) {
        mViewPager.setCurrentItem(num);
    }


}
