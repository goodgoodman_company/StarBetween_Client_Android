package com.matching.goodgoodman.constellationdating.popup;

import android.content.Context;

import com.matching.goodgoodman.constellationdating.Presenter;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.model.HoroscopeModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.utils.HoroscopeDescriptionGetter;

/**
 * Created by dorol on 2017-09-28.
 */

public class SlidePresenter implements Presenter<SlideView> {

    private SlideView mView;
    private Context mContext;
    private HoroscopeModel mHoroscopeModel;

    public SlidePresenter(Context context, SlideView view) {
        mContext = context;
        mHoroscopeModel = UserPreference.getInstance().getHoroscopeModel();
        attachView(view);
    }

    public void loadDiscription() {
        String[] descriptions
                = HoroscopeDescriptionGetter.getConstellationDescription(mContext.getApplicationContext(), DescriptionDBHelper.LANGUAGE_ENGLISH, mHoroscopeModel.getPlanets()[0]);

        mView.showDescription(descriptions);
    }

    @Override
    public void attachView(SlideView view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
