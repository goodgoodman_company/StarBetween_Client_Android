package com.matching.goodgoodman.constellationdating.popup.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.matching.goodgoodman.constellationdating.model.SynastryAspect;
import com.matching.goodgoodman.constellationdating.popup.MatchingPopupFragment;
import com.matching.goodgoodman.constellationdating.popup.PopUpMatchingDescriptionFragment;

import java.util.List;

/**
 * Created by dorol on 2017-11-05.
 */

public class CounterUserCustomFragmentPagerAdapter extends FragmentPagerAdapter {


    private static int mPage = 6;//기본값 11

    private int mCounterUserNo = -1;
    private List<SynastryAspect> mCounterUserSynastryAspects = null;

    public CounterUserCustomFragmentPagerAdapter(FragmentManager fm, int numOfPage) {
        super(fm);
        this.mPage = numOfPage;
    }

    public void setCounterUserSynastryAspects(List<SynastryAspect> counterUserSynastryAspects) {
        this.mCounterUserSynastryAspects = counterUserSynastryAspects;
    }

    public void setCounterUserNo(int counterUserNo) {
        mCounterUserNo = counterUserNo;
    }
    @Override
    public Fragment getItem(int pos) {

        if (pos == 0) {
            MatchingPopupFragment matchingPopupFragment = new MatchingPopupFragment();
            matchingPopupFragment.setCounterUserSynastryAspects(mCounterUserSynastryAspects);
            return matchingPopupFragment;
        } else {
            PopUpMatchingDescriptionFragment matchingPopUpDescriptionFragment = new PopUpMatchingDescriptionFragment();
            matchingPopUpDescriptionFragment.setSlidePopUpDescriptionFragmentposition(pos + 1);
            matchingPopUpDescriptionFragment.setCounterUserSynastryAspects(mCounterUserSynastryAspects);
            matchingPopUpDescriptionFragment.setCounterUserNo(mCounterUserNo);
            return matchingPopUpDescriptionFragment;
        }

    }

    @Override
    public int getCount() {
        return mPage;
    }
}
