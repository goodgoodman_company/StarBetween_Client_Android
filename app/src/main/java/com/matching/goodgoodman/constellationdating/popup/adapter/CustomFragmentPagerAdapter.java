package com.matching.goodgoodman.constellationdating.popup.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.matching.goodgoodman.constellationdating.popup.PopUpDescriptionFragment;
import com.matching.goodgoodman.constellationdating.popup.SlideFragment;

/**
 * Created by donghwan on 2017-05-17.
 */

public class CustomFragmentPagerAdapter extends FragmentPagerAdapter {

    private static int mPage = 11;

    public CustomFragmentPagerAdapter(FragmentManager fm, int numOfPage) {
        super(fm);
        this.mPage = numOfPage;
    }


    @Override
    public Fragment getItem(int pos) {

        //프래그먼트의 인스턴스를 생성
        if (pos == 0) {
            return new SlideFragment();
        } else {
            PopUpDescriptionFragment slidePopUpDescriptionFragment = new PopUpDescriptionFragment();
            slidePopUpDescriptionFragment.setSlidePopUpDescriptionFragmentposition(pos + 1);
            return slidePopUpDescriptionFragment;
        }

    }

    @Override
    public int getCount() {
        return mPage;
    }

}
