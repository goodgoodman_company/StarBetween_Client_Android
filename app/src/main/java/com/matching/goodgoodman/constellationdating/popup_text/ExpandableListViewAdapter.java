package com.matching.goodgoodman.constellationdating.popup_text;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.matching.goodgoodman.constellationdating.R;

import java.util.ArrayList;

/**
 * Created by dorol on 2017-11-03.
 */

public class ExpandableListViewAdapter extends BaseExpandableListAdapter {

    private int ANSWER_COUNT = 1;
    private ArrayList<String> mQuestionList = null;
    private ArrayList<String> mAnswerList = null;
    Context mContext;

    public ExpandableListViewAdapter(Context context, ArrayList<String> questions, ArrayList<String> answers) {
        mContext = context;
        mQuestionList = questions;
        mAnswerList = answers;
    }

    @Override
    public int getGroupCount() {
        return mQuestionList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return ANSWER_COUNT;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mQuestionList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mAnswerList.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(groupPosition);

        if(convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.popup_qna_question_item, null);
        }
        TextView questionTextView = (TextView) convertView.findViewById(R.id.question);

        questionTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        String listText = mAnswerList.get(groupPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.popup_qna_answer_item, null);
        }

        TextView answerTextView = (TextView) convertView.findViewById(R.id.answer);
        if(listText.equals("\n\n\n")) {
            LinearLayout anwserLayout = (LinearLayout) convertView.findViewById(R.id.anwser_layout);
            anwserLayout.setBackgroundColor(mContext.getResources().getColor(R.color.light_gray,null));
        }
        answerTextView.setText(listText);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
