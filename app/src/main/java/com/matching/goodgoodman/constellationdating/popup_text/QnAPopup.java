package com.matching.goodgoodman.constellationdating.popup_text;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.matching.goodgoodman.constellationdating.R;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class QnAPopup extends DialogFragment {

    private ArrayList<String> mQuestionList = null;
    private ArrayList<String> mAnswerList = null;

    private SharedPreferences sharedpreferences;
    private DescriptionDBHelper mDescriptionDBHelper;

    @BindView(R.id.popup_qna_list) ExpandableListView mListView;
    private ExpandableListViewAdapter mExpandableListViewAdapter;

    View.OnTouchListener mContainerTouchListener;
    @BindView(R.id.popup_pna_floating_layout) View mFloatingLayout;
    @BindView(R.id.popup_qna_container) View mContainer;
    @BindView(R.id.popup_pna_floatingActionButton_xout) ImageView mImageViewXout;

    public QnAPopup() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);

        mQuestionList = new ArrayList<>();
        mAnswerList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.popup_qna_dialog_fragment, container, false);
        ButterKnife.bind(this, view);

        setQuestion();
        setAnswer();

        initView();
        setListViewListener();
        setListener();

        return view;
    }

    //TODO :: mDescriptionDBHelper의 번호 바꿔야함
    private void setQuestion() {
        mQuestionList.add(getContext().getResources().getString(R.string.Question1));
        mQuestionList.add(getContext().getResources().getString(R.string.Question2));
        mQuestionList.add(getContext().getResources().getString(R.string.Question3));
        mQuestionList.add(getContext().getResources().getString(R.string.Question4));
        mQuestionList.add(getContext().getResources().getString(R.string.Question5));
        mQuestionList.add(getContext().getResources().getString(R.string.Question6));
        mQuestionList.add(getContext().getResources().getString(R.string.Question7));
        mQuestionList.add(getContext().getResources().getString(R.string.Question8));
        mQuestionList.add(getContext().getResources().getString(R.string.Question9));
        mQuestionList.add("\n\n\n");
    }

    //TODO :: mDescriptionDBHelper의 번호 바꿔야함
    private void setAnswer() {
        mAnswerList.add(getContext().getResources().getString(R.string.Answer1));
        mAnswerList.add(getContext().getResources().getString(R.string.Answer2));
        mAnswerList.add(getContext().getResources().getString(R.string.Answer3));
        mAnswerList.add(getContext().getResources().getString(R.string.Answer4));
        mAnswerList.add(getContext().getResources().getString(R.string.Answer5));
        mAnswerList.add(getContext().getResources().getString(R.string.Answer6));
        mAnswerList.add(getContext().getResources().getString(R.string.Answer7));
        mAnswerList.add(getContext().getResources().getString(R.string.Answer8));
        mAnswerList.add(getContext().getResources().getString(R.string.Answer9));
        mAnswerList.add("\n\n\n");
    }

    private void initView() {
        mFloatingLayout.setBackground(getContext().getResources().getDrawable(R.drawable.popup_bottom, null));

        mImageViewXout.setImageResource(R.mipmap.more);
        mImageViewXout.setRotation(45);
    }
    private void setListViewListener() {
        mExpandableListViewAdapter = new ExpandableListViewAdapter(getContext(), mQuestionList, mAnswerList);

        mListView.setAdapter(mExpandableListViewAdapter);

    }

    private void setListener() {
        if (mContainerTouchListener != null)
            mContainer.setOnTouchListener(mContainerTouchListener);

        mImageViewXout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public View getFloatingLayout() {
        return mFloatingLayout;
    }

    public void setTouchListener(View.OnTouchListener onTouchListener) {
        mContainerTouchListener = onTouchListener;
    }

}
