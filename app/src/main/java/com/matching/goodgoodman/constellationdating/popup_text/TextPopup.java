package com.matching.goodgoodman.constellationdating.popup_text;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.matching.goodgoodman.constellationdating.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dorol on 2017-10-31.
 */

public class TextPopup extends DialogFragment {

    private Context mContext;

    View.OnTouchListener mContainerTouchListener;
    private View mContainer;
    private View mFloatingLayout;
    @BindView(R.id.popup_text_floatingActionButton_xout) ImageView mImageViewXout;

    @BindView(R.id.privacy_policy_recyclerView) RecyclerView privacyPolicyRecyclerView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);

        mContext = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.popup_text_dialog_fragment, container);
        ButterKnife.bind(this, view);
        getDialog().setCanceledOnTouchOutside(true);

        initView(view);
        setListener();

        return view;
    }

    private void initView(View view) {

        mFloatingLayout = view.findViewById(R.id.popup_text_floating_layout);
        mContainer = view.findViewById(R.id.popup_text_container);

        mFloatingLayout.setBackground(getContext().getResources().getDrawable(R.drawable.popup_bottom, null));
        mImageViewXout.setImageResource(R.mipmap.more);
        mImageViewXout.setRotation(45);

        readPrivacy();
        //readTerms();
    }

    private void setListener() {

        if (mContainerTouchListener != null)
            mContainer.setOnTouchListener(mContainerTouchListener);

        mImageViewXout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public View getFloatingLayout() {
        return mFloatingLayout;
    }

    public void setTouchListener(View.OnTouchListener onTouchListener) {
        mContainerTouchListener = onTouchListener;
    }

    private void readPrivacy() {
        StringBuilder text = new StringBuilder();
        String[] privacyPolicyStrings = new String[]{};
        ArrayList<String> stringList = new ArrayList<>();
        try {
            AssetManager am = getContext().getAssets();
            InputStream inputStream = am.open("privacy_policy.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

            BufferedReader br = new BufferedReader(inputStreamReader);
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                AssetManager am = getContext().getAssets();
                InputStream inputStream = am.open("terms_of_service.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                BufferedReader br = new BufferedReader(inputStreamReader);
                String line;
                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }

                privacyPolicyStrings = text.toString().split("<title>");

                for(int i = 0; i < privacyPolicyStrings.length; ++i) {
                    stringList.add(privacyPolicyStrings[i]);
                }

                stringList.add("\n\n\n");

            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                privacyPolicyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                TextPopupRecyclerViewAdapter privacyAdapter = new TextPopupRecyclerViewAdapter(getContext(), stringList);
                privacyPolicyRecyclerView.setAdapter(privacyAdapter);
            }
        }
    }

}
