package com.matching.goodgoodman.constellationdating.popup_text;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.matching.goodgoodman.constellationdating.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dorol on 2017-11-01.
 */

public class TextPopupRecyclerViewAdapter extends RecyclerView.Adapter<TextPopupRecyclerViewAdapter.ViewHolder>{

    private Context mContext;
    private TextPopupRecyclerViewAdapter.ViewHolder mViewHolder;

    private ArrayList<String> mParagraphs;
    private int textNum;

    public TextPopupRecyclerViewAdapter(Context context, ArrayList<String> text) {
        mContext = context;
        //sharedpreferences = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
        //mDescriptionDBHelper = new DescriptionDBHelper(mContext,sharedpreferences.getString("DESCRIPTION_LANGUAGE", DescriptionDBHelper.LANGUAGE_ENGLISH));
        textNum = text.size() - 1;
        mParagraphs = text;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.popup_text_dialog_item, parent, false);

        //mDescriptionDBHelper.getDescription(369).getComment();

        mViewHolder = new ViewHolder(view);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String[] insideParagraphs = mParagraphs.get(position + 1).split("<body>");
        String titleString = insideParagraphs[0].replace('\n',' ');

        if(titleString.equals("Privacy Policy ") || titleString.equals("Terms of Service ")) {
            holder.mainTitle.setText(titleString);
            holder.title.setText("");
        }
        else {
            holder.mainTitle.setText("");
            holder.title.setText(titleString);
        }
        holder.text.setText(insideParagraphs[insideParagraphs.length -1]);
    }

    @Override
    public int getItemCount() {
        return textNum;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.setting_popup_recyclerView_title) TextView mainTitle;
        @BindView(R.id.setting_popup_recyclerView_subtitle) TextView title;
        @BindView(R.id.setting_popup_recyclerView_text) TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
