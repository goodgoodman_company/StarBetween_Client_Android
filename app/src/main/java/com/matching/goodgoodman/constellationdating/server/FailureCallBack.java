package com.matching.goodgoodman.constellationdating.server;

import retrofit2.Call;

/**
 * Created by Hyeong_Wook on 2017-08-24.
 */

public interface FailureCallBack<T> {
    void onCallBack(Call<T> call, Throwable t);
}
