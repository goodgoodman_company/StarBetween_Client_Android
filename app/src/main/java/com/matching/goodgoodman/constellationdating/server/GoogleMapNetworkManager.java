package com.matching.goodgoodman.constellationdating.server;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.matching.goodgoodman.constellationdating.server.model.GoogleTimeZoneInfo;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 구글 맵 api와 통신하기 위한 클래스
 * 단순 메소드 호출로 통신하는 Service를 제공한다.
 * @author Hyeong_Wook
 */

public class GoogleMapNetworkManager {
    public static String BASE_URL = "https://maps.googleapis.com/";
    public static String API_KEY_NAME = "com.google.android.geo.API_KEY";

    private Context mContext;
    private Retrofit mRetrofit;
    private GoogleMapService mGoogleMapService;

    private static volatile GoogleMapNetworkManager mInstance;
    public static GoogleMapNetworkManager getInstance(Context context) {
        if(mInstance == null) {
            synchronized (NetworkManager.class) {
                if(mInstance == null)
                    mInstance = new GoogleMapNetworkManager(context);
            }
        }
        return mInstance;
    }

    private GoogleMapNetworkManager(Context context) {
        mContext = context.getApplicationContext();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mGoogleMapService = mRetrofit.create(GoogleMapService.class);
    }

    /**
     * 메타데이터로부터 API key를 불러온다.
     * @return api key
     */
    private String getAPIKey() {
        try {
            ApplicationInfo applicationInfo = mContext.getPackageManager().getApplicationInfo(mContext.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;
            return bundle.getString(API_KEY_NAME);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 위치와 시간에 따른 timeZone 정보를 비동기적으로 받아온다.
     * @param location 위치정보, "위도,경도" 형식으로 보낸다.
     * @param timestamp 원하는 시간의 timestamp
     * @param successCallBack 성공시 callback
     * @param failureCallBack 실패시 callback
     */
    public void getGoogleTimeZoneInfo(String location, Long timestamp, SuccessCallBack<GoogleTimeZoneInfo> successCallBack, FailureCallBack<GoogleTimeZoneInfo> failureCallBack) {
        String key = getAPIKey();
        if(key == null)
            return;

        Call<GoogleTimeZoneInfo> googleMapInfoCall = mGoogleMapService.getGoogleMapInfo(location, timestamp, key);
        googleMapInfoCall.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * 위치와 시간에 따른 timeZone 정보를 비동기적으로 받아온다.
     * @param location 위치정보, "위도,경도" 형식으로 보낸다.
     * @param timestamp 원하는 시간의 timestamp
     * @param successCallBack 성공시 callback
     */
    public void getGoogleTimeZoneInfo(String location, Long timestamp, SuccessCallBack<GoogleTimeZoneInfo> successCallBack) {
        getGoogleTimeZoneInfo(location, timestamp, successCallBack, null);
    }
}
