package com.matching.goodgoodman.constellationdating.server;

import com.matching.goodgoodman.constellationdating.server.model.GoogleTimeZoneInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Hyeong_Wook on 2017-08-24.
 */

public interface GoogleMapService {
    @GET("maps/api/timezone/json")
    Call<GoogleTimeZoneInfo> getGoogleMapInfo(@Query("location")String location, @Query("timestamp")Long timestamp, @Query("key")String key);
}
