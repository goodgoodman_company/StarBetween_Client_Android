package com.matching.goodgoodman.constellationdating.server;

import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.server.model.ChatRoomListModel;
import com.matching.goodgoodman.constellationdating.server.model.LoginData;
import com.matching.goodgoodman.constellationdating.server.model.ResultStatus;
import com.matching.goodgoodman.constellationdating.server.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 서버와의 통신을 쉽게 하기 위한 RetroFit 래핑 클래스.
 * 단순 메소드 호출로 통신하는 Service를 제공한다.
 * @author Hyeong_Wook
 */

public class NetworkManager {
    private static final String BASE_URL = "http://35.196.128.68:8080/";
    private static final String PROPERTY_BODY = "body";

    private Context mContext;
    private Retrofit mRetrofit;
    private StarBetweenService mStarBetweenService;

    private static volatile NetworkManager mInstance;
    public static NetworkManager getInstance(Context context) {
        if(mInstance == null) {
            synchronized (NetworkManager.class) {
                if(mInstance == null)
                    mInstance = new NetworkManager(context);
            }
        }
        return mInstance;
    }

    private NetworkManager(Context context) {
        mContext = context.getApplicationContext();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                //.addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        mStarBetweenService = mRetrofit.create(StarBetweenService.class);
    }

    /**
     * 비동기적으로 모든 user들의 리스트를 구한다.
     * @param successCallBack 성공시의 callback
     * @param failureCallBack 실패시의 callback
     */
    public void getUsers(SuccessCallBack<List<User>> successCallBack, FailureCallBack<List<User>> failureCallBack) {
        Call<List<User>> userListCallBack = mStarBetweenService.getUsers();
        userListCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * 비동기적으로 모든 user들의 리스트를 구한다.
     * @param successCallBack 성공시의 callback
     */
    public void getUsers(SuccessCallBack<List<User>> successCallBack) {
        getUsers(successCallBack, null);
    }

    /**
     * userId를 이용하여 비동기적으로 특정 user객체를 구한다.
     * @param userNo 원하는 user의 id
     * @param successCallBack 성공시의 callback
     * @param failureCallBack 실패시의 callback
     */
    public void getUser(int userNo, SuccessCallBack<User> successCallBack, FailureCallBack<User> failureCallBack) {
        Call<User> userCallBack = mStarBetweenService.getUser(userNo);
        userCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * userId를 이용하여 비동기적으로 특정 user객체를 구한다.
     * @param userNo 원하는 user의 no
     * @param successCallBack 성공시의 callback
     */
    public void getUser(int userNo, SuccessCallBack<User> successCallBack) {
        getUser(userNo, successCallBack, null);
    }

    /**
     * userId를 이용하여 비동기적으로 특정 user객체를 구한다.
     * @param userNo 원하는 user의 id
     * @param fromNo 관계가 있는 user의 number
     * @param successCallBack 성공시의 callback
     * @param failureCallBack 실패시의 callback
     */
    public void getUser(int userNo, int fromNo, SuccessCallBack<User> successCallBack, FailureCallBack<User> failureCallBack) {
        Call<User> userCallBack = mStarBetweenService.getUser(userNo, fromNo);
        userCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * userId를 이용하여 비동기적으로 특정 user객체를 구한다.
     * @param userNo 원하는 user의 no
     * @param fromNo 관계가 있는 user의 number
     * @param successCallBack 성공시의 callback
     */
    public void getUser(int userNo, int fromNo, SuccessCallBack<User> successCallBack) {
        getUser(userNo, fromNo, successCallBack, null);
    }

    /**
     * userId를 이용하여 비동기적으로 특정 user의 Chat들의 리스트들 구한다.
     * @param userNo 원하는 user의 no
     * @param successCallBack 성공시 callback
     * @param failureCallBack 실패시 callback
     */
    public void getUserChats(int userNo, SuccessCallBack<ChatRoomListModel> successCallBack, FailureCallBack<ChatRoomListModel> failureCallBack) {
        Call<ChatRoomListModel> userChatListCallBack = mStarBetweenService.getChats(userNo);
        userChatListCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * userId를 이용하여 비동기적으로 특정 user의 Chat들의 리스트들 구한다.
     * @param userNo 원하는 user의 no
     * @param successCallBack 성공시 callback
     */
    public void getUserChats(int userNo, SuccessCallBack<ChatRoomListModel> successCallBack) {
        getUserChats(userNo, successCallBack, null);
    }

    /**
     * userId를 이용하여 비동기적으로 특정 user객체를 구한다.
     * @param userModel 원하는 user의 model
     * @param successCallBack 성공시의 callback : 매칭목록정보를 받아온다 :: 아직 확실히 정해지지 않음
     */
    public void getSignUp(UserModel userModel, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> userSignUpCallBack = mStarBetweenService.getSignUp(userModel.getId(), userModel.getFaceBookToken(), userModel.getEmail(), userModel.getContent(), userModel.getName(),
                userModel.getGender(), userModel.getLatitude(), userModel.getLongitude(), userModel.getBirthdaySimpleDateFormat());
        userSignUpCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    public void getSignUp(UserModel userModel, SuccessCallBack<ResultStatus> successCallBack) {
        getSignUp(userModel, successCallBack, null);
    }

    public void getLogin(UserModel userModel, SuccessCallBack<LoginData> successCallBack, FailureCallBack<LoginData> failureCallBack) {
        Call<LoginData> userSignUpCallBack = mStarBetweenService.getLogin(userModel.getId(), FirebaseInstanceId.getInstance().getToken(), userModel.getCurrentLatitude(), userModel.getCurrentLongitude());
        userSignUpCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    public void getLogin(UserModel userModel, SuccessCallBack<LoginData> successCallBack) {
        getLogin(userModel, successCallBack, null);
    }

    public void putUser(int userNo, String content, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> userCallBack = mStarBetweenService.modifyUser(userNo, content);
        userCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    public void putUser(int userNo, String content, SuccessCallBack<ResultStatus> successCallBack) {
        putUser(userNo, content, successCallBack, null);
    }

    public void putUseFreePoint(UserModel counterUserModel, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> userFreePointCallBack = mStarBetweenService.putUseFreePoint(counterUserModel.getMatchingNo());
        userFreePointCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    public void putUseFreePoint(UserModel userModel, SuccessCallBack<ResultStatus> successCallBack) {
        putUseFreePoint(userModel, successCallBack, null);
    }

    public void putUseStarPointforBlur(UserModel counterUserModel, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> userFreePointCallBack = mStarBetweenService.putUseStarPoint(counterUserModel.getMatchingNo(), counterUserModel.getGrade(), UserModel.POINT10);
        userFreePointCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    public void putUseStarPointforBlur(UserModel counterUserModel, SuccessCallBack<ResultStatus> successCallBack) {
        putUseStarPointforBlur(counterUserModel, successCallBack, null);
    }

    public void putUseStarPointforLike(UserModel counterUserModel, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> userFreePointCallBack = mStarBetweenService.putUseStarPoint(counterUserModel.getMatchingNo(), counterUserModel.getGrade(), UserModel.POINT314);
        userFreePointCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    public void putUseStarPointforLike(UserModel counterUserModel, SuccessCallBack<ResultStatus> successCallBack) {
        putUseStarPointforLike(counterUserModel, successCallBack, null);
    }

    /**
     * starPoint를 추가한다.
     * @param userNo point가 추가될 유저
     * @param point point의 양
     * @param successCallBack successCallback
     * @param failureCallBack failureCallback
     */
    public void putStarPoint(int userNo, int point, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> putStarPointCallBack = mStarBetweenService.putStarPoint(userNo, point);
        putStarPointCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * starPoint를 추가한다.
     * @param userNo point가 추가될 유저
     * @param point point의 양
     * @param successCallBack successCallback
     */
    public void putStarPoint(int userNo, int point, SuccessCallBack<ResultStatus> successCallBack) {
        putStarPoint(userNo, point, successCallBack, null);
    }

    /**
     * freePoint를 추가한다.
     * @param userNo point가 추가될 유저
     * @param point point의 양
     * @param successCallBack successCallback
     * @param failureCallBack failureCallback
     */
    public void putFreePoint(int userNo, int point, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> putFreePointCallBack = mStarBetweenService.putFreePoint(userNo, point);
        putFreePointCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * freePoint를 추가한다.
     * @param userNo point가 추가될 유저
     * @param point point의 양
     * @param successCallBack successCallback
     */
    public void putFreePoint(int userNo, int point, SuccessCallBack<ResultStatus> successCallBack) {
        putFreePoint(userNo, point, successCallBack, null);
    }

    /**
     * 상대방에게 좋아요를 보낸다.
     * @param userNo user의 no
     * @param counterNo 상대방의 no
     * @param successCallBack successCallback
     * @param failureCallBack failureCallback
     */
    public void putLike(int userNo, int counterNo, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> putLikeCallBack = mStarBetweenService.putLike(userNo, counterNo);
        putLikeCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * 상대방에게 좋아요를 보낸다.
     * @param userNo user의 no
     * @param counterNo 상대방의 no
     * @param successCallBack successCallback
     */
    public void putLike(int userNo, int counterNo, SuccessCallBack<ResultStatus> successCallBack) {
        putLike(userNo, counterNo, successCallBack, null);
    }

    /**
     * chatRoom을 파기한다.
     * @param roomKey 파기할 chatRoomId
     * @param successCallBack successCallBack
     * @param failureCallBack failureCallBack
     */
    public void putDestroyChatRoom(String roomKey, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> putDoNotLikeCallBack = mStarBetweenService.putDoNotLike(roomKey);
        putDoNotLikeCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * chatRoom을 파기한다.
     * @param roomKey 파기할 chatRoomId
     * @param successCallBack successCallBack
     */
    public void putDestroyChatRoom(String roomKey, SuccessCallBack<ResultStatus> successCallBack) {
        putDestroyChatRoom(roomKey, successCallBack, null);
    }

    /**
     * 프리미엄을 등록한다.
     * @param userNo 프리미엄 등록 유저
     * @param successCallBack successCallBack
     * @param failureCallBack failureCallBack
     */
    public void putPremium(int userNo, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> putPremiumCallBack = mStarBetweenService.putPremium(userNo);
        putPremiumCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * 프리미엄을 등록한다.
     * @param userNo 프리미엄 등록 유저
     * @param successCallBack successCallBack
     */
    public void putPremium(int userNo, SuccessCallBack<ResultStatus> successCallBack) {
        putPremium(userNo, successCallBack, null);
    }

    /**
     * 오늘의 최초접속임을 서버에 알린다.
     * @param userNo 접속한 유저
     * @param successCallBack successCallBack
     * @param failureCallBack failureCallBack
     */
    public void putTodayFirst(int userNo, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> putTodayFirstCallBack = mStarBetweenService.putTodayFirst(userNo);
        putTodayFirstCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * 오늘의 최초접속임을 서버에 알린다.
     * @param userNo 접속한 유저
     * @param successCallBack successCallBack
     */
    public void putTodayFirst(int userNo, SuccessCallBack<ResultStatus> successCallBack) {
        putTodayFirst(userNo, successCallBack, null);
    }

    /**
     * facebook 사진을 공개할지 말지 결정한다.
     * @param userNo 공개대상
     * @param value 값, Y 혹은 N
     * @param successCallBack successCallBack
     * @param failureCallBack failureCallBack
     */
    public void setFacebookOpen(int userNo, String value, SuccessCallBack<ResultStatus> successCallBack, FailureCallBack<ResultStatus> failureCallBack) {
        Call<ResultStatus> putFacebookOpenCallBack = mStarBetweenService.setFacebookPhotoOpen(userNo, value);
        putFacebookOpenCallBack.enqueue(new RetrofitCallBack<>(successCallBack, failureCallBack));
    }

    /**
     * facebook 사진을 공개할지 말지 결정한다.
     * @param userNo 공개대상
     * @param value 값, Y 혹은 N
     * @param successCallBack successCallBack
     */
    public void setFacebookOpen(int userNo, String value, SuccessCallBack<ResultStatus> successCallBack) {
        setFacebookOpen(userNo, value, successCallBack, null);
    }
}











