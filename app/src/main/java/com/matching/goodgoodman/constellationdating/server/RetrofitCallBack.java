package com.matching.goodgoodman.constellationdating.server;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitCallBack<T> implements Callback<T> {
    private SuccessCallBack<T> mSuccessCallBack;
    private FailureCallBack<T> mFailureCallBack;

    public RetrofitCallBack(SuccessCallBack<T> successCallBack, FailureCallBack<T> failureCallBack) {
        mSuccessCallBack = successCallBack;
        mFailureCallBack = failureCallBack;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if(response.isSuccessful()) {
            mSuccessCallBack.onCallBack(response.body());
        } else {
            if(response.code() >= 400 && response.code() < 500) {
                Log.e("request error", response.code() + ", " + response.body());
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }

            if(response.code() >= 500 && response.code() < 600)
                Log.e("server error", response.code() + ", " + response.body());

            onFailure(call, new Exception("onResponseFail"));
        }
    }



    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if(mFailureCallBack != null)
            mFailureCallBack.onCallBack(call, t);
    }
}