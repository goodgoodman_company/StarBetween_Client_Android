package com.matching.goodgoodman.constellationdating.server;

import com.matching.goodgoodman.constellationdating.server.model.ChatRoomListModel;
import com.matching.goodgoodman.constellationdating.server.model.LoginData;
import com.matching.goodgoodman.constellationdating.server.model.ResultStatus;
import com.matching.goodgoodman.constellationdating.server.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Songmoo on 2017-04-21.
 */

public interface StarBetweenService {

    @GET("users")
    Call<List<User>> getUsers();

    @GET("login/signup")
    Call<ResultStatus> getSignUp(@Query("mId") String id,
                                 @Query("mToken") String token,
                                 @Query("mEmail") String email,
                                 @Query("mSelfIntroduction") String content,
                                 @Query("mName") String name,
                                 @Query("mSex") String sex,
                                 @Query("mLatitude") float latitude,
                                 @Query("mLongitude") float longitude,
                                 @Query("mBirthday") String birthday);

    @GET("login")
    Call<LoginData> getLogin(@Query("mId") String id,
                             @Query("mFcmToken") String FcmToken,
                             @Query("mLatitude") float latitude,
                             @Query("mLongitude") float longitude);

    @GET("user/{user_no}")
    Call<User> getUser(@Path("user_no") int no);

    @GET("user/{user_no}")
    Call<User> getUser(@Path("user_no") int no, @Query("fromNo") int fromNo);

    @PUT("user/{user_no}")
    Call<ResultStatus> modifyUser(@Path("user_no") int no,
                                    @Query("mSelfIntroduction") String content);

    @PUT("point/freePoint/{matchingNo}")
    Call<ResultStatus> putUseFreePoint(@Path("matchingNo") int matchingNo);

    @PUT("point/starPoint/{matchingNo}")
    Call<ResultStatus> putUseStarPoint(@Path("matchingNo") int matchingNo,
                                         @Query("grade") String grade,
                                         @Query("pointCode") String point);

    @PUT("point/addStar/{userNo}")
    Call<ResultStatus> putStarPoint(@Path("userNo") int userNo, @Query("point") int point);

    @GET("/chatting/room/{user_no}")
    Call<ChatRoomListModel> getChats(@Path("user_no") int userNo);

    @PUT("/chatting/like")
    Call<ResultStatus> putLike(@Query("mLikeFrom") int userNo, @Query("mLikeTo") int counterNo);

    @PUT("/chatting/dontLike/{chatRoomId}")
    Call<ResultStatus> putDoNotLike(@Path("chatRoomId") String chatRoomId);

    @PUT("/user/premium/{userNo}")
    Call<ResultStatus> putPremium(@Path("userNo") int userNo);

    @PUT("/user/todayFirst/{userNo}")
    Call<ResultStatus> putTodayFirst(@Path("userNo") int userNo);

    @PUT("/user/imageOpen/{userNo}")
    Call<ResultStatus> setFacebookPhotoOpen(@Path("userNo") int userNo, @Query("val") String value);

    @PUT("/point/addFree/{userNo}")
    Call<ResultStatus> putFreePoint(@Path("userNo") int userNo, @Query("point") int point);
}



