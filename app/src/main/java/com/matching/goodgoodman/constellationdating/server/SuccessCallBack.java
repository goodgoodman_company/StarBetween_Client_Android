package com.matching.goodgoodman.constellationdating.server;

/**
 * Created by Hyeong_Wook on 2017-08-24.
 */

public interface SuccessCallBack<T> {
    void onCallBack(T result);
}
