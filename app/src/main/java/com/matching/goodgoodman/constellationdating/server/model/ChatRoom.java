package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by dorol on 2017-05-29.
 */

public class ChatRoom {
    private static final String YES = "Y";

    @SerializedName("mUserFromNo")
    private int mUserFromNo;
    @SerializedName("mUserToNo")
    private int mUserToNo;
    @SerializedName("mRoomId")
    private String mRoomKey;
    @SerializedName("mMatchingYn")
    private String mMatchingYn;
    @SerializedName("mCrtDt")
    private long mCreateDate;
    @SerializedName("mEndDt")
    private long mEndDate;
    @SerializedName("mUseYn")
    private String mUseYn;

    public ChatRoom(int userFromNo, int userToNo, String roomKey) {
        mUserFromNo = userFromNo;
        mUserToNo = userToNo;
        mRoomKey = roomKey;
    }

    public int getUserFromNo() {
        return mUserFromNo;
    }

    public void setUserFromNo(int userFromNo) {
        mUserFromNo = userFromNo;
    }

    public int getUserToNo() {
        return mUserToNo;
    }

    public void setUserToNo(int userToNo) {
        mUserToNo = userToNo;
    }

    public String getRoomKey() {
        return mRoomKey;
    }

    public void setRoomKey(String roomKey) {
        mRoomKey = roomKey;
    }

    public String getMatchingYn() {
        return mMatchingYn;
    }

    public void setMatchingYn(String matchingYn) {
        mMatchingYn = matchingYn;
    }

    public boolean isMatched() {
        return mMatchingYn.equals(YES);
    }

    public Date getCreateDate() {
        return new Date(mCreateDate);
    }

    public void setCreateDate(long createDate) {
        mCreateDate = createDate;
    }

    public Date getEndDate() {
        return new Date(mEndDate);
    }

    public void setEndDate(long endDate) {
        mEndDate = endDate;
    }

    public String getUseYn() {
        return mUseYn;
    }

    public void setUseYn(String useYn) {
        mUseYn = useYn;
    }

    public boolean isUsed() {
        return mUseYn.equals(YES);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ChatRoom) {
            ChatRoom chatRoom = (ChatRoom) obj;
            return (mUserFromNo == chatRoom.getUserFromNo() && mUserToNo == chatRoom.getUserToNo()) ||
                    (mUserFromNo == chatRoom.getUserToNo() && mUserToNo == chatRoom.getUserFromNo());
        }
        return false;
    }
}
