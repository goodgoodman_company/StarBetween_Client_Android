package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Hyeong_Wook on 2017-10-07.
 */

public class ChatRoomListModel {
    @SerializedName("list")
    List<ChatRoom> mChatRoomList;

    public List<ChatRoom> getChatRoomList() {
        return mChatRoomList;
    }

    public void setChatRoomList(List<ChatRoom> chatRoomList) {
        mChatRoomList = chatRoomList;
    }
}
