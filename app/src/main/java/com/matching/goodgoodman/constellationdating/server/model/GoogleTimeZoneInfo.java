package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hyeong_Wook on 2017-08-24.
 */

public class GoogleTimeZoneInfo {

    @SerializedName("dstOffset")
    private int mDstOffset;
    @SerializedName("rawOffset")
    private int mRawOffset;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("timeZoneId")
    private String mTimeZoneId;
    @SerializedName("timeZoneName")
    private String mTimeZoneName;
    @SerializedName("errorMessage")
    private String mErrorMessage;

    public void setDstOffset(int dstOffset) {
        mDstOffset = dstOffset;
    }

    public void setRawOffset(int rawOffset) {
        mRawOffset = rawOffset;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public void setTimeZoneId(String timeZoneId) {
        mTimeZoneId = timeZoneId;
    }

    public void setTimeZoneName(String timeZoneName) {
        mTimeZoneName = timeZoneName;
    }

    public void setErrorMessage(String errorMessage) {
        mErrorMessage = errorMessage;
    }

    public int getDstOffset() {
        return mDstOffset;
    }

    public int getRawOffset() {
        return mRawOffset;
    }

    public String getStatus() {
        return mStatus;
    }

    public String getTimeZoneId() {
        return  mTimeZoneId;
    }

    public String getTimeZoneName() {
        return mTimeZoneName;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }
}
