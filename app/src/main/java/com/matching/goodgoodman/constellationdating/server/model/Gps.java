package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dorol on 2017-09-11.
 */

public class Gps {
    @SerializedName("no")
    private int mNo;
    @SerializedName("user_id")
    private String mUserId;
    @SerializedName("latitude")
    private float mLatitude;
    @SerializedName("longitude")
    private float mLongitude;

    public int getNo() {
        return mNo;
    }

    public void setNo(int no) {
        this.mNo = no;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        this.mUserId = userId;
    }

    public float getLatitude() {
        return mLatitude;
    }

    public void setLatitude(float latitude) {
        this.mLatitude = latitude;
    }

    public float getLongitude() {
        return mLongitude;
    }

    public void setLongitude(float longitude) {
        this.mLongitude = longitude;
    }
}
