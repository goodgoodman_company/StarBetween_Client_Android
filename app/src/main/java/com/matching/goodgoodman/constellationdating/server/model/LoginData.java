package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by dorol on 2017-09-11.
 */

public class LoginData {
    @SerializedName("user")
    private User mUser;
    @SerializedName("matchingList")
    private List<User> mMatchingList;
    @SerializedName("point")
    private Point mPoint;
    @SerializedName("result")
    private String mResult = "";


    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public List<User> getMatchingList() {
        return mMatchingList;
    }

    public void setMatchingList(List<User> matchingList) {
        mMatchingList = matchingList;
    }

    public Point getPoint() {
        return mPoint;
    }

    public void setPoint(Point point) {
        mPoint = point;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }
}
