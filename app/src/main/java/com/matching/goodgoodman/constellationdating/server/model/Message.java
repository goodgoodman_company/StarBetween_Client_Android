package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dorol on 2017-05-29.
 */

public class Message {

    @SerializedName("id")
    private String messagesId;
    @SerializedName("chat_id")
    private String messagesChatId;
    @SerializedName("user_id")
    private String messagesUserId;
    @SerializedName("body")
    private String messagesBody;
    @SerializedName("created_at")
    private String messagesCreatedAt;
    @SerializedName("updated_at")
    private String messagesUpdatedAt;

    public String getMessagesId() {
        return messagesId;
    }

    public void setMessagesId(String messagesId) {
        this.messagesId = messagesId;
    }

    public String getMessagesChatId() {
        return messagesChatId;
    }

    public void setMessagesChatId(String messagesChatId) {
        this.messagesChatId = messagesChatId;
    }

    public String getMessagesUserId() {
        return messagesUserId;
    }

    public void setMessagesUserId(String messagesUserId) {
        this.messagesUserId = messagesUserId;
    }

    public String getMessagesBody() {
        return messagesBody;
    }

    public void setMessagesBody(String messagesBody) {
        this.messagesBody = messagesBody;
    }

    public String getMessagesCreatedAt() {
        return messagesCreatedAt;
    }

    public void setMessagesCreatedAt(String messagesCreatedAt) {
        this.messagesCreatedAt = messagesCreatedAt;
    }

    public String getMessagesUpdatedAt() {
        return messagesUpdatedAt;
    }

    public void setMessagesUpdatedAt(String messagesUpdatedAt) {
        this.messagesUpdatedAt = messagesUpdatedAt;
    }

    @Override
    public String toString() {
        return messagesBody + " // " ;
    }
}
