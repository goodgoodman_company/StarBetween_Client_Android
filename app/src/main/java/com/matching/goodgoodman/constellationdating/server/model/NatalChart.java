package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by GoodGoodMan on 2017-05-30.
 */

public class NatalChart {
    //TODO : 임시 star_sign_img
    @SerializedName("star_sign_img")
    private String mStarSignImag;
    @SerializedName("sun_sign_str")
    private String mSunSign;
    @SerializedName("asc_sign_str")
    private String mAscSign;
    @SerializedName("moon_sign_str")
    private String mMoonSign;
    @SerializedName("house_str")
    private String mHouseSign;
    @SerializedName("mer_sign_str")
    private String mMerSign;
    @SerializedName("ven_sign_str")
    private String mVenSign;
    @SerializedName("mars_sign_str")
    private String mMarsSign;
    @SerializedName("jup_sign_str")
    private String mJupSign;
    @SerializedName("sat_sign_str")
    private String mSatSign;

    public String getmStarSignImag() {
        return mStarSignImag;
    }

    public String getmSunSign() {
        return mSunSign;
    }

    public String getAscSign() {
        return mAscSign;
    }

    public String getMoonSign() {
        return mMoonSign;
    }

    public String getHouseSign() {
        return mHouseSign;
    }

    public String getMerSign() {
        return mMerSign;
    }

    public String getVenSign() {
        return mVenSign;
    }

    public String getMarsSign() {
        return mMarsSign;
    }

    public String getJupSign() {
        return mJupSign;
    }

    public String getSatSign() {
        return mSatSign;
    }

}
