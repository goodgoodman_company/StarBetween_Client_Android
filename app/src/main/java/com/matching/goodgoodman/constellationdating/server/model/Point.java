package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hyeong_Wook on 2017-09-24.
 */

public class Point {
    @SerializedName("mUserNo")
    private long mUserNo;
    @SerializedName("mFreePoint")
    private int mFreePoint;
    @SerializedName("mStarPoint")
    private int mStarPoint;
    @SerializedName("mAttendancePoint")
    private int mAttendancePoint;

    public long getUserNo() {
        return mUserNo;
    }

    public void setUserNo(long userNo) {
        mUserNo = userNo;
    }

    public int getFreePoint() {
        return mFreePoint;
    }

    public void setFreePoint(int freePoint) {
        this.mFreePoint = freePoint;
    }

    public int getStarPoint() {
        return mStarPoint;
    }

    public void setStarPoint(int starPoint) {
        this.mStarPoint = starPoint;
    }

    public int getAttendancePoint() {
        return mAttendancePoint;
    }

    public void setAttendancePoint(int attendancePoint) {
        this.mAttendancePoint = attendancePoint;
    }
}
