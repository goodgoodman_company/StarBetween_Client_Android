package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dorol on 2017-09-27.
 */

public class ResultStatus {
    public static final String SUCCESS = "success";

    @SerializedName("result")
    String mResult;
    @SerializedName("msg")
    String mMessage;

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }
}
