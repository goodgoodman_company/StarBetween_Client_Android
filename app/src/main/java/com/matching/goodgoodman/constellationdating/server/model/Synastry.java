package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dorol on 2017-05-27.
 */

public class Synastry {

    @SerializedName("id")
    int synastryId;
    @SerializedName("day")
    int synastryDay;
    @SerializedName("male_id")
    String synastryMaleId;
    @SerializedName("female_id")
    String synastryFemaleId;
    @SerializedName("grade")
    int synastryGrade;
    @SerializedName("synastry_char_m_img")
    String synastryChartMaleImage;
    @SerializedName("synastry_char_f_img")
    String synastryChartFemaleImage;

    public int getSynastryId() {
        return synastryId;
    }

    public void setSynastryId(int synastryId) {
        this.synastryId = synastryId;
    }

    public int getSynastryDay() {
        return synastryDay;
    }

    public void setSynastryDay(int synastryDay) {
        this.synastryDay = synastryDay;
    }

    public String getSynastryMaleId() {
        return synastryMaleId;
    }

    public void setSynastryMaleId(String synastryMaleId) {
        this.synastryMaleId = synastryMaleId;
    }

    public String getSynastryFemaleId() {
        return synastryFemaleId;
    }

    public void setSynastryFemaleId(String synastryFemaleId) {
        this.synastryFemaleId = synastryFemaleId;
    }

    public int getSynastryGrade() {
        return synastryGrade;
    }

    public void setSynastryGrade(int synastryGrade) {
        this.synastryGrade = synastryGrade;
    }

    public String getSynastryChartMaleImage() {
        return synastryChartMaleImage;
    }

    public void setSynastryChartMaleImage(String synastryChartMaleImage) {
        this.synastryChartMaleImage = synastryChartMaleImage;
    }

    public String getSynastryChartFemaleImage() {
        return synastryChartFemaleImage;
    }

    public void setSynastryChartFemaleImage(String synastryChartFemaleImage) {
        this.synastryChartFemaleImage = synastryChartFemaleImage;
    }
}
