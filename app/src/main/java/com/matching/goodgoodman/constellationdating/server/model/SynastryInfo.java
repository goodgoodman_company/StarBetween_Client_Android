package com.matching.goodgoodman.constellationdating.server.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dorol on 2017-05-28.
 */

public class SynastryInfo {
    @SerializedName("comp_sumry")
    private String compSumry;

    @SerializedName("comp_pros_str_1")
    private String compProsString1;
    @SerializedName("comp_pros_img_1")
    private String compProsImage1;
    @SerializedName("comp_pros_str_2")
    private String compProsString2;
    @SerializedName("comp_pros_img_2")
    private String compProsImage2;
    @SerializedName("comp_pros_str_3")
    private String compProsString3;
    @SerializedName("comp_pros_img_3")
    private String compProsImage3;

    @SerializedName("comp_cons_str_1")
    private String compConsString1;
    @SerializedName("comp_cons_img_1")
    private String compConsImage1;
    @SerializedName("comp_cons_str_2")
    private String compConsString2;
    @SerializedName("comp_cons_img_2")
    private String compConsImage2;

    public String getCompSumry() {
        return compSumry;
    }

    public void setCompSumry(String compSumry) {
        this.compSumry = compSumry;
    }

    public String getCompProsString1() {
        return compProsString1;
    }

    public void setCompProsString1(String compProsString1) {
        this.compProsString1 = compProsString1;
    }

    public String getCompProsImage1() {
        return compProsImage1;
    }

    public void setCompProsImage1(String compProsImage1) {
        this.compProsImage1 = compProsImage1;
    }

    public String getCompProsString2() {
        return compProsString2;
    }

    public void setCompProsString2(String compProsString2) {
        this.compProsString2 = compProsString2;
    }

    public String getCompProsImage2() {
        return compProsImage2;
    }

    public void setCompProsImage2(String compProsImage2) {
        this.compProsImage2 = compProsImage2;
    }

    public String getCompProsString3() {
        return compProsString3;
    }

    public void setCompProsString3(String compProsString3) {
        this.compProsString3 = compProsString3;
    }

    public String getCompProsImage3() {
        return compProsImage3;
    }

    public void setCompProsImage3(String compProsImage3) {
        this.compProsImage3 = compProsImage3;
    }

    public String getCompConsString1() {
        return compConsString1;
    }

    public void setCompConsString1(String compConsString1) {
        this.compConsString1 = compConsString1;
    }

    public String getCompConsImage1() {
        return compConsImage1;
    }

    public void setCompConsImage1(String compConsImage1) {
        this.compConsImage1 = compConsImage1;
    }

    public String getCompConsString2() {
        return compConsString2;
    }

    public void setCompConsString2(String compConsString2) {
        this.compConsString2 = compConsString2;
    }

    public String getCompConsImage2() {
        return compConsImage2;
    }

    public void setCompConsImage2(String compConsImage2) {
        this.compConsImage2 = compConsImage2;
    }

}
