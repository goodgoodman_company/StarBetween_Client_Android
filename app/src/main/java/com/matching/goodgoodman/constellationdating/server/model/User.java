package com.matching.goodgoodman.constellationdating.server.model;

import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by dorol on 2017-05-24.
 */

public class User {
    @SerializedName("mNo")
    private int mNo;
    @SerializedName("mId")
    private String mId;
    @SerializedName("mToken")
    private String mToken;
    @SerializedName("mName")
    private String mName;
    @SerializedName("mEmail")
    private String mEmail;
    @SerializedName("mSex")
    private String mSex;//0 male, 1 female
    @SerializedName("mLatitude")
    private float mLatitude;
    @SerializedName("mLongitude")
    private float mLongitude;
    @SerializedName("mBirthday")
    private long mBirthday;
    @SerializedName("mScore")
    private int mScore;
    @SerializedName("mRegDate")
    private long mRegDate;
    @SerializedName("mLastConDate")
    private long mLastConDate;
    @SerializedName("mLastConTime")
    private long mLastConTime;
    @SerializedName("mMatchingNo")
    private int mMatchingNo;
    @SerializedName("mUseBlurYn")
    private String mUseBlurYn;
    @SerializedName("mUseLikeYn")
    private String mUseLikeYn;
    @SerializedName("mPremiumYn")
    private String mPremiumYn;
    @SerializedName("mSelfIntroduction")
    private String mContent;
    @SerializedName("mLastLongitude")
    private float mLastLongitude;
    @SerializedName("mLastLatitude")
    private float mLastLatitude;
    @SerializedName("mMatchingUpdateYn")
    private String mMatchingUpdateYn;
    @SerializedName("mTodayFirst")
    private String mTodayFirst;
    @SerializedName("mImageOpenYn")
    private String mImageOpenYn;

    public int getNo() {
        return mNo;
    }

    public void setNo(int no) {
        this.mNo = no;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String mToken) {
        this.mToken = mToken;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getSex() {
        return mSex;
    }

    public void setSex(String mSex) {
        this.mSex = mSex;
    }

    public float getLatitude() {
        return mLatitude;
    }

    public void setLatitude(float mLatitude) {
        this.mLatitude = mLatitude;
    }

    public float getLongitude() {
        return mLongitude;
    }

    public void setLongitude(float mLongitude) {
        this.mLongitude = mLongitude;
    }

    public Date getBirthday() {
        return new Date(mBirthday);
    }

    public void setBirthday(Date birthday) {
        this.mBirthday = birthday.getTime();
    }

    public int getScore() {
        return mScore;
    }

    public void setScore(int score) {
        this.mScore = score;
    }

    public Date getRegDate() {
        return new Date(mRegDate);
    }

    public void setRegDate(Date regDate) {
        this.mRegDate = regDate.getTime();
    }

    public Date getLastConDate() {
        return new Date(mLastConDate);
    }

    public void setLastConDate(Date lastConDate) {
        this.mLastConDate = lastConDate.getTime();
    }

    public Date getLastConTime() {
        return new Date(mLastConTime);
    }

    public void setLastConTime(Date lastConTime) {
        mLastConTime = lastConTime.getTime();
    }

    public int getMatchingNo() {
        return mMatchingNo;
    }

    public void setMatchingNo(int matchingNo) {
        mMatchingNo = matchingNo;
    }

    public String getUseBlurYn() {
        if(mUseBlurYn == null)
            return UserModel.NO;

        return mUseBlurYn;
    }

    public void setUseBlurYn(String useFreePointYn) {
        mUseBlurYn = useFreePointYn;
    }

    public String getUseLikeYn() {
        if(mUseLikeYn == null)
            return UserModel.NO;

        return mUseLikeYn;
    }

    public void setUseLikeYn(String useLikeYn) {
        mUseLikeYn = useLikeYn;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public String getPremiumYn() {
        return mPremiumYn;
    }

    public void setPremiumYn(String premiumYn) {
        mPremiumYn = premiumYn;
    }

    public float getLastLongitude() {
        return mLastLongitude;
    }

    public void setLastLongitude(float lastLongitude) {
        mLastLongitude = lastLongitude;
    }

    public float getLastLatitude() {
        return mLastLatitude;
    }

    public void setLastLatitude(float lastLatitude) {
        mLastLatitude = lastLatitude;
    }

    public String getMatchingUpdateYn() {
        return mMatchingUpdateYn;
    }

    public void setMatchingUpdateYn(String matchingUpdateYn) {
        mMatchingUpdateYn = matchingUpdateYn;
    }

    public String getTodayFirst() {
        return mTodayFirst;
    }

    public void setTodayFirst(String todayFirst) {
        mTodayFirst = todayFirst;
    }

    public String getImageOpenYn() {
        return mImageOpenYn;
    }

    public void setImageOpenYn(String imageOpenYn) {
        mImageOpenYn = imageOpenYn;
    }
}
