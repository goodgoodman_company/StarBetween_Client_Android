package com.matching.goodgoodman.constellationdating.utils;

import android.os.Handler;
import android.os.Message;

/**
 * Created by Hyeong_Wook on 2017-09-30.
 */

public abstract class AsyncHelper {
    public static void run(Runnable runnable) {
        Handler handler = new Handler();
        handler.post(runnable);
    }

    public static void runLoop(Runnable runnable, int timeMillis) {
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                runnable.run();
                sendEmptyMessageDelayed(0, timeMillis);
            }
        };
        handler.postDelayed(runnable, timeMillis);
    }
}
