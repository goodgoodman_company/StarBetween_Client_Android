package com.matching.goodgoodman.constellationdating.utils;

import android.content.Context;

import com.matching.goodgoodman.constellationdating.ConstellationData;
import com.matching.goodgoodman.constellationdating.localDB.AscendentDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.LongitudeAdjustmentDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.MediumCoeliDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.PlanetDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.SiderealTimeAdjustmentDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.SiderealTimeDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.SynastryAspectScoreDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.ZodiacDBHelper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dao.Ascendent;
import dao.Description;
import dao.LongitudeAdjustment;
import dao.MediumCoeli;
import dao.Planet;
import dao.SiderealTime;
import dao.SiderealTimeAdjustment;
import dao.SynastryAspectScore;
import dao.Zodiac;

/**
 * .csv 파일을 파싱하는 유틸클래스.
 * @author Hyeong_Wook
 */

public abstract class CSVParser {
    private static final String[] SIDEREAL_TIMES = { "siderealTime_0.csv", "siderealTime_1.csv", "siderealTime_2.csv", "siderealTime_3.csv" };
    private static final String SIDEREAL_TIME_ADJUSTMENT = "siderealTimeAdjustment.csv";
    private static final String LONGITUDE_ADJUSTMENT = "longitudeAdjustment.csv";
    private static final String MEDIUM_COELI = "mediumCoeli.csv";
    private static final String ASCENDENT = "ascendent.csv";
    private static final String EMPA = "empa.csv";
    private static final String DESCRIPTION = "description.csv";
    private static final String ZODIAC = "zodiac.csv";
    private static final String SYNASTRY_ASPECT_SCORE = "synastryAspectScore.csv";

    private static final String BOM = "\uFEFF";

    /**
     * csv파일을 파싱한다.
     * @param context 안드로이드 aplicationContext
     * @param csvFile 원하는 csv파일의 이름, assets 폴더에 있어야 한다.
     * @return 2차원 리스트로 바뀐 csv파일
     */
    public static List<List<String>> parse(Context context, String csvFile) {
        List<List<String>> resultList = new ArrayList<>();
        String text = null;

        try {
             text = read(context, csvFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(text != null) {
            String[] lines = text.split("\n");

            for(int i = 0; i < lines.length; i++) {
                String[] elementArray = lines[i].trim().split(",");
                resultList.add(new ArrayList<>(Arrays.asList(elementArray)));
            }
        }

        return resultList;
    }

    /**
     * csv파일을 읽어온다.
     * @param context 안드로이드 aplicationContext
     * @param csvFile 원하는 csv파일의 이름, assets 폴더에 있어야 한다.
     * @return 읽어온 파일의 text
     */
    public static String read(Context context, String csvFile) throws IOException {
        InputStream inputStream = context.getAssets().open(csvFile);
        int size = inputStream.available();
        byte[] buffer = new byte[size];

        inputStream.read(buffer);
        inputStream.close();

        return new String(buffer);
    }

    /**
     * description csv파일을 localDB로 넣는다.
     * @param context 안드로이드 aplicationContext
     */
    public static void descriptionCsvToLocalDB(Context context) {
        DescriptionDBHelper descriptionDBHelper = new DescriptionDBHelper(context, DescriptionDBHelper.LANGUAGE_ENGLISH);
        for(String lang : descriptionDBHelper.LANGUAGES){
            descriptionDBHelper.setLanguage(lang);
            descriptionDBHelper.deleteAll();
        }
        if(descriptionDBHelper.getDescription(0) != null)
            return;

        List<List<Description>> dbList = new ArrayList<>();
        for(int i = 0; i < DescriptionDBHelper.LANGUAGES.length; i++)
            dbList.add(new ArrayList<>());

        List<List<String>> descriptionList = parse(context, DESCRIPTION);
        for(List<String> description : descriptionList) {
            String idStr = description.get(0).replace(BOM, "");
            int id = Integer.parseInt(idStr);

            List<String> languageList = description.subList(1, description.size());
            for(int i = 0; i < languageList.size(); i++) {
                String comment = languageList.get(i).replace('^', ',');
                dbList.get(i).add(new Description(null, id, comment));
            }
        }

        for(int i = 0; i < dbList.size(); i++) {
            List<Description> descriptionDBList = dbList.get(i);
            descriptionDBHelper.setLanguage(DescriptionDBHelper.LANGUAGES[i]);
            descriptionDBHelper.insert(descriptionDBList);
        }
    }

    /**
     * siderealTime csv파일을 localDB로 넣는다.
     * @param context 안드로이드 aplicationContext
     */
    public static void siderealTimeCsvToLocalDB(Context context) {
        SiderealTimeDBHelper siderealTimeDBHelper = new SiderealTimeDBHelper(context, 0);
        int tableNumber = 0;

        if(siderealTimeDBHelper.getSiderealTime(1, 1) != null)
            return;

        for(String csvFile : SIDEREAL_TIMES) {
            siderealTimeDBHelper.setTable(tableNumber);
            List<List<String>> siderealTimeList = parse(context, csvFile);
            List<SiderealTime> dbList = new ArrayList<>();

            for (int i = 0; i < siderealTimeList.size(); i++) {
                List<String> siderealTimesWithDay = siderealTimeList.get(i);
                for (int j = 0; j < siderealTimesWithDay.size(); j += 3) {
                    int month = j / 3 + 1;
                    int day = i + 1;
                    int hour = Integer.parseInt(siderealTimesWithDay.get(j));
                    int minute = Integer.parseInt(siderealTimesWithDay.get(j + 1));
                    int second = Integer.parseInt(siderealTimesWithDay.get(j + 2));

                    if(hour != -1)
                        dbList.add(new SiderealTime(null, month, day, hour, minute, second));
                }
            }

            siderealTimeDBHelper.insert(dbList);
            tableNumber++;
        }
    }

    /**
     * siderealTimeAdjustment csv파일을 localDB로 넣는다.
     * @param context 안드로이드 aplicationContext
     */
    public static void siderealTimeAdjustmentCsvToLocalDB(Context context) {
        SiderealTimeAdjustmentDBHelper siderealTimeAdjustmentDBHelper = new SiderealTimeAdjustmentDBHelper(context);
        List<SiderealTimeAdjustment> dbList = new ArrayList<>();

        if(siderealTimeAdjustmentDBHelper.getSiderealTimeAdjustment(2015) != null)
            return;

        List<List<String>> siderealTimeAdjustmentList = parse(context, SIDEREAL_TIME_ADJUSTMENT);
        for(List<String> siderealTimeAdjustment : siderealTimeAdjustmentList) {
            int year = Integer.parseInt(siderealTimeAdjustment.get(0));
            int hour = Integer.parseInt(siderealTimeAdjustment.get(1));
            int minute = Integer.parseInt(siderealTimeAdjustment.get(2));
            int second = Integer.parseInt(siderealTimeAdjustment.get(3));

            dbList.add(new SiderealTimeAdjustment(null, year, hour, minute, second));
        }

        siderealTimeAdjustmentDBHelper.insert(dbList);
    }

    /**
     * longitudeAdjustment csv파일을 localDB로 넣는다.
     * @param context 안드로이드 aplicationContext
     */
    public static void longitudeAdjustmentCsvToLocalDB(Context context) {
        LongitudeAdjustmentDBHelper longitudeAdjustmentDBHelper = new LongitudeAdjustmentDBHelper(context);
        List<LongitudeAdjustment> dbList = new ArrayList<>();

        if(longitudeAdjustmentDBHelper.getLongitudeAdjustment(1) != null)
            return;

        List<List<String>> longitudeAdjustmentList = parse(context, LONGITUDE_ADJUSTMENT);
        for(List<String> longitudeAdjustment : longitudeAdjustmentList) {
            int longitude = Integer.parseInt(longitudeAdjustment.get(0));
            int hour = Integer.parseInt(longitudeAdjustment.get(1));
            int minute = Integer.parseInt(longitudeAdjustment.get(2));

            dbList.add(new LongitudeAdjustment(null, longitude, hour, minute));
        }

        longitudeAdjustmentDBHelper.insert(dbList);
    }

    public static void mediumCoeliCsvToLocalDB(Context context) {
        MediumCoeliDBHelper mediumCoeliDBHelper = new MediumCoeliDBHelper(context);
        List<MediumCoeli> dbList = new ArrayList<>();

        if(mediumCoeliDBHelper.getMediumCoeli(0,0,0) != null)
            return;

        List<List<String>> mediumCoeliList = parse(context, MEDIUM_COELI);
        for(List<String> mediumCoeli : mediumCoeliList) {
            int hour = Integer.parseInt(mediumCoeli.get(0));
            int minute = Integer.parseInt(mediumCoeli.get(1));
            int second = Integer.parseInt(mediumCoeli.get(2));
            int location = Integer.parseInt(mediumCoeli.get(3));
            String constellation = mediumCoeli.get(4);

            dbList.add(new MediumCoeli(null, hour, minute, second, constellation, location));
        }

        mediumCoeliDBHelper.insert(dbList);
    }

    public static void ascendentCsvToLocalDB(Context context) {
        AscendentDBHelper ascendentDBHelper = new AscendentDBHelper(context);
        List<Ascendent> dbList = new ArrayList<>();

        if(ascendentDBHelper.getAscendent(0, 0, 0, 2.0) != null)
            return;

        List<List<String>> ascendentList = parse(context, ASCENDENT);
        for(List<String> ascendent : ascendentList) {
            int hour = Integer.parseInt(ascendent.get(0));
            int minute = Integer.parseInt(ascendent.get(1));
            int second = Integer.parseInt(ascendent.get(2));
            double latitude = Double.parseDouble(ascendent.get(3)) / 100.0;
            double location = Integer.parseInt(ascendent.get(4)) + Double.parseDouble(ascendent.get(6)) / 100.0;
            String constellation = ascendent.get(5);

            dbList.add(new Ascendent(null, hour, minute, second, latitude, location, constellation));
        }

        ascendentDBHelper.insert(dbList);
    }

    public static void planetCsvToLocalDB(Context context) {
        PlanetDBHelper planetDBHelper = new PlanetDBHelper(context);
        List<Planet> dbList = new ArrayList<>();

        if(planetDBHelper.getPlanet(1960, 1, 1, Planet.SUN) != null)
            return;

        List<List<String>> planetList = parse(context, EMPA);
        for(List<String> planets : planetList) {
            int year = Integer.parseInt(planets.get(0));
            int month = Integer.parseInt(planets.get(1));
            int day = Integer.parseInt(planets.get(2));

            for(int i = 3; i < planets.size(); i++) {
                String[] constellations = planets.get(i).split(":");
                String name = Planet.NAMES[i - 3];
                String constellation = constellations[1];
                double location = Integer.parseInt(constellations[0]) + Double.parseDouble(constellations[2]) / 100.0;

                dbList.add(new Planet(null, year, month, day, location, constellation, name));
            }
        }

        planetDBHelper.insert(dbList);
    }

    public static void zodiacCsvToLocalDB(Context context, String language) {
        ZodiacDBHelper zodiacDBHelper = new ZodiacDBHelper(context, language);
        List<Zodiac> dbList = new ArrayList<>();

        if(zodiacDBHelper.getZodiac(ConstellationData.AQUARIUS) != null)
            return;

        List<List<String>> zodiacList = parse(context, ZODIAC);
        for(List<String> zodiac : zodiacList) {
            String constellation = zodiac.get(0).replace(BOM, "");
            String constellationName = zodiac.get(1);
            int month = Integer.parseInt(zodiac.get(2));
            String planetName = zodiac.get(3);
            String element = zodiac.get(4);
            String color = zodiac.get(5);
            int degree = Integer.parseInt(zodiac.get(6));
            String keyword = zodiac.get(7);

            dbList.add(new Zodiac(null, constellation, constellationName, month, planetName, element, color, degree, keyword));
        }

        zodiacDBHelper.insert(dbList);
    }

    public static void synastryAspectScoreCsvToLocalDB(Context context) {
        SynastryAspectScoreDBHelper synastryAspectScoreDBHelper = new SynastryAspectScoreDBHelper(context);
        List<SynastryAspectScore> dbList = new ArrayList<>();

        if(synastryAspectScoreDBHelper.getSynastryAspectScore(201413) != null)
            return;

        List<List<String>> synastryAspectScores = parse(context, SYNASTRY_ASPECT_SCORE);
        for(List<String> synastryAspectScore : synastryAspectScores) {
            int id = Integer.parseInt(synastryAspectScore.get(0));
            int score = Integer.parseInt(synastryAspectScore.get(1));

            dbList.add(new SynastryAspectScore(null, id, score));
        }

        synastryAspectScoreDBHelper.insert(dbList);
    }
}
