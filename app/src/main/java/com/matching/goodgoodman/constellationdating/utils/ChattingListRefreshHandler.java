package com.matching.goodgoodman.constellationdating.utils;

/**
 * Created by Hyeong_Wook on 2017-11-04.
 */

public class ChattingListRefreshHandler extends DataChangeEventHandler<Boolean> {
    private static volatile ChattingListRefreshHandler mInstance;
    public static ChattingListRefreshHandler getInstance() {
        if(mInstance == null) {
            synchronized (ChattingListRefreshHandler.class) {
                if(mInstance == null)
                    mInstance = new ChattingListRefreshHandler();
            }
        }
        return mInstance;
    }
}
