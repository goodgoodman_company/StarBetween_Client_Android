package com.matching.goodgoodman.constellationdating.utils;

/**
 * Created by Hyeong_Wook on 2017-11-05.
 */

public abstract class ColorIndexHelper {
    private static final int[] LOVE_CONDITIONS = {90, 80, 70, 60, 50, 40, 30, 20, 10, 0};

    public static final int getLoveIndex(int score) {
        int index = 0;
        for(int condition : LOVE_CONDITIONS) {
            if(score >= condition)
                return index;
            index++;
        }
        return index;
    }
}
