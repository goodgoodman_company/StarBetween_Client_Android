package com.matching.goodgoodman.constellationdating.utils;

import android.content.Context;
import android.util.Log;

import com.matching.goodgoodman.constellationdating.ConstellationData;
import com.matching.goodgoodman.constellationdating.Location;
import com.matching.goodgoodman.constellationdating.localDB.AscendentDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.LongitudeAdjustmentDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.MediumCoeliDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.PlanetDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.SiderealTimeAdjustmentDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.SiderealTimeDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.SynastryAspectScoreDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.ZodiacDBHelper;
import com.matching.goodgoodman.constellationdating.model.Aspect;
import com.matching.goodgoodman.constellationdating.model.HoroscopeModel;
import com.matching.goodgoodman.constellationdating.model.House;
import com.matching.goodgoodman.constellationdating.model.SynastryAspect;
import com.matching.goodgoodman.constellationdating.server.GoogleMapNetworkManager;
import com.matching.goodgoodman.constellationdating.server.SuccessCallBack;
import com.matching.goodgoodman.constellationdating.server.model.GoogleTimeZoneInfo;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import dao.Ascendent;
import dao.LongitudeAdjustment;
import dao.MediumCoeli;
import dao.Planet;
import dao.SiderealTime;
import dao.SiderealTimeAdjustment;
import dao.SynastryAspectScore;

import static java.lang.Math.round;

/**
 * 별자리의 각도를 계산하는 유틸리티 클래스.
 * 날짜와 위치(추후 구현)을 넣으면 각 별자리들의 각도를 반환한다.
 *
 * @author Hyeong_Wook
 */

public abstract class ConstellationCalculator {
    private static final int[] ADJUSTMENT_TIMES = {3, 9, 15, 21};
    private static final double[] ADJUSTMENT_LONGITUDES = {0.08, 0.23, 0.38, 0.53};
    private static final int[] SYNASTRY_SCORES = {90, 80, 70, 60, 50, 40, 30, 20, 10};

    private static final String DATE_STR = "yyyy/MM/dd/hh/mm";
    private static final String DATE_FORMAT_STR = "%d/%d/%d/%d/%d";
    private static final String LOCATION_STR = "%f,%f";
    private static final String RESULT_OK = "OK";
    private static final String ZERO_RESULTS = "ZERO_RESULTS";

    private static SiderealTimeDBHelper mSiderealTimeDBHelper;
    private static SiderealTimeAdjustmentDBHelper mSiderealTimeAdjustmentDBHelper;
    private static LongitudeAdjustmentDBHelper mLongitudeAdjustmentDBHelper;
    private static GoogleMapNetworkManager mGoogleMapNetworkManager;
    private static MediumCoeliDBHelper mMediumCoeliDBHelper;
    private static AscendentDBHelper mAscendentDBHelper;
    private static PlanetDBHelper mPlanetDBHelper;
    private static ZodiacDBHelper mZodiacDBHelper;
    private static SynastryAspectScoreDBHelper mSynastryAspectScoreDBHelper;

    private static int sChangeDay = 0;

    /**
     * 별자리들의 각도를 계산하는 메서드.
     * TODO::구현이 완벽하지 않은 메서드입니다.
     *
     * @param context   android applicationContext
     * @param year      년
     * @param month     월
     * @param day       일
     * @param hour      시
     * @param minute    분
     * @param latitude  위도
     * @param longitude 경도
     */
    public static void getHoroscopeModel(Context context, int year, int month, int day, int hour, int minute, double latitude, double longitude
            , SuccessCallBack<HoroscopeModel> successCallBack) {
        sChangeDay = 0;
        if (mSiderealTimeDBHelper == null) {
            mSiderealTimeDBHelper = new SiderealTimeDBHelper(context, 0);
            mSiderealTimeAdjustmentDBHelper = new SiderealTimeAdjustmentDBHelper(context);
            mLongitudeAdjustmentDBHelper = new LongitudeAdjustmentDBHelper(context);
            mGoogleMapNetworkManager = GoogleMapNetworkManager.getInstance(context);
            mMediumCoeliDBHelper = new MediumCoeliDBHelper(context);
            mAscendentDBHelper = new AscendentDBHelper(context);
            mPlanetDBHelper = new PlanetDBHelper(context);
            mZodiacDBHelper = new ZodiacDBHelper(context, ZodiacDBHelper.LANGUAGE_ENGLISH);
            mSynastryAspectScoreDBHelper = new SynastryAspectScoreDBHelper(context);
        }

        getGoogleMapInfo(year, month, day, hour, minute, latitude, longitude, new SuccessCallBack<GoogleTimeZoneInfo>() {
            @Override
            public void onCallBack(GoogleTimeZoneInfo result) {
                if (!result.getStatus().equals(RESULT_OK) && !result.getStatus().equals(ZERO_RESULTS)) {
                    Log.e(result.getStatus(), result.getErrorMessage() == null ? "" : result.getErrorMessage());
                    return;
                }
                SiderealTime siderealTime = getSiderealTime(year, month, day, hour, minute, latitude, longitude, result);
                MediumCoeli mediumCoeli = getMediumCoeli(siderealTime, latitude, longitude);
                Ascendent ascendent = getAscendent(siderealTime, latitude, longitude);
                Planet[] planets = getPlanets(getTimeWithLocation(hour, minute, result), year, month, day + sChangeDay);
                List<Aspect> aspects = getAspects(ascendent, mediumCoeli, planets);
                List<House> houseList = getHouseList(ascendent, planets);

                HoroscopeModel horoscopeModel = new HoroscopeModel(ascendent, mediumCoeli, planets, aspects, houseList);

                successCallBack.onCallBack(horoscopeModel);
                Log.i("ST", String.valueOf(siderealTime.getHour() * 10000 + siderealTime.getMinute() * 100 + siderealTime.getSecond()));
                Log.i("MC", mediumCoeli.getLocation() + " " + mediumCoeli.getConstellation());
                Log.i("ASC", ascendent.getLocation().intValue() + " " + ascendent.getConstellation());
                for (Planet planet : planets)
                    Log.i(planet.getName(), planet.getLocation() + " " + planet.getConstellation());
                for (Aspect aspect : aspects)
                    Log.i(aspect.getName(), aspect.getPlanets()[0].getName() + " " + aspect.getPlanets()[1].getName());
                for(House house : houseList)
                    Log.i(house.getPlanet(), String.valueOf(house.getNumber()));
            }
        });
    }

    /**
     * 유저가 태어난 위치에 따른 시간 데이터를 얻습니다.
     *
     * @param year            년
     * @param month           월
     * @param day             일
     * @param hour            시
     * @param minute          분
     * @param latitude        위도
     * @param longitude       경도
     * @param successCallBack 데이터를 얻기 위한 callback
     */
    private static void getGoogleMapInfo(int year, int month, int day, int hour, int minute, double latitude, double longitude, SuccessCallBack<GoogleTimeZoneInfo> successCallBack) {
        Date formatDate = null;
        Timestamp timestamp = null;

        try {
            formatDate = (new SimpleDateFormat(DATE_STR)).parse(String.format(DATE_FORMAT_STR, year, month, day, hour, minute));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (formatDate != null)
            timestamp = new Timestamp(formatDate.getTime());

        mGoogleMapNetworkManager.getGoogleTimeZoneInfo(String.format(LOCATION_STR, latitude, longitude), timestamp.getTime() / 1000, successCallBack);
    }

    /**
     * siderealTime을 얻습니다.
     *
     * @param year               유저가 태어난 년도
     * @param month              유저가 태어난 월
     * @param day                유저가 태어난 일
     * @param hour               유저가 태어난 시간
     * @param minute             유저가 태어난 분
     * @param longitude          유저가 태어난 경도
     * @param googleTimeZoneInfo 유저가 태어난 위치에 따른 시간정보
     * @return siderealTime
     */
    public static SiderealTime getSiderealTime(int year, int month, int day, int hour, int minute, double latitude, double longitude, GoogleTimeZoneInfo googleTimeZoneInfo) {
        mSiderealTimeDBHelper.setTable((year - 1920) % 4);
        SiderealTime siderealTime = mSiderealTimeDBHelper.getSiderealTime(month, day);

        siderealTime = getAdjustedSiderealTime(siderealTime, year);
        siderealTime = getAdjustedTimeWithLocation(siderealTime, googleTimeZoneInfo, hour, minute);
        siderealTime = getAdjustedTimeWithHour(siderealTime);
        siderealTime = getAdjustedTimeWithLongitude(siderealTime, longitude);
        if(latitude < 0 && longitude > 0)
            siderealTime.setHour(siderealTime.getHour() + 12);

        return siderealTime;
    }

    /**
     * siderealTime보정 테이블에 의해 보정된 값을 얻습니다.
     *
     * @param siderealTime 보정할 siderealTime
     * @param year         유저가 태어난 년도
     * @return 보정된 시간
     */
    private static SiderealTime getAdjustedSiderealTime(SiderealTime siderealTime, int year) {
        SiderealTimeAdjustment siderealTimeAdjustment = mSiderealTimeAdjustmentDBHelper.getSiderealTimeAdjustment(year);
        if (siderealTimeAdjustment != null) {
            siderealTime.setHour(siderealTime.getHour() + siderealTimeAdjustment.getHour());
            siderealTime.setMinute(siderealTime.getMinute() + siderealTimeAdjustment.getMinute());
            siderealTime.setSecond(siderealTime.getSecond() + siderealTimeAdjustment.getSecond());
        }

        return siderealTime;
    }

    /**
     * 위치에 의해 보정된 siderealTime을 얻습니다.
     *
     * @param time               현재 siderealTime
     * @param googleTimeZoneInfo 유저가 태어난 위치에 맞는 정보
     * @param hour               user가 태어난 시
     * @param minute             user가 태어난 분
     * @return 보정된 시간
     */
    private static SiderealTime getAdjustedTimeWithLocation(SiderealTime time, GoogleTimeZoneInfo googleTimeZoneInfo, int hour, int minute) {
        SiderealTime adjustRealTime = getTimeWithLocation(hour, minute, googleTimeZoneInfo);

        time.setHour(time.getHour() + adjustRealTime.getHour());
        time.setMinute(time.getMinute() + adjustRealTime.getMinute());

        if (time.getSecond() >= 30)
            time.setMinute(time.getMinute() + 1);
        time.setSecond(0);

        return time;
    }

    /**
     * 서머타임, 시차에 의해 보정된 실제 시간을 얻습니다.
     *
     * @param hour               시
     * @param minute             분
     * @param googleTimeZoneInfo timezone
     * @return 보정된 실제 시간
     */
    private static SiderealTime getTimeWithLocation(int hour, int minute, GoogleTimeZoneInfo googleTimeZoneInfo) {
        hour -= (googleTimeZoneInfo.getRawOffset() + googleTimeZoneInfo.getDstOffset()) / 3600;
        if(hour >= 24)
            sChangeDay = 1;
        else if (hour < 0)
            sChangeDay = -1;

        SiderealTime result = new SiderealTime();
        result.setHour(hour);
        result.setMinute(minute);

        return result;
    }

    /**
     * hour에 의해 보정된 siderealTime을 얻습니다.
     *
     * @param siderealTime 보정할 시간
     * @return 보정된 시간
     */
    private static SiderealTime getAdjustedTimeWithHour(SiderealTime siderealTime) {
        int adjustedIndex = 0;
        int hour = siderealTime.getHour();
        if (hour >= 24)
            hour -= 24;

        while (adjustedIndex < ADJUSTMENT_TIMES.length && hour >= ADJUSTMENT_TIMES[adjustedIndex])
            adjustedIndex++;

        siderealTime.setMinute(siderealTime.getMinute() + adjustedIndex);
        return siderealTime;
    }

    /**
     * 경도에 의해 보정된 siderealTIme을 얻습니다.
     *
     * @param siderealTime 보정할 시간
     * @param longitude    경도
     * @return 보정된 시간
     */
    private static SiderealTime getAdjustedTimeWithLongitude(SiderealTime siderealTime, double longitude) {
        int westernAdjustment = longitude < 0 ? -1 : 1;
        int longitudeInt = (int) Math.abs(longitude);
        double leftLongitude = longitude % 1.00;
        int adjustedIndex = 0;

        LongitudeAdjustment longitudeAdjustment = mLongitudeAdjustmentDBHelper.getLongitudeAdjustment(longitudeInt);
        while (adjustedIndex < ADJUSTMENT_LONGITUDES.length && leftLongitude >= ADJUSTMENT_LONGITUDES[adjustedIndex])
            adjustedIndex++;

        if(longitudeAdjustment == null) {
            siderealTime.setHour(siderealTime.getHour());
            siderealTime.setMinute(siderealTime.getMinute() + adjustedIndex * westernAdjustment);
        } else {
            siderealTime.setHour(siderealTime.getHour() + (longitudeAdjustment.getHour() * westernAdjustment));
            siderealTime.setMinute(siderealTime.getMinute() + (longitudeAdjustment.getMinute() + adjustedIndex) * westernAdjustment);
        }

        return siderealTime;
    }

    /**
     * siderealTime을 통해 보조 기준점인 MC를 구한다.
     *
     * @param siderealTime 유저에 맞는 siderealTime
     * @return MediumCoeli
     */
    private static MediumCoeli getMediumCoeli(SiderealTime siderealTime, double latitude, double longitude) {
        MediumCoeli mediumCoeli = mMediumCoeliDBHelper.getMediumCoeli(siderealTime.getHour(), siderealTime.getMinute(), siderealTime.getSecond());
        if (mediumCoeli != null)
            return mediumCoeli;

        MediumCoeli[] approximateMediumCoelis
                = mMediumCoeliDBHelper.getApproximateMediumCoeli(siderealTime.getHour(), siderealTime.getMinute(), siderealTime.getSecond());
        mediumCoeli = new MediumCoeli(null, siderealTime.getHour(), siderealTime.getMinute(), siderealTime.getSecond()
                , approximateMediumCoelis[0].getConstellation(), 0);

        int mediumCoeliSecond = siderealTime.getSecondsAll();
        int lowMediumCoeliSecond = approximateMediumCoelis[0].getSecondAll();
        int highMediumCoeliSecond = approximateMediumCoelis[1].getSecondAll();
        int difference = highMediumCoeliSecond - lowMediumCoeliSecond;
        double term = difference / 6.0;

        int approximateIndex = getApproximateIndex(lowMediumCoeliSecond, mediumCoeliSecond, term);
        double adjustLocation = new Location(approximateMediumCoelis[0].getLocation() + 0.1 * approximateIndex).getDouble();

        mediumCoeli.setLocation((int) round(adjustLocation));

        if(latitude < 0 && longitude > 0)
            mediumCoeli.setConstellation(getOppositeConstellation(mediumCoeli.getConstellation()));

        return mediumCoeli;
    }

    /**
     * lowValue에서 goalValue의 가장 가까운 근사값으로 가기 위해 필요한 term의 개수를 얻습니다.
     *
     * @param lowValue  시작값
     * @param goalValue 목표값
     * @param term      한번 시행될 때 늘어나는 값의 양
     * @return lowValue에서 goalValue의 가장 가까운 근사값으로 가기 위해 필요한 term의 개수
     */
    private static int getApproximateIndex(double lowValue, double goalValue, double term) {
        int index = 0;
        Location lowLocation = new Location(lowValue);
        Location goalLocation = new Location(goalValue);
        Location differenceLocation = Location.sub(lowLocation, goalLocation);
        Location termLocation = new Location(term);

        while (differenceLocation.getDouble() < 0) {
            lowLocation = Location.sum(lowLocation, termLocation);
            differenceLocation = Location.sub(lowLocation, goalLocation);
            index++;
        }

        Location tempLocation = Location.sub(lowLocation, termLocation);
        if (Location.sub(goalLocation, tempLocation).getDouble() < differenceLocation.getDouble())
            index--;

        return index;
    }

    /**
     * ASC를 찾는다.
     *
     * @param siderealTime ASC에 맞는 siderealTime
     * @param latitude     ASC에 맞는 latitude
     * @return ASC
     */
    private static Ascendent getAscendent(SiderealTime siderealTime, double latitude, double longitude) {
        latitude = Math.abs(latitude);
        Ascendent ascendent = mAscendentDBHelper.getAscendent(siderealTime.getHour(), siderealTime.getMinute(), siderealTime.getSecond(), latitude);
        if (ascendent != null)
            return ascendent;
        ascendent = getAdjustAscendent(siderealTime, latitude);
        if(latitude < 0 && longitude > 0)
            ascendent.setConstellation(getOppositeConstellation(ascendent.getConstellation()));

        return getAdjustAscendent(siderealTime, latitude);
    }

    /**
     * 근사값 4개로부터 보정된 ASC값을 얻는다.
     *
     * @param siderealTime ASC에 맞는 siderealTime
     * @param latitude     ASC에 맞는 위도
     * @return 보정된 ASC값
     */
    private static Ascendent getAdjustAscendent(SiderealTime siderealTime, double latitude) {
        Ascendent[] ascendents
                = mAscendentDBHelper.getApproximateAscendent(siderealTime.getHour(), siderealTime.getMinute(), siderealTime.getSecond(), latitude);

        Location goalLatitude = new Location(latitude);
        Location lowLatitude = new Location(ascendents[0].getLatitude());
        Location highLatitude = new Location(ascendents[1].getLatitude());

        Location lowLocation = new Location(ascendents[0].getLocation());
        Location highLocation = new Location(ascendents[1].getLocation());

        SiderealTime highSiderealTime = new SiderealTime(null, null, null, ascendents[2].getHour(), ascendents[2].getMinute(), ascendents[2].getSecond());
        SiderealTime lowSiderealTime = new SiderealTime(null, null, null, ascendents[0].getHour(), ascendents[0].getMinute(), ascendents[0].getSecond());

        double adjustLocationWithLatitude1 = getAdjustValue(Location.subtraction(goalLatitude, lowLatitude).getMinutesAll()
                , Location.subtraction(highLatitude, lowLatitude).getMinutesAll()
                , Location.subtraction(highLocation, lowLocation).getMinutesAll(), 1);
        Location adjustLowLocation = Location.sum(new Location(adjustLocationWithLatitude1 / 100.0), lowLocation);

        lowLocation = new Location(ascendents[2].getLocation());
        highLocation = new Location(ascendents[3].getLocation());

        double adjustLocationWithLatitude2 = getAdjustValue(goalLatitude.getMinutesAll() - lowLatitude.getMinutesAll()
                , highLatitude.getMinutesAll() - lowLatitude.getMinutesAll()
                , Location.subtraction(highLocation, lowLocation).getMinutesAll(), 1);
        Location adjustHighLocation = Location.sum(new Location(adjustLocationWithLatitude2 / 100.0), lowLocation);

        double adjustLocationdouble = getAdjustValue(siderealTime.getSecondsAll() - lowSiderealTime.getSecondsAll()
                , highSiderealTime.getSecondsAll() - lowSiderealTime.getSecondsAll()
                , Location.subtraction(adjustHighLocation, adjustLowLocation).getMinutesAll(), 1);

        Location adjustLocation = Location.sum(adjustLowLocation, new Location(adjustLocationdouble / 100.0));

        Ascendent result = new Ascendent(null
                , siderealTime.getHour()
                , siderealTime.getMinute()
                , siderealTime.getSecond()
                , latitude
                , (double) Math.round(adjustLocation.getDouble()), null);
        result.setConstellation(getApproximateConstellation(ascendents, result));

        return result;
    }

    /**
     * 반대방향에 있는 별자리를 구합니다.
     * @param constellation 현재 별자리
     * @return 반대방향의 별자리
     */
    private static String getOppositeConstellation(String constellation) {
        int index = ConstellationData.getIndex(constellation);
        if(index == -1)
            return null;

        index += 6;
        if(index >= ConstellationData.CONSTELLATIONS.length)
            index -= ConstellationData.CONSTELLATIONS.length;

        return ConstellationData.CONSTELLATIONS[index];
    }

    /**
     * 비율에 의한 보정값을 얻습니다.
     *
     * @param ratio        원래 있던 값의 분자
     * @param currentValue 원래 있던 값의 분모
     * @param wholeValue   구하려고 하는 값의 분모
     * @param digit        얻는 값을 반올림하는 소수점 아래의 자리수
     * @return 보정된 값
     */
    private static double getAdjustValue(double ratio, double currentValue, double wholeValue, int digit) {
        double digits = Math.pow(10, digit - 1);
        return Math.round(ratio / currentValue * wholeValue * digits) / digits;
    }

    /**
     * location이 가장 가까운 ASC의 별자리를 얻습니다.
     *
     * @param approxiAscendents 근사 asc 배열
     * @param goalAscendent     원하는 asc
     * @return 별자리
     */
    private static String getApproximateConstellation(Ascendent[] approxiAscendents, Ascendent goalAscendent) {
        Ascendent resultAscendent = null;
        int minimum = -1;
        for (Ascendent ascendent : approxiAscendents) {
            int difference = Location.subtraction(new Location(goalAscendent.getLocation()), new Location(ascendent.getLocation())).getMinutesAll();
            if (minimum > difference || minimum == -1) {
                minimum = difference;
                resultAscendent = ascendent;
            }
        }

        return resultAscendent.getConstellation();
    }

    /**
     * 사용자의 year, month, day와 보정된 hour, minute를 통해 행성들의 각도를 구합니다.
     *
     * @param adjustTime 보정된 시간
     * @param year       년
     * @param month      월
     * @param day        일
     * @return 행성들의 각도 배열
     */
    private static Planet[] getPlanets(SiderealTime adjustTime, int year, int month, int day) {
        Planet[] result = new Planet[Planet.NAMES.length];
        Date today = DateConverter.strToDate(DATE_STR, String.format(DATE_FORMAT_STR, year, month, day, 0, 0));
        Date tomorrow = DateConverter.getTomorrow(today);
        int[] tomorrowTimes = DateConverter.dateToDividedTime(tomorrow);

        double totalDay = (adjustTime.getMinute() / 60.0 + adjustTime.getHour()) / 24.0;

        List<Planet> todayPlanetList = mPlanetDBHelper.getPlanetList(year, month, day);
        List<Planet> tomorrowPlanetList = mPlanetDBHelper.getPlanetList(tomorrowTimes[0], tomorrowTimes[1], tomorrowTimes[2]);

        for (int i = 0; i < result.length; i++) {
            Planet todayPlanet = todayPlanetList.get(i);
            Planet tomorrowPlanet = tomorrowPlanetList.get(i);
            Location todayPlanetLocation = new Location(todayPlanet.getLocation());
            Location tomorrowPlanetLocation = new Location(tomorrowPlanet.getLocation());
            String resultConstellation = todayPlanet.getConstellation();

            int difference = tomorrowPlanetLocation.getMinutesAll() - todayPlanetLocation.getMinutesAll();
            if (difference < 0 && !todayPlanet.getConstellation().equals(tomorrowPlanet.getConstellation()))
                tomorrowPlanetLocation.setDegree(tomorrowPlanetLocation.getDegree() + 30);

            Location differenceLocation = Location.subtraction(tomorrowPlanetLocation, todayPlanetLocation);
            Location adjustLocation = new Location(differenceLocation.getDouble() * totalDay);
            double resultLocation = Location.sum(adjustLocation, todayPlanetLocation).getDouble();

            if (resultLocation >= 30) {
                resultLocation -= 30;
                resultConstellation = tomorrowPlanet.getConstellation();
            }

            result[i] = new Planet(null, 0, 0, 0, resultLocation, resultConstellation, Planet.NAMES[i]);
        }

        return result;
    }

    /**
     * 의미 있는 AspectList를 얻습니다.
     *
     * @param ascendent   ASC
     * @param mediumCoeli MC
     * @param planets     행성들
     * @return AspectList
     */
    private static List<Aspect> getAspects(Ascendent ascendent, MediumCoeli mediumCoeli, Planet[] planets) {
        List<Aspect> aspectList = new ArrayList<>();
        String rulingPlanetName = mZodiacDBHelper.getZodiac(planets[0].getConstellation()).getPlanetName();

        for (Planet planet : planets) {
            Planet ascendentPlanet = getPlanet(ascendent);
            Planet mediumCoeliPlanet = getPlanet(mediumCoeli);

            Aspect ascendentAspect = getAspect(new Planet[]{ascendentPlanet, planet}, new String[]{rulingPlanetName});
            Aspect mediumCoeliAspect = getAspect(new Planet[]{mediumCoeliPlanet, planet}, new String[]{rulingPlanetName});

            if (ascendentAspect != null)
                aspectList.add(ascendentAspect);
            if (mediumCoeliAspect != null)
                aspectList.add(mediumCoeliAspect);
        }

        for (Planet startPlanet : planets) {
            for (Planet endPlanet : planets) {
                if (startPlanet.getName().equals(endPlanet.getName()))
                    continue;

                Aspect aspect = getAspect(new Planet[]{startPlanet, endPlanet}, new String[]{rulingPlanetName});
                if (aspect != null && !isAlreadyExistAspect(aspect, aspectList))
                    aspectList.add(aspect);
            }
        }

        return aspectList;
    }

    /**
     * asc를 planet의 형태로 만듭니다.
     * @param ascendent asc
     * @return planet 형태의 asc
     */
    private static Planet getPlanet(Ascendent ascendent) {
        return new Planet(null, null, null, null, ascendent.getLocation(), ascendent.getConstellation(), Planet.ASC);
    }

    /**
     * mc를 planet의 형태로 만듭니다.
     * @param mediumCoeli mc
     * @return planet 형태의 mc
     */
    private static Planet getPlanet(MediumCoeli mediumCoeli) {
        return new Planet(null, null, null, null, (double) mediumCoeli.getLocation(), mediumCoeli.getConstellation(), Planet.MC);
    }

    /**
     * aspect가 list에 있는지 확인합니다.
     *
     * @param aspect     aspect
     * @param aspectList aspectList
     * @return aspect와 동일한 값이 list에 있는지 여부
     */
    private static boolean isAlreadyExistAspect(Aspect aspect, List<Aspect> aspectList) {
        for (Aspect existingAspect : aspectList) {
            String existName1 = existingAspect.getPlanets()[0].getName();
            String existName2 = existingAspect.getPlanets()[1].getName();
            String name1 = aspect.getPlanets()[0].getName();
            String name2 = aspect.getPlanets()[1].getName();

            if ((existName1.equals(name1) && existName2.equals(name2)) || (existName1.equals(name2) && existName2.equals(name1)))
                return true;
        }
        return false;
    }

    public static int getAngleDifference(Planet[] planets) {
        int startConstellationIndex = ConstellationData.getIndex(planets[0].getConstellation());
        int endConstellationIndex = ConstellationData.getIndex(planets[1].getConstellation());
        int startConstellationAngle = ConstellationData.DEGREES[startConstellationIndex] + planets[0].getLocation().intValue();
        int endConstellationAngle = ConstellationData.DEGREES[endConstellationIndex]  + planets[1].getLocation().intValue();
        int angleDifference = startConstellationAngle - endConstellationAngle;
        int angleDifferenceAbs = Math.abs(angleDifference);
        int angleDifferenceCandidateAbs = 360 - angleDifferenceAbs;

        int angleDifferenceCandidate = angleDifference < 0? angleDifferenceCandidateAbs : -angleDifferenceCandidateAbs;

        if (angleDifferenceCandidateAbs < angleDifferenceAbs)
            angleDifference = angleDifferenceCandidate;

        return angleDifference;
    }

    /**
     * 의미 있는 aspect를 얻습니다. 의미있는 것이 없다면 null을 반환합니다.
     *
     * @param planets           aspect를 구할 두 행성
     * @param rulingPlanetNames rulingPlanet들
     * @return 의미 있는 aspect
     */
    private static Aspect getAspect(Planet[] planets, String[] rulingPlanetNames) {
        String startPlanetName = planets[0].getName();
        String endPlanetName = planets[1].getName();

        int angleDifference = Math.abs(getAngleDifference(planets));

        for (int i = 0; i < Aspect.DISTANCES.length; i++) {
            int distance = Aspect.DISTANCES[i];
            int orb = Aspect.ORBS[i];

            if (Arrays.asList(Aspect.SUN_MOON_NAMES).contains(startPlanetName) || Arrays.asList(Aspect.SUN_MOON_NAMES).contains(endPlanetName))
                orb = Aspect.SUN_MOON_EXCEPTION[i];
            else if (Arrays.asList(rulingPlanetNames).contains(startPlanetName) || Arrays.asList(rulingPlanetNames).contains(endPlanetName))
                orb = Aspect.RULING_PLANET_EXCEPTION[i];

            int leftDistance = distance - orb;
            int rightDistance = distance + orb;

            if (angleDifference >= leftDistance && angleDifference <= rightDistance)
                return new Aspect(planets, Aspect.NAMES[i]);
        }

        return null;
    }

    private static List<House> getHouseList(Ascendent ascendent, Planet[] planets) {
        List<House> result = new ArrayList<>();
        List<House> benchMarkList = new ArrayList<>();
        int benchMark = ascendent.getLocation().intValue() + mZodiacDBHelper.getZodiac(ascendent.getConstellation()).getDegree();
        int termAngle = 30;

        for(int i = 0; i < ConstellationData.CONSTELLATIONS.length; i++)
            benchMarkList.add(new House("", benchMark + termAngle * i, i + 1));

        Collections.sort(benchMarkList, new Comparator<House>() {
            @Override
            public int compare(House house, House t1) {
                return (house.getDistance() < t1.getDistance()) ? -1: (house.getDistance() > t1.getDistance()) ? 1:0 ;
            }
        });

        for(Planet planet : planets) {
            double distance = planet.getLocation() + mZodiacDBHelper.getZodiac(planet.getConstellation()).getDegree();
            for(int i = 0; i < benchMarkList.size(); i++) {

                if(distance < benchMarkList.get(i).getDistance()) {
                    if(i == 0)
                        i = benchMarkList.size();

                    result.add(new House(planet.getName(), distance, benchMarkList.get(i - 1).getNumber()));
                    break;
                }
                else if (i == benchMarkList.size() - 1)
                    result.add(new House(planet.getName(), distance, benchMarkList.get(i).getNumber()));
            }
        }

        return result;
    }

    /**
     * 궁합점수를 얻습니다.
     * @param synastryAspectList 궁합aspects
     * @return 궁합점수, 0 ~ 100
     */
    public static int getSynastryScore(List<SynastryAspect> synastryAspectList) {
        float result = 0f;
        for(SynastryAspect synastryAspect : synastryAspectList)
            result += synastryAspect.getScore();

        result = (400f + result) / 800f;
        if(result > 1)
            result = 1;
        else if (result < 0)
            result = 0;

        Log.i("궁합률", String.valueOf((int)(result * 100)));
        return (int)(result * 100);
    }

    /**
     * 궁합률에 따른 description id를 얻습니다.
     * @param score 궁합점수
     * @return description id
     */
    public static int getSynastryDescriptionid(int score) {
        int descriptionId = 400;
        for(int synastryScore : SYNASTRY_SCORES) {
            if(score >= synastryScore)
                return descriptionId;
            else
                descriptionId++;
        }

        return descriptionId;
    }

    /**
     * 최고점을 지닌 number개의 aspects를 얻습니다.
     * @param synastryAspects 궁합aspects
     * @param number max값 개수
     * @return number개의 aspects
     */
    public static List<SynastryAspect> getMaxScoreSynastryAspects(ArrayList<SynastryAspect> synastryAspects, int number) {
        Collections.sort(synastryAspects, (synastryAspect, t1) -> new Integer(t1.getScore()).compareTo(new Integer(synastryAspect.getScore())));
        return getNumberOfSynastry(synastryAspects, number);
    }

    /**
     * 최저점을 지닌 number개의 aspects를 얻습니다.
     * @param synastryAspects 궁합aspects
     * @param number min값 개수
     * @return number개의 aspects
     */
    public static List<SynastryAspect> getMinScoreSynastryAspects(ArrayList<SynastryAspect> synastryAspects, int number) {
        Collections.sort(synastryAspects, (synastryAspect, t1) -> new Integer(synastryAspect.getScore()).compareTo(new Integer(t1.getScore())));
        return getNumberOfSynastry(synastryAspects, number);
    }

    /**
     * 맨 처음부터 number개의 aspect를 얻습니다.
     * @param synastryAspects aspects
     * @param number 개수
     * @return number개의 aspects
     */
    private static List<SynastryAspect> getNumberOfSynastry(List<SynastryAspect> synastryAspects, int number) {
        List<SynastryAspect> result = new ArrayList<>();

        for(int i = 0; i < synastryAspects.size(); i++) {
            if(i >= number)
                break;

            result.add(synastryAspects.get(i));
        }

        return result;
    }

    public static List<SynastryAspect> getSynastryAspectList(HoroscopeModel userHoroscopeModel, HoroscopeModel counterHoroscopeModel) {
        List<SynastryAspect> result = new ArrayList<>();
        List<Aspect> aspectList = getAspectList(userHoroscopeModel, counterHoroscopeModel);

        List<Integer[]> aspectNumberList = getSynastryNumberList(aspectList);

        for(int i = 0; i < aspectList.size(); i++) {
            Aspect aspect = aspectList.get(i);
            int aspectNumber = aspectNumberList.get(i)[0];
            int aspectScore = 0;

            SynastryAspectScore synastryAspectScore = null;
            for(int number : aspectNumberList.get(i)) {
                synastryAspectScore = mSynastryAspectScoreDBHelper.getSynastryAspectScore(number);
                if(synastryAspectScore != null) {
                    aspectNumber = number;
                    break;
                }
            }

            if(synastryAspectScore != null)
                aspectScore = synastryAspectScore.getScore();

            result.add(new SynastryAspect(aspect, aspectNumber, aspectScore));
        }

        return result;
    }

    private static List<Aspect> getAspectList(HoroscopeModel userHoroscopeModel, HoroscopeModel counterHoroscopeModel) {
        List<Aspect> aspectList = new ArrayList<>();
        Planet[] userPlanets = getPlanetsWithBenchmark(userHoroscopeModel.getPlanets(), userHoroscopeModel.getAscendent(), userHoroscopeModel.getMediumCoeli());
        Planet[] counterPlanets = getPlanetsWithBenchmark(counterHoroscopeModel.getPlanets(), counterHoroscopeModel.getAscendent(), counterHoroscopeModel.getMediumCoeli());
        String userRulingPlanet = mZodiacDBHelper.getZodiac(userPlanets[0].getConstellation()).getPlanetName();
        String counterRulingPlanet = mZodiacDBHelper.getZodiac(counterPlanets[0].getConstellation()).getPlanetName();

        for(Planet userPlanet : userPlanets) {
            for(Planet counterPlanet : counterPlanets) {
                if(Arrays.asList(Planet.BANCH_MARK).contains(userPlanet.getName()) && Arrays.asList(Planet.BANCH_MARK).contains(counterPlanet.getName())
                        && (!userPlanet.getName().equals(Planet.ASC) || !counterPlanet.getName().equals(Planet.ASC)))
                    continue;

                Aspect aspect = getAspect(new Planet[]{userPlanet, counterPlanet}, new String[]{userRulingPlanet, counterRulingPlanet});
                if (aspect != null)
                    aspectList.add(aspect);
            }
        }

        return aspectList;
    }

    private static List<Integer[]> getSynastryNumberList(List<Aspect> aspectList) {
        List<Integer[]> synastryNumberList = new ArrayList<>();

        for(Aspect aspect : aspectList) {
            int userPlanetNumber = Planet.getIndex(aspect.getPlanets()[0].getName()) + 1;
            int counterPlanetNumber = Planet.getIndex(aspect.getPlanets()[1].getName()) + 1;
            int aspectNumber = Aspect.getIndex(aspect.getName()) + 1;

            int synastryNumber = 2 * 100000 + userPlanetNumber * 1000 + aspectNumber * 100 + counterPlanetNumber;
            int synastryReverseNumber = 2 * 100000 + counterPlanetNumber * 1000 + aspectNumber * 100 + userPlanetNumber;

            synastryNumberList.add(new Integer[]{synastryNumber, synastryReverseNumber});
        }

        return synastryNumberList;
    }

    private static Planet[] getPlanetsWithBenchmark(Planet[] planets, Ascendent ascendent, MediumCoeli mediumCoeli) {
        List<Planet> planetList = new ArrayList<>(Arrays.asList(planets));
        planetList.add(getPlanet(ascendent));
        planetList.add(getPlanet(mediumCoeli));

        return planetList.toArray(new Planet[planetList.size()]);
    }
}
