package com.matching.goodgoodman.constellationdating.utils;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Data의 변화가 일어났을 때 listener들에게 변화사실을 알려주는 util 클래스.
 * 상속을 받아 각 데이터에 대한 세부 class를 작성하여 사용한다.
 * @author Hyeong_Wook
 */

public abstract class DataChangeEventHandler<DataType> {
    List<DataChangeListener<DataType>> mDataChangeListeners;

    public DataChangeEventHandler() {
        mDataChangeListeners = new ArrayList<>();
    }

    /**
     * data의 변화를 받는 listener를 추가한다.
     * @param dataChangeListener
     */
    public void addChangeListener(DataChangeListener<DataType> dataChangeListener) {
        mDataChangeListeners.add(dataChangeListener);
    }

    /**
     * data의 변화를 받는 listener를 제거한다.
     * @param dataChangeListener
     */
    public void removeChangeListener(DataChangeListener<DataType> dataChangeListener) {
        mDataChangeListeners.remove(dataChangeListener);
    }

    /**
     * data가 변경되었음을 알린다.
     * @param data
     */
    public void notifyChangeData(DataType data) {
        Handler handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                for(DataChangeListener<DataType> dataChangeListener : mDataChangeListeners)
                    dataChangeListener.onDataChange(data);
            }
        };
        handler.sendEmptyMessage(0);
    }

    public interface DataChangeListener<DataType> {
        void onDataChange(DataType data);
    }
}
