package com.matching.goodgoodman.constellationdating.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Date 객체를 다른 형태로 바꿔주는 유틸 클래스입니다.
 * @author Hyeong_Wook
 */

public abstract class DateConverter {
    private static final String DATE_FORMAT = "yyyy:MM:dd:hh:mm";
    private static final String SPLIT_CHAR = ":";

    public static final String GMT = "GMT";

    /**
     * date를 year, month, day, hour, minute 로 구성된 integer 자료형의 배열로 바꾼다.
     * year부터 minute까지 차례대로 index 0~4에 지정된다.
     * @param date 원하는 date
     * @return 나눠진 시간의 배열
     */
    public static int[] dateToDividedTime(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(GMT));
        String dateStr = simpleDateFormat.format(date);
        String[] dividedTimes = dateStr.split(SPLIT_CHAR);
        int[] result = new int[5];

        for(int i = 0; i < result.length; i++)
            result[i] = Integer.parseInt(dividedTimes[i]);

        return result;
    }

    /**
     * formatString 형식에 맞는 str date를 Date객체로 얻는다.
     * @param formatStr format
     * @param str string date
     * @return Date 객체
     */
    public static Date strToDate(String formatStr, String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatStr);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(GMT));
        Date date = null;
        try {
            date = simpleDateFormat.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    /**
     * date의 내일을 얻는다.
     * @param date date
     * @return 내일
     */
    public static Date getTomorrow(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);

        return calendar.getTime();
    }
}
