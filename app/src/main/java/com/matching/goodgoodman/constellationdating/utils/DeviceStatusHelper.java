package com.matching.goodgoodman.constellationdating.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.provider.Settings;

import java.util.List;

/**
 * Device의 세팅값을 받아오거나, 세팅하는 클래스입니다.
 *
 * @author Hyeong_Wook
 */

public abstract class DeviceStatusHelper {
    /**
     * GPS가 켜져있는지의 여부를 얻는다.
     * @param context applicationContext
     * @return
     */
    public static boolean isEnabledGPS(Context context) {
        int locationMode = 0;
        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        if(locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY || locationMode == Settings.Secure.LOCATION_MODE_SENSORS_ONLY)
            return true;

        return false;
    }

    public static boolean isAppRunningInForeground(Context context) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfos = activityManager.getRunningAppProcesses();
        String packageName = context.getPackageName();

        if (runningAppProcessInfos != null)
        {
            for (final ActivityManager.RunningAppProcessInfo processInfo : runningAppProcessInfos) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && processInfo.processName.equals(packageName))
                    return true;
            }
        }

        return false;
    }
}
