package com.matching.goodgoodman.constellationdating.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.matching.goodgoodman.constellationdating.R;
import com.matching.goodgoodman.constellationdating.model.FacebookModel;
import com.matching.goodgoodman.constellationdating.model.UserModel;
import com.matching.goodgoodman.constellationdating.model.UserPreference;
import com.matching.goodgoodman.constellationdating.server.SuccessCallBack;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * facebook과 연동하는 클래스입니다.
 * 페이스북에 로그인하고, 정보를 가져오는 작업을 담당합니다.
 *
 * @author Hyeong_Wook
 */

public class FacebookModule {
    public static final String PERMISSION_USER_PROFILE = "public_profile";
    public static final String PERMISSION_USER_PHOTOS = "user_photos";
    public static final String PERMISSION_EMAIL = "email";
    public static final String PERMISSION_USER_BIRTHDAY = "user_birthday";

    public static final String URL_PROFILE = "%s/picture";
    public static final String URL_PROFILE_LARGE = "%s/picture?type=large";
    public static final String URL_ALBUMS = "%s/albums?fields=photos{picture}";

    public static final String JSON_PICTURE = "picture";
    public static final String JSON_DATA = "data";
    public static final String JSON_PHOTOS = "photos";
    public static final String JSON_EMAIL = "email";
    public static final String JSON_BIRTHDAY = "birthday";
    public static final String JSON_GENDER = "gender";
    public static final String JSON_NAME = "name";
    public static final String JSON_LOCATION = "location";

    public static final String PARAMETER_FIELDS = "fields";
    public static final String FIELD_INFO = "name,birthday,email,gender,location";

    public static final String GENDER_MALE = "male";
    public static final String GENDER_FEMALE = "female";

    private static final String DATE_STR = "MM/dd/yyyy";

    public static final String[] PERMISSIONS = {PERMISSION_USER_PROFILE, PERMISSION_USER_PHOTOS, PERMISSION_EMAIL, PERMISSION_USER_BIRTHDAY};


    private CallbackManager mCallbackManager;
    private LoginManager mLoginManager;

    private static volatile FacebookModule mInstance;

    public static FacebookModule getInstance() {
        if (mInstance == null) {
            synchronized (FacebookModule.class) {
                if (mInstance == null)
                    mInstance = new FacebookModule();
            }
        }
        return mInstance;
    }

    private FacebookModule() {
        mCallbackManager = CallbackManager.Factory.create();
        mLoginManager = LoginManager.getInstance();
    }

    /**
     * 로그인 결과값을 받을 activity를 설정합니다.
     *
     * @param activity 로그인 결과값을 받을 activity
     */
    public void setLoginArea(Activity activity) {
        mLoginManager.logInWithReadPermissions(activity, Arrays.asList(PERMISSIONS));
    }

    /**
     * 로그인 결과값을 받을 fragment를 설정합니다.
     *
     * @param fragment 로그인 결과값을 받을 fragment
     */
    public void setLoginArea(Fragment fragment) {
        mLoginManager.logInWithReadPermissions(fragment, Arrays.asList(PERMISSIONS));
    }

    /**
     * Facebook에 로그인합니다.
     * 이를 호출하는 Area의 onActivityResult 메서드에 onActivityResult() 메서드를 추가해야 합니다.
     *
     * @param facebookCallback 결과값을 받는 callback
     */
    public void login(FacebookCallback<AccessToken> facebookCallback) {
        if(isLogin()) {
            facebookCallback.onSuccess(AccessToken.getCurrentAccessToken());
            return;
        }

        mLoginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                String token = accessToken.getToken();
                String userId = accessToken.getUserId();

                UserPreference.getInstance().addFacebookModel(new FacebookModel(token, userId));
                facebookCallback.onSuccess(accessToken);
            }

            @Override
            public void onCancel() {
                Log.d("loginFacebook", "취소되었습니다.");
                facebookCallback.onCancel();
            }

            @Override
            public void onError(FacebookException error) {
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null)
                        mLoginManager.logOut();
                }
                error.printStackTrace();
                facebookCallback.onError(error);
            }
        });
    }

    public void logOut() {
        mLoginManager.logOut();
    }

    /**
     * 페이스북 프로필 사진을 가져온다.
     * 실패시 null을 반환한다.
     *
     * @param accessToken     사용자의 accessToken
     * @param successCallBack 이미지를 받는 callBack
     */
    public void getProfilePicture(AccessToken accessToken, SuccessCallBack<Bitmap> successCallBack) {
        getProfilePicture(accessToken, false, successCallBack);
    }

    public void getProfilePicture(AccessToken accessToken, boolean isLarge, SuccessCallBack<Bitmap> successCallBack) {
        String url = isLarge? URL_PROFILE_LARGE : URL_PROFILE;
        new GraphRequest(
                accessToken,
                String.format(url, accessToken.getUserId()),
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        URL profileUrl = response.getConnection().getURL();
                        getProfilePicture(profileUrl, successCallBack);
                    }
                }
        ).executeAsync();
    }

    /**
     * 페이스북 프로필 사진을 가져온다.
     * 실패시 null을 반환한다.
     *
     * @param profileUrl      프로필 url
     * @param successCallBack 이미지를 받는 callback
     */
    public void getProfilePicture(URL profileUrl, SuccessCallBack<Bitmap> successCallBack) {
        new AsyncTask<Object, Object, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Object... objects) {
                try {
                    Bitmap profilePicture = BitmapFactory.decodeStream(profileUrl.openConnection().getInputStream());
                    return profilePicture;

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                successCallBack.onCallBack(bitmap);
            }
        }.execute(null, null);
    }

    /**
     * 페이스북 사진첩의 사진들을 가져온다.
     *
     * @param accessToken     사용자의 accessToken
     * @param successCallBack 이미지 리스트를 받는 callback
     */
    public void getPhotos(AccessToken accessToken, SuccessCallBack<List<Bitmap>> successCallBack) {
        new GraphRequest(
                accessToken,
                String.format(URL_ALBUMS, accessToken.getUserId()),
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        try {
                            JSONArray albumJsonArray = response.getJSONObject().getJSONArray(JSON_DATA);
                            getPhotos(albumJsonArray, successCallBack);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }

    /**
     * 페이스북 사진첩의 사진들을 가져온다.
     *
     * @param albumJsonArray  사진들의 json형태 배열
     * @param successCallBack 이미지리스트를 받을 callback
     */
    private void getPhotos(JSONArray albumJsonArray, SuccessCallBack<List<Bitmap>> successCallBack) {
        new AsyncTask<Object, Object, List<Bitmap>>() {
            @Override
            protected List<Bitmap> doInBackground(Object... objects) {
                try {
                    List<Bitmap> bitmapList = new ArrayList<>();
                    for (int i = 0; i < albumJsonArray.length(); i++) {
                        if (albumJsonArray.getJSONObject(i).isNull(JSON_PHOTOS))
                            continue;

                        JSONArray photoJsonArray = albumJsonArray.getJSONObject(i).getJSONObject(JSON_PHOTOS).getJSONArray(JSON_DATA);
                        for (int j = 0; j < photoJsonArray.length(); j++) {
                            URL photoUrl = new URL(photoJsonArray.getJSONObject(j).getString(JSON_PICTURE));
                            Bitmap photo = BitmapFactory.decodeStream(photoUrl.openConnection().getInputStream());
                            bitmapList.add(photo);
                        }
                    }

                    return bitmapList;

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(List<Bitmap> bitmapList) {
                successCallBack.onCallBack(bitmapList);
            }
        }.execute(null, null);
    }

    /**
     * 유저의 기본정보들을 가져온다.
     *
     * @param accessToken     사용자의 accessToken
     * @param successCallBack 사용자의 정보가 들어있는 userModel 객체를 받는 callback
     */
    public void getUserInfo(AccessToken accessToken, SuccessCallBack<UserModel> successCallBack) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            UserModel userModel = new UserModel();
            try {
                userModel.setName(object.getString(JSON_NAME));
                userModel.setGender(object.getString(JSON_GENDER).equals(GENDER_MALE) ? UserModel.GENDER_MALE : UserModel.GENDER_FEMALE);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                userModel.setEmail(object.getString(JSON_EMAIL));
                userModel.setBirthday(DateConverter.strToDate(DATE_STR, object.getString(JSON_BIRTHDAY)));
                userModel.setLocation(object.getJSONObject(JSON_LOCATION).getString(JSON_NAME));

                successCallBack.onCallBack(userModel);
            } catch (JSONException e) {
                e.printStackTrace();
                userModel.setEmail(null);
                userModel.setBirthday(null);
                userModel.setLocation(null);

                successCallBack.onCallBack(userModel);
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString(PARAMETER_FIELDS, FIELD_INFO);
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    public boolean isLogin() {
        return AccessToken.getCurrentAccessToken() != null;
    }

    /**
     * 로그인 성공 시 결과를 전달한다.
     * 이 메서드는 이 클래스를 사용하는 activity, 혹은 fragment의 onActivityResult에서 호출되어야 합니다.
     *
     * @param requestCode 요청 코드
     * @param resultCode  결과 코드
     * @param data        데이터
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 특정 user의 facebook에 접속한다.
     *
     * @param context 이를 호출하는 context
     * @param userId  원하는 user의 id
     */
    public void connectFacebook(Context context, String userId) {
        String connectionUrl = String.format(context.getResources().getString(R.string.facebook_connect_url), userId);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(connectionUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
