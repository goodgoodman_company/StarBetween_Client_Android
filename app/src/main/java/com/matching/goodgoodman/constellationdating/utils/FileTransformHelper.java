package com.matching.goodgoodman.constellationdating.utils;

import android.content.Context;

import com.matching.goodgoodman.constellationdating.localDB.AscendentDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.LongitudeAdjustmentDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.MediumCoeliDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.PlanetDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.SiderealTimeAdjustmentDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.SiderealTimeDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.SynastryAspectScoreDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.ZodiacDBHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hyeong_Wook on 2017-09-27.
 */

public abstract class FileTransformHelper {
    public static final String PATH_DB_STORAGE = "/data/com.matching.goodgoodman.constellationdating/databases/";
    public static final String PATH_DB = "/databases/";
    public static final String PATH_SD = "mnt/sdcard/";
    public static final String JOURNAL = "-journal";

    public static void copyFromAssetsToStorage(Context context, String fileName) {
        if(isExists(context.getApplicationInfo().dataDir + PATH_DB + fileName))
            return;
        try {
            InputStream input = context.getAssets().open(fileName);
            String outFileName = context.getApplicationInfo().dataDir + PATH_DB + fileName;
            OutputStream output = new FileOutputStream(outFileName);
            byte[] mBuffer = new byte[1024];
            int length;
            while ((length = input.read(mBuffer)) > 0)
                output.write(mBuffer, 0, length);

            output.flush();
            output.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void copyDatabase(Context context, String databaseName) {
        copyFromAssetsToStorage(context, databaseName);
        copyFromAssetsToStorage(context, databaseName + JOURNAL);
    }

    public static void copyDatabases(Context context) {
        File dbDirectory = new File(context.getApplicationInfo().dataDir + PATH_DB);
        if(!dbDirectory.isDirectory())
            dbDirectory.mkdir();

        List<String> databaseList = new ArrayList<>();
        databaseList.add(AscendentDBHelper.DATABASE_NAME);
        databaseList.add(LongitudeAdjustmentDBHelper.DATABASE_NAME);
        databaseList.add(MediumCoeliDBHelper.DATABASE_NAME);
        databaseList.add(PlanetDBHelper.DATABASE_NAME);
        databaseList.add(SiderealTimeAdjustmentDBHelper.DATABASE_NAME);
        databaseList.add(SynastryAspectScoreDBHelper.DATABASE_NAME);
        for(String DBName : DescriptionDBHelper.LANGUAGES)
            databaseList.add(DBName);
        for(String DBName : SiderealTimeDBHelper.TABLE_NAMES)
            databaseList.add(DBName);
        for(String DBName : ZodiacDBHelper.LANGUAGES)
            databaseList.add(DBName);

        for(String DB : databaseList)
            copyDatabase(context, DB);
    }

    public static boolean isExists(String path) {
        return new File(path).exists();
    }
}
