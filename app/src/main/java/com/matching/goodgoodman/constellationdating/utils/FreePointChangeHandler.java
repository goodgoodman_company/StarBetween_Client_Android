package com.matching.goodgoodman.constellationdating.utils;

/**
 * Created by Hyeong_Wook on 2017-11-07.
 */

public class FreePointChangeHandler extends DataChangeEventHandler<Integer> {
    private static volatile FreePointChangeHandler mInstance;
    public static FreePointChangeHandler getInstance() {
        if(mInstance == null) {
            synchronized (FreePointChangeHandler.class) {
                if(mInstance == null)
                    mInstance = new FreePointChangeHandler();
            }
        }
        return mInstance;
    }
}
