package com.matching.goodgoodman.constellationdating.utils;

import android.content.Context;

import com.matching.goodgoodman.constellationdating.localDB.DescriptionDBHelper;
import com.matching.goodgoodman.constellationdating.localDB.ZodiacDBHelper;
import com.matching.goodgoodman.constellationdating.model.House;
import com.matching.goodgoodman.constellationdating.model.SynastryAspect;

import java.util.ArrayList;
import java.util.List;

import dao.Planet;
import dao.Zodiac;

/**
 * Created by Hyeong_Wook on 2017-09-24.
 */

public abstract class HoroscopeDescriptionGetter {
    public static final int INDEX_MONTH = 0;
    public static final int INDEX_CONSTELLATION = 1;
    public static final int INDEX_KEYWORD = 2;
    public static final int INDEX_COMMENT = 3;

    public static final int INDEX_PLANET = 0;
    public static final int INDEX_TITLE = 1;
    public static final int INDEX_SUBTITLE = 2;

    public static final int INDEX_IMAGE = 0;
    public static final int INDEX_COLOR = 4;

    private static final int[] LOVE_CONDITIONS = {80, 60, 40, 20};

    private static final String MATCH_STR = "%d%% match";
    private static final String ASPECT_STR = "%s %s %s\n";
    private static final String HIGHLIGHT_PLANET_STR = "%s:%s";

    private static final int MAX_POSITIVE_DESCRIPTION = 3;
    private static final int MAX_NAGATIVE_DESCRIPTION = 2;

    private static ZodiacDBHelper mZodiacDBHelper;
    private static DescriptionDBHelper mDescriptionDBHelper;

    public static String[] getConstellationDescription(Context context, String language, Planet sun) {
        initialize(context, language);
        String[] result = new String[4];
        String constellation = sun.getConstellation();
        Zodiac zodiac = mZodiacDBHelper.getZodiac(constellation);
        int id = 10000 + 100 + zodiac.getMonth();

        result[INDEX_MONTH] = String.valueOf(zodiac.getMonth());
        result[INDEX_CONSTELLATION] = zodiac.getConstellationName().replace("\\n-", "");
        result[INDEX_KEYWORD] = zodiac.getKeyword();
        result[INDEX_COMMENT] = mDescriptionDBHelper.getDescription(id).getComment();

        return result;
    }

    public static String[] getHouseDescription(Context context, String language, House house, int pageNumber) {
        initialize(context, language);
        String[] result = new String[4];
        int houseNumber = house.getNumber() + 100;
        int titleId = pageNumber + 999;
        int subTitleId = titleId + 100;
        int commentId = 10000 + pageNumber * 100 + house.getNumber();

        String houseStr = mDescriptionDBHelper.getDescription(houseNumber).getComment();
        String title = mDescriptionDBHelper.getDescription(titleId).getComment();
        title = String.format(title, houseStr);
        String subTitle = mDescriptionDBHelper.getDescription(subTitleId).getComment();
        String comment = mDescriptionDBHelper.getDescription(commentId).getComment();

        result[INDEX_PLANET] = house.getPlanet();
        result[INDEX_TITLE] = title;
        result[INDEX_SUBTITLE] = subTitle;
        result[INDEX_COMMENT] = comment;

        return result;
    }

    private static int getDrawableIndex(int score) {
        int index = 0;
        for(int condition : LOVE_CONDITIONS) {
            if(score >= condition)
                return index;
            index++;
        }
        return index;
    }

    public static String[] getMatchingDescription(Context context, String language, List<SynastryAspect> synastryAspects) {
        initialize(context, language);
        String[] result = new String[5];
        int synastryScore = ConstellationCalculator.getSynastryScore(synastryAspects);
        int subTitleId = ConstellationCalculator.getSynastryDescriptionid(synastryScore);
        String subTitle = mDescriptionDBHelper.getDescription(subTitleId).getComment();
        String comment = "";

        for(SynastryAspect synastryAspect : ConstellationCalculator.getMaxScoreSynastryAspects(new ArrayList<>(synastryAspects), MAX_POSITIVE_DESCRIPTION)) {
            if(synastryAspect.getScore() != 0) {
                Planet[] planets = synastryAspect.getAspect().getPlanets();
                comment += String.format(ASPECT_STR, planets[0].getName(), synastryAspect.getAspect().getName(), planets[1].getName());
            }
        }

        for(SynastryAspect synastryAspect : ConstellationCalculator.getMinScoreSynastryAspects(new ArrayList<>(synastryAspects), MAX_NAGATIVE_DESCRIPTION)) {
            if(synastryAspect.getScore() != 0) {
                Planet[] planets = synastryAspect.getAspect().getPlanets();
                comment += String.format(ASPECT_STR, planets[0].getName(), synastryAspect.getAspect().getName(), planets[1].getName());
            }
        }

        result[INDEX_IMAGE] = String.valueOf(getDrawableIndex(synastryScore));
        result[INDEX_TITLE] = String.format(MATCH_STR, synastryScore);
        result[INDEX_SUBTITLE] = subTitle;
        result[INDEX_COMMENT] = comment;
        result[INDEX_COLOR] = String.valueOf(ColorIndexHelper.getLoveIndex(synastryScore));

        return result;
    }

    public static String[] getSynastryAspectDescription(Context context, String language, List<SynastryAspect> synastryAspects, int pageNumber) {
        initialize(context, language);
        String[] result = new String[4];
        List<SynastryAspect> mostMeaningfulSynastryAspects = ConstellationCalculator.getMaxScoreSynastryAspects(new ArrayList<>(synastryAspects), MAX_POSITIVE_DESCRIPTION);
        mostMeaningfulSynastryAspects.addAll(ConstellationCalculator.getMinScoreSynastryAspects(new ArrayList<>(synastryAspects), MAX_NAGATIVE_DESCRIPTION));
        if(pageNumber - 2 >= mostMeaningfulSynastryAspects.size())
            return null;

        SynastryAspect synastryAspect = mostMeaningfulSynastryAspects.get(pageNumber - 2);
        Planet[] planets = synastryAspect.getAspect().getPlanets();

        result[INDEX_PLANET] = String.format(HIGHLIGHT_PLANET_STR, planets[0].getName(), planets[1].getName());
        result[INDEX_TITLE] = String.format(ASPECT_STR, planets[0].getName(), synastryAspect.getAspect().getName(), planets[1].getName());
        result[INDEX_COMMENT] = mDescriptionDBHelper.getDescription(synastryAspect.getAspectNumber()).getComment();

        return result;
    }

    private static void initialize(Context context, String language) {
        if(mZodiacDBHelper == null) {
            mZodiacDBHelper = new ZodiacDBHelper(context, ZodiacDBHelper.LANGUAGE_ENGLISH);
            mDescriptionDBHelper = new DescriptionDBHelper(context, language);
        }
        mDescriptionDBHelper.setLanguage(language);
    }
}
