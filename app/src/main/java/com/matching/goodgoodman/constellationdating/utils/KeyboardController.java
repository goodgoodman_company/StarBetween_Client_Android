package com.matching.goodgoodman.constellationdating.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * 키보드를 조절하는 클래스입니다.
 * context는 getAppicationContext를 쓸 것.
 * @author Hyeong_Wook
 */

public abstract class KeyboardController {
    private static InputMethodManager sInputMethodManager = null;

    /**
     * 키보드를 보여준다.
     * @param context systemService를 가져오기 위한 context.
     * @param focusView 포커스를 줄 뷰
     */
    public static void showKeyboard(Context context, View focusView) {
        if(sInputMethodManager == null)
            sInputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        focusView.requestFocus();
        sInputMethodManager.showSoftInput(focusView, 0);
    }

    /**
     * 키보드를 감춘다.
     * @param context systemService를 가져오기 위한 context.
     * @param focusView 포커스를 없앨 뷰
     */
    public static void hideKeyboard(Context context, View focusView) {
        if(sInputMethodManager == null)
            sInputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        focusView.clearFocus();
        sInputMethodManager.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
    }
}
