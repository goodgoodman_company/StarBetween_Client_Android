package com.matching.goodgoodman.constellationdating.utils;

/**
 * Created by Hyeong_Wook on 2017-10-13.
 */

public class StarPointChangeHandler extends DataChangeEventHandler<Integer> {
    private static volatile StarPointChangeHandler mInstance;
    public static StarPointChangeHandler getInstance() {
        if(mInstance == null) {
            synchronized (StarPointChangeHandler.class) {
                if(mInstance == null)
                    mInstance = new StarPointChangeHandler();
            }
        }
        return mInstance;
    }
}
