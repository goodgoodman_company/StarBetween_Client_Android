package com.matching.goodgoodman.constellationdating.utils;

import android.view.MotionEvent;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

/**
 * view를 이동시키는 클래스입니다.
 * 'MotionEvent'를 받아 작동하므로 'MotionEvent'가 있는 touchEvent 계열의 메서드에서 사용해야 합니다.
 * @author Hyeong_Wook
 */

public abstract class ViewMover {
    private static Map<View, Float> sVerticalPointMap = new HashMap<>();
    private static Map<View, Float> sHorizontalPointMap = new HashMap<>();

    /**
     * 스크롤 모션에 따라 뷰를 올리거나 내리는 모션을 취하게 합니다.
     * 기본적으로 스크롤을 내리는 모션을 취할 때 maxY 방향으로 내려가고, 올리는 모션을 취할 때 minY 방향으로 올라갑니다.
     * @param view 움직일 뷰
     * @param motionEvent 스크롤 모션
     * @param minY 뷰가 올라갈 수 있는 최대치의 값
     * @param maxY 뷰가 내려갈 수 있는 최대치의 값
     * @param isReverse 역모션을 취할지의 여부, 기본값은 false
     */
    public static void moveVerticalWithScrollMotion(View view, MotionEvent motionEvent, float minY, float maxY, boolean isReverse) {
        switch (motionEvent.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                addPoint(view, motionEvent);
                break;

            case MotionEvent.ACTION_MOVE:
                float verticalMoveDistance = sVerticalPointMap.get(view) - motionEvent.getRawY();
                float horizontalMoveDistance = sHorizontalPointMap.get(view) - motionEvent.getRawX();

                if(isReverse)
                    verticalMoveDistance = -verticalMoveDistance;

                if(Math.abs(verticalMoveDistance) > Math.abs(horizontalMoveDistance))
                    view.setY(view.getY() + verticalMoveDistance);

                if(view.getY() < minY)
                    view.setY(minY);
                else if (view.getY() > maxY)
                    view.setY(maxY);

                addPoint(view, motionEvent);
                break;

            case MotionEvent.ACTION_UP:
                float visibleArea = (minY + maxY) / 2.0f;

                if(view.getY() > visibleArea)
                    view.setY(maxY);
                else
                    view.setY(minY);

                removePoint(view);
                break;
        }
    }

    public static void moveVerticalWithScrollMotion(View view, MotionEvent motionEvent, float minY, float maxY) {
        moveVerticalWithScrollMotion(view, motionEvent, minY, maxY, false);
    }

    /**
     * 현재 포함된 view의 포인트를 추가합니다.
     * @param view
     * @param motionEvent
     */
    private static void addPoint(View view, MotionEvent motionEvent) {
        sVerticalPointMap.put(view, motionEvent.getRawY());
        sHorizontalPointMap.put(view, motionEvent.getRawX());
    }

    /**
     * view가 움직임을 멈출 때 객체를 떼어냅니다.
     * @param view
     */
    private static void removePoint(View view) {
        sVerticalPointMap.remove(view);
        sHorizontalPointMap.remove(view);
    }
}
