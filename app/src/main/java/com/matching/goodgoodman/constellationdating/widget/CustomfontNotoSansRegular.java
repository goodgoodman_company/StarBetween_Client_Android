package com.matching.goodgoodman.constellationdating.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by dorol on 2017-10-06.
 */

public class CustomfontNotoSansRegular extends android.support.v7.widget.AppCompatTextView {

    public CustomfontNotoSansRegular(Context context) {
        super(context);
        setType(context);
    }

    public CustomfontNotoSansRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setType(context);
    }

    public CustomfontNotoSansRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setType(context);
    }

    private void setType(Context context) {
        //asset에 폰트 복사
        //NotoSnat 경령화된 폰트 위치: https://github.com/theeluwin/NotoSansKR-Hestia
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "NotoSans-Regular.ttf"));
    }
}
