package dao;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "DESCRIPTION".
 */
@Entity
public class Description {

    @Id
    private Long id;

    @Unique
    private Integer intId;
    private String comment;

    @Generated
    public Description() {
    }

    public Description(Long id) {
        this.id = id;
    }

    @Generated
    public Description(Long id, Integer intId, String comment) {
        this.id = id;
        this.intId = intId;
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIntId() {
        return intId;
    }

    public void setIntId(Integer intId) {
        this.intId = intId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
