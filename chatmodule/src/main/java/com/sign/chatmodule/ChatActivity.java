package com.sign.chatmodule;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.sign.chatmodule.adapter.ChatRecyclerAdapter;
import com.sign.chatmodule.helper.DateHelper;
import com.sign.chatmodule.helper.LanguageHelper;
import com.sign.chatmodule.model.ChatMessage;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public abstract class ChatActivity extends AppCompatActivity {
    public static final int SEND = 1;
    public static final int RECEIVE = 2;

    private SendMessageHandler mSendMessageHandler;

    //recyclerview
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mManager;
    private ChatRecyclerAdapter mRecyclerAdapter;

    //view
    private EditText mEditTextMessage;
    private ImageView mImageViewTitle;
    private ImageView mImageViewSend;
    private ImageView mImageViewBack;
    private ImageView mImageViewSetting;

    //message List
    private ArrayList<ChatMessage> mMessageList;

    private String mMyName;
    private String mLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize(R.layout.activity_chat);
    }
    @Override
    protected void onDestroy() {

        mSendMessageHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    public void initialize(int layoutId) {
        setContentView(layoutId);
        initVariables();
        initView();
        initRecyclerView();
        setViewListener();
    }

    private void initVariables(){
        mSendMessageHandler = new SendMessageHandler(this);
        SharedPreferences pref = this.getSharedPreferences("pref", MODE_PRIVATE);

        //앱에 저장된 language를 불러온다. 없을 경우 기기내 선택된 언어를 불러온다.
        mLanguage=pref.getString("chat_language", Locale.getDefault().getLanguage());

    }


    private void initView(){
        mRecyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        mEditTextMessage=(EditText)findViewById(R.id.et_message);
        mImageViewSend=(ImageView)findViewById(R.id.iv_send);
        mImageViewBack=(ImageView)findViewById(R.id.iv_back);
        mImageViewSetting=(ImageView)findViewById(R.id.iv_setting);
        mImageViewTitle=(ImageView)findViewById(R.id.iv_title);

        setViewAsLanguage();
    }

    private void initRecyclerView(){
        mManager=new LinearLayoutManager(this);
        mManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(mManager);
    }
    private void setViewListener(){
        mImageViewSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
        mEditTextMessage.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override public void afterTextChanged(Editable s) {
                String message=mEditTextMessage.getText().toString();
                if(message.isEmpty())
                    mImageViewSend.setImageResource(R.drawable.ic_send_off);
                else
                    mImageViewSend.setImageResource(R.drawable.ic_send);
            }

        });
        mImageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBacked();
            }
        });

        mImageViewSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TextDialog(ChatActivity.this, "Do you want to get out of this conversation?", "Yes", "No", new TextDialog.OnButtonClickListener() {
                    @Override
                    public void onButton1Click() {
                        onLeaved();
                    }
                    @Override public void onButton2Click() {}
                }).show();
            }
        });
    }

    protected void initRecyclerAdapter(ArrayList<ChatMessage> messageList,String myName, String yourName, Uri myThumbnailUri, Uri yourThumbnailUri){
        Collections.sort(messageList);
        this.mMessageList =messageList;
        mMessageList.add(0,new ChatMessage(2,null,0));//입장메시지

        mRecyclerAdapter=new ChatRecyclerAdapter(this, messageList, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOpponentThumbnailClicked();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSystemTextClicked();
            }
        });

        mRecyclerAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int chatMessageCount=mRecyclerAdapter.getItemCount();
                int lastVisiblePosition=mManager.findLastCompletelyVisibleItemPosition();

                if (lastVisiblePosition == -1 ||
                        (positionStart >= (chatMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mRecyclerView.scrollToPosition(positionStart);
                }
            }
        });

        mMyName=myName;
        setMyName(myName);
        setMyThumbnailUri(myThumbnailUri);

        setYourName(yourName);
        setYourThumbnailUri(yourThumbnailUri);
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }
    protected void initRecyclerAdapter(ArrayList<ChatMessage> messageList,String myName, String yourName, Bitmap myThumbnailBitmap, Bitmap yourThumbnailBitmap){
        Collections.sort(messageList);
        this.mMessageList =messageList;
        mMessageList.add(0,new ChatMessage(2,null,0));//입장메시지

        mRecyclerAdapter=new ChatRecyclerAdapter(this, messageList, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOpponentThumbnailClicked();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSystemTextClicked();
            }
        });

        mRecyclerAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int chatMessageCount=mRecyclerAdapter.getItemCount();
                int lastVisiblePosition=mManager.findLastCompletelyVisibleItemPosition();

                if (lastVisiblePosition == -1 ||
                        (positionStart >= (chatMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mRecyclerView.scrollToPosition(positionStart);
                }
            }
        });

        mMyName=myName;
        setMyName(myName);
        setMyThumbnailBitmap(myThumbnailBitmap);

        setYourName(yourName);
        setYourThumbnailBitmap(yourThumbnailBitmap);
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    public String getEditTextValue() {
        return mEditTextMessage.getText().toString();
    }

    public void setTitleImage(Drawable image) {
        mImageViewTitle.setImageDrawable(image);
    }

    public void setEditTextMessage(CharSequence message) {
        mEditTextMessage.setText(message);
    }

    static class SendMessageHandler extends Handler {
        WeakReference<ChatActivity> mActivity;

        SendMessageHandler(ChatActivity activity) {
            mActivity = new WeakReference<ChatActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {

            ChatActivity theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    case SEND:
                        String body=theActivity.mEditTextMessage.getText().toString();

                        long date=DateHelper.getCurrentTime();
                        System.out.println("myname"+theActivity.mMyName);
                        Log.d("myName:",theActivity.mMyName);
                        String msg_id=theActivity.mMyName.hashCode()+""+date;
                        ChatMessage chatMessage=new ChatMessage(msg_id,ChatMessage.MSG_TYPE_MY_MESSAGE, body, date);
                        theActivity.mEditTextMessage.setText("");
                        theActivity.mMessageList.add(chatMessage);
                        theActivity.mRecyclerAdapter.notifyItemInserted(theActivity.mMessageList.size() - 1);

                        theActivity.onSentMessage(msg_id,body, DateHelper.getCurrentTime());
                        break;

                    case RECEIVE:
                        theActivity.mRecyclerAdapter.notifyItemInserted(theActivity.mMessageList.size() - 1);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    protected void changeMessageId(String original_id, String new_id){
        int index=searchIndexUsingId(original_id);
        mMessageList.get(index).setId(new_id);
    }

    protected void applySentResult(String id, boolean isSentOK){
        if(isSentOK){

        }
        else{
            int index=searchIndexUsingId(id);
            if(index!=-1) {
                mMessageList.remove(index);
                mRecyclerAdapter.notifyItemRemoved(index);
            }
        }
    }

    protected void receiveOpponentMessage(final String body,final long date){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ChatMessage message=new ChatMessage(ChatMessage.MSG_TYPE_YOUR_MESSAGE,body,date);
                mMessageList.add(message);
                mSendMessageHandler.sendEmptyMessage(RECEIVE);
            }
        }).start();
    }

    //don't be used
    protected void receiveSystemMessage(final int systemType,final long date){
        new Thread(new Runnable() {
            @Override
            public void run() {

                //Todo if(systemType == 입장)
                ChatMessage message=new ChatMessage(ChatMessage.MSG_TYPE_SYSTEM_PARTICIPATE,"system body",date);
                mMessageList.add(message);

                //else if(systemTYpe==퇴장)
                //..

                mSendMessageHandler.sendEmptyMessage(RECEIVE);
            }
        }).start();
    }
    protected void sendMessage() {
        String body=mEditTextMessage.getText().toString();
        if(!body.isEmpty()){
            mSendMessageHandler.sendEmptyMessage(SEND);
        }
    }

    abstract protected void onSentMessage(String id,String body,long date);

    abstract protected void onBacked();
    abstract protected void onLeaved();
    abstract protected void onOpponentThumbnailClicked();
    abstract protected void onSystemTextClicked();

    protected int searchIndexUsingId(String id){

        for(int i=0; i<mMessageList.size(); i++){
            String msg_id=mMessageList.get(i).getId();

            System.out.println("id:"+id);
            if(msg_id!=null) {
                System.out.println("index:"+i+" msg_id:" + msg_id);
            }
            if(msg_id!=null && msg_id.equals(id))
                return i;
        }

        return -1;
    }

    private void setViewAsLanguage(){
        if(mLanguage.equals(LanguageHelper.KOREAN)){
            //mImageViewTitle.setImageResource(R.drawable.star_between_kr);
            mEditTextMessage.setHint(R.string.chat_edittext_ko);
        }

        //else if(mLanguage.equals(LanguageHelper.ENGLISH)){}
        //else if(mLanguage.equals(LanguageHelper.France)){}


        //default is English
        else{
            //mImageViewTitle.setImageResource(R.drawable.star_between_en);
            mEditTextMessage.setHint(R.string.chat_edittext_en);
        }
    }

    protected void setLanguage(String lang){
        mLanguage=lang;

        //언어저장
        SharedPreferences pref = this.getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("chat_language", mLanguage);
        editor.commit();

        setViewAsLanguage();
    }

    protected void setMyThumbnailBitmap(Bitmap bitmap){
        mRecyclerAdapter.setMyThumbnailBitmap(bitmap);
    }

    protected void setYourThumbnailBitmap(Bitmap bitmap){
       mRecyclerAdapter.setYourThumbnailBitmap(bitmap);
    }

    protected void setMyThumbnailUri(Uri uri){
        mRecyclerAdapter.setMyThumbnailUri(uri);
    }
    protected void setYourThumbnailUri(Uri uri){
        mRecyclerAdapter.setYourThumbnailUri(uri);
    }

    protected void setMyName(String name){
        mRecyclerAdapter.setMyName(name);
    }
    protected void setYourName(String name){
        mRecyclerAdapter.setYourName(name);
    }
}
