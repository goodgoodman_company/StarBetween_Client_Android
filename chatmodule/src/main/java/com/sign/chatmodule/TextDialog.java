package com.sign.chatmodule;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by donghwan on 2017-05-19.
 */

public class TextDialog extends Dialog {

    private String mTitle=null;
    private String mButtonText1=null;
    private String mButtonText2=null;

    private OnButtonClickListener onButtonClickListener=null;

    //view
    private TextView mTextViewTitle;
    private TextView mTextViewYes;
    private TextView mTextViewNo;


    public TextDialog(Context context, String title, String button1Text, String button2Text, OnButtonClickListener buttonClickListener){
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mTitle=title;
        mButtonText1=button1Text;
        mButtonText2=button2Text;
        onButtonClickListener=buttonClickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_text);

        initView();
        setViewListener();
    }

    private void initView(){
        mTextViewTitle=(TextView)findViewById(R.id.tv_title);
        mTextViewYes=(TextView)findViewById(R.id.tv_yes);
        mTextViewNo=(TextView)findViewById(R.id.tv_no);
        mTextViewTitle.setText(mTitle);
    }
    private void setViewListener(){

        mTextViewYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onButtonClickListener!=null)
                    onButtonClickListener.onButton1Click();
                dismiss();
            }
        });
        mTextViewNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onButtonClickListener!=null)
                    onButtonClickListener.onButton2Click();
                dismiss();
            }
        });
    }

    public interface OnButtonClickListener{
        void onButton1Click();
        void onButton2Click();
    }
}
