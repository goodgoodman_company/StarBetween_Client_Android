package com.sign.chatmodule.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sign.chatmodule.R;
import com.sign.chatmodule.helper.LanguageHelper;
import com.sign.chatmodule.model.ChatMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by donghwan on 2017-05-16.
 */

public class ChatRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context mContext;
    private List<ChatMessage> mChatMessageList=new ArrayList<>();
    private View.OnClickListener mOppoClickListener;
    private View.OnClickListener mSystemClickListener;

    private Bitmap myThumbnailBitmap=null;
    private Bitmap yourThumbnailBitmap=null;

    private Uri myThumbnailUri=null;
    private Uri yourThumbnailUri=null;

    private String myName;
    private String yourName;

    private String language;

    public static final int MSG_TYPE_MY_MESSAGE = 0;
    public static final int MSG_TYPE_YOUR_MESSAGE = 1;
    public static final int MSG_TYPE_SYSTEM_PARTICIPATE =2;

    public ChatRecyclerAdapter(Context context, List<ChatMessage> msgList, View.OnClickListener oppoClickListener, View.OnClickListener systemClickListener){
        mContext=context;
        mChatMessageList=msgList;
        mOppoClickListener=oppoClickListener;
        mSystemClickListener = systemClickListener;

        language=context.getSharedPreferences("pref", MODE_PRIVATE).getString("chat_language", Locale.getDefault().getLanguage());
    }


    public void setMyThumbnailBitmap(Bitmap bitmap){
        myThumbnailBitmap=bitmap;
    }
    public void setYourThumbnailBitmap(Bitmap bitmap){
        yourThumbnailBitmap=bitmap;
    }

    public void setMyThumbnailUri(Uri uri){
        myThumbnailUri=uri;
    }
    public void setYourThumbnailUri(Uri uri){
        yourThumbnailUri=uri;
    }

    public void setMyName(String name){
        myName=name;
    }
    public void setYourName(String name){
        yourName=name;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=null;
        RecyclerView.ViewHolder holder=null;
        switch(viewType){
            case MSG_TYPE_MY_MESSAGE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_my_message, parent, false);
                holder = new MessageViewHolder(view);
                break;
            case MSG_TYPE_YOUR_MESSAGE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_your_message, parent, false);
                holder = new MessageViewHolder(view);
                break;
            case MSG_TYPE_SYSTEM_PARTICIPATE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_system_message, parent, false);
                holder = new SystemMessageViewHolder(view);
                break;

        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatMessage chatMessage= mChatMessageList.get(position);
        int type=getItemViewType(position);

        switch(type){
            case MSG_TYPE_MY_MESSAGE:
                ((MessageViewHolder)holder).bindToMessage(chatMessage);
                break;

            case MSG_TYPE_YOUR_MESSAGE:
                ((MessageViewHolder)holder).bindToMessage(chatMessage);
                break;

            case MSG_TYPE_SYSTEM_PARTICIPATE:
                ((SystemMessageViewHolder)holder).bindToSystemMessage(chatMessage);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mChatMessageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mChatMessageList.get(position).getType();
    }

    class MessageViewHolder extends RecyclerView.ViewHolder{
        private TextView mTextViewName;
        private TextView mTextViewBody;
        private CircleImageView mImageViewThumbnail;

        public MessageViewHolder(View view){
            super(view);
            mTextViewName=(TextView)view.findViewById(R.id.tv_name);
            mTextViewBody=(TextView)view.findViewById(R.id.tv_body);
            mImageViewThumbnail=(CircleImageView)view.findViewById(R.id.iv_thumbnail);
        }

        public void bindToMessage(ChatMessage message){
            mTextViewBody.setText(message.getBody());

            if(message.getType()== MSG_TYPE_MY_MESSAGE)
            {
                mTextViewName.setText(myName);
                if(myThumbnailUri!=null)
                    mImageViewThumbnail.setImageURI(myThumbnailUri);
                else if(myThumbnailBitmap!=null)
                    mImageViewThumbnail.setImageBitmap(myThumbnailBitmap);
            }
            else if(message.getType()== MSG_TYPE_YOUR_MESSAGE){
                mTextViewName.setText(yourName);

                mImageViewThumbnail.setOnClickListener(mOppoClickListener);

                if(yourThumbnailUri!=null)
                    mImageViewThumbnail.setImageURI(yourThumbnailUri);
                else if(yourThumbnailBitmap!=null)
                    mImageViewThumbnail.setImageBitmap(yourThumbnailBitmap);
            }
        }
    }
    class SystemMessageViewHolder extends RecyclerView.ViewHolder{
        private TextView mTextViewBody;

        public SystemMessageViewHolder(View view){
            super(view);
            mTextViewBody=(TextView)view.findViewById(R.id.tv_body);
        }


        public void bindToSystemMessage(ChatMessage message){

            if(message.getType()==MSG_TYPE_SYSTEM_PARTICIPATE) {
                mTextViewBody.setOnClickListener(mSystemClickListener);
                if(language.equals(LanguageHelper.KOREAN)){
                    String systemStr = yourName + mContext.getString(R.string.system_participate_msg_ko);
                    SpannableString content = new SpannableString(systemStr);
                    content.setSpan(new UnderlineSpan(), 0, systemStr.length(), 0);
                    mTextViewBody.setText(content);
                }

                //else if(language.equals(LanguageHelper.FRANCE){}
                //..


                //default is english
                else{
                    String systemStr = yourName + mContext.getString(R.string.system_participate_msg_en);
                    SpannableString content = new SpannableString(systemStr);
                    content.setSpan(new UnderlineSpan(), 0, systemStr.length(), 0);
                    mTextViewBody.setText(content);
                }
            }

            //else if(message.getType()==MSG_TYPE_SYSTEM_LEAVE)
            //..
        }
    }
}
