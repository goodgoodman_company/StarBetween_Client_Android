package com.sign.chatmodule.model;

import android.support.annotation.NonNull;

/**
 * Created by donghwan on 2017-05-16.
 */

public class ChatMessage implements Comparable<ChatMessage>{

    public static final int MSG_TYPE_MY_MESSAGE = 0;
    public static final int MSG_TYPE_YOUR_MESSAGE = 1;
    public static final int MSG_TYPE_SYSTEM_PARTICIPATE =2;

    private long date;
    private String id;
    private int type;
    private String body;
    private String createdAt;
    private String updatedAt;

    //initial message
    public ChatMessage(int type, String body, long date){
        this.type=type;
        this.body=body;
        this.date=date;
    }

    //normal message
    public ChatMessage(String id,int type, String body,long date){
        this.id=id;
        this.type=type;
        this.body=body;
        this.date=date;
    }

    public ChatMessage(String id,int type, String body,long date, String createdAt, String updatedAt) {
        this.id=id;
        this.type=type;
        this.body=body;
        this.date=date;
        this.createdAt=createdAt;
        this.updatedAt=updatedAt;
    }


    public long getDate(){
        return this.date;
    }
    public void setDate(long date){
        this.date=date;
    }
    public String getId(){
        return id;
    }
    public void setId(String id){
        this.id=id;
    }

    public String getBody(){
        return body;
    }
    public void setBody(String body){
        this.body=body;
    }

    public int getType(){
        return type;
    }
    public void setType(int type){
        this.type=type;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int compareTo(@NonNull ChatMessage o) {
        if(date>o.getDate()){
            return 1;
        }
        else if(date<o.getDate()){
            return -1;
        }
        return 0;
    }
}
