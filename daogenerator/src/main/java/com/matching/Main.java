package com.matching;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class Main {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "dao");

        Entity description = schema.addEntity("Description");
        description.addIdProperty();
        description.addIntProperty("intId").unique().getProperty();
        description.addStringProperty("comment");

        Entity siderealTime = schema.addEntity("SiderealTime");
        siderealTime.addIdProperty();
        siderealTime.addIntProperty("month");
        siderealTime.addIntProperty("day");
        siderealTime.addIntProperty("hour");
        siderealTime.addIntProperty("minute");
        siderealTime.addIntProperty("second");

        Entity siderealErrorAdjustment = schema.addEntity("SiderealTimeAdjustment");
        siderealErrorAdjustment.addIdProperty();
        siderealErrorAdjustment.addIntProperty("year").unique().getProperty();
        siderealErrorAdjustment.addIntProperty("hour");
        siderealErrorAdjustment.addIntProperty("minute");
        siderealErrorAdjustment.addIntProperty("second");

        Entity longitudeAdjustment = schema.addEntity("LongitudeAdjustment");
        longitudeAdjustment.addIdProperty();
        longitudeAdjustment.addIntProperty("longitude").unique().getProperty();
        longitudeAdjustment.addIntProperty("hour");
        longitudeAdjustment.addIntProperty("minute");

        Entity mediumCoeli = schema.addEntity("MediumCoeli");
        mediumCoeli.addIdProperty();
        mediumCoeli.addIntProperty("hour");
        mediumCoeli.addIntProperty("minute");
        mediumCoeli.addIntProperty("second");
        mediumCoeli.addStringProperty("constellation");
        mediumCoeli.addIntProperty("location");

        Entity ascendent = schema.addEntity("Ascendent");
        ascendent.addIdProperty();
        ascendent.addIntProperty("hour");
        ascendent.addIntProperty("minute");
        ascendent.addIntProperty("second");
        ascendent.addDoubleProperty("latitude");
        ascendent.addDoubleProperty("location");
        ascendent.addStringProperty("constellation");

        Entity planet = schema.addEntity("Planet");
        planet.addIdProperty();
        planet.addIntProperty("year");
        planet.addIntProperty("month");
        planet.addIntProperty("day");
        planet.addDoubleProperty("location");
        planet.addStringProperty("constellation");
        planet.addStringProperty("name");

        Entity zodiac = schema.addEntity("Zodiac");
        zodiac.addIdProperty();
        zodiac.addStringProperty("constellation");
        zodiac.addStringProperty("constellationName");
        zodiac.addIntProperty("month");
        zodiac.addStringProperty("planetName");
        zodiac.addStringProperty("element");
        zodiac.addStringProperty("color");
        zodiac.addIntProperty("degree");
        zodiac.addStringProperty("keyword");

        Entity synastryAspectScore = schema.addEntity("SynastryAspectScore");
        synastryAspectScore.addIdProperty();
        synastryAspectScore.addIntProperty("intId").unique().getProperty();
        synastryAspectScore.addIntProperty("score");

        Entity matchingScore = schema.addEntity("MatchingScore");
        matchingScore.addIdProperty();
        matchingScore.addIntProperty("userId");
        matchingScore.addIntProperty("counterId");
        matchingScore.addIntProperty("score");

        new DaoGenerator().generateAll(schema, "./app/src/main/java");
    }
}