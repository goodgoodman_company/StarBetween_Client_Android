package com.sign.slidepopuplib;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;

public class PopupData {
    private String mTitle;
    private String mBody;
    private String[] mBodyList;
    private Bitmap mBitmap;
    private Drawable mDrawable;
    private Uri mUri;
    private String mStringImg;
    private int mTabNumber;
    private int mDrawableImg = -1;

    public PopupData(int tabnumber, String title, String body, Bitmap bitmap) {
        this.mTitle = title;
        this.mBody = body;
        this.mBitmap = bitmap;
        this.mTabNumber = tabnumber;
    }

    public PopupData(int tabnumber, String title, String body, Drawable drawable) {
        this.mTitle = title;
        this.mBody = body;
        this.mDrawable = drawable;
        this.mTabNumber = tabnumber;
    }

    public PopupData(int tabnumber, String title, String body, Uri uri) {
        this.mTitle = title;
        this.mBody = body;
        this.mUri = uri;
        this.mTabNumber = tabnumber;
    }

    //사용
    public PopupData(int tabnumber, String title, String body, String stringimg) {
        this.mTitle = title;
        this.mBody = body;
        this.mStringImg = stringimg;
        this.mTabNumber = tabnumber;
    }
    //사용
    public PopupData(int tabnumber, String title, String body, int drawableimg) {
        this.mTitle = title;
        this.mBody = body;
        this.mDrawableImg = drawableimg;
        this.mTabNumber = tabnumber;
    }

    public PopupData(int tabnumber, String title, String[] bodylist, String stringimg) {
        this.mTitle = title;
        this.mBodyList = bodylist;
        this.mStringImg = stringimg;
        this.mTabNumber = tabnumber;
    }

    public PopupData(int tabnumber, String title, String[] bodylist, int drawableimg) {
        this.mTitle = title;
        this.mBodyList = bodylist;
        this.mDrawableImg = drawableimg;
        this.mTabNumber = tabnumber;
    }

    //

    public String getTitle() {
        return mTitle;
    }

    public String getBody() {
        return mBody;
    }

    public String[] getBodyList() {
        return mBodyList;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public Drawable getDrawable() {
        return mDrawable;
    }

    public Uri getUri() {
        return mUri;
    }

    public String getStringImg() {
        return mStringImg;
    }

    public int getTabNumber() {
        return mTabNumber;
    }

    public int getDrawableImg() {
        return mDrawableImg;
    }

}