package com.sign.slidepopuplib;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by donghwan on 2017-05-17.
 */

public class SlideFragment extends Fragment {
    private String mTitle;
    private String mBody;
    private Object mImage;
    private int mImageType = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_slide,container,false);
        initViews(view);
        return view;
    }

    private void initViews(View view){
        TextView tv_title=(TextView)view.findViewById(R.id.popup_title);
        TextView tv_body=(TextView)view.findViewById(R.id.tv_body);
        //ImageView iv=(ImageView)view.findViewById(R.id.popup_imageview);

        if(mTitle !=null) {
            tv_title.setText(mTitle);
        }

        if(mBody !=null) {
            tv_body.setText(mBody);
        }
/*
        if(mImage !=null){
            if(mImageType ==0)
                iv.setImageBitmap((Bitmap) mImage);
            else if(mImageType ==1)
                iv.setImageDrawable((Drawable) mImage);
            else if(mImageType ==2)
                iv.setImageURI((Uri) mImage);
            else if(mImageType ==3)
                Glide.with(getContext()).load((String) mImage).into(iv);
            else if(mImageType ==4)
                Glide.with(getContext()).load((Integer) mImage).into(iv);
        }
        */
    }

    public static SlideFragment newInstance(PopupData popupData){
        SlideFragment f=new SlideFragment();

        if(popupData!=null){
            f.mTitle =popupData.getTitle();
            f.mBody =popupData.getBody();
            if(popupData.getBitmap()!=null){
                f.mImageType =0;
                f.mImage =popupData.getBitmap();
            }
            else if(popupData.getDrawable()!=null){
                f.mImageType =1;
                f.mImage =popupData.getDrawable();
            }
            else if(popupData.getUri()!=null){
                f.mImageType =2;
                f.mImage =popupData.getUri();
            }
            else if(popupData.getStringImg()!=null){
                f.mImageType =3;
                f.mImage =popupData.getStringImg();
            }
            else if(popupData.getDrawableImg()!=-1){
                f.mImageType =4;
                f.mImage = popupData.getDrawableImg();
            }
        }
        return f;
    }

}