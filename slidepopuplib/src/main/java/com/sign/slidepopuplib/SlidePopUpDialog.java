package com.sign.slidepopuplib;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.sign.slidepopuplib.adapter.CustomFragmentPagerAdapter;
import com.sign.slidepopuplib.adapter.ScaleCircleNavigator;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;

import java.util.ArrayList;


/**
 * Created by donghwan on 2017-05-17.
 */

public class SlidePopUpDialog extends DialogFragment  {
    private ViewPager mViewPager;
    private CustomFragmentPagerAdapter mFragmentPagerAdapter;

    Context mContext;

    private ImageView mImageViewXout;
    private View mContainer;
    private View mFloatingLayout;
    View.OnTouchListener mContainerTouchListener;

    private ArrayList<PopupData> mDataList;

    public SlidePopUpDialog(){

    }
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog);

        mContext = getContext();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.dialog_slide, container);
        getDialog().setCanceledOnTouchOutside(true);

        //setmTabwidths(view);
        initView(view);
        initMagicIndicator(view);
        setViewListener(view);


        return view;
    }

    public void setDatas(ArrayList<PopupData> popupDatas){
        mDataList=new ArrayList<>();
        mDataList.addAll(popupDatas);//페이지들 내용 전체 전달
    }

    public View getFloatingLayout() {
        return mFloatingLayout;
    }

    public void setTouchListener(View.OnTouchListener onTouchListener) {
        mContainerTouchListener = onTouchListener;
    }

    //밑의 동그라미
    private void initMagicIndicator(View view) {
        MagicIndicator magicIndicator = (MagicIndicator) view.findViewById(R.id.magic_indicator);

        ScaleCircleNavigator scaleCircleNavigator = new ScaleCircleNavigator(getContext());
        scaleCircleNavigator.setCircleCount(mDataList.size());//탭의 수대로가 아닌, 페이지 수 대로 나오도록

        scaleCircleNavigator.setNormalCircleColor(getContext().getResources().getColor(R.color.deep_gray));
        scaleCircleNavigator.setSelectedCircleColor(getContext().getResources().getColor(R.color.sign_purple));
        scaleCircleNavigator.setCircleClickListener(ccl);

        magicIndicator.setNavigator(scaleCircleNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

    ScaleCircleNavigator.OnCircleClickListener ccl=new ScaleCircleNavigator.OnCircleClickListener() {
        @Override
        public void onClick(int index) {
            mViewPager.setCurrentItem(index);
        }
    };
    //////////////////////////////////

    private void initView(final View view){
        mFragmentPagerAdapter = new CustomFragmentPagerAdapter(getChildFragmentManager(),mDataList.size(),mDataList);
        mViewPager = (ViewPager) view.findViewById(R.id.view_pager);
        mViewPager.setAdapter(mFragmentPagerAdapter);

        mContainer = view.findViewById(R.id.ll_container);//팝업의 내용 창부분
        mFloatingLayout = view.findViewById(R.id.floating_layout);

        mImageViewXout = (ImageView) view.findViewById(R.id.floatingActionButton_xout);//x버튼
        mImageViewXout.setImageResource(R.mipmap.more);
        mImageViewXout.setRotation(45);
    }

    private static final int MIN_VALUE=17;
    private void setViewListener(final View view){

        //summary 등의 바에서 해당 페이지가 됬을때
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                setSelectedTab(position);

            }
            @Override public void onPageScrollStateChanged(int state) {}
        });

        mImageViewXout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RotateAnimation rotateDialogButton = new RotateAnimation(0, -45, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                rotateDialogButton.setDuration(300);
                rotateDialogButton.setRepeatCount(Animation.ABSOLUTE);
                rotateDialogButton.setFillAfter(true);
                mImageViewXout.startAnimation(rotateDialogButton);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismiss();
                    }
                }, 300);
            }
        });

    }

    //탭의 position으로 page가 이동하도록 하는것
    private void setSelectedTab(final int num) {
        mViewPager.setCurrentItem(num);
    }



}
