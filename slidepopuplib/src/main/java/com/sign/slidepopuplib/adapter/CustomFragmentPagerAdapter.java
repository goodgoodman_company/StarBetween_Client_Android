package com.sign.slidepopuplib.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sign.slidepopuplib.PopupData;
import com.sign.slidepopuplib.SlideFragment;

import java.util.ArrayList;

/**
 * Created by donghwan on 2017-05-17.
 */

public class CustomFragmentPagerAdapter extends FragmentPagerAdapter {

    private FragmentManager mfragmentManager;
    int numOfPage;

    private ArrayList<PopupData> dataList;

    public CustomFragmentPagerAdapter(FragmentManager fm, int numOfPage, ArrayList<PopupData> dataList){
        super(fm);
        this.mfragmentManager = fm;
        this.numOfPage = numOfPage;
        this.dataList = dataList;

    }

    @Override
    public Fragment getItem(int pos) {

        //프래그먼트의 인스턴스를 생성
        if(dataList != null && dataList.size() > pos)
            return SlideFragment.newInstance(dataList.get(pos));
        else
            return SlideFragment.newInstance(null);

    }
    @Override
    public int getCount() {
        return numOfPage;
    }

}
